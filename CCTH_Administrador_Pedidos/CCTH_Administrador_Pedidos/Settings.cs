﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos
{
    public static class Settings
    {
        public static int RequestTimeOut { get { return Convert.ToInt32(ConfigurationManager.AppSettings["RequestTimeOut"]); } }

        public static string ApiIntermedia { get { return ConfigurationManager.AppSettings["API_Integraciones"]; } }
    }
}