﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Enums
{
    public enum ProcessStatus
    {
        PROCESO = 1,
        CORRECTO = 2,
        FALLO = 3
    }
}