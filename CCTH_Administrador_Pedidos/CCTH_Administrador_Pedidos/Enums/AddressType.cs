﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Enums
{
    public static class AddressType
    {
        public static string SHIPPING = "shipping";
        public static string BILLING = "billing";
    }
}