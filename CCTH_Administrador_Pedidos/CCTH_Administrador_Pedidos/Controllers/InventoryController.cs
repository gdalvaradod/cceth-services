﻿using CCTH_Administrador_Pedidos.Controllers.Utils;
using CCTH_Administrador_Pedidos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CCTH_Administrador_Pedidos.Controllers
{
    public class InventoryController : Controller
    {
        // GET: Inventory
        public ActionResult Index()
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) {
                var account = Session["UserID"] as LoginResponse;
                int cedisId = Convert.ToInt32(account.Cedis.CedisId);
                List<InventoryEnc> Inventory = new APIIntermediaController().GetInventoryEnc(cedisId);
                ViewBag.ShowView = "Pedidos";
                ViewBag.Option = "Inventory";
                return View(Inventory);
            }
            return RedirectToAction("Index", "Home");
        }
        // GET: Inventory/Details/5
        public ActionResult Details(int id)
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) 
            {
                List<InventoryDet> Inventory = new APIIntermediaController().GetInventoryDet(id);
                ViewBag.ShowView = "Pedidos";
                ViewBag.Option = "Inventory";
                return View(Inventory);
            }
            return RedirectToAction("Index", "Home");
        }

    }
}
