﻿using CCTH_Administrador_Pedidos.Controllers.DataAcces;
using CCTH_Administrador_Pedidos.Controllers.Utils;
using CCTH_Administrador_Pedidos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CCTH_Administrador_Pedidos.Controllers
{
    public class DestinosController : Controller
    {
        // GET: Destinos
        public ActionResult Index()
        {
            ViewBag.Cedis = Session["Cedi"];
            ViewBag.ShowView = "Configuracion";
            ViewBag.Option = "Destinos";
            return View();
        }

        [HttpGet]
        public JsonResult GetDestinations(string name, string cedis, string status, int? draw, int? start = 0, int? length = 200, string opeRoute = null)
        {
            List<Destinos> destinations = new APIIntermediaDestinoController().Get();
            var totalRecords = destinations.Count;
            if (!string.IsNullOrEmpty(name))
                destinations = destinations.Where(destination => destination.Nombre.Contains(name)).ToList();
            if (!string.IsNullOrEmpty(cedis))
                destinations = destinations.Where(destination => destination.Cedis == Convert.ToInt32(cedis)).ToList();
            if (!string.IsNullOrEmpty(status))
                destinations = destinations.Where(destination => destination.Enable == Convert.ToInt32(status)).ToList();
            if (!string.IsNullOrEmpty(opeRoute))
                destinations = destinations.Where(destination => destination.RouteCodeOpecd == Convert.ToInt32(opeRoute)).ToList();

            return Json(new { draw = draw, recordsTotal = totalRecords, recordsFiltered = destinations.Count, data = destinations.Skip((int)start).Take((int)length) }, JsonRequestBehavior.AllowGet);
        }

        // GET: Destinos/Details/5
        public ActionResult Details(int id)
        {
            ViewBag.ShowView = "Configuracion";
            ViewBag.Option = "Destinos";
            return View();
        }
        // GET: Destinos/Create
        public ActionResult Create()
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) 
            {
                ViewBag.ShowView = "Configuracion";
                ViewBag.Option = "Destinos";
                return View();
            }
            return RedirectToAction("Index", "Home");
        }
        // POST: Destinos/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) 
            {
                string json = "{\"Id\":0,\"RouteCodeCCHT\":\""+ collection[2] + "\",\"RouteCodeOpecd\":"+ collection[3] + ",\"Nombre\":\"" + collection[4] + "\",\"Manager\":" + collection[5] + ",\"Cuc\":" + collection[6] + ",\"Enable\":1,\"Cedis\":" + Session["Cedi"] + ",\"CucCCTH\":" + collection[7] + "}";
                string resp = new APIIntermediaDestinoController().Post(json);
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index", "Home");
        }
        // GET: Destinos/Edit/5
        public ActionResult Edit(int id)
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) {
                List<Destinos> listdestino = new APIIntermediaDestinoController().Get();
                Destinos destino = null;
                if (listdestino != null) {
                    foreach (var key in listdestino) {
                        if (key.Id == id) {
                            destino = new Destinos() {
                                Id = key.Id,
                                Cuc = key.Cuc,
                                Manager = key.Manager,
                                RouteCodeCCHT = key.RouteCodeCCHT,
                                Enable = key.Enable,
                                Nombre = key.Nombre,
                                RouteCodeOpecd = key.RouteCodeOpecd,
                                CucCCTH = key.CucCCTH
                            };
                        }
                    }
                }
                ViewBag.ShowView = "Configuracion";
                ViewBag.Option = "Destinos";
                return View(destino);
            }
            return RedirectToAction("Index", "Home");
        }
        // POST: Destinos/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) 
            {
                // TODO: Add update logic here
                string json = "{\"RouteCodeCCHT\":\"" + collection[2] + "\",\"RouteCodeOpecd\":" + collection[3] + ",\"Nombre\":\"" + collection[4] + "\",\"Manager\":" + collection[5] + ",\"Cuc\":" + collection[6] + ",\"Enable\":" + collection[8] + ",\"CucCCTH\":" + collection[7] + "}  ";
                string resp = new APIIntermediaDestinoController().Put(id, json, Session["Cedi"].ToString());
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index", "Home");
        }
    }
}
