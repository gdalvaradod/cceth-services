﻿using CCTH_Administrador_Pedidos.Controllers.Utils;
using CCTH_Administrador_Pedidos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CCTH_Administrador_Pedidos.Controllers
{
    public class ReportsController : Controller
    {
        // GET: Reports
        public ActionResult Index()
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) 
            {
                ViewBag.ShowView = "Reportes";
                ViewBag.Option = "Index";
                return View();
            }
            return RedirectToAction("Index", "Home");
        }
        public JsonResult GetSales() {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) 
            {
                List<EncVentas> sales = new List<EncVentas>();
                return Json(new { data = sales }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Error = "Inicia sesion." }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SalesReportDetails() {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) 
            {
                ViewBag.ShowView = "Reportes";
                ViewBag.Option = "Index";
                return View();
            }
            return RedirectToAction("Index", "Home");
        }
    }
}
