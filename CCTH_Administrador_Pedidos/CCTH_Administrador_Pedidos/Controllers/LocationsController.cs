﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using CCTH_Administrador_Pedidos.Controllers.DataAcces;
using CCTH_Administrador_Pedidos.Models;

namespace CCTH_Administrador_Pedidos.Controllers
{
    public class LocationsController : Controller
    {
        private readonly APIIntermediaConfigBase intermediaConfigBase = new APIIntermediaConfigBase();

        public JsonResult GetCountries()
        {
            var countries = intermediaConfigBase.GetPaises();
            return Json(new { Records = countries }, JsonRequestBehavior.AllowGet);
        }
   
        public JsonResult GetStates(float paisOpeId)
        {
            var states = intermediaConfigBase.GetEstados(paisOpeId);
            return Json(new { Records = states }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMunicipalities(float estadoOpeId)
        {
            var municipalities = intermediaConfigBase.GetMunicipios(estadoOpeId);
            return Json(new { Records = municipalities }, JsonRequestBehavior.AllowGet);
        }
      

        public JsonResult GetNeighborhoods(float estadoOpeId, float municipioOpeId)
        {
            var neighborhoods = intermediaConfigBase.GetColonias(estadoOpeId, municipioOpeId);
            return Json(new { Records = neighborhoods }, JsonRequestBehavior.AllowGet);
        }
    }
}
