﻿using CCTH_Administrador_Pedidos.Enums;
using CCTH_Administrador_Pedidos.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CCTH_Administrador_Pedidos.Models.Dtos;
using RestSharp;

namespace CCTH_Administrador_Pedidos.Controllers.DataAcces
{
    public class APIIntermediaCustomersController : Controller
    {
        public DataTableResult<Customers> Get(int? statusConfirmation, string name, string route, string cedisId, string customerType, int? start = null, int? length = null)
        {
            string cad = "";
            var serviceUrl = string.Format("{0}Clientes", ConfigurationManager.AppSettings[0]);
            var queryString = "?";
            if (statusConfirmation != null)
                queryString += $"&status={statusConfirmation}";
            if (!string.IsNullOrEmpty(name))
                queryString += $"&name={name}";
            if (!string.IsNullOrEmpty(route))
                queryString += $"&route={route}";
            if (start != null)
                queryString += $"&start={start}";
            if (length != null)
                queryString += $"&length={length}";
            if (!string.IsNullOrEmpty(cedisId))
                queryString += $"&cedisId={cedisId}";
            if (!string.IsNullOrEmpty(customerType))
                queryString += $"&customerType={customerType}";
            if (queryString.Length > 1)
            {
                queryString = queryString.Remove(1,1);
                serviceUrl = serviceUrl + queryString;
            }
            try
            {
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(serviceUrl);
                webrequest.Method = "GET";
                webrequest.KeepAlive = false;
                webrequest.Timeout = Settings.RequestTimeOut;
                webrequest.ContentType = "application/json";

                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream()))
                        {
                            cad = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else
                    {
                        cad = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                var ERROR = e.Message;
            }
            var dataTableResult = Map(cad);
            return dataTableResult;
        }
        public Customers Get(int Id)
        {
            string cad = "";
            try
            {
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "Clientes/" + Id);
                webrequest.Method = "GET";
                webrequest.KeepAlive = false;
                webrequest.Timeout = Settings.RequestTimeOut;
                //webrequest.Headers.Add("Authorization", "Bearer poqod6gxqbuxoncfa7q5pl75ui61yf1y");
                webrequest.ContentType = "application/json";

                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream()))
                        {
                            cad = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else
                    {
                        cad = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                var ERROR = e.Message;
            }
            Customers Listcustomers = MapRow(cad);
            return Listcustomers;
        }
        public List<WeekDay> GetVisitDays(int? cedisId, int? routeId, int? zipCode, string neighborhoodId)
        {
            try
            {
                var endpoint = ConfigurationManager.AppSettings[0] + string.Format("Clientes/VisitDays?cedisId={0}&routeId={1}&zipCode={2}&neighborhood={3}", cedisId, routeId, zipCode, neighborhoodId);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(endpoint);
                request.ContentType = "application/json";
                request.Method = "GET";
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream stream = response.GetResponseStream();
                StreamReader reader = new StreamReader(stream);
                var jsonResponse = reader.ReadToEnd();

                var visitDays = JsonConvert.DeserializeObject<List<WeekDay>>(jsonResponse);
                return visitDays;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<CustomerFilter> GetFilter(int cedi, string barCode)
        {
            string cad = "";
            try
            {
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "Customer/GetFilter?cedi=" + cedi + "&BarCode=" + barCode);
                webrequest.Method = "GET";
                webrequest.KeepAlive = false;
                webrequest.Timeout = Settings.RequestTimeOut;
                //webrequest.Headers.Add("Authorization", "Bearer poqod6gxqbuxoncfa7q5pl75ui61yf1y");
                webrequest.ContentType = "application/json";

                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream()))
                        {
                            cad = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else
                    {
                        cad = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                var ERROR = e.Message;
            }
            List<CustomerFilter> Listcustomers = Mapfilter(cad.Replace(@"\r", "").Replace(@"\n", "").Replace(@"    \", "").Replace(@"\", "").Replace("\"[", "[").Replace("]\"", "]").Replace(@"\", ""));
            return Listcustomers;
        }
        public List<CustomerSearchFilter> GetSerachFilter(int cedi, string name,string email,string tel,string barcode,int? cuc = 0,int? ruta = null) {
            string cad = "";
            try {
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "Customer/GetSearchFilter/?cedi=" + cedi + "&Name=" + name + "&Email=" + email + "&Tel=" + tel + "&Barcode=" + barcode + "&Cuc=" + cuc + "&Route=" + ruta);
                webrequest.Method = "GET";
                webrequest.KeepAlive = false;
                webrequest.Timeout = Settings.RequestTimeOut;
                //webrequest.Headers.Add("Authorization", "Bearer poqod6gxqbuxoncfa7q5pl75ui61yf1y");
                webrequest.ContentType = "application/json";

                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200) {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream())) {
                            cad = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream())) {
                            cad = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
            }
            catch (WebException e) {
                var ERROR = e.Message;
            }
            List<CustomerSearchFilter> Listcustomers = MapSearchfilter(cad.Replace(@"\r", "").Replace(@"\n", "").Replace(@"    \", "").Replace(@"\", "").Replace("\"[", "[").Replace("]\"", "]").Replace(@"\", ""));
            return Listcustomers;
        }
        public BaseResponse Put(Customers customer, string customerId)
        {
            var restClient = new RestClient(ConfigurationManager.AppSettings[0]);
            var request = new RestRequest("Clientes/" + customerId, Method.PUT);
            request.Timeout = Settings.RequestTimeOut;
            request.RequestFormat = DataFormat.Json;
            request.AddBody(customer);
            var response = restClient.Execute(request);
            return new BaseResponse {
                Success = response.StatusCode == HttpStatusCode.InternalServerError ? false : true,
                Message = response.StatusCode == HttpStatusCode.InternalServerError ? response.Content : ""
            };
            

        }
        
        public OrdenAddresses Get(int orderGeneralId, string addressType)
        {
            var restClient = new RestClient(ConfigurationManager.AppSettings[0]);
            var request = new RestRequest($"OrderAddress?orderGeneralId={orderGeneralId}&addressType={addressType}", Method.GET);
            request.Timeout = Settings.RequestTimeOut;
            request.RequestFormat = DataFormat.Json;
            var response = restClient.Execute<OrdenAddresses>(request);
            return response.Data;
        }
        public BaseResponse Put(OrdenAddresses orderAddress)
        {
            var restClient = new RestClient(ConfigurationManager.AppSettings[0]);
            var request = new RestRequest($"OrderAddress/{orderAddress.Id}?orderGeneralId={orderAddress.OrderGeneralId}", Method.PUT);
            request.Timeout = Settings.RequestTimeOut;
            request.RequestFormat = DataFormat.Json;
            request.AddBody(orderAddress);
            var response = restClient.Execute(request);
            return new BaseResponse
            {
                Success = response.StatusCode == HttpStatusCode.InternalServerError ? false : true,
                Message = response.StatusCode == HttpStatusCode.InternalServerError ? response.Content : ""
            };
        }
        public string POST(string customer)
        {
            var resp = "";
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "Clientes");
            webrequest.Method = "POST";
            webrequest.KeepAlive = false;
            webrequest.Timeout = Settings.RequestTimeOut;
            webrequest.ContentType = "application/json";

            byte[] byteArray = Encoding.UTF8.GetBytes(customer);
            webrequest.ContentLength = byteArray.Length;

            using (var streamWriter = new StreamWriter(webrequest.GetRequestStream()))
            {
                streamWriter.Write(customer.ToString());
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 500) {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream())) {
                            resp = responsePr.ContentType;
                        }
                    }
                return resp;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        private DataTableResult<Customers> Map(string json)
        {
            var tableResult = JsonConvert.DeserializeObject<DataTableResult<Customers>>(json);
            return tableResult;
        }
        private List<CustomerFilter> Mapfilter(string json)
        {
            List<CustomerFilter> list = JsonConvert.DeserializeObject<List<CustomerFilter>>(json);
            return list;
        }
        private List<CustomerSearchFilter> MapSearchfilter(string json) {
            List<CustomerSearchFilter> list = JsonConvert.DeserializeObject<List<CustomerSearchFilter>>(json);
            return list;
        }
        private Customers MapRow(string json)
        {
            Customers Row = JsonConvert.DeserializeObject<Customers>(json);
            return Row;
        }
    }
}
