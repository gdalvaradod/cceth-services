﻿using CCTH_Administrador_Pedidos.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace CCTH_Administrador_Pedidos.Controllers.DataAcces
{
    public class APIIntermediaCedisController
    {
        public List<Cedis> Get()
        {
            string cad = "";
            try
            {
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "Cedis");
                webrequest.Method = "GET";
                webrequest.KeepAlive = false;
                webrequest.Timeout = Settings.RequestTimeOut;
                //webrequest.Headers.Add("Authorization", "Bearer poqod6gxqbuxoncfa7q5pl75ui61yf1y");
                webrequest.ContentType = "application/json";

                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream()))
                        {
                            cad = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else
                    {
                        cad = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                var ERROR = e.Message;
            }
            List<Cedis> Listcedis = Map(cad.Replace(@"\r", "").Replace(@"\n", "").Replace(@"    \", "").Replace(@"\", "").Replace("\"[", "[").Replace("]\"", "]").Replace(@"\", ""));
            return Listcedis;
        }
        public string Post(string json)
        {
            string resp = "";
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "Cedis");
            webrequest.Method = "POST";
            webrequest.KeepAlive = false;
            webrequest.Timeout = Settings.RequestTimeOut;
            webrequest.ContentType = "application/json";

            byte[] byteArray = Encoding.UTF8.GetBytes(json.ToString());
            webrequest.ContentLength = byteArray.Length;

            using (var streamWriter = new StreamWriter(webrequest.GetRequestStream()))
            {
                streamWriter.Write(json.ToString());
                streamWriter.Flush();
                streamWriter.Close();
            }

            try
            {
                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        resp = responsePr.ContentType;
                    }
                    else
                    {
                        resp = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                resp = e.Message;
            }
            return resp;
        }
        public string Put(int CedisId, int brachCode, string cedis)
        {
            string resp = "";
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "Cedis/" + CedisId + "?BranchCode=" + brachCode);
            webrequest.Method = "PUT";
            webrequest.KeepAlive = false;
            webrequest.Timeout = Settings.RequestTimeOut;
            webrequest.ContentType = "application/json";

            byte[] byteArray = Encoding.UTF8.GetBytes(cedis.ToString());
            webrequest.ContentLength = byteArray.Length;

            using (var streamWriter = new StreamWriter(webrequest.GetRequestStream()))
            {
                streamWriter.Write(cedis.ToString());
                streamWriter.Flush();
                streamWriter.Close();
            }

            try
            {
                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        resp = responsePr.ContentType;
                    }
                    else
                    {
                        resp = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                resp = e.Message;
            }
            return resp;
        }
        private List<Cedis> Map(string json)
        {
            List<Cedis> list = JsonConvert.DeserializeObject<List<Cedis>>(json);
            return list;
        }
    }
}