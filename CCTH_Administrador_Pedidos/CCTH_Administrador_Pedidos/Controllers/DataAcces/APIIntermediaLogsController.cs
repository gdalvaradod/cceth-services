﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CCTH_Administrador_Pedidos.Controllers.Utils;
using CCTH_Administrador_Pedidos.Models;
using CCTH_Administrador_Pedidos.Models.Dtos;
using CCTH_Administrador_Pedidos.Models.ViewModels;

namespace CCTH_Administrador_Pedidos.Controllers.DataAcces
{
    public class APIIntermediaLogsController
    {
        public DataTableResult<Log> FindLogsForDataTable(DateTime? deliveryDate, int? cedisId, int? routeId, int? trip, int? processStatus, int? start = 0, int? length = 100)
        {
            var restClient = new RestClientUtil<DataTableResult<Log>>(Settings.ApiIntermedia);

            var uri = $"log/dataTable?start={start}&length={length}";
            if (deliveryDate != null)
                uri += $"&deliveryDate={deliveryDate.Value.ToString("yyyy/MM/dd")}";
            if (cedisId != null)
                uri += $"&cedisId={cedisId}";
            if (routeId != null)
                uri += $"&routeId={routeId}";
            if (trip != null)
                uri += $"&trip={trip}";
            if (processStatus != null)
                uri += $"&processStatus={processStatus}";

            var logs = restClient.GetSingle(uri);
            return logs;
            
        }

        public Log Find(int id)
        {
            var restClient = new RestClientUtil<Log>(Settings.ApiIntermedia);
            return restClient.GetSingle($"log/{id}");
        }
    }
}