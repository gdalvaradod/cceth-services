﻿using CCTH_Administrador_Pedidos.Controllers.Utils;
using CCTH_Administrador_Pedidos.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace CCTH_Administrador_Pedidos.Controllers.DataAcces
{
    public class APIIntermediaAccountController : Controller
    {
        public List<UserCedis> Get(string user,out string msj)
        {
            string cad = "";
            var ERROR = "";
            try
            {
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "AdminUsers/GetUserCedis?username=" + user);
                webrequest.Method = "GET";
                webrequest.KeepAlive = false;
                webrequest.Timeout = Settings.RequestTimeOut;
                webrequest.ContentType = "application/json";

                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream()))
                        {
                            cad = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else
                    {
                        cad = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                 ERROR = e.Message;
                
            }
            msj = ERROR;
            List<UserCedis> ListCedis = Map(cad.Replace(@"\r", "").Replace(@"\n", "").Replace(@"    \", "").Replace(@"\", "").Replace("\"[", "[").Replace("]\"", "]").Replace(@"\", ""));
            return ListCedis;
        }
        public LoginResponse Post(string json)
        {
            string resp = "";
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "AdminUsers/Login");
            webrequest.Method = "POST";
            webrequest.KeepAlive = false;
            webrequest.Timeout = Settings.RequestTimeOut;
            webrequest.ContentType = "application/json";

            byte[] byteArray = Encoding.UTF8.GetBytes(json.ToString());
            webrequest.ContentLength = byteArray.Length;

            using (var streamWriter = new StreamWriter(webrequest.GetRequestStream()))
            {
                streamWriter.Write(json.ToString());
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream()))
                        {
                            resp = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else
                    {
                        resp = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                resp = e.Message;
            }
            LoginResponse ListCedis = Mapsession(resp.Replace(@"\r", "").Replace(@"\n", "").Replace(@"    \", "").Replace(@"\", "").Replace("\"[", "[").Replace("]\"", "]").Replace(@"\", ""));
            return ListCedis;
        }
        private List<UserCedis> Map(string json)
        {
            List<UserCedis> list = JsonConvert.DeserializeObject<List<UserCedis>>(json);
            return list;
        }
        private LoginResponse Mapsession(string json)
        {
            LoginResponse login = JsonConvert.DeserializeObject<LoginResponse>(json);
            return login;
        }
    }
}
