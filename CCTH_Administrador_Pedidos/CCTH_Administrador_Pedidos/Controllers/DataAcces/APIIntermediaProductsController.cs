﻿using CCTH_Administrador_Pedidos.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace CCTH_Administrador_Pedidos.Controllers.DataAcces
{
    public class APIIntermediaProductsController : Controller
    {
        public List<Productos> Get(int cedisId)
        {
            string cad = "";
            try
            {
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "/AdminProduct/GetProducts?cedisId=" + cedisId);
                webrequest.Method = "GET";
                webrequest.KeepAlive = false;
                webrequest.Timeout = Settings.RequestTimeOut;
                //webrequest.Headers.Add("Authorization", "Bearer poqod6gxqbuxoncfa7q5pl75ui61yf1y");
                webrequest.ContentType = "application/json";

                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream()))
                        {
                            cad = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else
                    {
                        cad = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                var ERROR = e.Message;
            }
            List<Productos> Productos = Map(cad.Replace(@"\r", "").Replace(@"\n", "").Replace(@"    \", "").Replace(@"\", "").Replace("\"[", "[").Replace("]\"", "]").Replace(@"\", ""));
            return Productos;
        }
        public List<Productos> Get(string ruta,int cedisId)
        {
            string cad = "";
            try
            {              
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "AdminProduct/"+ cedisId);
                webrequest.Method = "GET";
                webrequest.KeepAlive = false;
                webrequest.Timeout = Settings.RequestTimeOut;
                //webrequest.Headers.Add("Authorization", "Bearer poqod6gxqbuxoncfa7q5pl75ui61yf1y");
                webrequest.ContentType = "application/json";

                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream()))
                        {
                            cad = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else
                    {
                        cad = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                var ERROR = e.Message;
            }
            List<Productos> Productos = Map(cad.Replace(@"\r", "").Replace(@"\n", "").Replace(@"    \", "").Replace(@"\", "").Replace("\"[", "[").Replace("]\"", "]").Replace(@"\", ""));
            return Productos;
        }
        public List<ProductSearchFilter> GetSerachFilter(int cedi,int route, string name, string code) {
            string cad = "";
            try {
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "Product/GetSearchFilter/?cedi=" + cedi + "&route=" + route + "&Name=" + name + "&code=" + code);
                webrequest.Method = "GET";
                webrequest.KeepAlive = false;
                webrequest.Timeout = Settings.RequestTimeOut;
                //webrequest.Headers.Add("Authorization", "Bearer poqod6gxqbuxoncfa7q5pl75ui61yf1y");
                webrequest.ContentType = "application/json";

                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200) {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream())) {
                            cad = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream())) {
                            cad = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
            }
            catch (WebException e) {
                var ERROR = e.Message;
            }
            List<ProductSearchFilter> ListProducts = MapSearchfilter(cad.Replace(@"\r", "").Replace(@"\n", "").Replace(@"    \", "").Replace(@"\", "").Replace("\"[", "[").Replace("]\"", "]").Replace(@"\", ""));
            return ListProducts;
        }
        public string Post(string json,int cediId)
        {
            string resp = "";
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "AdminProduct/Productos?cedi=" + cediId);
            webrequest.Method = "POST";
            webrequest.KeepAlive = false;
            webrequest.Timeout = Settings.RequestTimeOut;
            webrequest.ContentType = "application/json";

            byte[] byteArray = Encoding.UTF8.GetBytes(json.ToString());
            webrequest.ContentLength = byteArray.Length;

            using (var streamWriter = new StreamWriter(webrequest.GetRequestStream()))
            {
                streamWriter.Write(json.ToString());
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 201)
                    {
                        using (var readerOrder = new StreamReader(responsePr.GetResponseStream())) {
                            resp = readerOrder.ReadToEnd().ToString();
                        }
                    }
                    else
                    {
                        resp = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                resp = e.Message;
            }
            return resp;
        }
        private List<Productos> Map(string json)
        {
            List<Productos> list = JsonConvert.DeserializeObject<List<Productos>>(json);
            return list;
        }
        private List<ProductSearchFilter> MapSearchfilter(string json) {
            List<ProductSearchFilter> list = JsonConvert.DeserializeObject<List<ProductSearchFilter>>(json);
            return list;
        }
    }
}