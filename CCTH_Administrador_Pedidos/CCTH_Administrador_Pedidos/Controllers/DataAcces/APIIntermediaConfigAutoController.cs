﻿using CCTH_Administrador_Pedidos.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CCTH_Administrador_Pedidos.Controllers.DataAcces
{
    public class APIIntermediaConfigAutoController : Controller
    {
        public List<ConfigAuto> Get()
        {
            string cad = "";
            try
            {
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "AdminConfigAuto/GetAll");
                webrequest.Method = "GET";
                webrequest.KeepAlive = false;
                webrequest.Timeout = Settings.RequestTimeOut;
                //webrequest.Headers.Add("Authorization", "Bearer poqod6gxqbuxoncfa7q5pl75ui61yf1y");
                webrequest.ContentType = "application/json";

                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream()))
                        {
                            cad = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else
                    {
                        cad = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                var ERROR = e.Message;
            }
            List<ConfigAuto> ListAuto = Map(cad.Replace(@"\r", "").Replace(@"\n", "").Replace(@"    \", "").Replace(@"\", "").Replace("\"[", "[").Replace("]\"", "]").Replace(@"\", ""));
            return ListAuto;
        }
        public ConfigAuto Get(int Id)
        {
            string cad = "";
            try
            {
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "AdminConfigAuto/Get/" + Id);
                webrequest.Method = "GET";
                webrequest.KeepAlive = false;
                webrequest.Timeout = Settings.RequestTimeOut;
                //webrequest.Headers.Add("Authorization", "Bearer poqod6gxqbuxoncfa7q5pl75ui61yf1y");
                webrequest.ContentType = "application/json";

                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream()))
                        {
                            cad = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else
                    {
                        cad = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                var ERROR = e.Message;
            }
            ConfigAuto Auto = MapObject(cad.Replace(@"\r", "").Replace(@"\n", "").Replace(@"    \", "").Replace(@"\", "").Replace("\"[", "[").Replace("]\"", "]").Replace(@"\", ""));
            return Auto;
        }
        private List<ConfigAuto> Map(string json)
        {
            List<ConfigAuto> list = JsonConvert.DeserializeObject<List<ConfigAuto>>(json);
            return list;
        }
        private ConfigAuto MapObject(string json)
        {
            ConfigAuto Auto = JsonConvert.DeserializeObject<ConfigAuto>(json);
            return Auto;
        }
    }
}
