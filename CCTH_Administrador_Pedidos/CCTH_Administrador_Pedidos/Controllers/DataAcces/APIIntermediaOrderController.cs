﻿using CCTH_Administrador_Pedidos.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CCTH_Administrador_Pedidos.Models.Dtos;

namespace CCTH_Administrador_Pedidos.Controllers
{
    public class APIIntermediaController : Controller
    {
        // GET: APIIntermedia
        public DataTableResult<PedidosEnc> Get(int cedisId, DateTime? deliveryDate, string status, string route, int? start = 0, int? length = int.MaxValue)
        {
            string cad = "";
            var serviceUrl = string.Format("{0}Pedidos?cedisId={1}", ConfigurationManager.AppSettings[0], cedisId);
            if (deliveryDate != null)
                serviceUrl += $"&deliveryDate={deliveryDate.Value.ToString("yyyy/MM/dd")}";
            if (!string.IsNullOrEmpty(status))
                serviceUrl += $"&status={status}";
            if (!string.IsNullOrEmpty(route))
                serviceUrl += $"&route={route}";
            if (start != null)
                serviceUrl += $"&start={start}";
            if (length != null)
                serviceUrl += $"&length={length}";
            try {
                //var connectionString = ConfigurationManager.AppSettings[0];
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(serviceUrl);
                webrequest.Method = "GET";
                webrequest.KeepAlive = false;
                webrequest.Timeout = Settings.RequestTimeOut;
                //webrequest.Headers.Add("Authorization", "Bearer poqod6gxqbuxoncfa7q5pl75ui61yf1y");
                webrequest.ContentType = "application/json";

                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream()))
                        {
                            cad = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else
                    {
                        cad = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                var ERROR = e.Message;
            }
            var pedidos = JsonConvert.DeserializeObject<DataTableResult<PedidosEnc>>(cad);
            return pedidos;
        }
        public List<PedidosDet> Get(string id)
        {
            string cad = "";
            try
            {
                string[]  values = id.Split(',');
                DateTime date = Convert.ToDateTime(values[1]);
                var temp = date.ToLongTimeString();
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "AdminPedidos/" + values[0] + "?date=" + values[1] + "&status=" + values[2] + "&route=" + values[3]);
                webrequest.Method = "GET";
                webrequest.KeepAlive = false;
                webrequest.Timeout = Settings.RequestTimeOut;
                //webrequest.Headers.Add("Authorization", "Bearer poqod6gxqbuxoncfa7q5pl75ui61yf1y");
                webrequest.ContentType = "application/json";

                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream()))
                        {
                            cad = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else
                    {
                        cad = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                var ERROR = e.Message;
            }
            List<PedidosDet> pedido = MapDet(cad);
            return pedido;
        }
        public PedidoEdit GetOrder(int id)
        {
            string cad = "";
            try
            {
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "AdminPedidos/GetById/" + id);
                webrequest.Method = "GET";
                webrequest.KeepAlive = false;
                webrequest.Timeout = Settings.RequestTimeOut;
                //webrequest.Headers.Add("Authorization", "Bearer poqod6gxqbuxoncfa7q5pl75ui61yf1y");
                webrequest.ContentType = "application/json";

                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream()))
                        {
                            cad = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else
                    {
                        cad = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                var ERROR = e.Message;
            }
            PedidoEdit pedido = MapDetEdit(cad);
            return pedido;
        }
        public PayDesk GetPayDesk(int cediId) {
            string cad = "";
            try {
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["API_OpeCd"] + "/PayDesk?cedisId=" + cediId);
                webrequest.Method = "GET";
                webrequest.KeepAlive = false;
                webrequest.Timeout = Settings.RequestTimeOut;
                //webrequest.Headers.Add("Authorization", "Bearer poqod6gxqbuxoncfa7q5pl75ui61yf1y");
                webrequest.ContentType = "application/json";

                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200) {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream())) {
                            cad = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else {
                        cad = responsePr.ContentType;
                    }
            }
            catch (WebException e) {
                var ERROR = e.Message;
            }
            return new PayDesk { payDeskDate = cad.Trim('"')};
        }
        public string Post(string json)
        {
            string resp = "";
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "Pedidos");
            webrequest.Method = "POST";
            webrequest.KeepAlive = false;
            webrequest.Timeout = Settings.RequestTimeOut;
            webrequest.ContentType = "application/json";

            byte[] byteArray = Encoding.UTF8.GetBytes(json.ToString());
            webrequest.ContentLength = byteArray.Length;

            using (var streamWriter = new StreamWriter(webrequest.GetRequestStream()))
            {
                streamWriter.Write(json.ToString());
                streamWriter.Flush();
                streamWriter.Close();
            }

            try
            {
                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        using (var readerOrder = new StreamReader(responsePr.GetResponseStream()))
                        {
                            resp = readerOrder.ReadToEnd().ToString();
                        }
                    }
                    else
                    {
                        resp = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                resp = e.Message;
            }
            return resp;
        }
        public List<int> Put(string ids)
        {
            string resp = "";
            String[] listorden = ids.Trim().Split(',');
            string ListIds = "[";
            for (var key = 0; key < listorden.Length; key++) {
                ListIds += listorden[key];
                if (key != listorden.Length - 1) {
                    ListIds += ",";
                }
            }        
            ListIds += "]";
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "AdminPedidos/" + 0);
            webrequest.Method = "PUT";
            webrequest.KeepAlive = false;
            webrequest.Timeout = Settings.RequestTimeOut;
            webrequest.ContentType = "application/json";

            byte[] byteArray = Encoding.UTF8.GetBytes(ListIds.ToString());
            webrequest.ContentLength = byteArray.Length;

            using (var streamWriter = new StreamWriter(webrequest.GetRequestStream()))
            {
                streamWriter.Write(ListIds.ToString());
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        using (var readerOrder = new StreamReader(responsePr.GetResponseStream()))
                        {
                            resp = readerOrder.ReadToEnd().ToString();
                        }
                    }
            }
            catch (WebException e)
            {
                throw e;
            }
            return JsonConvert.DeserializeObject<List<int>>(resp);
        }
        public string PutOrder(string json,int InterId)
        {
            string resp = "";
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "AdminPedidos/PutOrder/" + InterId);
            webrequest.Method = "PUT";
            webrequest.KeepAlive = false;
            webrequest.Timeout = Settings.RequestTimeOut;
            webrequest.ContentType = "application/json";

            byte[] byteArray = Encoding.UTF8.GetBytes(json.ToString());
            webrequest.ContentLength = byteArray.Length;

            using (var streamWriter = new StreamWriter(webrequest.GetRequestStream()))
            {
                streamWriter.Write(json.ToString());
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        using (var readerOrder =
                           new StreamReader(responsePr.GetResponseStream()))
                        {
                            resp = readerOrder.ReadToEnd().ToString();
                        }
                        // resp = responsePr.ContentEncoding;
                    }
                    else
                    {
                        resp = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                resp = e.Message;
            }
            return resp;
        }
        public List<InventoryEnc> GetInventoryEnc(int cedisId)
        {
            string cad = "";
            try
            {
                //var connectionString = ConfigurationManager.AppSettings[0];
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "Inventory/GetInventoryEnc/" + cedisId);
                webrequest.Method = "GET";
                webrequest.KeepAlive = false;
                webrequest.Timeout = Settings.RequestTimeOut;
                //webrequest.Headers.Add("Authorization", "Bearer poqod6gxqbuxoncfa7q5pl75ui61yf1y");
                webrequest.ContentType = "application/json";

                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream()))
                        {
                            cad = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else
                    {
                        cad = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                var ERROR = e.Message;
            }
            List<InventoryEnc> inventory = MapInventoryEnc(cad.Replace(@"\r", "").Replace(@"\n", "").Replace(@"    \", "").Replace(@"\", "").Replace("\"[", "[").Replace("]\"", "]").Replace(@"\", ""));
            return inventory;
        }
        public List<InventoryDet> GetInventoryDet(int InventoryId)
        {
            string cad = "";
            try
            {
                //var connectionString = ConfigurationManager.AppSettings[0];
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "Inventory/GetInventoryDet/" + InventoryId);
                webrequest.Method = "GET";
                webrequest.KeepAlive = false;
                webrequest.Timeout = Settings.RequestTimeOut;
                //webrequest.Headers.Add("Authorization", "Bearer poqod6gxqbuxoncfa7q5pl75ui61yf1y");
                webrequest.ContentType = "application/json";

                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream()))
                        {
                            cad = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else
                    {
                        cad = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                var ERROR = e.Message;
            }
            List<InventoryDet> inventory = MapInventoryDet(cad.Replace(@"\r", "").Replace(@"\n", "").Replace(@"    \", "").Replace(@"\", "").Replace("\"[", "[").Replace("]\"", "]").Replace(@"\", ""));
            return inventory;
        }
        private List<PedidosEnc> MapEnc(string json)
        {
            List<PedidosEnc> order = JsonConvert.DeserializeObject<List<PedidosEnc>>(json);
            return order;
        }
        private List<PedidosDet> MapDet(string json)
        {
            List<PedidosDet> order = JsonConvert.DeserializeObject<List<PedidosDet>>(json);
            return order;
        }
        private PedidoEdit MapDetEdit(string json)
        {
            PedidoEdit order = JsonConvert.DeserializeObject<PedidoEdit>(json);
            return order;
        }
        private List<InventoryEnc> MapInventoryEnc(string json)
        {
            List<InventoryEnc> inventory = JsonConvert.DeserializeObject<List<InventoryEnc>>(json);
            return inventory;
        }
        private List<InventoryDet> MapInventoryDet(string json)
        {
            List<InventoryDet> inventory = JsonConvert.DeserializeObject<List<InventoryDet>>(json);
            return inventory;
        }
      
    }
}
