﻿using CCTH_Administrador_Pedidos.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using CCTH_Administrador_Pedidos.Controllers.Utils;

namespace CCTH_Administrador_Pedidos.Controllers.DataAcces
{
    public class APIIntermediaConfigBase
    {
        public List<Pais> GetPaises()
        {
            var restClient = new RestClientUtil<Pais>(ConfigurationManager.AppSettings["API_Integraciones"]);
            var paises = restClient.Get("adminConfig/paises");
            return paises;
        }
        public List<Estado> GetEstados(float paisOpeId)
        {
            var restClient = new RestClientUtil<Estado>(ConfigurationManager.AppSettings["API_Integraciones"]);
            var estados = restClient.Get($"adminConfig/estados?paisOpeId={paisOpeId}");
            return estados;
        }
        public List<Municipio> GetMunicipios(float estadoOpeId)
        {
            var restClient = new RestClientUtil<Municipio>(ConfigurationManager.AppSettings["API_Integraciones"]);
            var municipios = restClient.Get($"adminConfig/municipios?estadoOpeId={estadoOpeId}");
            return municipios;
        }

        public List<Colonia> GetColonias(float estadoOpeId, float municipioOpeId)
        {
            var restClient = new RestClientUtil<Colonia>(ConfigurationManager.AppSettings["API_Integraciones"]);
            var municipios = restClient.Get($"adminConfig/colonias?estadoOpeId={estadoOpeId}&municipioOpeId={municipioOpeId}");
            return municipios;
        }
    }
}