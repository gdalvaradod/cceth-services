﻿using CCTH_Administrador_Pedidos.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace CCTH_Administrador_Pedidos.Controllers.DataAcces
{
    public class APIIntermediaSites
    {

        public List<Site> GetSites()
        {
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[0] + "Sites");
                webRequest.Method = "GET";
                webRequest.ContentType = "application/json";

                using (HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse())
                    if ((int)response.StatusCode == 200)
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            var jsonResponse = reader.ReadToEnd();
                            return JsonConvert.DeserializeObject<List<Site>>(jsonResponse);
                        }
                    }
                    else
                    {
                        throw new Exception(response.ContentType);
                    }
            }
            catch (WebException e)
            {
                throw new Exception(e.Message);
            }
        }
        
    }
}