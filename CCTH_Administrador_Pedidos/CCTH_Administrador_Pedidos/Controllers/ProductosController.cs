﻿using CCTH_Administrador_Pedidos.Controllers.DataAcces;
using CCTH_Administrador_Pedidos.Controllers.Utils;
using CCTH_Administrador_Pedidos.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CCTH_Administrador_Pedidos.Controllers
{
    public class ProductosController : Controller
    {
        // GET: Productos
        public ActionResult Index(string text = null, bool complete = false)
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"]))
            {
                ResponseMessagesAlerts alertsx = null;
                if (text != null) {
                    ResponseMessagesAlerts alert = new ResponseMessagesAlerts() {
                        Success = complete,
                        Content = complete ? text : "",
                        Error = !complete ? text : ""
                    };
                    alertsx = alert;
                }
                ViewBag.alerts = alertsx;
                ViewBag.ShowView = "Productos";
                ViewBag.Option = "Index";
                return View();
            }
            return RedirectToAction("Index", "Home");      
        }
        public JsonResult GetProducts(int? draw, int? start = 0, int? length = 200) {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) {
                var account = Session["UserID"] as LoginResponse;
                int cedisId = Convert.ToInt32(account.Cedis.CedisId);
                List<Productos> productos = new APIIntermediaProductsController().Get(cedisId);
                var totalRecords = productos.Count;
                return Json(new { draw = draw, recordsTotal = totalRecords, recordsFiltered = productos.Count, data = productos.Skip((int)start).Take((int)length) }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Error = "Inicia sesion." }, JsonRequestBehavior.AllowGet);
        }
        // GET: Productos/Create
        public ActionResult Create()
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"]))
            {
                var account = Session["UserID"] as LoginResponse;
                int cedisId = Convert.ToInt32(account.Cedis.CedisId);
                List<Destinos> rutas = listDestinos();           
                List<Productos> Productos = new APIIntermediaProductsController().Get("450", cedisId);
                if (Productos != null)
                    ViewBag.count = Productos.Count;
                else
                    ViewBag.count = 0;
                ViewBag.Rutas = rutas;
                ViewBag.ShowView = "Productos";
                ViewBag.Option = "Create";
                return View(Productos);
            }
            return RedirectToAction("Index", "Home");

        }
        public ActionResult CreateInter(string id, string Nombre, string Ruta, string Cedis, string Tipo)
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) 
            {
                //string[] values = id.Split(',');
                string producto = "{\"Code\": \"" + id + "\",\"Name\": \"" + Nombre + "\",\"Ruta\":" + Ruta + ",\"Cedis\": " + Cedis + ",\"Tipo\": " + Tipo + "}";
                //var resul = new APIIntermediaProductsController().Post(producto);
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index", "Home");
        }
        public ActionResult CreateProduct(string Producto, string Ruta) {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) {
                var account = Session["UserID"] as LoginResponse;
                int cedisId = Convert.ToInt32(account.Cedis.CedisId);
                string[] listProduct = Producto.Split(',');
                string[] listRoute = Ruta.Split(',');
                string producto = "{\"Cedi\":" + cedisId + ",\"Codigo\":[";
                foreach (var key in listProduct) {
                    producto += "\"" + key + "\"";
                    if (key != listProduct.Last()) {
                        producto += ",";
                    }
                }
                producto += "],\"Ruta\":[";
                foreach (var key in listRoute) {
                    producto += "\"" + key + "\"";
                    if (key != listRoute.Last()) {
                        producto += ",";
                    }
                }
                producto += "]}";
                var resul = new APIIntermediaProductsController().Post(producto, cedisId);
                if (resul == "\"Creado\"")
                    return RedirectToAction("Index", new { text = "Pedido creado.", complete = true });
                return RedirectToAction("Index", new { text = "Producto no creado." + resul.Replace("\"", ""), complete = false });
            }
            return RedirectToAction("Index", "Home");
        }
        public List<Destinos> listDestinos()
        {
            List<Destinos> listDestino = new APIIntermediaDestinoController().Get();
            List<Destinos> listDestinoCedis = new List<Destinos>();
            Int32 cedis = Convert.ToInt32(Session["Cedi"]);
            if (listDestino != null)
            {
                foreach (var key in listDestino)
                {
                    if (key.Cedis == cedis && key.Enable == 1)
                    {
                        listDestinoCedis.Add(key);
                    }
                }
            }
            return listDestinoCedis;
        }
    }
}
