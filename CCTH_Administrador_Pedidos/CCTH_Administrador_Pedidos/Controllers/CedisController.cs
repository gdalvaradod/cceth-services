﻿using CCTH_Administrador_Pedidos.Controllers.DataAcces;
using CCTH_Administrador_Pedidos.Controllers.Utils;
using CCTH_Administrador_Pedidos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 

namespace CCTH_Administrador_Pedidos.Controllers
{
    public class CedisController : Controller
    {
        // GET: Cedis
        public ActionResult Index()
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) 
            {                      
                ViewBag.ShowView = "Configuracion";
                ViewBag.Option = "Cedis";
                return View();
            }
            return RedirectToAction("Index", "Home");
        }

        public JsonResult GetCedis(string name, string status) {
            List<Cedis> cedisList = new APIIntermediaCedisController().Get();
            if (!string.IsNullOrEmpty(name))
                cedisList = cedisList.Where(cedis => cedis.CedisName.Contains(name)).ToList();
            if (!string.IsNullOrEmpty(status))
                cedisList = cedisList.Where(cedis => cedis.Enable == Convert.ToInt32(status)).ToList();
            return Json(new { data = cedisList }, JsonRequestBehavior.AllowGet);
        }
        // GET: Cedis/Details/5
        public ActionResult Details(int id)
        {
            ViewBag.ShowView = "Configuracion";
            ViewBag.Option = "Cedis";
            return View();
        }
        // GET: Cedis/Create
        public ActionResult Create()
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) 
            {
                ViewBag.Sites = new APIIntermediaSites().GetSites();
                ViewBag.ShowView = "Configuracion";
                ViewBag.Option = "Cedis";
                return View();
            }
            return RedirectToAction("Index", "Home");
        }
        // POST: Cedis/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) 
            {
                //agregar al json el siteId
                string json = "{\"Id\": 0,\"CedisNameCCHT\": \""+ collection[2] + "\",\"CedisIdOpecd\": "+ collection[3] + ",\"Nombre\": \""+ collection[4] + "\",\"Enable\": 1" + ", \"SiteId\":" + collection[5]+ "}";
                string resp = new APIIntermediaCedisController().Post(json);
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index", "Home");
        }
        // GET: Cedis/Edit/5
        public ActionResult Edit(int id)
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) 
            {
                List<Cedis> listCedis = new APIIntermediaCedisController().Get();
                Cedis cedi = null;
                if (listCedis != null)
                {
                    foreach (var key in listCedis)
                    {
                        if (key.Id == id)
                        {
                            cedi = new Cedis()
                            {
                                Id = key.Id,
                                CedisName = key.CedisName,
                                CedisId = key.CedisId,
                                Nombre = key.Nombre,
                                Enable = key.Enable
                             };
                         }
                    }
                }
                ViewBag.ShowView = "Configuracion";
                ViewBag.Option = "Cedis";
                return View(cedi);
            }
            return RedirectToAction("Index", "Home");
        }
        // POST: Cedis/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Cedis cedis)
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) 
            {
                // TODO: Add update logic here
                string json = "{\"CedisNameCCHT\": \""+ cedis.CedisName + "\",\"Nombre\": \"" + cedis.Nombre + "\",\"Enable\":"+ cedis.Enable +"}";
                string resp = new APIIntermediaCedisController().Put(id, cedis.CedisId, json);
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index", "Home");
        }
    }
}
