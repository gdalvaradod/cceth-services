﻿using CCTH_Administrador_Pedidos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Controllers.Utils {
    public class alertsx {
        public ResponseMessagesAlerts SetMessagesAlerts(string text , bool complete) 
        {
            ResponseMessagesAlerts alertsx = new ResponseMessagesAlerts();
            if (text != null) {
                alertsx.Success = complete;
                alertsx.Content = complete ? text : "";
                alertsx.Error = !complete ? text : "";
            }
            return alertsx;
        }
    }
}