﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using CCTH_Administrador_Pedidos.Models.Dtos;
using Newtonsoft.Json;

namespace CCTH_Administrador_Pedidos.Controllers.Utils
{
    public class RestClientUtil<T>
        where T : class
    {
        private string _baseUrl;

        public RestClientUtil(string baseUrl)
        {
            _baseUrl = baseUrl;
        }
        public void SetBaseUrl(string baseUrl)
        {
            _baseUrl = baseUrl;
        }
        public string GetBaseUrl()
        {
            return _baseUrl.Clone().ToString();
        }

        public List<T> Get(string uri)
        {
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(CreateEndpoint(uri));
                webRequest.Method = "GET";
                webRequest.KeepAlive = false;
                webRequest.Timeout = Settings.RequestTimeOut;
                webRequest.ContentType = "application/json";

                string responseContent = "";
                using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse())
                    if ((int)webResponse.StatusCode == 200)
                    {
                        using (StreamReader reader = new StreamReader(webResponse.GetResponseStream()))
                        {
                            responseContent = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else
                    {
                        responseContent = webResponse.ContentType;
                    }
                return JsonConvert.DeserializeObject<List<T>>(responseContent);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public T GetSingle(string uri)
        {
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(CreateEndpoint(uri));
                webRequest.Method = "GET";
                webRequest.KeepAlive = false;
                webRequest.Timeout = Settings.RequestTimeOut;
                webRequest.ContentType = "application/json";

                string responseContent = "";
                using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse())
                    if ((int)webResponse.StatusCode == 200)
                    {
                        using (StreamReader reader = new StreamReader(webResponse.GetResponseStream()))
                        {
                            responseContent = reader.ReadToEnd();
                            reader.Close();
                            reader.Dispose();
                        }
                    }
                    else
                    {
                        responseContent = webResponse.ContentType;
                    }
                return JsonConvert.DeserializeObject<T>(responseContent);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Post(string uri, T entity)
        {
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(CreateEndpoint(uri));
                webRequest.Method = "POST";
                webRequest.KeepAlive = false;
                webRequest.ContentType = "application/json";

                var jsonEntity = JsonConvert.SerializeObject(entity);
                byte[] byteArray = Encoding.UTF8.GetBytes(jsonEntity);
                webRequest.ContentLength = byteArray.Length;

                using (var streamWriter = new StreamWriter(webRequest.GetRequestStream()))
                {
                    streamWriter.Write(jsonEntity);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var responseContent = string.Empty;
                using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(webResponse.GetResponseStream()))
                    {
                        responseContent = reader.ReadToEnd();
                        reader.Close();
                        reader.Dispose();
                    }
                }
                var postResponse = JsonConvert.DeserializeObject<PostResponse>(responseContent);
                if (postResponse.Success)
                    return postResponse.Id;
                throw new Exception(postResponse.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void Put(string uri, T entity)
        {
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(CreateEndpoint(uri));
                webRequest.Method = "PUT";
                webRequest.KeepAlive = false;
                webRequest.ContentType = "application/json";

                var jsonEntity = JsonConvert.SerializeObject(entity);
                byte[] byteArray = Encoding.UTF8.GetBytes(jsonEntity);
                webRequest.ContentLength = byteArray.Length;

                using (var streamWriter = new StreamWriter(webRequest.GetRequestStream()))
                {
                    streamWriter.Write(jsonEntity);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var responseContent = string.Empty;
                using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(webResponse.GetResponseStream()))
                    {
                        responseContent = reader.ReadToEnd();
                        reader.Close();
                        reader.Dispose();
                    }
                }
                var putResponse = JsonConvert.DeserializeObject<BaseResponse>(responseContent);
                if (!putResponse.Success)
                    throw new Exception(putResponse.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void Delete(string uri)
        {
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(CreateEndpoint(uri));
                webRequest.Method = "DELETE";
                webRequest.KeepAlive = false;
                webRequest.ContentType = "application/json";

                string responseContent = "";
                using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse())

                using (StreamReader reader = new StreamReader(webResponse.GetResponseStream()))
                {
                    responseContent = reader.ReadToEnd();
                    reader.Close();
                    reader.Dispose();
                }

                var deleteResponse = JsonConvert.DeserializeObject<BaseResponse>(responseContent);
                if (!deleteResponse.Success)
                    throw new Exception(deleteResponse.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string CreateEndpoint(string uri)
        {
            return string.Format("{0}{1}", _baseUrl, uri);
        }
    }
}