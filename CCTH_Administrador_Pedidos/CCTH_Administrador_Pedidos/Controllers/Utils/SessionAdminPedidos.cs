﻿using CCTH_Administrador_Pedidos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CCTH_Administrador_Pedidos.Controllers.Utils
{
    public class SessionAdminPedidos 
    {
        public bool validateSession(LoginResponse Session)
        {
            bool resp = false;
            if (Session != null)
            {
                resp = Session.Access;
            }
            return resp;
        }
    }
}