﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Controllers.Utils
{
    public class ErrorsMsj
    {
        public ErrorsMsj() { }

        public StringWriter MensajeError(Exception ex) {
            var sw = new StringWriter();
            sw.WriteLine("[{\"value\":\"Error\",\"name\":\""+ ex.Message + "\"}]");
            return sw;
        }
    }
}