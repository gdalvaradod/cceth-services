﻿using CCTH_Administrador_Pedidos.Controllers.DataAcces;
using CCTH_Administrador_Pedidos.Controllers.Utils;
using CCTH_Administrador_Pedidos.Enums;
using CCTH_Administrador_Pedidos.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CCTH_Administrador_Pedidos.Models.ViewModels;
using System.Data.SqlTypes;
using System.Configuration;
using CCTH_Administrador_Pedidos.Models.Dtos;

namespace CCTH_Administrador_Pedidos.Controllers
{
    public class OrdenAddressesController : Controller
    {
        private APIIntermediaCustomersController APIIntermedia = new APIIntermediaCustomersController();

        public JsonResult CustomerFilter(string id) {
            int tempCedis = Convert.ToInt32(Session["Cedi"]);
            List<CustomerFilter> customer = APIIntermedia.GetFilter(tempCedis, id);
            var count = customer.Count; //listprod.Count; //results.Count;
            if (count == 0)
                return Json(new { Success = false, Data = customer, Error = "No existe el código del cliente." }, JsonRequestBehavior.AllowGet);
            return Json(new { Success = true, Data = customer, Error = "" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index(string text = null, bool complete = false) {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) {
                Int32 cedis = Convert.ToInt32(Session["Cedi"]);
                List<Destinos> listDestino = new APIIntermediaDestinoController().Get();
                var rutas = new List<Destinos>();
                if (listDestino != null && listDestino.Any())
                    rutas = listDestino.Where(destino => destino.Cedis == cedis).ToList();

                ResponseMessagesAlerts alertsx = null;
                if (text != null) {
                    ResponseMessagesAlerts alert = new ResponseMessagesAlerts() {
                        Success = complete,
                        Content = complete ? text : "",
                        Error = !complete ? text : ""
                    };
                    alertsx = alert;
                }
                ViewBag.Cedis = Session["Cedi"];
                ViewBag.alerts = alertsx;
                ViewBag.Rutas = rutas;
                ViewBag.ShowView = "Clientes";
                ViewBag.Option = "Index";
                return View();
            }
            return RedirectToAction("Index", "Home");

        }

        public JsonResult GetCustomers(int? statusConfirmation, string name, string route, string cedisId, string customerType) {
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);

            var dataTableResult = APIIntermedia.Get(statusConfirmation, name, route, cedisId, customerType, start, length);
            return Json(new { recordsTotal = dataTableResult.totalRecords, recordsFiltered = dataTableResult.filteredRecords, data = dataTableResult.data, draw = Request["draw"] }, JsonRequestBehavior.AllowGet);
        }

        // GET: OrdenAddresses/Details/5
        public ActionResult Details(int id) {
            return View();
        }

        // GET: OrdenAddresses/Create
        public ActionResult Create() {
            if (!new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"]))
                return RedirectToAction("Index", "Home");

            //var paises = new APIIntermediaConfigBase().GetPaises();
            List<Destinos> rutas = listDestinos();

            //ViewBag.Paises = paises;
            ViewBag.Rutas = rutas;
            ViewBag.CedisId = Session["Cedi"];
            ViewBag.Action = "Create";
            ViewBag.ShowView = "Clientes";
            ViewBag.Option = "Create";
            return View(new CustomerVM());
        }
        // POST: OrdenAddresses/Create
        [HttpPost]
        public JsonResult Create(CustomerVM customerVM) {
            try {
                if (!new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"]))
                    return Json(new { url = Url.Action("Index", "Home"), complete = false, message = "Inicie sesion" }, JsonRequestBehavior.AllowGet);
                var customer = new Customers();
                customer.Id = customerVM.Id;
                customer.CustomerId = customerVM.CustomerId;
                customer.CRMId = customerVM.CRMId;
                customer.CCTHId = customerVM.CCTHId;
                customer.StatusConfirmation = customerVM.StatusConfirmation;
                customer.Name = customerVM.Name;
                customer.Cuc = customerVM.Cuc;
                customer.BarCode = customerVM.BarCode;
                customer.PriceList = 0;
                customer.Sequence = 1;
                customer.DistributionCenter = Session["Cedi"].ToString();
                customer.RouteCode = customerVM.RouteCode;
                customer.Manager = customerVM.Manager;
                customer.PhysicalAddress = customerVM.PhysicalAddress;
                customer.Street = customerVM.Street;
                customer.IndoorNumber = customerVM.IndoorNumber;
                customer.OutdoorNumber = customerVM.OutdoorNumber;
                customer.Intersection1 = customerVM.Intersection1;
                customer.Intersection2 = customerVM.Intersection2;
                customer.Country = customerVM.Country;
                customer.State = customerVM.State;
                customer.City = customerVM.City;
                customer.Neighborhood = customerVM.Neighborhood;
                customer.ClientType = true;
                customer.Visit.Monday = customerVM.Monday;
                customer.Visit.Tuesday = customerVM.Tuesday;
                customer.Visit.Wednesday = customerVM.Wednesday;
                customer.Visit.Thursday = customerVM.Thursday;
                customer.Visit.Friday = customerVM.Friday;
                customer.Visit.Saturday = customerVM.Saturday;
                customer.Visit.Sunday = customerVM.Sunday;
                customer.CustomerVarious = customerVM.CustomerVarious;
                customer.CustomerType = "CLIENTE_PORTAL";
                customer.Email = customerVM.Email;
                customer.PostCode = customerVM.PostCode;
                customer.Telephone = customerVM.Telephone;
                customer.Contact = customerVM.Contact;
                customer.Description = customerVM.Description;
                customer.AddressType = customerVM.AddressType;

                var errorMessage = APIIntermedia.POST(JsonConvert.SerializeObject(customer));
                if (string.IsNullOrEmpty(errorMessage)) {
                    return Json(new {
                        url = Url.Action("Index", new { text = "Cliente creado.", complete = true }),
                        complete = true,
                        message = "Cliente creado."
                    },
                    JsonRequestBehavior.AllowGet);
                }
                else {
                    return Json(new {
                        url = Url.Action("Index", new { text = "Cliente no creado.", complete = false }),
                        complete = false,
                        message = errorMessage
                    },
                   JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex) {
                return Json(new {
                    url = Url.Action("Index", new { text = ex.Message, complete = false }),
                    complete = false,
                    message = ex.Message
                },
                   JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult Edit(int id, int? orderGeneralId = null) {
            if (!new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"]))
                return RedirectToAction("Index", "Home");

            var paises = new APIIntermediaConfigBase().GetPaises();
            CustomerVM customerVm = new CustomerVM(); ;
            Customers customer = APIIntermedia.Get(id);

            Map(customer, customerVm);
            if (orderGeneralId != null) {
                var ordenAddress = APIIntermedia.Get((int)orderGeneralId, AddressType.BILLING);
                Map(ordenAddress, customerVm);
            }
            ViewBag.Rutas = listDestinos();
            ViewBag.CedisId = Session["Cedi"];
            ViewBag.RutasDefault = customer.RouteCode;
            ViewBag.Action = "Edit";
            ViewBag.ShowView = "Clientes";
            ViewBag.Option = "Create";
            return View("Create", customerVm);
        }

        [HttpPost]
        [Route("OrdenAddresses/Edit/{id}")]
        public JsonResult Edit(CustomerVM customerVM, int id) {
            if (!new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"]))
                return Json(new { url = Url.Action("Index", "Home") }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index", "Home");
            try {
                var customer = new Customers();
                customer.Id = customerVM.Id;
                customer.CustomerId = customerVM.CustomerId;
                customer.CRMId = customerVM.CRMId;
                customer.CCTHId = customerVM.CCTHId;
                customer.StatusConfirmation = customerVM.StatusConfirmation;
                customer.Name = customerVM.Name;
                if (!string.IsNullOrEmpty(customerVM.Cuc) && Convert.ToInt32(customerVM.Cuc) == 0) {
                    customer.Cuc = "";
                }
                else {
                    customer.Cuc = customerVM.Cuc;
                }
                customer.BarCode = customerVM.BarCode;
                customer.PriceList = 0;
                customer.Sequence = 1;
                customer.DistributionCenter = Session["Cedi"].ToString();
                customer.RouteCode = customerVM.RouteCode;
                customer.Manager = customerVM.Manager;
                customer.PhysicalAddress = customerVM.PhysicalAddress;
                customer.Street = customerVM.Street;
                customer.IndoorNumber = customerVM.IndoorNumber;
                customer.OutdoorNumber = customerVM.OutdoorNumber;
                customer.Intersection1 = customerVM.Intersection1;
                customer.Intersection2 = customerVM.Intersection2;
                customer.Country = customerVM.Country;
                customer.State = customerVM.State;
                customer.City = customerVM.City;
                customer.Neighborhood = customerVM.Neighborhood;
                customer.ClientType = true;
                customer.Visit.Monday = customerVM.Monday;
                customer.Visit.Tuesday = customerVM.Tuesday;
                customer.Visit.Wednesday = customerVM.Wednesday;
                customer.Visit.Thursday = customerVM.Thursday;
                customer.Visit.Friday = customerVM.Friday;
                customer.Visit.Saturday = customerVM.Saturday;
                customer.Visit.Sunday = customerVM.Sunday;
                customer.CustomerVarious = customerVM.CustomerVarious;
                customer.CustomerType = customerVM.CustomerType;
                customer.Email = customerVM.Email;
                customer.PostCode = customerVM.PostCode;
                customer.Telephone = customerVM.Telephone;
                customer.Contact = customerVM.Contact;
                customer.Description = customerVM.Description;
                //var _customers = new RestClientUtil<CustomerRelationResponse>(ConfigurationManager.AppSettings["API_Integraciones"])
                       //.Get($"Customer/GetSearchFilterRelation?cedi={customer.DistributionCenter}&crmId={customer.CRMId}");
                var putCustomerResponse = new BaseResponse { Success = false };
                //for (int i = 0; i < _customers.Count; i ++)
                //{
                    customer.AddressType = "shipping";
                    //customer.Id = _customers[i].Id.ToString();
                    //APIIntermedia = new APIIntermediaCustomersController();
                    putCustomerResponse = APIIntermedia.Put(customer, customer.Id);
                //}
                
                if (putCustomerResponse.Success && customerVM.OrderGeneralId > 0) {
                    if (customerVM.WantBill) {
                        var orderAddress = Map(customerVM, new OrdenAddresses());
                        var putOrderAddressResponse = APIIntermedia.Put(orderAddress);
                        if (putOrderAddressResponse.Success)
                            return Json(new {
                                url = Url.Action("Details", "OrdenGenerals", new {
                                    text = "Cliente editado.",
                                    complete = putOrderAddressResponse.Success,
                                    status = Session["OrderStatus"],
                                    date = Session["OrderDelivaryDate"],
                                    route = Session["OrderRoute"],
                                    id = Session["OrderOpeId"]
                                }),
                                complete = putOrderAddressResponse.Success,
                                message = putOrderAddressResponse.Message
                            }, JsonRequestBehavior.AllowGet);
                        //return RedirectToAction("Details", "OrdenGenerals", new
                        //{
                        //    text = "Cliente editado.",
                        //    complete = putOrderAddressResponse.Success,
                        //    status = Session["OrderStatus"],
                        //    date = Session["OrderDelivaryDate"],
                        //    route = Session["OrderRoute"],
                        //    id = Session["OrderOpeId"]
                        //});
                        return Json(new {
                            url = Url.Action("Details", "OrdenGenerals", new {
                                text = putOrderAddressResponse.Message,
                                complete = putOrderAddressResponse.Success,
                                status = Session["OrderStatus"],
                                date = Session["OrderDelivaryDate"],
                                route = Session["OrderRoute"],
                                id = Session["OrderOpeId"]
                            }),
                            complete = putOrderAddressResponse.Success,
                            message = putOrderAddressResponse.Message
                        }, JsonRequestBehavior.AllowGet);
                        //return RedirectToAction("Details", "OrdenGenerals", new
                        //{
                        //    text = putOrderAddressResponse.Message,
                        //    complete = putOrderAddressResponse.Success,
                        //    status = Session["OrderStatus"],
                        //    date = Session["OrderDelivaryDate"],
                        //    route = Session["OrderRoute"],
                        //    id = Session["OrderOpeId"]
                        //});
                    }
                    else
                        return Json(new {
                            url = Url.Action("Details", "OrdenGenerals", new {
                                text = "Cliente editado.",
                                complete = true,
                                status = Session["OrderStatus"],
                                date = Session["OrderDelivaryDate"],
                                route = Session["OrderRoute"],
                                id = Session["OrderOpeId"]
                            }),
                            complete = true,
                            message = "Cliente editado"
                        }, JsonRequestBehavior.AllowGet);
                    //return RedirectToAction("Details", "OrdenGenerals", new
                    //{
                    //    text = "Cliente editado.",
                    //    complete = true,
                    //    status = Session["OrderStatus"],
                    //    date = Session["OrderDelivaryDate"],
                    //    route = Session["OrderRoute"],
                    //    id = Session["OrderOpeId"]
                    //});
                }
                if (!putCustomerResponse.Success && customerVM.OrderGeneralId > 0)
                    return Json(new {
                        url =
                         this.Url.Action("Details", "OrdenGenerals", new {
                             text = putCustomerResponse.Message,
                             complete = putCustomerResponse.Success,
                             status = Session["OrderStatus"],
                             date = Session["OrderDelivaryDate"],
                             route = Session["OrderRoute"],
                             id = Session["OrderOpeId"]
                         }),
                        complete = putCustomerResponse.Success,
                        message = putCustomerResponse.Message
                    }, JsonRequestBehavior.AllowGet);

                //return RedirectToAction("Details", "OrdenGenerals", new
                //{
                //    text = putCustomerResponse.Message,
                //    complete = putCustomerResponse.Success,
                //    status = Session["OrderStatus"],
                //    date = Session["OrderDelivaryDate"],
                //    route = Session["OrderRoute"],
                //    id = Session["OrderOpeId"]
                //});
                if (putCustomerResponse.Success && customerVM.OrderGeneralId <= 0)
                    return Json(new {
                        url = Url.Action("Index", new { text = "Cliente editado.", complete = putCustomerResponse.Success }),
                        complete = putCustomerResponse.Success,
                        message = putCustomerResponse.Message
                    },
                    JsonRequestBehavior.AllowGet);
                //return RedirectToAction("Index", new { text = "Cliente editado.", complete = putCustomerResponse.Success });
                return Json(new {
                    url = Url.Action("Index", new { text = putCustomerResponse.Message, complete = putCustomerResponse.Success }),
                    complete = putCustomerResponse.Success,
                    message = putCustomerResponse.Message
                },
                JsonRequestBehavior.AllowGet);
                //return RedirectToAction("Edit", new { id = id, orderGeneralId = customerVM.OrderGeneralId, customerVM = customerVM, Error = putCustomerResponse.Message });
                //return RedirectToAction("Index", new { text = putCustomerResponse.Message, complete = putCustomerResponse.Success });
            }
            catch (Exception ex) {
                return Json(new { url = Url.Action("Index", new { text = ex.Message, complete = false }) }, JsonRequestBehavior.AllowGet);
            }

        }
        //[HttpPost]
        //public ActionResult Edit(int id, string RouteCode, int Cuc)
        //{
        //    if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"]))
        //    {
        //        try
        //        {
        //            string customer = "{ \"CustomerId\":" + id + ",\"status\":1,\"routeCode\":\"" + RouteCode + "\",\"cuc\":\"" + Cuc + "\"}";
        //            string resp = APIIntermedia.Put(customer, id);
        //            return RedirectToAction("Index");
        //        }
        //        catch
        //        {
        //            return View();
        //        }
        //    }
        //    return RedirectToAction("Index", "Home");

        //}

        // GET: OrdenAddresses/Delete/5
        public ActionResult Delete(int id) {
            return View();
        }

        // POST: OrdenAddresses/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection) {
            try {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch {
                return View();
            }
        }
        public List<Destinos> listDestinos() {
            List<Destinos> listDestino = new APIIntermediaDestinoController().Get();
            List<Destinos> listDestinoCedis = new List<Destinos>();
            Int32 cedis = Convert.ToInt32(Session["Cedi"]);
            if (listDestino != null) {
                foreach (var key in listDestino) {
                    if (key.Cedis == cedis && key.Enable == 1) {
                        listDestinoCedis.Add(key);
                    }
                }
            }
            return listDestinoCedis;
        }

        [HttpGet]
        [Route("ordenAddresses/getVisitDays")]
        public JsonResult GetVisitDays(int cedisId, int routeId, int zipCode, string neighborhood) {
            try {
                var visitDays = APIIntermedia.GetVisitDays(cedisId, routeId, zipCode, neighborhood);
                var visitDaysValues = new List<int>();
                visitDays.ForEach(visitDay => visitDaysValues.Add((int)visitDay));
                return Json(new { data = visitDaysValues }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex) {
                throw ex;
            }

        }

        #region private methods
        private CustomerVM Map(Customers customer, CustomerVM customerVm) {
            customerVm.Id = customer.Id;
            customerVm.CustomerId = customer.CustomerId;
            customerVm.CRMId = customer.CRMId;
            customerVm.CCTHId = customer.CCTHId;
            customerVm.StatusConfirmation = customer.StatusConfirmation;
            customerVm.Name = customer.Name;
            customerVm.Cuc = customer.Cuc;
            customerVm.BarCode = customer.BarCode;
            customerVm.PriceList = customer.PriceList;
            customerVm.Sequence = customer.Sequence;
            customerVm.DistributionCenter = customer.DistributionCenter;
            customerVm.RouteCode = customer.RouteCode;
            customerVm.Manager = customer.Manager;
            customerVm.PhysicalAddress = customer.PhysicalAddress;
            customerVm.Street = customer.Street;
            customerVm.IndoorNumber = customer.IndoorNumber;
            customerVm.OutdoorNumber = customer.OutdoorNumber;
            customerVm.Intersection1 = customer.Intersection1;
            customerVm.Intersection2 = customer.Intersection2;
            customerVm.Country = customer.Country;
            customerVm.State = customer.State;
            customerVm.City = customer.City;
            customerVm.Neighborhood = customer.Neighborhood;
            customerVm.ClientType = customer.ClientType;
            customerVm.Monday = customer.Visit.Monday;
            customerVm.Tuesday = customer.Visit.Tuesday;
            customerVm.Wednesday = customer.Visit.Wednesday;
            customerVm.Thursday = customer.Visit.Thursday;
            customerVm.Friday = customer.Visit.Friday;
            customerVm.Saturday = customer.Visit.Saturday;
            customerVm.Sunday = customer.Visit.Sunday;
            customerVm.CustomerVarious = customer.CustomerVarious;
            customerVm.CustomerType = customer.CustomerType;
            customerVm.Email = customer.Email;
            customerVm.PostCode = customer.PostCode;
            customerVm.Telephone = customer.Telephone;
            customerVm.Contact = customer.Contact;
            customerVm.Description = customer.Description;
            customerVm.AddressType = customer.AddressType;

            return customerVm;
        }
        private Customers Map(CustomerVM customerVm, Customers customer) {
            customer.Id = customerVm.Id;
            customer.CustomerId = customerVm.CustomerId;
            customer.CRMId = customerVm.CRMId;
            customer.CCTHId = customerVm.CCTHId;
            customer.StatusConfirmation = customerVm.StatusConfirmation;
            customer.Name = customerVm.Name;
            customer.Cuc = customerVm.Cuc;
            customer.BarCode = customerVm.BarCode;
            customer.PriceList = customerVm.PriceList;
            customer.Sequence = customer.Sequence;
            customer.DistributionCenter = customer.DistributionCenter;
            customer.RouteCode = customerVm.RouteCode;
            customer.Manager = customerVm.Manager;
            customer.PhysicalAddress = customerVm.PhysicalAddress;
            customer.Street = customerVm.Street;
            customer.IndoorNumber = customerVm.IndoorNumber;
            customer.OutdoorNumber = customerVm.OutdoorNumber;
            customer.Intersection1 = customerVm.Intersection1;
            customer.Intersection2 = customerVm.Intersection2;
            customer.Country = customerVm.Country;
            customer.State = customerVm.State;
            customer.City = customerVm.City;
            customer.Neighborhood = customerVm.Neighborhood;
            customer.ClientType = customer.ClientType;
            customer.Visit.Monday = customerVm.Monday;
            customer.Visit.Tuesday = customerVm.Tuesday;
            customer.Visit.Wednesday = customerVm.Wednesday;
            customer.Visit.Thursday = customerVm.Thursday;
            customer.Visit.Friday = customerVm.Friday;
            customer.Visit.Saturday = customerVm.Saturday;
            customer.Visit.Sunday = customerVm.Sunday;
            customer.CustomerVarious = customerVm.CustomerVarious;
            customer.CustomerType = customerVm.CustomerType;
            customer.Email = customerVm.Email;
            customer.PostCode = customerVm.PostCode;
            customer.Telephone = customerVm.Telephone;
            customer.Contact = customerVm.Contact;
            customer.Description = customerVm.Description;
            customer.AddressType = customerVm.AddressType;

            return customer;
        }
        private CustomerVM Map(OrdenAddresses orderAddress, CustomerVM customerVm) {
            customerVm.WantBill = orderAddress.WANT_BILL == 1 ? true : false;
            customerVm.BillingFirstname = orderAddress.FIRSTNAME;
            customerVm.BillingLastname = orderAddress.LASTNAME;
            customerVm.BillingCity = orderAddress.CITY;
            customerVm.BillingCompany = orderAddress.COMPANY;
            customerVm.BillingEmail = orderAddress.EMAIL;
            customerVm.BillingMunicipality = orderAddress.MUNICIPALITY;
            customerVm.BillingNeighborhood = orderAddress.NEIGHBORHOOD;
            customerVm.BillingNumber = orderAddress.NUMBER;
            customerVm.BillingPostCode = orderAddress.POSTCODE;
            customerVm.BillingRfc = orderAddress.RFC;
            customerVm.BillingTelephone = orderAddress.TELEPHONE;
            customerVm.BillingStreet = orderAddress.STREET;
            customerVm.OrderGeneralId = orderAddress.OrderGeneralId;
            customerVm.OrderAddressId = orderAddress.Id;

            return customerVm;
        }
        private OrdenAddresses Map(CustomerVM customerVm, OrdenAddresses orderAddress) {
            orderAddress.WANT_BILL = (short?)(customerVm.WantBill ? 1 : 0);
            orderAddress.FIRSTNAME = customerVm.BillingFirstname;
            orderAddress.LASTNAME = customerVm.BillingLastname;
            orderAddress.CITY = customerVm.BillingCity;
            orderAddress.COMPANY = customerVm.BillingCompany;
            orderAddress.EMAIL = customerVm.BillingEmail;
            orderAddress.MUNICIPALITY = customerVm.BillingMunicipality;
            orderAddress.NEIGHBORHOOD = customerVm.BillingNeighborhood;
            orderAddress.NUMBER = customerVm.BillingNumber;
            orderAddress.POSTCODE = customerVm.BillingPostCode;
            orderAddress.RFC = customerVm.BillingRfc;
            orderAddress.TELEPHONE = customerVm.BillingTelephone;
            orderAddress.STREET = customerVm.BillingStreet;
            orderAddress.OrderGeneralId = customerVm.OrderGeneralId;
            orderAddress.Id = customerVm.OrderAddressId;

            return orderAddress;
        }

        #endregion
    }
}
