﻿using CCTH_Administrador_Pedidos.Controllers.DataAcces;
using CCTH_Administrador_Pedidos.Controllers.Utils;
using CCTH_Administrador_Pedidos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;

namespace CCTH_Administrador_Pedidos.Controllers
{
    public class ConfiguracionAutomaticaController : Controller
    {
        private readonly RestClientUtil<ConfigAuto> _restClient = new RestClientUtil<ConfigAuto>(ConfigurationManager.AppSettings["API_Integraciones"]);

        // GET: ConfiguracionAutomatica
        public ActionResult Index()
        {
            if (!new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"]))
                return RedirectToAction("Index", "Home");

            List<ConfigAuto> Auto = _restClient.Get("AdminConfigAuto");
            ViewBag.ShowView = "Configuracion";
            ViewBag.Option = "ConfigAuto";
            return View(Auto);
        }

        public ActionResult Create()
        {
            if (!new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"]))
                return RedirectToAction("Index", "Home");
            ViewBag.Action = "Create";
            ViewBag.HttpMethod = "POST";
            return View(new ConfigAuto());
        }

        [HttpPost]
        public JsonResult Create(ConfigAuto configuration)
        {
            try
            {
                ValidateAutomaticConfiguration(configuration);
                var configurationId = _restClient.Post("AdminConfigAuto", configuration);
                return Json(new { Success = true, Message = "Creado", Id = configurationId });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

        public ActionResult Edit(int id)
        {
            if (!new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"]))
                return RedirectToAction("Index", "Home");

            var automaticConfiguration = _restClient.GetSingle("AdminConfigAuto/" + id);
            if (automaticConfiguration != null && automaticConfiguration.Id > 0)
            {
                ViewBag.Action = "Edit";
                ViewBag.HttpMethod = "POST";
                return View("Create", automaticConfiguration);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        [Route("ConfiguracionAutomatica/{id}")]
        public JsonResult Delete(int id)
        {
            try
            {
                _restClient.Delete("AdminConfigAuto/" + id);
                return Json(new { Success = true, Message = "Eliminado" });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

        [HttpPost]
        [Route("ConfiguracionAutomatica/Edit/{id}")]
        public JsonResult Edit(int id, ConfigAuto configuration)
        {
            try
            {
                ValidateAutomaticConfiguration(configuration);
                _restClient.Put("AdminConfigAuto/" + id, configuration);
                return Json(new { Success = true, Message = "Editado" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        private void ValidateAutomaticConfiguration(ConfigAuto automaticConfiguration)
        {
            var errorMessage = string.Empty;
            if (string.IsNullOrEmpty(automaticConfiguration.Nombre))
                errorMessage += "Nombre es requerido, ";
            if (string.IsNullOrEmpty(automaticConfiguration.Sistema))
                errorMessage += "Sistema es requerido, ";
            if (string.IsNullOrEmpty(automaticConfiguration.Hora))
                errorMessage += "Hora es requerido, ";
            if (automaticConfiguration.Cedis <= 0)
                errorMessage += "Cedis es requerido ";
            if (automaticConfiguration.Ruta <= 0)
                errorMessage += "Ruta es requerido";
            if (!string.IsNullOrEmpty(errorMessage))
                throw new ArgumentException(errorMessage);
        }
    }
}
