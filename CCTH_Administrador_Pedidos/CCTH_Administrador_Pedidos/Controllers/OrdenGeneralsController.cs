﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CCTH_Administrador_Pedidos.Models;
using CCTH_Administrador_Pedidos.Controllers.Utils;
using CCTH_Administrador_Pedidos.Controllers.DataAcces;
using System.IO;
using System.Configuration;
using CCTH_Administrador_Pedidos.Models.Dtos;

namespace CCTH_Administrador_Pedidos.Controllers
{
    public class OrdenGeneralsController : Controller
    {
        private APIIntermediaController APIIntegraciones = new APIIntermediaController();
        private Encript Encript = new Encript();

        #region loginRequest      
        [HttpPost]
        public ActionResult Validate(string User, string Password,string Cedi)
        {
            
            var accion = "";
            var ctrl = "";
            string PassEncript = Encript.Encriptar(Password);
            Session["msj"] = "Error de Usuario o contraseña";
            accion = "Index";
            ctrl = "Home";

            if (User != null && !String.IsNullOrWhiteSpace(User))
            {
                if (Password != null && !String.IsNullOrWhiteSpace(Password))
                {
                    if (Cedi != "Error" && !String.IsNullOrWhiteSpace(Cedi))
                    {
                        string sw ="{\"Username\":\""+ User +"\",\"Password\":\""+ PassEncript + "\",\"CedisId\":" + Convert.ToInt32(Cedi) + "}";
                        LoginResponse sesionUser = new APIIntermediaAccountController().Post(sw);
                        if (sesionUser.Access)
                        {
                            Session.Remove("msj");
                            Session["UserID"] = sesionUser;
                            Session.Timeout = Settings.RequestTimeOut;
                            Session["Cedi"] = Cedi; 
                            accion = "Index";
                            ctrl = "OrdenGenerals";
                        }
                    }
                }
            }
            return RedirectToAction(accion, ctrl);
        }
        [HttpPost]
        public void GetCedis(string User, bool onlyEnabled)
        {
            List<UserCedis> ListCedis = null;
            var sw = new StringWriter();
            string error = "";
            try
            {
                if (!String.IsNullOrWhiteSpace(User))
                {
                    ListCedis = new APIIntermediaAccountController().Get(User, out error);
                }
                var count = 0;
                if (error == "" && ListCedis.Count() > 0)
                {
                    ListCedis = onlyEnabled ? ListCedis.Where(cedis => cedis.Enable == 1).ToList() : ListCedis;
                    count = ListCedis.Count();
                }
                if (count == 0)
                {
                    string mensaje = error != "" ? error : "" ;
                    sw.WriteLine("[{\"value\":\"Usuario sin Cedi\",\"name\":\"" + mensaje + "\",\"error\":true}]");
                }
                else
                {
                    sw.WriteLine("[");
                    for (var key = 0; key < count; key++)
                    {
                        sw.WriteLine("{\"value\":\"" + ListCedis[key].CedisIdPecd + "\",\"name\":\"" + ListCedis[key].Nombre + "\",\"error\":false}");
                        if (key != count - 1)
                        {
                            sw.WriteLine(",");
                        }
                    }
                    sw.WriteLine("]");
                }
            }
            catch (Exception ex)
            {
                sw = new ErrorsMsj().MensajeError(ex);
            }
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=success");
            Response.ContentType = "text/json";
            Response.Write(sw);
            Response.End();
        }
        public ActionResult Logout()
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"]))
            {
                Session.Remove("UserID");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
            }

            return RedirectToAction("Index", "Home");
        }
        #endregion
        public ActionResult Index()
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"]))
            {
                var account = Session["UserID"] as LoginResponse;
                int cedisId = Convert.ToInt32(account.Cedis.CedisId);
                ViewBag.Cedis = cedisId;
                ViewBag.alerts = Session["Alerts"] as ResponseMessagesAlerts != null ? Session["Alerts"] as ResponseMessagesAlerts : null;
                Session.Remove("Alerts");
                List<Destinos> rutas = listDestinos();
                ViewBag.Rutas = rutas;                
                ViewBag.ShowView = "Pedidos";
                ViewBag.Option = "Index";
                return View();
            }
            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        [Route("OrdenGenerals/GetPedidos")]
        public JsonResult GetPedidos(DateTime? deliveryDate, string status = null, string route = null, int? draw = 0, int? start = 0, int? length = 200)
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) 
            {
                var account = Session["UserID"] as LoginResponse;
                int cedisId = Convert.ToInt32(account.Cedis.CedisId);
                var dataTableResult = APIIntegraciones.Get(cedisId, deliveryDate, status, route, start, length);
                
                return Json(new { draw = draw, recordsTotal = dataTableResult.totalRecords, recordsFiltered = dataTableResult.filteredRecords, data = dataTableResult.data
                }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Error = "Inicia sesion." }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [Route("OrdenGenerals/GetSearchFilter")]
        public JsonResult GetCustomersSearchFilter(string ItemSearch) 
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) {
                var account = Session["UserID"] as LoginResponse;
                int cedisId = Convert.ToInt32(account.Cedis.CedisId);

                String[] Searching = ItemSearch.Trim().Split(',');
                var name = Searching[0] == "0" ? "" : Searching[0];
                var email = Searching[1] == "0" ? "" : Searching[1];
                var tel = Searching[2] == "0" ? "" : Searching[2];
                int? cuc = null;
                int? route = null;
                cuc = Searching[3] == "1" ? cuc : Convert.ToInt32(Searching[3]);
                route = Searching[4] == "0" ? 0 : Convert.ToInt32(Searching[4]);
                var barcode = Searching[5] == "0" ? "" : Searching[5];
                //string search = "{\"Name\":\"" + name + "\",\"Email\":\"" + email + "\",\"Tel\":\"" + tel + "\",\"Cuc\":" + cuc +"}";

                List<CustomerSearchFilter> Listcustomers = new APIIntermediaCustomersController().GetSerachFilter(cedisId, name, email, tel, barcode,cuc, route)
                    .Where(customer => !string.IsNullOrEmpty(customer.CrmId)).ToList();
                var crmIds = Listcustomers.Select(c => c.CrmId).Distinct().ToList();
                var filteredCustomers = new List<CustomerSearchFilter>();
                crmIds.ForEach(crm => filteredCustomers.Add(Listcustomers.First(c => c.CrmId == crm)));
                return Json(new { data = filteredCustomers }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Error = "Inicia sesion." }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [Route("OrdenGenerals/GetProductsSearchFilter")]
        public JsonResult GetProductsSearchFilter(string ItemSearch , int route) {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) {
                var account = Session["UserID"] as LoginResponse;
                int cedisId = Convert.ToInt32(account.Cedis.CedisId);

                String[] Searching = ItemSearch.Trim().Split(',');
                var name = Searching[0] == "0" ? "" : Searching[0];
                var code = Searching[1] == "0" ? "" : Searching[1];

                //string search = "{\"Name\":\"" + name + "\",\"Email\":\"" + email + "\",\"Tel\":\"" + tel + "\",\"Cuc\":" + cuc +"}";

                List<ProductSearchFilter> Listproducts = new APIIntermediaProductsController().GetSerachFilter(cedisId, route, name, code);
                return Json(new { data = Listproducts }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Error = "Inicia sesion." }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ValidateListOrder(string Ids) 
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) {
                var resp = new APIIntermediaController().Put(Convert.ToString(Ids));
                if (!resp.Any())
                    return Json(new { Success = true, Error = "Pedidos Validados." }, JsonRequestBehavior.AllowGet);
                return Json(new { Success = false, Error = "Existen ordenes que no tienen clientes validados"}, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Error = "Inicia sesion." }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ValidateProductList(string code, int route ) {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) {
                List<Productos> ProductList = Session["Product"] as List<Productos>;
                Productos Product = new Productos();
                foreach (var key in ProductList) {
                    if (key.CodigoProducto == code && key.Ruta == route) {
                        Product = key;
                    }
                }
                if (Product.CodigoProducto == null)
                    return Json(new { Success = false, Data = Product, Error = "No existe el código del producto." }, JsonRequestBehavior.AllowGet);
                return Json(new { Success = true, Data = Product, Error = "" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Error = "Inicia sesion." }, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult Details(string date, string status, string route, string id = "0", string text = null, bool? complete = false) {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) {
                List<PedidosDet> pedidos = APIIntegraciones.Get(id + "," + date + "," + status + "," + route);
                if (pedidos == null || !pedidos.Any())
                {
                    Session["Alerts"] = new ResponseMessagesAlerts() { Success = false, Error = "No se encontraron los pedidos solicitados" };
                    return RedirectToAction("Index");
                }
                var account = Session["UserID"] as LoginResponse;
                int cedisId = Convert.ToInt32(account.Cedis.CedisId);
                foreach (var pedido in pedidos)
                {
                    //var customer = new RestClientUtil<Customers>(ConfigurationManager.AppSettings["API_Integraciones"])
                    //    .Get($"Customer/GetCustomer?name={pedido.Cliente.FIRSTNAME} {pedido.Cliente.LASTNAME}&email={pedido.Cliente.EMAIL}&cedis={cedisId}&route={route}").FirstOrDefault();
                    var customer = new RestClientUtil<CustomerRelationResponse>(ConfigurationManager.AppSettings["API_Integraciones"])
                        .Get($"Customer/GetSearchFilterRelation?cedi={0}&crmId={pedido.Cliente.CRM_Id}").FirstOrDefault();
                    if (customer != null)
                        pedido.Cliente.Id = Convert.ToInt32(customer.Id);
                    else
                    {
                        Session["Alerts"] = new ResponseMessagesAlerts() { Success = false, Error = "No se encontraron los datos del cliente relacionados con la orden" };
                        return RedirectToAction("Index");
                    }
                }
                ResponseMessagesAlerts alertsx = null;
                if (text != null)
                {
                    ResponseMessagesAlerts alert = new ResponseMessagesAlerts()
                    {
                        Success = (bool)complete,
                        Content = (bool)complete ? text : "",
                        Error = (bool)!complete ? text : ""
                    };
                    alertsx = alert;
                }

                ViewBag.CantConfirm = pedidos.Where(pedido => string.IsNullOrEmpty(pedido.Cliente.CRM_Id) || string.IsNullOrEmpty(pedido.Cliente.BarCode)).Count() >= 1;
                ViewBag.stateOrder = status;
                ViewBag.ShowView = "Pedidos";
                ViewBag.Option = "Index";
                ViewBag.Ids = pedidos;
                ViewBag.alerts = alertsx;
                ViewBag.RouteId = route;
                Session["OrderDelivaryDate"] = date;
                Session["OrderStatus"] = status;
                Session["OrderRoute"] = route;
                Session["OrderOpeId"] = id;
                return View(pedidos);
            }
            return RedirectToAction("Index", "Home");
        }
        public ActionResult Create()
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"]))
            {
                var account = Session["UserID"] as LoginResponse;
                int cedisId = Convert.ToInt32(account.Cedis.CedisId);
                List<Productos> Productos = new APIIntermediaProductsController().Get(cedisId);
                Session["Product"] = Productos;
                List<Destinos> rutas = listDestinos();
                PayDesk paydesk = new APIIntermediaController().GetPayDesk(cedisId);
                var nextdays = Convert.ToDateTime(paydesk.payDeskDate).AddDays(4);
                ViewBag.PayDesk = paydesk.payDeskDate.Substring(0,10);
                ViewBag.MinDate = DateTime.Now.ToString("yyyy-MM-dd");
                ViewBag.NextDay = nextdays.Year + "-" + (nextdays.Month < 10 ? "0" + nextdays.Month : "" ) + "-" + (nextdays.Day < 10 ? "0" + nextdays.Day : "");
                ViewBag.ShowView = "Pedidos";
                ViewBag.Option = "Create";

                ViewBag.alerts = Session["Alerts"] as ResponseMessagesAlerts != null ? Session["Alerts"] as ResponseMessagesAlerts : null;
                Session.Remove("Alerts");
                ViewBag.Rutas = rutas;
                return View();
            }
            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(int Ruta,string FEntrega, string Cliente,string ArrayProducto, string WantBill)
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"]))
            {
                short bill = WantBill == "1" ? (short)1 : (short)0;
                String[] listproducto = ArrayProducto.Trim().Split(',');
                var account = Session["UserID"] as LoginResponse;
                int tempCedis = Convert.ToInt32(account.Cedis.CedisId);
                string pedido = "{ \"delivery_date\":\"" + FEntrega + "\",\"route_code\":" + Ruta + ",\"customer_id\":\"" + Cliente.Trim() + "\",\"cedis_id\":"+ tempCedis + ",\"payment_method\":\"cashondelivery\",\"WantBill\":" + bill + ",";
                pedido += "\"items\":[";
                for (var key = 0; key < listproducto.Length; key++) {
                    String[] row = listproducto[key].Split('$');
                    pedido += "{\"item_id\": \"" + row[0]+ "\",\"parent_item_id\": 0,\"sku\": \"" + row[0] + "\",\"name\": \"" + row[1] + "\",\"qty_canceled\": \"0.0000\",\"qty_invoiced\": \"0.0000\",\"qty_ordered\": \"" + row[2] + "\",\"qty_refunded\": \"0.0000\",\"qty_shipped\": \"0.0000\",\"price\": \"0.0000\",\"original_price\": \"0.0000\",\"discount_amount\": \"0.0000\",\"row_total\": \"0.0000\",\"price_incl_tax\": \"0.0000\",\"row_total_incl_tax\": \"0.0000\"}";
                    if (key != listproducto.Length - 1) {
                        pedido += ",";
                    }
                }
                pedido += "]}";
                string resp = APIIntegraciones.Post(pedido);
                Session.Remove("Product");
                if (resp == "true")
                    Session["Alerts"] = new alertsx().SetMessagesAlerts("Pedido creado.", true);
                else
                 Session["Alerts"] = new alertsx().SetMessagesAlerts("Pedido no creado." + resp.Replace("\"", ""), false);
                return RedirectToAction("Create");
            }
            return RedirectToAction("Index", "Home");
        }
        public JsonResult CreateOrder(int Ruta, string FEntrega, string Cliente, string ArrayProducto, string WantBill) {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) {
                short bill = WantBill == "1" ? (short)1 : (short)0;
                String[] listproducto = ArrayProducto.Trim().Split(',');
                var account = Session["UserID"] as LoginResponse;
                int tempCedis = Convert.ToInt32(account.Cedis.CedisId);
                string pedido = "{ \"delivery_date\":\"" + FEntrega + "\",\"route_code\":" + Ruta + ",\"customer_id\":\"" + Cliente.Trim() + "\",\"cedis_id\":" + tempCedis + ",\"payment_method\":\"cashondelivery\",\"WantBill\":" + bill + ",";
                pedido += "\"items\":[";
                for (var key = 0; key < listproducto.Length; key++) {
                    String[] row = listproducto[key].Split('$');
                    pedido += "{\"item_id\": \"" + row[0] + "\",\"parent_item_id\": 0,\"sku\": \"" + row[0] + "\",\"name\": \"" + row[1] + "\",\"qty_canceled\": \"0.0000\",\"qty_invoiced\": \"0.0000\",\"qty_ordered\": \"" + row[2] + "\",\"qty_refunded\": \"0.0000\",\"qty_shipped\": \"0.0000\",\"price\": \"0.0000\",\"original_price\": \"0.0000\",\"discount_amount\": \"0.0000\",\"row_total\": \"0.0000\",\"price_incl_tax\": \"0.0000\",\"row_total_incl_tax\": \"0.0000\"}";
                    if (key != listproducto.Length - 1) {
                        pedido += ",";
                    }
                }
                pedido += "]}";
                string resp = APIIntegraciones.Post(pedido);
                Session.Remove("Product");
                if (resp == "\"completado!\"") 
                {
                    Session["Alerts"] = new alertsx().SetMessagesAlerts("Pedido creado.", true);
                    return Json(new { Data = new alertsx().SetMessagesAlerts("Pedido creado.", true)}, JsonRequestBehavior.AllowGet);
                }
                else {
                    Session["Alerts"] = new alertsx().SetMessagesAlerts("Pedido no creado." + resp.Replace("\"", ""), false);
                    return Json(new { Data = new alertsx().SetMessagesAlerts("Pedido no creado." + resp.Replace("\"", ""), false) }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { Error = "Inicia sesion." }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult EditOrder(int Id) {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"])) {
                var account = Session["UserID"] as LoginResponse;
                int cedisId = Convert.ToInt32(account.Cedis.CedisId);
                List<Productos> Productos = new APIIntermediaProductsController().Get(cedisId);
                Session["Product"] = Productos;
                var customername = "";
                PedidoEdit pedido = new APIIntermediaController().GetOrder(Id);
                List<Destinos> rutas = listDestinos();
                PayDesk paydesk = new APIIntermediaController().GetPayDesk(cedisId);
                var nextdays = Convert.ToDateTime(paydesk.payDeskDate).AddDays(4);
                ViewBag.MinDate = DateTime.Now.ToString("yyyy-MM-dd");
                ViewBag.PayDesk = paydesk.payDeskDate.Substring(0, 10);
                ViewBag.NextDay = nextdays.Year + "-" + (nextdays.Month < 10 ? "0" + nextdays.Month : "") + "-" + (nextdays.Day < 10 ? "0" + nextdays.Day : "");

                ViewBag.alerts = Session["Alerts"] as ResponseMessagesAlerts != null ? Session["Alerts"] as ResponseMessagesAlerts : null;
                Session.Remove("Alerts");
                ViewBag.Rutas = rutas;
                ViewBag.Pedido = pedido;
                ViewBag.Customername = customername;
                ViewBag.ShowView = "Pedidos";
                ViewBag.Option = "Edit";
                return View("Edit", pedido);
            }
            return RedirectToAction("Index", "Home");
        }
        // GET: OrdenGenerals/Edit/5
        public ActionResult Edit(int Ruta, string FEntrega, string Cliente, string payment,int InterId, string ArrayProducto,string ArrayRemove, string WantBill)
        {
            if (new SessionAdminPedidos().validateSession((LoginResponse)Session["UserID"]))
            {
                short bill = WantBill == "1" ? (short)1 : (short)0;
                String[] listproducto = ArrayProducto.Trim().Split(',');
                String[] listproductoRemove = ArrayRemove.Trim().Split(',');
                var account = Session["UserID"] as LoginResponse;
                int tempCedis = Convert.ToInt32(account.Cedis.CedisId);
                string pedido = "{ \"delivery_date\":\"" + FEntrega + "\",\"route_code\":" + Ruta + ",\"customer_id\":\"" + Cliente + "\",\"cedis_id\":" + tempCedis + ",\"payment_method\":\"" + payment + "\",\"WantBill\":" + bill + ",";
                pedido += "\"items\":[";
                for (var key = 0; key < listproducto.Length; key++)
                {
                    String[] row = listproducto[key].Split('$');
                    pedido += "{\"InterId\": " + row[3] + ",\"item_id\": \"" + row[0] + "\",\"parent_item_id\": 0,\"sku\": \"0\",\"name\": \"" + row[1] + "\",\"qty_canceled\": \"0.0000\",\"qty_invoiced\": \"0.0000\",\"qty_ordered\": \"" + row[2] + "\",\"qty_refunded\": \"0.0000\",\"qty_shipped\": \"0.0000\",\"price\": \"0.0000\",\"original_price\": \"0.0000\",\"discount_amount\": \"0.0000\",\"row_total\": \"0.0000\",\"price_incl_tax\": \"0.0000\",\"row_total_incl_tax\": \"0.0000\"}";
                    if (key != listproducto.Length - 1)
                    {
                        pedido += ",";
                    }
                }
                pedido += "],\"Removes\": [";
                for (var key = 0; key < listproductoRemove.Length; key++)
                {
                    String[] row = listproductoRemove[key].Split('$');
                    pedido += row[0];
                    if (key != listproductoRemove.Length - 1)
                    {
                        pedido += ",";
                    }
                }
                pedido += "]}";
                string resp = APIIntegraciones.PutOrder(pedido, InterId);
                Session.Remove("Product");
                if (resp == "true")
                    Session["Alerts"] = new alertsx().SetMessagesAlerts("Pedido editado.", true);
                else
                    Session["Alerts"] = new alertsx().SetMessagesAlerts("Pedido no editado.", false);
                return RedirectToAction("EditOrder",new { Id = InterId });
            }
            return RedirectToAction("Index", "Home");
        }
        public List<Destinos> listDestinos()
        {
            List<Destinos> listDestino = new APIIntermediaDestinoController().Get();
            List<Destinos> listDestinoCedis = new List<Destinos>();
            Int32 cedis = Convert.ToInt32(Session["Cedi"]);
            if (listDestino != null)
            {
                foreach (var key in listDestino)
                {
                    if (key.Cedis == cedis && key.Enable == 1)
                    {
                        listDestinoCedis.Add(key);
                    }
                }
            }
            return listDestinoCedis;
        }
    }
}
