﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CCTH_Administrador_Pedidos.Controllers.DataAcces;
using CCTH_Administrador_Pedidos.Models;
using CCTH_Administrador_Pedidos.Models.Dtos;
using CCTH_Administrador_Pedidos.Models.ViewModels;

namespace CCTH_Administrador_Pedidos.Controllers
{
    public class LogsController : Controller
    {
        private readonly APIIntermediaLogsController _logsRepository = new APIIntermediaLogsController();

        public ActionResult Index()
        {
            var cedisId = Convert.ToInt32(Session["Cedi"]);
            ViewBag.CedisId = cedisId;
            ViewBag.Routes = new APIIntermediaDestinoController().Get().Where(route => route.Cedis == cedisId).ToList();
            ViewBag.ShowView = "Reportes";
            return View();
        }

        public JsonResult GetLogsForDataTable(int draw, DateTime? deliveryDate = null, int? cedisId = null, int? routeId = null, int? trip = null, int? processStatus = null, int? start = null, int? length = null)
        {
            
            var logs = _logsRepository.FindLogsForDataTable(deliveryDate, cedisId, routeId, trip, processStatus, start, length);
            var dataTableLogs = new List<DataTableLog>();
            logs.data.ForEach(log => dataTableLogs.Add(new DataTableLog
            {
                Id = log.Id,
                DeliveryDate = log.DeliveryDate.ToString("dd/MM/yyyy"),
                CedisId = log.CedisId,
                RouteId = log.RouteId,
                Trip = log.Trip,
                ProcessDescription = log.Process.Description,
                ProcessStatus = Enum.GetName(typeof(Enums.ProcessStatus), log.ProcessStatus)
            }));
            return Json(new { draw = draw, recordsTotal = logs.totalRecords, recordsFiltered = logs.filteredRecords, data = dataTableLogs }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Details(int id)
        {
            var log = _logsRepository.Find(id);
            ViewBag.ShowView = "Reportes";
            return View(log);
        }
       
    }
}