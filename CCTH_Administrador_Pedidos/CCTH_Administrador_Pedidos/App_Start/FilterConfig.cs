﻿using System.Web;
using System.Web.Mvc;

namespace CCTH_Administrador_Pedidos
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
