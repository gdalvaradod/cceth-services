﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class InventoryDet
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("TripId")]
        public int TripId { get; set; }

        [JsonProperty("ItemId")]
        public string ItemId { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Qty")]
        public int Qty { get; set; }

        [JsonProperty("WbcId")]
        public int WbcId { get; set; }
    }
}