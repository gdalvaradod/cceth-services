﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class InventoryEnc
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("TripId")]
        public int TripId { get; set; }

        [JsonProperty("Route")]
        public int Route { get; set; }

        [JsonProperty("DeliveryDate")]
        public Nullable<System.DateTime> DeliveryDate { get; set; }

        [JsonProperty("CedisIdOpeCd")]
        public int CedisIdOpeCd { get; set; }
    }
}