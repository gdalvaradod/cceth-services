﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class Customers
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("CustomerId")]
        public string CustomerId { get; set; }

        [JsonProperty("CRMId")]
        public string CRMId { get; set; }

        [JsonProperty("StatusConfirmation")]
        public int StatusConfirmation { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Cuc")]
        public string Cuc { get; set; }

        [JsonProperty("BarCode")]
        public string BarCode { get; set; }

        [JsonProperty("PriceList")]
        public long PriceList { get; set; }

        [JsonProperty("Sequence")]
        public int Sequence { get; set; }

        [JsonProperty("DistributionCenter")]
        public string DistributionCenter { get; set; }

        [JsonProperty("RouteCode")]
        public string RouteCode { get; set; }

        [JsonProperty("Manager")]
        public string Manager { get; set; }

        [JsonProperty("PhysicalAddress")]
        public string PhysicalAddress { get; set; }

        [JsonProperty("Street")]
        public string Street { get; set; }

        [JsonProperty("IndoorNumber")]
        public string IndoorNumber { get; set; }

        [JsonProperty("OutdoorNumber")]
        public string OutdoorNumber { get; set; }

        [JsonProperty("Intersection1")]
        public string Intersection1 { get; set; }

        [JsonProperty("Intersection2")]
        public string Intersection2 { get; set; }

        [JsonProperty("Country")]
        public string Country { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("City")]
        public string City { get; set; }

        [JsonProperty("Neighborhood")]
        public string Neighborhood { get; set; }

        [JsonProperty("ClientType")]
        public bool ClientType { get; set; }

        [JsonProperty("Visit")]
        public Visit Visit { get; set; }

        [JsonProperty("CustomerVarious")]
        public bool CustomerVarious { get; set; }

        [JsonProperty("CustomerType")]
        public string CustomerType { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("PostCode")]
        public string PostCode { get; set; }

        [JsonProperty("Telephone")]
        public string Telephone { get; set; }

        [JsonProperty("Latitude")]
        public string Latitude { get; set; }

        [JsonProperty("Longitude")]
        public string Longitude { get; set; }

        [JsonProperty("CreatedOn")]
        public string CreatedOn { get; set; }

        [JsonProperty("ModifiedOn")]
        public string ModifiedOn { get; set; }

        [JsonProperty("Contact")]
        public string Contact { get; set; }

        [JsonProperty("Address")]
        public string Address => PhysicalAddress;

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Code")]
        public string Code => Cuc;

        [JsonProperty("AddressType")]
        public string AddressType { get; set; }

        public string CCTHId { get; set; }

     
        public Customers()
        {
            Visit = new Visit();
        }
    }
}