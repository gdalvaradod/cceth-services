﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CCTH_Administrador_Pedidos.Models
{
    // Puede agregar datos del perfil del usuario agregando más propiedades a la clase ApplicationUser. Para más información, visite http://go.microsoft.com/fwlink/?LinkID=317594.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Agregar aquí notificaciones personalizadas de usuario
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<CCTH_Administrador_Pedidos.Models.OrdenGeneral> OrdenGenerals { get; set; }

        public System.Data.Entity.DbSet<CCTH_Administrador_Pedidos.Models.OrdenAddresses> OrdenAddresses { get; set; }

        public System.Data.Entity.DbSet<CCTH_Administrador_Pedidos.Models.Customers> Customers { get; set; }

        public System.Data.Entity.DbSet<CCTH_Administrador_Pedidos.Models.Cedis> Cedis { get; set; }

        public System.Data.Entity.DbSet<CCTH_Administrador_Pedidos.Models.Destinos> Destinos { get; set; }

        public System.Data.Entity.DbSet<CCTH_Administrador_Pedidos.Models.ConfigAuto> ConfigAutoes { get; set; }

        public System.Data.Entity.DbSet<CCTH_Administrador_Pedidos.Models.InventoryEnc> InventoryEncs { get; set; }

        public System.Data.Entity.DbSet<CCTH_Administrador_Pedidos.Models.InventoryDet> InventoryDets { get; set; }
    }
}