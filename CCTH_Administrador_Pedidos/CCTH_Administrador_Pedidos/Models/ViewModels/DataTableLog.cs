﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models.ViewModels
{
    public class DataTableLog
    {
        public int Id { get; set; }
        public string DeliveryDate { get; set; }
        public int CedisId { get; set; }
        public int RouteId { get; set; }
        public int Trip { get; set; }
        public string ProcessDescription { get; set; }
        public string ProcessStatus { get; set; }
    }
}