﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models.ViewModels
{
    public class CustomerVM
    {
        //CustomerData
        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string CRMId { get; set; }
        public int StatusConfirmation { get; set; }
        public string Name { get; set; }
        public string Cuc { get; set; }
        public string BarCode { get; set; }
        public long PriceList { get; set; }
        public int Sequence { get; set; }
        public string DistributionCenter { get; set; }
        public string RouteCode { get; set; }
        public string Manager { get; set; }
        public string PhysicalAddress { get; set; }
        public string Street { get; set; }
        public string IndoorNumber { get; set; }
        public string OutdoorNumber { get; set; }
        public string Intersection1 { get; set; }
        public string Intersection2 { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Neighborhood { get; set; }
        public bool ClientType { get; set; }
        public bool CustomerVarious { get; set; }
        public string CustomerType { get; set; }
        public string Email { get; set; }
        public string PostCode { get; set; }
        public string Telephone { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CreatedOn { get; set; }
        public string ModifiedOn { get; set; }
        public string Contact { get; set; }
        public string Address => PhysicalAddress;
        public string Description { get; set; }
        public string Code => Cuc;
        public string AddressType { get; set; }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }
        public bool Sunday { get; set; }
        public string CCTHId { get; set; }


        //BillingData
        public bool WantBill { get; set; }
        public int OrderGeneralId { get; set; }
        public int OrderAddressId { get; set; }
        public string BillingPostCode { get; set; }
        public string BillingLastname { get; set; }
        public string BillingFirstname { get; set; }
        public string BillingCity { get; set; }
        public string BillingEmail { get; set; }
        public string BillingTelephone { get; set; }
        public string BillingCompany { get; set; }
        public string BillingRfc { get; set; }
        public string BillingNumber { get; set; }
        public string BillingStreet { get; set; }
        public string BillingNeighborhood { get; set; }
        public string BillingMunicipality { get; set; }
    }
}