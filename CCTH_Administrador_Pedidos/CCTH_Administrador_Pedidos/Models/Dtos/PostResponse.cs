﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models.Dtos
{
    public class PostResponse : BaseResponse
    {
        public int Id { get; set; }
    }
}