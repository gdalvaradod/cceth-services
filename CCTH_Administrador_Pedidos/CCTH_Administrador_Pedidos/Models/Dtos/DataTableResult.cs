﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models.Dtos
{
    public class DataTableResult <T>
        where T : class
    {
        public List<T> data { get; set; }
        public int totalRecords { get; set; }
        public int filteredRecords { get; set; }

        public DataTableResult()
        {
            data = new List<T>();
        }
    }
}