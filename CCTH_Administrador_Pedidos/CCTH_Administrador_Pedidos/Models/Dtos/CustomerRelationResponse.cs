﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models.Dtos
{
    public class CustomerRelationResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Tel { get; set; }
        public int? Cuc { get; set; }
        public string CrmId { get; set; }
        public string BarCode { get; set; }
        public string CustomerType { get; set; }

    }
}