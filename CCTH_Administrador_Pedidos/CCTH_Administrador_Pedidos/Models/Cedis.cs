﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class Cedis
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("CedisNameCCHT")]
        public string CedisName { get; set; }

        [JsonProperty("CedisIdOpecd")]
        public int CedisId { get; set; }

        [JsonProperty("Nombre")]
        public string Nombre { get; set; }

        [JsonProperty("SiteId")]
        public int SiteId { get; set; }

        [JsonProperty("Enable")]
        public int Enable { get; set; }
    }
}