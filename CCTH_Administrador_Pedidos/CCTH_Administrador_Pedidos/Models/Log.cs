﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class Log
    {
        public int Id { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int CedisId { get; set; }
        public int RouteId { get; set; }
        public int Trip { get; set; }
        public Process Process { get; set; }
        public Enums.ProcessStatus ProcessStatus { get; set; }
        public List<LogDetail> LogDetail { get; set; }
    }
}