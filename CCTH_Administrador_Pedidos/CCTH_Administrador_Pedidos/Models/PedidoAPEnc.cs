﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class PedidoAPEnc
    {
        [JsonProperty("id")]
        public int Ruta { get; set; }

        [JsonProperty("id")]
        public DateTime FechaEntrega { get; set; }

        [JsonProperty("id")]
        public int Viaje { get; set; }

        [JsonProperty("id")]
        public string CLiente { get; set; }

        [JsonProperty("id")]
        public List<PedidoAPDet> Productos { get; set; }
    }
}