﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class PedidosEnc
    {
        [JsonProperty("OPECId")]
        public Nullable<long> ORD_OPECDId { get; set; }

        [JsonProperty("delivery_date")]
        public string DELIVERY_DATE { get; set; }

        [JsonProperty("route_code")]
        public string ROUTE_CODE { get; set; }

        [JsonProperty("viaje")]
        public string Viaje { get; set; }

        [JsonProperty("statusOrden")]
        public string ORD_StatusOrden { get; set; }

        [JsonProperty("createFrom")]
        public string ORD_CreateFrom { get; set; }

    }
}