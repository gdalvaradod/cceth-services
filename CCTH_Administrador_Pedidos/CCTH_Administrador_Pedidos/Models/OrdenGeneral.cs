﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class OrdenGeneral
    {
        public int Id { get; set; }
        public int ORD_EntityId { get; set; }
        public string ORD_Status { get; set; }
        public string ORD_CouponCode { get; set; }
        public Nullable<int> ORD_CustomerId { get; set; }
        public string ORD_ShippingDescription { get; set; }
        public decimal ORD_BaseDiscountAmount { get; set; }
        public decimal ORD_BaseGrandTotal { get; set; }
        public decimal ORD_BaseShippingAmount { get; set; }
        public decimal ORD_BaseShippingTaxAmount { get; set; }
        public decimal ORD_BaseSubtotal { get; set; }
        public decimal ORD_BaseTaxAmount { get; set; }
        public Nullable<decimal> ORD_BaseTotalPaid { get; set; }
        public Nullable<decimal> ORD_BaseTotalRefunded { get; set; }
        public decimal ORD_DiscountAmount { get; set; }
        public decimal ORD_GrandTotal { get; set; }
        public Nullable<decimal> ORD_ShippingAmount { get; set; }
        public Nullable<decimal> ORD_ShippingTaxAmount { get; set; }
        public Nullable<decimal> ORD_StoreToOrderRate { get; set; }
        public decimal ORD_Subtotal { get; set; }
        public decimal ORD_TaxAmount { get; set; }
        public Nullable<decimal> ORD_TotalPaid { get; set; }
        public Nullable<decimal> ORD_TotalQtyOrdered { get; set; }
        public Nullable<decimal> ORD_TotalRefunded { get; set; }
        public decimal ORD_BaseShippingDiscountAmount { get; set; }
        public decimal ORD_BaseSubtotalInclTax { get; set; }
        public decimal ORD_BaseTotalDue { get; set; }
        public decimal ORD_ShippingDiscountAmount { get; set; }
        public decimal ORD_SubtotalInclTax { get; set; }
        public decimal ORD_TotalDue { get; set; }
        public string ORD_IncrementId { get; set; }
        public string ORD_AppliedRuleIds { get; set; }
        public string ORD_BaseCurrencyCode { get; set; }
        public string ORD_CustomerEmail { get; set; }
        public string ORD_DiscountDescription { get; set; }
        public string ORD_RemoteIp { get; set; }
        public string ORD_StoreCurrencyCode { get; set; }
        public string ORD_StoreName { get; set; }
        public string ORD_CreatedAt { get; set; }
        public short ORD_TotalItemCount { get; set; }
        public decimal ORD_ShippingInclTax { get; set; }
        public Nullable<decimal> ORD_BaseCustomerBalanceAmount { get; set; }
        public Nullable<decimal> ORD_CustomerBalanceAmount { get; set; }
        public decimal ORD_BaseGiftCardsAmount { get; set; }
        public decimal ORD_GiftCardsAmount { get; set; }
        public Nullable<int> ORD_RewardPointsBalance { get; set; }
        public Nullable<decimal> ORD_BaseRewardCurrencyAmount { get; set; }
        public Nullable<decimal> ORD_RewardCurrencyAmount { get; set; }
        public string ORD_PaymentMethod { get; set; }
        public Nullable<long> ORD_OPECDId { get; set; }
        public string ORD_Comments { get; set; }
        public string ORD_StatusOrden { get; set; }
        public string ORD_CreateFrom { get; set; }
        public Nullable<System.DateTime> ORD_CreateDateInter { get; set; }
    }
}