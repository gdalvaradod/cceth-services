﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class EncVentas
    {
        public int Id { get; set; }
        public Nullable<int> Codope { get; set; }
        public Nullable<int> CUC { get; set; }
        public Nullable<int> WBCSaleId { get; set; }
        public Nullable<int> Facturado { get; set; }
        public string RazonDev { get; set; }
    }
}