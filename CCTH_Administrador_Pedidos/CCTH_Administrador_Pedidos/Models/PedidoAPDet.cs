﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class PedidoAPDet
    {
        [JsonProperty("id")]
        public int Codigo { get; set; }

        [JsonProperty("id")]
        public string Nombre { get; set; }

        [JsonProperty("id")]
        public int Cantidad { get; set; }
    }
}