﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class Productos
    {
        public int Id { get; set; }
        public string CodigoProducto { get; set; }
        public string Nombre { get; set; }
        public int Ruta { get; set; }
        public int Cedis { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> WBCId { get; set; }
        public Nullable<int> Tipo { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }
}