﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class OrderItem
    {
        public OrderItem() { }
        // [JsonProperty("id")]
        // public int ID { get; set; }
        [JsonProperty("InterId")]
        public int InterId { get; set; }

        [JsonProperty("item_id")]
        public int ITEM_ID { get; set; }

        [JsonProperty("parent_item_id")]
        public int PARENT_ITEM_ID { get; set; }

        [JsonProperty("sku")]
        public string SKU { get; set; }

        [JsonProperty("name")]
        public string NAME { get; set; }

        [JsonProperty("qty_canceled")]
        public decimal QTY_CANCELED { get; set; }

        [JsonProperty("qty_invoiced")]
        public decimal QTY_INVOICED { get; set; }

        [JsonProperty("qty_ordered")]
        public decimal QTY_ORDERED { get; set; }

        [JsonProperty("qty_refunded")]
        public decimal QTY_REFUNDED { get; set; }

        [JsonProperty("qty_shipped")]
        public decimal QTY_SHIPPED { get; set; }

        [JsonProperty("price")]
        public decimal PRICE { get; set; }

        [JsonProperty("original_price")]
        public decimal ORIGINAL_PRICE { get; set; }

        [JsonProperty("discount_amount")]
        public decimal DISCOUNT_AMOUNT { get; set; }

        [JsonProperty("row_total")]
        public decimal ROW_TOTAL { get; set; }

        [JsonProperty("price_incl_tax")]
        public decimal? PRICE_INCL_TAX { get; set; }

        [JsonProperty("row_total_incl_tax")]
        public decimal? ROW_TOTAL_INCL_TAX { get; set; }
    }
}