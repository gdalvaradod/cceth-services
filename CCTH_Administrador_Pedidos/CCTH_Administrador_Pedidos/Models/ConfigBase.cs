﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class ConfigBase
    {
        public List<Estado> Estados { get; set; }
        public List<Municipio> Municipios { get; set; }
        public List<Pais> Paises { get; set; }
        public List<Colonia> Colonias { get; set; }
    }
}