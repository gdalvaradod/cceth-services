﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class ConfigAuto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Sistema { get; set; }
        public string Hora { get; set; }
        public int Cedis { get; set; }
        public int Ruta { get; set; }
        public Nullable<int> Status { get; set; }
    }
}