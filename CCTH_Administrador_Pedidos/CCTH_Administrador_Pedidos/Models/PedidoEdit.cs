﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class PedidoEdit
    {
        [JsonProperty("InterId")]
        public int InterId { get; set; }

        [JsonProperty("delivery_date")]
        public string delivery_date { get; set; }

        [JsonProperty("route_code")]
        public string route_code { get; set; }

        [JsonProperty("customer_id")]
        public string customer_id { get; set; }

        [JsonProperty("customer_name")]
        public string name { get; set; }

        [JsonProperty("cedis_id")]
        public int cedis_id { get; set; }

        [JsonProperty("payment_method")]
        public string payment_method { get; set; }

        [JsonProperty("WantBill")]
        public short WantBill { get; set; }

        [JsonProperty("items")]
        public List<OrderItem> items { get; set; }
    }
}