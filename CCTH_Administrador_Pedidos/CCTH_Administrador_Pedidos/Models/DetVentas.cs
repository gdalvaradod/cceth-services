﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class DetVentas
    {
        public int Id { get; set; }
        public Nullable<int> Cantidad { get; set; }
        public Nullable<int> CantidadCredito { get; set; }
        public int Product { get; set; }
        public int SaleId { get; set; }
    }
}