﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class UserCedis
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("CedisNameCCHT")]
        public string CedisNameCCHT { get; set; }

        [JsonProperty("CedisIdOpecd")]
        public string CedisIdPecd { get; set; }

        [JsonProperty("Nombre")]
        public string Nombre { get; set; }

        [JsonProperty("Enable")]
        public int Enable { get; set; }
    }
}