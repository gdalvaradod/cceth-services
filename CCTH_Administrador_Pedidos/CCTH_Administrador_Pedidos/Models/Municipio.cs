﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class Municipio
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CodeCCHT { get; set; }
        public string CodeCRM { get; set; }
        public string OpeId { get; set; }
    }
}