﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class CustomerFilter
    {
        public string Name { get; set; }
        public string BarCode { get; set; }
        public string AddressType { get; set; }
    }
}