﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCTH_Administrador_Pedidos.Models
{
    public class LoginResponse
    {
        [JsonProperty("User")]
        public User User { get; set; }

        [JsonProperty("Cedis")]
        public Cedis Cedis { get; set; }

        [JsonProperty("Access")]
        public bool Access { get; set; }

        [JsonProperty("Message")]
        public string Message { get; set; }
    }
}