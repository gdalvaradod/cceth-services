﻿
function executeRequest(serviceUrl, httpMethod, jsonParams, callback) {
    $.ajax({
        url: serviceUrl,
        type: httpMethod,
        data: jsonParams,
        dataType: "json",
        success: function (response) {
            if (typeof (callback) === "function")
                callback(response);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError);
        }
    });
}