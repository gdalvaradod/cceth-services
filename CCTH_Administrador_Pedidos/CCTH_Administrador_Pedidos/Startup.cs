﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CCTH_Administrador_Pedidos.Startup))]
namespace CCTH_Administrador_Pedidos
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
