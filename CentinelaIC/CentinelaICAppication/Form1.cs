﻿using System;
using System.Collections.Specialized;
using System.Threading;
using System.Windows.Forms;
using CentinelaIC.Entities;
using System.Configuration;
using Timer = System.Timers.Timer;


namespace CentinelaICAppication
{
    public partial class Form1 : Form
    {
        

        Thread hilo;
        Boolean bandCentinel;
        String serviceName = null;
        private Timer monitorTimer;

        public Form1()
        {
            InitializeComponent();
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                DateTime.Now + " : Iniciando Servicio Centinela CCETH. ", true);
            try
            {
                IntegracionCtlr.ActualizaConfig();
                ValidaServicios.ValidarServiciosDetenidos();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now + " : Error al iniciar el servicio: " + ex.Message, true);
                
            }
        }
        
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

    }
}
