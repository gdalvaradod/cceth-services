﻿using System.ServiceProcess;

namespace CentinelaIC
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new MainCentinel() 
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
