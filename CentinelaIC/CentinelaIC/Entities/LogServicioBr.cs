﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;

namespace CentinelaIC.Entities
{
    public class LogServicioBr
    {
        #region Atributos

        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        private static readonly string RutaRaiz = AppSettings()["RutaFisicaLog"]; //System.Reflection.Assembly.GetExecutingAssembly().Location.Substring(0, System.Reflection.Assembly.GetExecutingAssembly().Location.LastIndexOf("\\", StringComparison.Ordinal)) + "\\";

        #endregion

        #region Métodos

        /// <summary>
        /// Escribir registro en el archivo log del Servicio de Windows
        /// </summary>
        /// <param name="proyecto">El proyecto que genera el log</param>
        /// <param name="mensaje">Actividad a registrar</param>
        /// <param name="esDiario"></param>
        /// <returns>Indica si fue exitoso</returns>
        public static bool EscribirLog(string proyecto, string mensaje, bool esDiario = false)
        {
            try
            {
                string logArchivo;
                if (!esDiario)
                    logArchivo = RutaRaiz + "\\Log_" + proyecto + ".txt";
                else
                    logArchivo = RutaRaiz + "\\Log_" + proyecto + "_" + DateTime.Today.ToString("yyyy.MM.dd") + ".txt";

                if (!Directory.Exists(RutaRaiz))
                {
                    Directory.CreateDirectory(RutaRaiz);
                }

                var swLog = !File.Exists(logArchivo) ? new StreamWriter(logArchivo) : File.AppendText(logArchivo);
                lock (swLog)
                {
                    swLog.WriteLine(mensaje);
                    swLog.Close();
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return false;
            }
        }
        #endregion
    }
}
