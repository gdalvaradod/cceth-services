﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace CentinelaIC.Entities
{
    public static class ValidaServicios
    {
        public static void ValidarServiciosDetenidos()
        {
            string[] servicios;
            string[] estados;
            try
            {
                servicios = AppSettings()["ServiceList"].Trim().Split(',');
                estados = AppSettings()["ServiceStatusList"].Trim().Split(',');
            }
            catch (Exception e)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now + " : No se pudieron leer AppSettings de App.config.",
                    true);
                return;
            }
            if (estados.Length == servicios.Length)
            {

                for (int x = 0; x < servicios.Length; x++)
                {
                    try
                    {
                        ServiceController sc = new ServiceController(servicios[x]);
                        if (estados[x].ToUpper() == "OFF")
                        {
                            if ((sc.Status.Equals(ServiceControllerStatus.Running)) ||
                                (sc.Status.Equals(ServiceControllerStatus.StartPending)))
                            {
                                // Start the service if the current status is stopped.
                                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                    DateTime.Now + " : " + servicios[x] + " se encuentra funcionando... deteniendo",
                                    true);
                                if(sc.CanStop)
                                    sc.Stop();

                            }
                        }
                        else if (estados[x].ToUpper() == "ON")
                        {
                            if ((sc.Status.Equals(ServiceControllerStatus.Stopped)) ||
                                (sc.Status.Equals(ServiceControllerStatus.StopPending)))
                            {
                                // Start the service if the current status is stopped.
                                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                    DateTime.Now + " : " + servicios[x] + " se encuentra detenido... reiniciando",
                                    true);
                                sc.Start();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                            DateTime.Now + " : " + servicios[x] + " no se encuentra instalado en este equipo" +
                            ex.Message,
                            true);
                    }
                }
            }
            else
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now + " :  La Cantidad de estados debe de ser igual a la de servicios.",
                    true);
            }

        }

        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }
    }
}
