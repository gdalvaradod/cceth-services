﻿using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace CentinelaIC
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }

        private void SetServiceName()
        {
            var processInstaller = new ServiceProcessInstaller();
            var serviceInstaller = new ServiceInstaller();
            //set the privileges
            processInstaller.Account = ServiceAccount.LocalSystem;
            serviceInstaller.DisplayName = "CCETH_Servicio Centinela";
            serviceInstaller.StartType = ServiceStartMode.Automatic;
            //must be the same as what was set in Program's constructor
            serviceInstaller.ServiceName = "CCETH_Centinela";
            serviceInstaller.Description = "CCETH_Servicio Centinela";
            this.Installers.Add(processInstaller);
            this.Installers.Add(serviceInstaller);
        }
   
        protected override void OnAfterInstall(IDictionary savedState)
        {
            base.OnAfterInstall(savedState);
            using (var serviceController = new ServiceController("CCETH_Centinela"))
            {
                serviceController.Start();
            }

        }

        protected override void OnBeforeInstall(IDictionary savedState)
        {
            SetServiceName();
            base.OnBeforeInstall(savedState);
        }

        protected override void OnBeforeUninstall(IDictionary savedState)
        {
            SetServiceName();
            base.OnBeforeUninstall(savedState);
        }
    }

}
