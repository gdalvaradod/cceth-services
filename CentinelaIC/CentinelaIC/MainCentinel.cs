﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using CentinelaIC.Entities;
using Timer = System.Timers.Timer;


namespace CentinelaIC
{
    public partial class MainCentinel : ServiceBase
    {
        private static Timer VerifyTimer;

        public MainCentinel()
        {
            InitializeComponent(); 
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now + " : Iniciando Servicio Centinela CCETH. ", true);
                IntegracionCtlr.ActualizaConfig();
                VerifyTimer = new Timer();
                VerifyTimer.Interval = int.Parse(AppSettings()["VerifyTimer"]);
                VerifyTimer.Elapsed += OnTimerVerify;
                VerifyTimer.Start();
            }
            catch
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now + " : problema al iniciar Centinela CCETH. ", true);
            }
        }

        protected void OnTimerVerify(object sender, ElapsedEventArgs args)
        {
            try
            {
                VerifyTimer.Stop();
                IntegracionCtlr.ActualizaConfig();
                VerifyTimer.Interval = int.Parse(AppSettings()["VerifyTimer"]);
                ValidaServicios.ValidarServiciosDetenidos();
                VerifyTimer.Start();
            }
            catch (Exception e)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now + " : Error al Ejecutar Verificacion: " + e.Message, true);
                base.Stop();
            }
        }

        protected override void OnStop()
        {
            try
            {
                //bandCentinel = false;
                VerifyTimer.Stop();
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now + " : Servicio detenido con éxito.", true);
            }
            catch (Exception e)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now + " : Error al detener el servicio: " + e.Message, true);
            }
        }

        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }
    }
}
