﻿using System.ServiceProcess;

namespace SRV_CCETH
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            var servicesToRun = new ServiceBase[]
            {
                new SRV_CCETH_Service()
            };
            ServiceBase.Run(servicesToRun);
        }
    }
}
