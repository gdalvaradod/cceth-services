﻿using CCETH091703.Model.BR;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Timers;
using Timer = System.Timers.Timer;
using CCETH091703.Model.Services;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.Enum;

namespace SRV_CCETH
{
    public partial class SRV_CCETH_Service : ServiceBase
    {
        private Timer _tmrInterno;
        private Timer _tmrActualizaEdo;
        private Task _taskActualizaEdo;
        private Task _taskInterno;

        public SRV_CCETH_Service()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            IntegracionCtlr.ActualizaConfig();

            LogServicioBr.EscribirLog(AppSettings()["UsandoApisLog"],
                DateTime.Now.ToString(CultureInfo.InvariantCulture) + Environment.NewLine +
                ": Api_Integracion=" + AppSettings()["URL_INTEGRACION_API"] + Environment.NewLine +
                ": Api_Cocacola=" + AppSettings()["URL_COCACOLA_API"] + Environment.NewLine);

            try
            {
                if (!AppSettings()["X_KO_CLIENT_ID"].StartsWith("1_"))
                {
                    AddUpdateAppSettings("X_KO_CLIENT_ID",
                        "1_" + Metodos.Seguridad_Cifrar(AppSettings()["X_KO_CLIENT_ID"]).Objeto);
                }
                if (!AppSettings()["X_KO_CLIENT_SECRET"].StartsWith("1_"))
                {
                    AddUpdateAppSettings("X_KO_CLIENT_SECRET",
                        "1_" + Metodos.Seguridad_Cifrar(AppSettings()["X_KO_CLIENT_SECRET"]).Objeto);
                }
                if (!AppSettings()["username"].StartsWith("1_"))
                {
                    AddUpdateAppSettings("username", "1_" + Metodos.Seguridad_Cifrar(AppSettings()["username"]).Objeto);
                }
                if (!AppSettings()["password"].StartsWith("1_"))
                {
                    AddUpdateAppSettings("password", "1_" + Metodos.Seguridad_Cifrar(AppSettings()["password"]).Objeto);
                }
                InternoAsync();
                ActualizaEdoAsync();

                IniciaTimerInterno();
                IniciaTimerActualizaEdo();
            }
            catch (Exception e)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_SRV_CCETH_Service.OnStart: " + e.Message +
                ".\n" + e.StackTrace + ".\n", true);
            }
        }

        protected override void OnStop()
        {
            try
            {
                StopTimerInterno();
                StopTimerActualizaEdo();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_SRV_CCETH_Service.OnStop: " + ex.Message +
                ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        #region TimerInterno

        protected void IniciaTimerInterno()
        {
            try
            {
                _tmrInterno = new Timer { Interval = int.Parse(AppSettings()["IntervalTimer"]) };
                _tmrInterno.Elapsed += OnTimerInterno;
                _tmrInterno.Start();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""), true);
            }
        }

        protected void StopTimerInterno()
        {
            _tmrInterno.Stop();
            _tmrInterno = null;
        }

        protected void OnTimerInterno(object sender, ElapsedEventArgs args)
        {
            try
            {
                InternoAsync();

                #region transferido a OrdenCtrl
                //try
                //{
                //    if (_token.Equals(""))
                //    {
                //        Result<string> resulttoken = TokenCtrl.GetToken();
                //        if (resulttoken.ResultType != ResultTypes.Success)
                //        {
                //            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + "No se obtuvo Token de API_CCETH", true);
                //            _token = "";
                //            return;
                //        }
                //        else
                //        {
                //            _token = resulttoken.Objeto;
                //        }
                //    }

                //    var resultOrden = OrdenesCtrl.GetOrdenGeneral(_token);
                //    if (resultOrden.ResultType == ResultTypes.Success)
                //    {
                //        //var result = OrdenesCtrl.PostOrdenGeneral(resultOrden.Objeto);
                //    }

                //}
                //catch (Exception ex)
                //{
                //    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""), true);
                //}
                #endregion
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                 DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_SRV_CCETH_Service.OnTimerInterno: " + ex.Message +
                 ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        private async void InternoAsync()
        {
            if (_taskInterno != null)
            {
                if (_taskInterno.IsCompleted)
                {
                    await (
                        _taskInterno = Task.Run(() =>
                        {
                            try
                            {
                                var resulttoken = TokenCtrl.GetToken();
                                if (resulttoken.ResultType != ResultTypes.Success)
                                {
                                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                        DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," +
                                        "No se obtuvo Token de API_CCETH", true);
                                    return;
                                }
                                var token = resulttoken.Objeto;

                                if (!string.IsNullOrEmpty(token))
                                {
                                    OrdenesCtrl.ProcesarPedidos(token);
                                }
                            }
                            catch (Exception ex)
                            {
                                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " +
                                    ex.Message.Replace(",", ""),
                                    true);
                            }
                        })
                    );
                }
            }
            else
            {
                await (
                    _taskInterno = Task.Run(() =>
                    {
                        try
                        {
                            var resulttoken = TokenCtrl.GetToken();
                            if (resulttoken.ResultType != ResultTypes.Success)
                            {
                                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," +
                                    "No se obtuvo Token de API_CCETH", true);
                                return;
                            }
                            var token = resulttoken.Objeto;

                            if (!string.IsNullOrEmpty(token))
                            {
                                OrdenesCtrl.ProcesarPedidos(token);
                            }
                        }
                        catch (Exception ex)
                        {
                            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + ex.Message.Replace(",", ""),
                                true);
                        }
                    })
                );
            }
        }
        
        #endregion

        #region TimerActualizaEdo

        protected void IniciaTimerActualizaEdo()
        {
            try
            {
                _tmrActualizaEdo = new Timer { Interval = int.Parse(AppSettings()["ActualizaEdoTimer"]) };
                _tmrActualizaEdo.Elapsed += OnTimerActualizaEdo;
                _tmrActualizaEdo.Start();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""), true);
            }
        }

        protected void StopTimerActualizaEdo()
        {
            _tmrActualizaEdo.Stop();
            _tmrActualizaEdo = null;
        }

        protected void OnTimerActualizaEdo(object sender, ElapsedEventArgs args)
        {
            try
            {
                ActualizaEdoAsync();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                 DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_SRV_CCETH_Service.OnTimerActualizaEdo: " + ex.Message +
                 ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        private async void ActualizaEdoAsync()
        {
            if (_taskActualizaEdo != null)
            {
                if (_taskActualizaEdo.IsCompleted)
                {
                    await(
                        _taskActualizaEdo = Task.Run(() =>
                        {
                            try
                            {
                                var resulttoken = TokenCtrl.GetToken();
                                if (resulttoken.ResultType != ResultTypes.Success)
                                {
                                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                        DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," +
                                        "No se obtuvo Token de API_CCETH", true);
                                    return;
                                }
                                var token = resulttoken.Objeto;

                                if (!string.IsNullOrEmpty(token))
                                {
                                    OrdenesCtrl.ActualizaStatusCoca(token);
                                }
                            }
                            catch (Exception ex)
                            {
                                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " +
                                    ex.Message.Replace(",", ""),
                                    true);
                            }
                        })
                    );
                }
            }
            else
            {
                await(
                    _taskActualizaEdo = Task.Run(() =>
                    {
                        try
                        {
                            var resulttoken = TokenCtrl.GetToken();
                            if (resulttoken.ResultType != ResultTypes.Success)
                            {
                                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," +
                                    "No se obtuvo Token de API_CCETH", true);
                                return;
                            }
                            var token = resulttoken.Objeto;

                            if (!string.IsNullOrEmpty(token))
                            {
                                OrdenesCtrl.ActualizaStatusCoca(token);
                            }
                        }
                        catch (Exception ex)
                        {
                            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + ex.Message.Replace(",", ""),
                                true);
                        }
                    })
                );
            }
        }

        #endregion


        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        private static void AddUpdateAppSettings(string key, string value)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {
                    settings[key].Value = value;
                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException e)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_AddUpdateAppSettings",
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + e.Message.Replace(",", ""), true);
                Console.WriteLine(e.Message);
            }
        }
    }
}
