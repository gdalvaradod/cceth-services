﻿namespace SRV_CCETH
{
    partial class SRV_CCETH_Installer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.srvProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.CCETHInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // srvProcessInstaller
            // 
            this.srvProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.srvProcessInstaller.Password = null;
            this.srvProcessInstaller.Username = null;
            // 
            // CCETHInstaller
            // 
            this.CCETHInstaller.ServiceName = "SRV_CCETH";
            this.CCETHInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // SRV_CCETH_Installer
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.srvProcessInstaller,
            this.CCETHInstaller});

        }

        #endregion

        public System.ServiceProcess.ServiceProcessInstaller srvProcessInstaller;
        public System.ServiceProcess.ServiceInstaller CCETHInstaller;
    }
}