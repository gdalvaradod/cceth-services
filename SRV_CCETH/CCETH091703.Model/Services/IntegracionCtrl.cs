﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCETH091703.Model.Objects.Entity;
using System.Globalization;
using CCETH091703.Model.BR;

namespace CCETH091703.Model.Services
{
    public static class IntegracionCtlr
    {
        public static void ActualizaConfig()
        {
            try { 
            var configs = new CCTH_INTERMEDIA_Entities();
            var server = AppSettings()["ServerName"];
            var seleccion = from sel in configs.CONF_Globales
                            where sel.CGL_Servicio.Trim() == server
                            select new { sel.CGL_Key, sel.CGL_Value };

            foreach (var reg in seleccion)
            {
                AddUpdateAppSettings(reg.CGL_Key.Trim(), reg.CGL_Value.Trim());
            }
            }
            catch (Exception ex)
            {
                // Si hay error, usará los valores que ya tiene en el config
            }

        }

        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        private static void AddUpdateAppSettings(string key, string value)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {
                    settings[key].Value = value;
                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException e)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_AddUpdateAppSettings",
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + e.Message.Replace(",", ""), true);
                Console.WriteLine(e.Message);
            }
        }
    }
}