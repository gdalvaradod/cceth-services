﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using CCETH091703.Base;
using CCETH091703.Model.BR;
using CCETH091703.Model.DataAccess;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CCETH091703.Model.Services
{
    public static class OrdenesCtrl
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        #region Ordenes COCACOLA

        public static Result<List<OrderGeneralJson>> GetOrdenGeneral(string token)
        {
            var result = new Result<List<OrderGeneralJson>>();
            try
            {
                var resultOrdenes = OrdenesHelper.GetOrderGeneral(AppSettings()["URL_COCACOLA_API"] + ApiCocaColaCnx.GetOrders(),
                    int.Parse(AppSettings()["TimeOutCnx"]), token, Metodos.Seguridad_Descifrar(AppSettings()["X_KO_CLIENT_ID"]).Objeto, Metodos.Seguridad_Descifrar(AppSettings()["X_KO_CLIENT_SECRET"]).Objeto);

                if (resultOrdenes.ResultType == ResultTypes.Success)
                {
                    result.Objeto = new List<OrderGeneralJson>();
                    foreach (var item in resultOrdenes.Objeto)
                    {
                        var cad = ConvertOrderGeneral("[" + item + "]");
                        var ca = cad.Remove(0, 1);
                        var ca1 = ca.Remove(ca.Length - 1, 1);
                        result.Objeto.Add(JsonConvert.DeserializeObject<OrderGeneralJson>(ca1));
                    }
                    result.ResultType = ResultTypes.Success;
                }
                else
                {
                    result.Mensaje = resultOrdenes.Mensaje;
                    result.ResultType = resultOrdenes.ResultType;
                }

                return result;
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_InventoryCtrl.TrupInventory: " +
                    ex.Message + "." + Environment.NewLine + ex.StackTrace + "." + Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "." + Environment.NewLine + ex.StackTrace + "." + Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        private static readonly Dictionary<string, string> PedidosActualizados=new Dictionary<string, string>();
        private static DateTime _diaActual = DateTime.Today;

        public static void ActualizaStatusCoca(string token)
        {
            try
            {
                if (_diaActual != DateTime.Today)
                {
                    PedidosActualizados.Clear();
                    _diaActual = DateTime.Today;
                }
                Result<List<CedisRutas>> cedisrutasResult = OrdenesHelper.GetCedisRutas(AppSettings()["URL_INTEGRACION_API"]+ApiIntegracionesCnx.GetCedisRutas(),int.Parse(AppSettings()["TimeOutCnx"]));
                if (cedisrutasResult.ResultType !=ResultTypes.Success)
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Error_No se obtuvieron cedis-rutas de intermedia para actualizar status: ", true);
                    return;
                }

                string fecha=DateTime.Today.ToString("yyyy-MM-dd");

                foreach (var cedis in cedisrutasResult.Objeto)
                {
                    foreach (var ruta in cedis.Destinos)
                    {
                        
                        var resultOrden = GetOrdersStatus(3,cedis.cedi.ToString(),ruta.ruta,fecha);
                        if (resultOrden.ResultType == ResultTypes.Success)
                        {
                            foreach (var item in resultOrden.Objeto)
                            {
                                var orderCceth = new OrderCceth
                                {
                                    Comment = "",
                                    NotifyCustomer = "1",
                                    Status = "holded"
                                };
                                if (!PedidosActualizados.ContainsKey(item.IncrementId) ||
                                    PedidosActualizados[item.IncrementId] != orderCceth.Status)
                                {
                                    var result = PutOrdenGeneral(item.IncrementId, orderCceth, token);
                                    if (result.ResultType == ResultTypes.Error)
                                    {
                                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                            ": Error_Actualizando Status holded para item: " + item.IncrementId, true);
                                    }
                                    else
                                    {
                                        if (PedidosActualizados.ContainsKey(item.IncrementId))
                                        {
                                            PedidosActualizados[item.IncrementId] = orderCceth.Status;
                                        }
                                        else
                                        {
                                            PedidosActualizados.Add(item.IncrementId, orderCceth.Status);
                                        }
                                    }
                                }
                            }
                        }

                        resultOrden = GetOrdersStatus(4,cedis.cedi.ToString(), ruta.ruta, fecha);
                        if (resultOrden.ResultType == ResultTypes.Success)
                        {
                            foreach (var item in resultOrden.Objeto)
                            {
                                var orderCceth = new OrderCceth
                                {
                                    Comment = "",
                                    NotifyCustomer = "1",
                                    Status = "shipped"
                                };
                                if (!PedidosActualizados.ContainsKey(item.IncrementId) ||
                                    PedidosActualizados[item.IncrementId] != orderCceth.Status)
                                {
                                    var result = PutOrdenGeneral(item.IncrementId, orderCceth, token);
                                    if (result.ResultType == ResultTypes.Error)
                                    {
                                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                            ": Error_Actualizando Status shipped para item: " + item.IncrementId, true);
                                    }
                                    else
                                    {
                                        if (PedidosActualizados.ContainsKey(item.IncrementId))
                                        {
                                            PedidosActualizados[item.IncrementId] = orderCceth.Status;
                                        }
                                        else
                                        {
                                            PedidosActualizados.Add(item.IncrementId, orderCceth.Status);
                                        }
                                    }
                                }
                            }
                        }

                        resultOrden = GetOrdersStatus(5, cedis.cedi.ToString(), ruta.ruta, fecha);
                        if (resultOrden.ResultType == ResultTypes.Success)
                        {
                            foreach (var item in resultOrden.Objeto)
                            {
                                var orderCceth = new OrderCceth
                                {
                                    Comment = "",
                                    NotifyCustomer = "1",
                                    Status = "reprogrammed"
                                };
                                if (!PedidosActualizados.ContainsKey(item.IncrementId) ||
                                    PedidosActualizados[item.IncrementId] != orderCceth.Status)
                                {
                                    var result = PutOrdenGeneral(item.IncrementId, orderCceth, token);
                                    if (result.ResultType == ResultTypes.Error)
                                    {
                                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                            ": Error_Actualizando Status reprogrammed para item: " + item.IncrementId,
                                            true);
                                    }
                                    else
                                    {
                                        if (PedidosActualizados.ContainsKey(item.IncrementId))
                                        {
                                            PedidosActualizados[item.IncrementId] = orderCceth.Status;
                                        }
                                        else
                                        {
                                            PedidosActualizados.Add(item.IncrementId, orderCceth.Status);
                                        }
                                    }
                                }
                            }
                        }

                        resultOrden = GetOrdersStatus(6, cedis.cedi.ToString(), ruta.ruta, fecha);
                        if (resultOrden.ResultType == ResultTypes.Success)
                        {
                            foreach (var item in resultOrden.Objeto)
                            {
                                var orderCceth = new OrderCceth
                                {
                                    Comment = "",
                                    NotifyCustomer = "1",
                                    Status = "complete"
                                };
                                if (!PedidosActualizados.ContainsKey(item.IncrementId) ||
                                    PedidosActualizados[item.IncrementId] != orderCceth.Status)
                                {
                                    var result = PutOrdenGeneral(item.IncrementId, orderCceth, token);
                                    if (result.ResultType == ResultTypes.Error)
                                    {
                                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                            ": Error_Actualizando Status complete para item: " + item.IncrementId,
                                            true);
                                    }
                                    else
                                    {
                                        if (PedidosActualizados.ContainsKey(item.IncrementId))
                                        {
                                            PedidosActualizados[item.IncrementId] = orderCceth.Status;
                                        }
                                        else
                                        {
                                            PedidosActualizados.Add(item.IncrementId, orderCceth.Status);
                                        }
                                    }
                                }
                            }
                        }

                        resultOrden = GetOrdersStatus(7, cedis.cedi.ToString(), ruta.ruta, fecha);
                        if (resultOrden.ResultType == ResultTypes.Success)
                        {
                            foreach (var item in resultOrden.Objeto)
                            {
                                var orderCceth = new OrderCceth
                                {
                                    Comment = "",
                                    NotifyCustomer = "1",
                                    Status = "canceled"
                                };
                                if (!PedidosActualizados.ContainsKey(item.IncrementId) ||
                                    PedidosActualizados[item.IncrementId] != orderCceth.Status)
                                {
                                    var result = PutOrdenGeneral(item.IncrementId, orderCceth, token);
                                    if (result.ResultType == ResultTypes.Error)
                                    {
                                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                            ": Error_Actualizando Status canceled para item: " + item.IncrementId,
                                            true);
                                    }
                                    else
                                    {
                                        if (PedidosActualizados.ContainsKey(item.IncrementId))
                                        {
                                            PedidosActualizados[item.IncrementId] = orderCceth.Status;
                                        }
                                        else
                                        {
                                            PedidosActualizados.Add(item.IncrementId, orderCceth.Status);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (AppSettings()["OrderStatusLog"].ToUpper() != "NOLOG")
                {
                    using (StreamWriter _fs =
                        new StreamWriter(AppSettings()["RutaFisicaLog"] + "\\" + AppSettings()["OrderStatusLog"],
                            false))
                    {
                        _fs.Write(JsonConvert.SerializeObject(PedidosActualizados));
                    }
                }
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""), true);
            }
        }

        public static Result<string> PutOrdenGeneral(string incrementeId, OrderCceth orderCceth, string token)
        {
            var result = new Result<string>();
            try
            {
                return OrdenesHelper.PutOrderGeneral(AppSettings()["URL_COCACOLA_API"] + ApiCocaColaCnx.PutStatusOrders(incrementeId),
                    int.Parse(AppSettings()["TimeOutCnx"]), orderCceth, token, Metodos.Seguridad_Descifrar(AppSettings()["X_KO_CLIENT_ID"]).Objeto, Metodos.Seguridad_Descifrar(AppSettings()["X_KO_CLIENT_SECRET"]).Objeto);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_InventoryCtrl.TrupInventory: " +
                    ex.Message + "." + Environment.NewLine + ex.StackTrace + "." + Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "." + Environment.NewLine + ex.StackTrace + "." + Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        private static string ConvertOrderGeneral(string json)
        {
            var array = JArray.Parse(json);
            var listOrder = (from key in array select key.First() into temp select temp.First() into item select JsonConvert.DeserializeObject<OrderGeneralJson>(item.ToString())).ToList();
            return CreateJson(listOrder);
        }

        private static string CreateJson(IEnumerable<OrderGeneralJson> listOrder)
        {
            var json = new JArray();

            foreach (var key in listOrder)
            {
                var row = JObject.FromObject(key);
                json.Add(row);
            }
            return json.ToString();
        }

        #endregion

        #region Ordenes PORTAL

        public static Result<List<OrderPostResult>> PostOrdenGeneral(List<OrderGeneralJson> ordenGeneralJsons)
        {
            var result = new Result<List<OrderPostResult>>();
            try
            {
                return OrdenesHelper.PostOrderGeneral(AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.PostOrderGeneral(),
                    int.Parse(AppSettings()["TimeOutCnx"]), ordenGeneralJsons);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_InventoryCtrl.TrupInventory: " +
                    ex.Message + "." + Environment.NewLine + ex.StackTrace + "." + Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "." + Environment.NewLine + ex.StackTrace + "." + Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<List<Delivery>> GetOrdersStatus(int status,string cedi, string ruta, string fecha="")
        {
            var result = new Result<List<Delivery>>();
            try
            {
                return OrdenesHelper.GetOrdersStatus(AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.GetOrderS(status,cedi,ruta,fecha),
                    int.Parse(AppSettings()["TimeOutCnx"]));
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_InventoryCtrl.TrupInventory: " +
                    ex.Message + "." + Environment.NewLine + ex.StackTrace + "." + Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "." + Environment.NewLine + ex.StackTrace + "." + Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static void ProcesarPedidos(string token)
        {
            try
            {
                var resultOrden = GetOrdenGeneral(token);  //"i7f66uqhjtxt8ax8gu3x4sumcqlo1urc");
                if (resultOrden.ResultType == ResultTypes.Success)
                {
                    var result = PostOrdenGeneral(resultOrden.Objeto);
                    if (result.ResultType == ResultTypes.Success && result.Objeto.Count > 0)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], Environment.NewLine+DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Se reportaron los siguientes problemas al publicar pedidos en Intermedia -", true);
                        foreach (var mensaje in result.Objeto)
                        {
                            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], "    -> "+mensaje.Entity_Id+" : "+mensaje.Message, true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""), true);
            }
        }

        #endregion
    }
}
