﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using CCETH091703.Base;
using CCETH091703.Model.BR;
using CCETH091703.Model.DataAccess;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;

namespace CCETH091703.Model.Services
{
    public static class TokenCtrl
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        public static Result<string> GetToken()
        {
            var result = new Result<string>();
            try
            {

                var helper = new TokenHelper();
                result = helper.GetToken(AppSettings()["URL_COCACOLA_API"] + ApiCocaColaCnx.PostToken(Metodos.Seguridad_Descifrar(AppSettings()["username"]).Objeto, Metodos.Seguridad_Descifrar(AppSettings()["password"]).Objeto),
                    int.Parse(AppSettings()["TimeOutCnx"]),"", Metodos.Seguridad_Descifrar(AppSettings()["X_KO_CLIENT_ID"]).Objeto, Metodos.Seguridad_Descifrar(AppSettings()["X_KO_CLIENT_SECRET"]).Objeto);
                if (result.ResultType != ResultTypes.Success)
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) + "Error al obtener token: " + result.ResultType +
                        "_TokenCtrl.GetToken: " + result.Mensaje + "."+Environment.NewLine, true);
                }
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_TokenCtrl.GetToken: " + ex.Message +
                    "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }
    }
}