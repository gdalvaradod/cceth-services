﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;

namespace CCETH091703.Model.BR
{

    //#region Métodos ant

    ///// <summary>
    ///// Escribir registro en el archivo log del Servicio de Windows
    ///// </summary>
    ///// <param name="proyecto">El proyecto que genera el log</param>
    ///// <param name="mensaje">Actividad a registrar</param>
    ///// <param name="esDiario"></param>
    ///// <returns>Indica si fue exitoso</returns>
    //public static bool EscribirLog(string proyecto, string mensaje, bool esDiario = false) {
    //    try
    //    {
    //        string logArchivo;
    //        if (!esDiario)
    //            logArchivo = RutaRaiz + "\\Log\\Log_" + proyecto + ".csv";
    //        else
    //            logArchivo = RutaRaiz + "\\Log\\Log_" + proyecto + "_" + DateTime.Today.ToString("yyyy.MM.dd") + ".csv";

    //        System.IO.StreamWriter swLog;
    //        if (!System.IO.File.Exists(logArchivo))
    //            swLog = new System.IO.StreamWriter(logArchivo);
    //        else
    //            swLog = System.IO.File.AppendText(logArchivo);
    //        swLog.WriteLine(mensaje);
    //        swLog.Close();
    //        return true;
    //    } catch {
    //        return false;
    //    }
    //}
    //#endregion

    public class LogServicioBr
    {
        #region Atributos

        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        private static readonly string
            RutaRaiz = AppSettings()[
                "RutaFisicaLog"]; 

        #endregion

        #region Métodos

        /// <summary>
        /// Escribir registro en el archivo log del Servicio de Windows
        /// </summary>
        /// <param name="proyecto">El proyecto que genera el log</param>
        /// <param name="mensaje">Actividad a registrar</param>
        /// <param name="esDiario"></param>
        /// <returns>Indica si fue exitoso</returns>
        public static bool EscribirLog(string proyecto, string mensaje, bool esDiario = false)
        {
            try
            {
                if (proyecto.ToUpper() == "NOLOG") return true;
                string logArchivo;
                if (!esDiario)
                    logArchivo = RutaRaiz + "\\Log_" + proyecto + ".txt";
                else
                    logArchivo = RutaRaiz + "\\Log_" + proyecto + "_" + DateTime.Today.ToString("yyyy.MM.dd") + ".txt";

                if (!Directory.Exists(RutaRaiz))
                {
                    Directory.CreateDirectory(RutaRaiz);
                }

                var swLog = !File.Exists(logArchivo) ? new StreamWriter(logArchivo) : File.AppendText(logArchivo);
                lock (swLog)
                {
                    swLog.WriteLine(mensaje);
                    swLog.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return false;
            }
        }

        #endregion
    }


}

