﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using CCETH091703.Model.BR;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;

namespace CCETH091703.Model.DataAccess
{
    public class TokenHelper
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        public Result<string> GetToken(string urlApi, int timeOut, string json, string clienteId, string clientSecret)
        {
            var result = new Result<string>();
            try
            {
                return Metodos.Conexion_Post<string>(urlApi, timeOut, json, "", clienteId, clientSecret);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + "No se pudo obtener el token del Api_CCETH: "+Environment.NewLine+"Error_TokenHelper.GetToken: " + ex.Message +
                    "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.ResultType = ResultTypes.Error;
                result.Objeto = null;
                result.Mensaje = ex.Message;
            }
            return result;
        }
    }
}