﻿using System;
using System.Collections.Generic;
using CCETH091703.Model.Objects.BO;
using Newtonsoft.Json;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;

namespace CCETH091703.Model.DataAccess
{
    public class OrdenesHelper
    {
        public static Result<List<Delivery>> GetOrdersStatus(string urlApi, int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<List<Delivery>>(urlApi, timeOut);
            }
            catch (Exception e)
            {
                var result = new Result<List<Delivery>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public static Result<List<CedisRutas>> GetCedisRutas(string urlApi, int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<List<CedisRutas>>(urlApi, timeOut);
            }
            catch (Exception e)
            {
                var result = new Result<List<CedisRutas>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public static Result<List<object>> GetOrderGeneral(string urlApi, int timeOut, string token, string clientId, string clientSecret)
        {
            try
            {
                return Metodos.Conexion_Get<List<object>>(urlApi, timeOut, token, clientId, clientSecret);
            }
            catch (Exception e)
            {
                var result = new Result<List<object>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public static Result<List<OrderPostResult>> PostOrderGeneral(string urlApi, int timeOut, List<OrderGeneralJson> ordenGeneralJsons)
        {
            try
            {
                var cad = JsonConvert.SerializeObject(ordenGeneralJsons);
                return Metodos.Conexion_Post<List<OrderPostResult>>(urlApi, timeOut, JsonConvert.SerializeObject(ordenGeneralJsons));
            }
            catch (Exception e)
            {
                var result = new Result<List<OrderPostResult>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public static Result<string> PutOrderGeneral(string urlApi, int timeOut, OrderCceth json, string token, string clientId, string clientSecret)
        {
            try
            {
                return Metodos.Conexion_Put<string>(urlApi, timeOut, JsonConvert.SerializeObject(json), token, clientId,
                    clientSecret);
            }
            catch (Exception e)
            {
                var result = new Result<string>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }
    }
}
