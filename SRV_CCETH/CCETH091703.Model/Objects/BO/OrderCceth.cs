﻿using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class OrderCceth
    {
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("comment")]
        public string Comment { get; set; }
        [JsonProperty("notify_customer")]
        public string NotifyCustomer { get; set; }
    }
}
