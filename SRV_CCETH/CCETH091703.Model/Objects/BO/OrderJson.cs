﻿using System.Collections.Generic;

namespace CCETH091703.Model.Objects.BO
{
    public class OrderJson
    {
        //[JsonProperty("439")]
        public List<OrderGeneralJson> Order{ get; set; }
    }
}
