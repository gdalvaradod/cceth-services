﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class CedisRutas
    {
        public int cedi { get; set; }
        public List<Ruta> Destinos { get; set; }

    }

    public class Ruta
    {
        [JsonProperty("RouteCodeOpecd")]
        public string ruta{ get; set; }
    }
}
