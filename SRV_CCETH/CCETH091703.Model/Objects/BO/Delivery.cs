﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class Delivery
    {
        [JsonProperty("OPeID")]
        public string OpeId { get; set; }

        [JsonProperty("entity_id")]
        public int EntityId { get; set; }

        [JsonProperty("increment_id")]
        public string IncrementId { get; set; }

        [JsonProperty("inventoryid")]
        public string InventoryId { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("Bottler_Data")]
        public BottlerDataJson BottlerData { get; set; }

        [JsonProperty("addresses")]
        public List<Address> Addresses { get; set; }

        [JsonProperty("order_items")]
        public List<OrderItem> OrderItems { get; set; }

        public class OrderItem
        {
            public OrderItem(int itemid, decimal qty, int wbcid, int prodttype)
            {
                ItemId = itemid;
                QtyOrdered = qty;
                WbcId = wbcid;
                Tipo = prodttype;
            }
            [JsonProperty("item_id")]
            public int ItemId { get; set; }
            [JsonProperty("qty_ordered")]
            public decimal QtyOrdered { get; set; }
            [JsonProperty("WBCId")]
            public int WbcId { get; set; }
            [JsonProperty("PRODUCTTYPE")]
            public int Tipo { get; set; }
        }

        public class Address
        {
            public Address(string custid, int wanabill, string wbcid)
            {
                CustomerId = custid;
                WantBill = wanabill;
                WbcId = wbcid;
            }

            [JsonProperty("customer_id")]
            public string CustomerId { get; set; }

            [JsonProperty("want_bill")]
            public int WantBill { get; set; }

            public string WbcId { get; set; }
        }
    }
}
