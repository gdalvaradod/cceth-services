﻿using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class OrderItemJson
    {
        [JsonProperty("item_id")]
        public int ItemId { get; set; }

        [JsonProperty("parent_item_id")]
        public int? ParentItemId { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("qty_canceled")]
        public decimal QtyCanceled { get; set; }

        [JsonProperty("qty_invoiced")]
        public decimal QtyInvoiced { get; set; }

        [JsonProperty("qty_ordered")]
        public decimal QtyOrdered { get; set; }

        [JsonProperty("qty_refunded")]
        public decimal QtyRefunded { get; set; }

        [JsonProperty("qty_shipped")]
        public decimal QtyShipped { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("original_price")]
        public decimal OriginalPrice { get; set; }

        [JsonProperty("discount_amount")]
        public decimal DiscountAmount { get; set; }

        [JsonProperty("row_total")]
        public decimal RowTotal { get; set; }

        [JsonProperty("price_incl_tax")]
        public decimal? PriceInclTax { get; set; }

        [JsonProperty("row_total_incl_tax")]
        public decimal? RowTotalInclTax { get; set; }

        [JsonProperty("applied_rule_ids")]
        public int? AppliedRuleIds { get; set; }
    }
}
