﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCETH091703.Model.Objects.BO
{
    public class OrderPostResult
    {
        public int Entity_Id { get; set; }
        public string Message { get; set; }
    }
}
