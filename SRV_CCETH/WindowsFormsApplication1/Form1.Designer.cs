﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ejecutarbtn = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.btn_actualizarPedidos = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ejecutarbtn
            // 
            this.ejecutarbtn.Location = new System.Drawing.Point(44, 226);
            this.ejecutarbtn.Name = "ejecutarbtn";
            this.ejecutarbtn.Size = new System.Drawing.Size(99, 23);
            this.ejecutarbtn.TabIndex = 0;
            this.ejecutarbtn.Text = "ObtenerPedidos";
            this.ejecutarbtn.UseVisualStyleBackColor = true;
            this.ejecutarbtn.Click += new System.EventHandler(this.ejecutarbtn_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(0, 0);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(282, 220);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // btn_actualizarPedidos
            // 
            this.btn_actualizarPedidos.Location = new System.Drawing.Point(149, 226);
            this.btn_actualizarPedidos.Name = "btn_actualizarPedidos";
            this.btn_actualizarPedidos.Size = new System.Drawing.Size(123, 23);
            this.btn_actualizarPedidos.TabIndex = 2;
            this.btn_actualizarPedidos.Text = "ActualizarPedidos";
            this.btn_actualizarPedidos.UseVisualStyleBackColor = true;
            this.btn_actualizarPedidos.Click += new System.EventHandler(this.btn_actualizarPedidos_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btn_actualizarPedidos);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.ejecutarbtn);
            this.Name = "Form1";
            this.Text = "Servicio CCETH";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ejecutarbtn;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button btn_actualizarPedidos;
    }
}

