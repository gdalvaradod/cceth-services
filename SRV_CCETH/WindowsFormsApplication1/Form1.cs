﻿using CCETH091703.Model.BR;
using CCETH091703.Model.Services;
using GrupoGitLib.Model.Objects.Enum;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.Windows.Forms;
using GrupoGitLib;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            IntegracionCtlr.ActualizaConfig();

            LogServicioBr.EscribirLog(AppSettings()["UsandoApisLog"],
                DateTime.Now.ToString(CultureInfo.InvariantCulture) + Environment.NewLine +
                ": Api_Integracion=" + AppSettings()["URL_INTEGRACION_API"] + Environment.NewLine +
                ": Api_Cocacola=" + AppSettings()["URL_COCACOLA_API"] + Environment.NewLine);

            if (!AppSettings()["X_KO_CLIENT_ID"].StartsWith("1_"))
            {
                AddUpdateAppSettings("X_KO_CLIENT_ID",
                    "1_" + Metodos.Seguridad_Cifrar(AppSettings()["X_KO_CLIENT_ID"]).Objeto);
            }
            if (!AppSettings()["X_KO_CLIENT_SECRET"].StartsWith("1_"))
            {
                AddUpdateAppSettings("X_KO_CLIENT_SECRET",
                    "1_" + Metodos.Seguridad_Cifrar(AppSettings()["X_KO_CLIENT_SECRET"]).Objeto);
            }
            if (!AppSettings()["username"].StartsWith("1_"))
            {
                AddUpdateAppSettings("username", "1_" + Metodos.Seguridad_Cifrar(AppSettings()["username"]).Objeto);
            }
            if (!AppSettings()["password"].StartsWith("1_"))
            {
                AddUpdateAppSettings("password", "1_" + Metodos.Seguridad_Cifrar(AppSettings()["password"]).Objeto);
            }
        }

        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        private void ejecutarbtn_Click(object sender, EventArgs e)
        {
            try
            {
                var resulttoken = TokenCtrl.GetToken();
                if (resulttoken.ResultType != ResultTypes.Success)
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + "No se obtuvo Token de API_CCETH",
                        true);
                    return;
                }
                var token = resulttoken.Objeto;
                if (!string.IsNullOrEmpty(token))
                {
                    OrdenesCtrl.ProcesarPedidos(token);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }

            #region transferido a OrdenCtrl
            //try
            //{
            //    if (_token.Equals(""))
            //    {
            //        var resulttoken = TokenCtrl.GetToken();
            //        if (resulttoken.ResultType != ResultTypes.Success)
            //        {
            //            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + "No se obtuvo Token de API_CCETH", true);
            //            _token = "";
            //            return;
            //        }
            //        _token = resulttoken.Objeto;
            //    }

            //    var resultOrden = OrdenesCtrl.GetOrdenGeneral(_token);  //"i7f66uqhjtxt8ax8gu3x4sumcqlo1urc");
            //    if (resultOrden.ResultType == ResultTypes.Success)
            //    {
            //        var result = OrdenesCtrl.PostOrdenGeneral(resultOrden.Objeto);
            //        richTextBox1.Text = result.Mensaje;
            //    }
            //    richTextBox1.Text = resultOrden.Mensaje;
            //}
            //catch (Exception ex)
            //{
            //    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""), true);
            //}
            #endregion
        }

        private void btn_actualizarPedidos_Click(object sender, EventArgs e)
        {
            try
            {
                var resulttoken = TokenCtrl.GetToken();
                if (resulttoken.ResultType != ResultTypes.Success)
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + "No se obtuvo Token de API_CCETH",
                        true);
                    return;
                }
                var token = resulttoken.Objeto;

                if (!string.IsNullOrEmpty(token))
                {
                    OrdenesCtrl.ActualizaStatusCoca(token);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }

            #region Movido_a_Estadoctrl
            //try
            //{
            //    if (_token.Equals(""))
            //    {
            //        var resulttoken = TokenCtrl.GetToken();
            //        if (resulttoken.ResultType != ResultTypes.Success)
            //        {
            //            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + "No se obtuvo Token de API_CCETH", true);
            //            _token = "";
            //            return;
            //        }
            //        _token = resulttoken.Objeto;
            //    }

            //    var resultOrden = OrdenesCtrl.GetOrdersStatus(3);
            //    if (resultOrden.ResultType != ResultTypes.Success) return;
            //    foreach (var item in resultOrden.Objeto)
            //    {
            //        var orderCceth = new OrderCceth
            //        {
            //            Comment = "",
            //            NotifyCustomer = "1",
            //            Status = "holded"
            //        };
            //        var result = OrdenesCtrl.PutOrdenGeneral(item.IncrementId, orderCceth, _token);
            //        richTextBox1.Text += result.Mensaje;
            //    }

            //    resultOrden = OrdenesCtrl.GetOrdersStatus(4);
            //    if (resultOrden.ResultType != ResultTypes.Success) return;
            //    foreach (var item in resultOrden.Objeto)
            //    {
            //        var orderCceth = new OrderCceth
            //        {
            //            Comment = "",
            //            NotifyCustomer = "1",
            //            Status = "shipped"
            //        };
            //        var result = OrdenesCtrl.PutOrdenGeneral(item.IncrementId, orderCceth, _token);
            //        richTextBox1.Text += result.Mensaje;
            //    }
            //    resultOrden = OrdenesCtrl.GetOrdersStatus(5);
            //    if (resultOrden.ResultType != ResultTypes.Success) return;
            //    foreach (var item in resultOrden.Objeto)
            //    {
            //        var orderCceth = new OrderCceth
            //        {
            //            Comment = "",
            //            NotifyCustomer = "1",
            //            Status = "reprogrammed"
            //        };
            //        var result = OrdenesCtrl.PutOrdenGeneral(item.IncrementId, orderCceth, _token);
            //        richTextBox1.Text += result.Mensaje;
            //    }
            //    resultOrden = OrdenesCtrl.GetOrdersStatus(6);
            //    if (resultOrden.ResultType != ResultTypes.Success) return;
            //    foreach (var item in resultOrden.Objeto)
            //    {
            //        var orderCceth = new OrderCceth
            //        {
            //            Comment = "",
            //            NotifyCustomer = "1",
            //            Status = "complete"
            //        };
            //        var result = OrdenesCtrl.PutOrdenGeneral(item.IncrementId, orderCceth, _token);
            //        richTextBox1.Text += result.Mensaje;
            //    }
            //    resultOrden = OrdenesCtrl.GetOrdersStatus(7);
            //    if (resultOrden.ResultType != ResultTypes.Success) return;
            //    foreach (var item in resultOrden.Objeto)
            //    {
            //        var orderCceth = new OrderCceth
            //        {
            //            Comment = "",
            //            NotifyCustomer = "1",
            //            Status = "canceled"
            //        };
            //        var result = OrdenesCtrl.PutOrdenGeneral(item.IncrementId, orderCceth, _token);
            //        richTextBox1.Text += result.Mensaje;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""), true);
            //}
            #endregion
        }

        private static void AddUpdateAppSettings(string key, string value)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {
                    settings[key].Value = value;
                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException e)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_AddUpdateAppSettings",
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + e.Message.Replace(",", ""), true);
                Console.WriteLine(e.Message);
            }
        }
    }
}
