﻿namespace CCETH091703.Base
{
    public class ApiCocaColaCnx
    {
        //Authenticate
        public static string PostToken(string username, string password)
        {
            return "/integration/admin/token?username=" + username +"&password=" + password;
        }

        //GetOrders
        public static string GetOrders()
        {
            return "/bottler/orders";
        }

        //PutStatusOrders
        public static string PutStatusOrders(string incrementId)
        {
            return "/bottler/orders/" + incrementId;
        }
    }
}
