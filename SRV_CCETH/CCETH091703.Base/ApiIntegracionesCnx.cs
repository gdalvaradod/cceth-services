﻿using System;
using System.Text;

namespace CCETH091703.Base
{
    public class ApiIntegracionesCnx
    {
        public static string PostOrderGeneral()
        {
            return "/api/OrdenCCTH/Productos";
        }

        public static string GetCedisRutas()
        {
            return "/api/OrdenWBC/CedisInfo";
        }

        
        public static string GetOrdersWbc(int status)
        {
            return "/api/OrdenOpeCd/GetOrders/" + status;
        }

        public static string GetOrderS(int status, string cedi, string ruta, string fecha="")
        {
            StringBuilder filtro = new StringBuilder("/api/OrdenOpeCd/GetOrders?Status=" + status + "&cedis=" + cedi + "&ruta=" + ruta);
            fecha = fecha ?? "";
            filtro.Append((fecha != "" ? "&deliveryDate=" + fecha : ""));
            return filtro.ToString();
        }
    }
}
