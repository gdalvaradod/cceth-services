/****** Object:  Table [dbo].[BIT_PriceList]    Script Date: 15/11/2018 10:44:09 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BIT_PriceList](
	[priceListId] [int] NOT NULL,
	[branchId] [int] NULL,
	[cedisId] [int] NULL,
	[priceListCode] [varchar](max) NULL,
	[priceListName] [varchar](max) NULL,
	[priceListMaster] [bit] NULL,
	[priceListValidityStart] [datetime] NULL,
	[priceListValidityEnd] [datetime] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_BIT_PriceList] PRIMARY KEY CLUSTERED 
(
	[priceListId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BIT_PriceList_Detail]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BIT_PriceList_Detail](
	[priceListId] [int] NULL,
	[priceListCode] [varchar](max) NULL,
	[priceListProductId] [int] NULL,
	[priceListProductCode] [int] NULL,
	[price] [real] NULL,
	[priceBase] [real] NULL,
	[discount] [real] NULL,
	[discountPercent] [real] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedOn] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CAT_BottlerData]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_BottlerData](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BOT_RouteId] [int] NOT NULL,
	[BOT_Website] [varchar](25) NULL,
	[BOT_Store] [varchar](25) NULL,
	[BOT_Storeview] [varchar](25) NULL,
	[BOT_Postalcode] [varchar](6) NOT NULL,
	[BOT_Deliveryco] [varchar](16) NULL,
	[BOT_Status] [int] NOT NULL,
	[BOT_Settlement] [varchar](100) NULL,
	[BOT_DeliveryDate] [datetime] NULL,
	[BOT_RouteCode] [varchar](255) NULL,
	[BOT_OrderGeneralId] [int] NOT NULL,
	[BOT_CediName] [varchar](100) NULL,
	[BOT_Trip] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CAT_Companies]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_Companies](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CCO_WBCId] [int] NOT NULL,
	[CCO_Code] [varchar](255) NOT NULL,
	[CCO_Name] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CAT_Customers]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_Customers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CCL_CRMId] [varchar](255) NULL,
	[CCL_StatusConfirmation] [int] NULL,
	[CCL_Name] [varchar](100) NULL,
	[CCL_Cuc] [int] NULL,
	[CCL_BarCode] [varchar](30) NULL,
	[CCL_PriceList] [bigint] NULL,
	[CCL_Sequence] [int] NULL,
	[CCL_DistributionCenter] [int] NULL,
	[CCL_RouteCode] [int] NULL,
	[CCL_Manager] [int] NULL,
	[CCL_Street] [varchar](30) NULL,
	[CCL_IndoorNumber] [varchar](30) NULL,
	[CCL_OutdoorNumber] [varchar](30) NULL,
	[CCL_Intersection1] [varchar](30) NULL,
	[CCL_Intersection2] [varchar](30) NULL,
	[CCL_Country] [int] NULL,
	[CCL_State] [int] NULL,
	[CCL_City] [int] NULL,
	[CCL_Neighborhood] [int] NULL,
	[CCL_ClientType] [bit] NULL,
	[CCL_CustomerVarious] [bit] NULL,
	[CCL_CustomerType] [varchar](30) NULL,
	[CCL_Email] [varchar](60) NULL,
	[CCL_PostCode] [varchar](30) NULL,
	[CCL_Telephone] [varchar](30) NULL,
	[CCL_Latitude] [varchar](30) NULL,
	[CCL_Longitude] [varchar](30) NULL,
	[CCL_CreatedOn] [datetime] NULL,
	[CCL_ModifiedOn] [datetime] NULL,
	[CCL_Contact] [varchar](100) NULL,
	[CCL_Description] [varchar](300) NULL,
	[CCL_AddressType] [varchar](30) NULL,
	[CCL_PhysicalAddress] [varchar](150) NULL,
	[CCL_WBCId] [varchar](255) NULL,
	[CCL_CRMCreateOn] [varchar](60) NULL,
	[CCL_CCTHId] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CAT_OrderAddresses]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_OrderAddresses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ORE_EntityId] [int] NOT NULL,
	[ORE_ParentId] [int] NOT NULL,
	[ORE_CustomerAddressId] [int] NULL,
	[ORE_QuoteAddressId] [int] NULL,
	[ORE_RegionId] [int] NULL,
	[ORE_Fax] [int] NULL,
	[ORE_Region] [varchar](255) NULL,
	[ORE_Postcode] [varchar](255) NULL,
	[ORE_Lastname] [varchar](255) NULL,
	[ORE_Street] [varchar](255) NULL,
	[ORE_City] [varchar](255) NULL,
	[ORE_Email] [varchar](255) NULL,
	[ORE_Telephone] [varchar](255) NULL,
	[ORE_CountryId] [varchar](2) NULL,
	[ORE_Firstname] [varchar](255) NULL,
	[ORE_AddressType] [varchar](255) NOT NULL,
	[ORE_Prefix] [varchar](255) NULL,
	[ORE_Middlename] [varchar](255) NULL,
	[ORE_Suffix] [varchar](255) NULL,
	[ORE_Company] [varchar](255) NULL,
	[ORE_VatId] [text] NULL,
	[ORE_VatIsValid] [smallint] NULL,
	[ORE_VatRequestId] [text] NULL,
	[ORE_VatRequestDate] [varchar](255) NULL,
	[ORE_VatRequestSuccess] [smallint] NULL,
	[ORE_GiftregistryItemId] [int] NULL,
	[ORE_WantBill] [smallint] NULL,
	[ORE_Rfc] [varchar](255) NULL,
	[ORE_BillingName] [varchar](255) NULL,
	[ORE_Number] [varchar](255) NULL,
	[ORE_NumberInt] [varchar](255) NULL,
	[ORE_Neighborhood] [varchar](255) NULL,
	[ORE_Municipality] [varchar](255) NULL,
	[ORE_References] [text] NULL,
	[ORE_Alias] [varchar](255) NULL,
	[ORE_PaymentMethod] [text] NULL,
	[ORE_OrderGeneralId] [int] NOT NULL,
	[ORE_StatusConfirmacion] [int] NULL,
	[ORE_CustomerId] [varchar](255) NULL,
	[ORE_CRMId] [varchar](255) NULL,
	[ORE_BarCode] [varchar](30) NULL,
	[ORE_address_flag] [varchar](3) NULL,
	[ORE_Billing_district] [varchar](60) NULL,
	[ORE_Billing_neighborhood] [varchar](60) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CAT_OrderComments]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_OrderComments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OCM_EntityId] [int] NOT NULL,
	[OCM_ParentId] [int] NOT NULL,
	[OCM_IsCustomerNotified] [int] NULL,
	[OCM_IsVisibleOnFront] [smallint] NULL,
	[OCM_Comment] [text] NULL,
	[OCM_Status] [varchar](32) NULL,
	[OCM_CreatedAt] [varchar](255) NULL,
	[OCM_EntityName] [varchar](32) NULL,
	[OCM_StoreId] [int] NULL,
	[OCM_OrderGeneralId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CAT_OrderGeneral]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_OrderGeneral](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ORD_EntityId] [int] NOT NULL,
	[ORD_Status] [varchar](32) NOT NULL,
	[ORD_CouponCode] [varchar](255) NULL,
	[ORD_CustomerId] [int] NULL,
	[ORD_ShippingDescription] [varchar](255) NULL,
	[ORD_BaseDiscountAmount] [decimal](12, 4) NOT NULL,
	[ORD_BaseGrandTotal] [decimal](12, 4) NOT NULL,
	[ORD_BaseShippingAmount] [decimal](12, 4) NOT NULL,
	[ORD_BaseShippingTaxAmount] [decimal](12, 4) NOT NULL,
	[ORD_BaseSubtotal] [decimal](12, 4) NOT NULL,
	[ORD_BaseTaxAmount] [decimal](12, 4) NOT NULL,
	[ORD_BaseTotalPaid] [decimal](12, 4) NULL,
	[ORD_BaseTotalRefunded] [decimal](12, 4) NULL,
	[ORD_DiscountAmount] [decimal](12, 4) NOT NULL,
	[ORD_GrandTotal] [decimal](12, 4) NOT NULL,
	[ORD_ShippingAmount] [decimal](12, 4) NULL,
	[ORD_ShippingTaxAmount] [decimal](12, 4) NULL,
	[ORD_StoreToOrderRate] [decimal](12, 4) NULL,
	[ORD_Subtotal] [decimal](12, 4) NOT NULL,
	[ORD_TaxAmount] [decimal](12, 4) NOT NULL,
	[ORD_TotalPaid] [decimal](12, 4) NULL,
	[ORD_TotalQtyOrdered] [decimal](12, 4) NULL,
	[ORD_TotalRefunded] [decimal](12, 4) NULL,
	[ORD_BaseShippingDiscountAmount] [decimal](12, 4) NOT NULL,
	[ORD_BaseSubtotalInclTax] [decimal](12, 4) NOT NULL,
	[ORD_BaseTotalDue] [decimal](12, 4) NOT NULL,
	[ORD_ShippingDiscountAmount] [decimal](12, 4) NOT NULL,
	[ORD_SubtotalInclTax] [decimal](12, 4) NOT NULL,
	[ORD_TotalDue] [decimal](12, 4) NOT NULL,
	[ORD_IncrementId] [varchar](50) NOT NULL,
	[ORD_AppliedRuleIds] [varchar](255) NULL,
	[ORD_BaseCurrencyCode] [varchar](3) NOT NULL,
	[ORD_CustomerEmail] [varchar](255) NOT NULL,
	[ORD_DiscountDescription] [varchar](255) NULL,
	[ORD_RemoteIp] [varchar](32) NULL,
	[ORD_StoreCurrencyCode] [varchar](3) NULL,
	[ORD_StoreName] [varchar](500) NULL,
	[ORD_CreatedAt] [text] NOT NULL,
	[ORD_TotalItemCount] [smallint] NOT NULL,
	[ORD_ShippingInclTax] [decimal](12, 4) NOT NULL,
	[ORD_BaseCustomerBalanceAmount] [decimal](12, 4) NULL,
	[ORD_CustomerBalanceAmount] [decimal](12, 4) NULL,
	[ORD_BaseGiftCardsAmount] [decimal](12, 4) NOT NULL,
	[ORD_GiftCardsAmount] [decimal](12, 4) NOT NULL,
	[ORD_RewardPointsBalance] [int] NULL,
	[ORD_BaseRewardCurrencyAmount] [decimal](12, 4) NULL,
	[ORD_RewardCurrencyAmount] [decimal](12, 4) NULL,
	[ORD_PaymentMethod] [varchar](255) NOT NULL,
	[ORD_OPECDId] [bigint] NULL,
	[ORD_Comments] [varchar](max) NULL,
	[ORD_StatusOrden] [varchar](30) NULL,
	[ORD_CreateFrom] [varchar](25) NULL,
	[ORD_CreateDateInter] [datetime] NOT NULL,
	[ORD_Method_title] [varchar](200) NULL,
	[ORD_Cc_name] [varchar](200) NULL,
	[ORD_Cc_las_4] [varchar](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CAT_OrderItems]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_OrderItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ORI_ItemId] [int] NOT NULL,
	[ORI_ParentItemId] [int] NOT NULL,
	[ORI_Sku] [varchar](255) NULL,
	[ORI_Name] [varchar](255) NULL,
	[ORI_QtyCanceled] [decimal](12, 4) NOT NULL,
	[ORI_QtyInvoiced] [decimal](12, 4) NOT NULL,
	[ORI_QtyOrdered] [decimal](12, 4) NOT NULL,
	[ORI_QtyRefunded] [decimal](12, 4) NOT NULL,
	[ORI_QtyShipped] [decimal](12, 4) NOT NULL,
	[ORI_Price] [decimal](12, 4) NOT NULL,
	[ORI_OriginalPrice] [decimal](12, 4) NOT NULL,
	[ORI_DiscountAmount] [decimal](12, 4) NOT NULL,
	[ORI_RowTotal] [decimal](12, 4) NOT NULL,
	[ORI_PriceInclTax] [decimal](12, 4) NULL,
	[ORI_RowTotalInclTax] [decimal](12, 4) NULL,
	[ORI_OrderGeneralId] [int] NOT NULL,
	[ORI_InterId] [int] NOT NULL,
	[ORI_Applied_rule_ids] [varchar](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CAT_Process]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_Process](
	[CPR_Id] [int] IDENTITY(1,1) NOT NULL,
	[CPR_Description] [varchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[CPR_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CAT_Products]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_Products](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CPD_CodigoProducto] [varchar](255) NOT NULL,
	[CPD_Nombre] [varchar](255) NOT NULL,
	[CPD_Ruta] [int] NOT NULL,
	[CPD_Cedis] [int] NOT NULL,
	[CPD_Status] [int] NULL,
	[CPD_WBCId] [int] NULL,
	[CPD_Tipo] [int] NULL,
	[CPD_CreateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CAT_Users]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CUS_User] [varchar](255) NOT NULL,
	[CUS_Pass] [varchar](255) NOT NULL,
	[CUS_Enable] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CAT_Visits]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_Visits](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CTV_CustomerId] [int] NOT NULL,
	[CTV_Monday] [bit] NOT NULL,
	[CTV_Tuesday] [bit] NOT NULL,
	[CTV_Wednesday] [bit] NOT NULL,
	[CTV_Thursday] [bit] NOT NULL,
	[CTV_Friday] [bit] NOT NULL,
	[CTV_Saturday] [bit] NOT NULL,
	[CTV_Sunday] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[CTV_CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CONF_Colonia]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONF_Colonia](
	[COD_Name] [nvarchar](255) NULL,
	[COD_OpeId] [float] NULL,
	[COD_EstadoOpeId] [float] NULL,
	[COD_MunicipioOpeId] [float] NULL,
	[COD_CodeCCTH] [varchar](255) NULL,
	[COD_CodeCRM] [varchar](255) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CONF_Destinos]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONF_Destinos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[COD_RouteCodeCCHT] [varchar](255) NULL,
	[COD_RouteCodeOpecd] [int] NOT NULL,
	[COD_Nombre] [varchar](255) NULL,
	[COD_Manager] [int] NULL,
	[COD_Cuc] [int] NULL,
	[COD_CedisRefId] [int] NOT NULL,
	[COD_Enable] [int] NULL,
	[COD_CucCCTH] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CONF_Estado]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONF_Estado](
	[COD_Name] [nvarchar](255) NULL,
	[COD_OpeId] [float] NULL,
	[COD_CodeCCTH] [varchar](255) NULL,
	[COD_CodeCRM] [varchar](255) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[COD_PaisOpeId] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CONF_Globales]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONF_Globales](
	[CGL_Servicio] [nchar](25) NOT NULL,
	[CGL_Tipo] [nchar](10) NOT NULL,
	[CGL_Key] [nchar](50) NOT NULL,
	[CGL_Value] [nchar](500) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CGL_Servicio] ASC,
	[CGL_Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CONF_Municipio]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONF_Municipio](
	[COD_Name] [nvarchar](255) NULL,
	[COD_OpeId] [float] NULL,
	[COD_EstadoOpeId] [float] NULL,
	[COD_CodeCCTH] [varchar](255) NULL,
	[COD_CodeCRM] [varchar](255) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CONF_Pais]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONF_Pais](
	[COD_Name] [nvarchar](255) NULL,
	[COD_OpeId] [float] NULL,
	[COD_CodeCCTH] [varchar](255) NULL,
	[COD_CodeCRM] [varchar](255) NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CONF_ProcesosAutomaticos]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONF_ProcesosAutomaticos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CPA_Nombre] [varchar](255) NOT NULL,
	[CPA_Sistema] [varchar](255) NOT NULL,
	[CPA_Hora] [varchar](30) NULL,
	[CPA_Cedis] [int] NOT NULL,
	[CPA_Modificado] [datetime] NOT NULL,
	[CPA_Status] [int] NULL,
	[CPA_Ruta] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CONF_Routes]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONF_Routes](
	[RouteId] [int] NULL,
	[CedisId] [int] NULL,
	[ZipCode] [int] NULL,
	[Neighborhood] [nvarchar](255) NULL,
	[DeliveryDays] [nvarchar](255) NULL,
	[Status] [bit] NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DET_Log]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DET_Log](
	[DLO_Id] [int] IDENTITY(1,1) NOT NULL,
	[DLO_LogId] [int] NOT NULL,
	[DLO_ProcessId] [int] NULL,
	[DLO_ErrorDescription] [varchar](600) NULL,
	[DLO_Success] [bit] NOT NULL,
	[DLO_CreatedOn] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[DLO_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DET_Promotions]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DET_Promotions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DEP_Product] [int] NOT NULL,
	[DEP_Quantity] [int] NOT NULL,
	[DEP_Type] [int] NOT NULL,
	[DEP_SaleId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DET_Sales]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DET_Sales](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DSS_Producto] [int] NULL,
	[DSS_Cantidad] [int] NULL,
	[DSS_CantidadCredito] [int] NULL,
	[DSS_SaleId] [int] NULL,
	[DSS_Precio] [decimal](19, 4) NOT NULL,
	[DSS_PrecioBase] [decimal](19, 4) NOT NULL,
	[DSS_Descuento] [decimal](19, 4) NOT NULL,
	[DSS_PorcentajeDescuento] [decimal](19, 4) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ENC_Log]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ENC_Log](
	[ELO_Id] [int] IDENTITY(1,1) NOT NULL,
	[ELO_DeliveryDate] [datetime] NULL,
	[ELO_CedisId] [int] NULL,
	[ELO_RouteId] [int] NULL,
	[ELO_Trip] [int] NULL,
	[ELO_ProcessId] [int] NULL,
	[ELO_ProcessStatus] [int] NULL,
	[ELO_CreatedOn] [datetime] NOT NULL,
	[ELO_ModifiedOn] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ELO_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ELO_DeliveryDate] ASC,
	[ELO_CedisId] ASC,
	[ELO_RouteId] ASC,
	[ELO_Trip] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ENC_Sales]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ENC_Sales](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ESS_Codope] [int] NULL,
	[ESS_CUC] [int] NULL,
	[ESS_WBCSaleId] [int] NULL,
	[ESS_Facturado] [int] NULL,
	[ESS_RazonDev] [text] NULL,
	[ESS_FechaCreacion] [datetime] NOT NULL,
	[ESS_CedisId] [int] NULL,
	[ESS_RouteCode] [int] NULL,
	[ESS_Trip] [smallint] NULL,
	[ESS_DeliveryDate] [datetime] NULL,
	[ESS_CustomerCCTH] [bit] NOT NULL,
	[ESS_Barcode] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ENC_Trips]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ENC_Trips](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TRI_TripId] [int] NOT NULL,
	[TRI_Route] [int] NOT NULL,
	[TRI_DeliveryDate] [datetime] NULL,
	[TRI_CedisIdOpeCd] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderItemPrice]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderItemPrice](
	[OrderId] [int] NOT NULL,
	[DeliveryDate] [smalldatetime] NOT NULL,
	[CustBarCode] [varchar](12) NOT NULL,
	[ItemId] [int] NOT NULL,
	[Price] [decimal](19, 4) NOT NULL,
	[BasePrice] [decimal](19, 4) NOT NULL,
	[Discount] [decimal](19, 4) NOT NULL,
	[DiscountPercent] [decimal](19, 4) NOT NULL,
	[CreatedAt] [smalldatetime] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[REF_Cedis]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[REF_Cedis](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[REC_CedisNameCCHT] [varchar](255) NULL,
	[REC_CedisIdOpecd] [int] NOT NULL,
	[REC_Nombre] [varchar](255) NULL,
	[REC_Enable] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[REF_ClosedJourneys]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[REF_ClosedJourneys](
	[CJ_CedisIdOpeCd] [int] NOT NULL,
	[CJ_Route] [int] NOT NULL,
	[CJ_DeliveryDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ClosedJourneys] PRIMARY KEY CLUSTERED 
(
	[CJ_CedisIdOpeCd] ASC,
	[CJ_Route] ASC,
	[CJ_DeliveryDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[REF_TripItems]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[REF_TripItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TIT_TripId] [int] NOT NULL,
	[TIT_ItemId] [varchar](30) NOT NULL,
	[TIT_Qty] [int] NOT NULL,
	[TIT_WbcId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[REF_UserCedis]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[REF_UserCedis](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[CedisId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RES_Customers]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RES_Customers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CCL_Firstname] [varchar](255) NULL,
	[CCL_Lastname] [varchar](255) NULL,
	[CCL_Postcode] [varchar](255) NULL,
	[CCL_Email] [varchar](255) NULL,
	[CCL_Telephone] [varchar](255) NULL,
	[CCL_AddressType] [varchar](255) NOT NULL,
	[CCL_CountryId] [varchar](2) NULL,
	[CCL_City] [varchar](255) NULL,
	[CCL_StatusConfirmacion] [int] NULL,
	[CCL_CRMId] [varchar](255) NULL,
	[CCL_RouteCode] [varchar](255) NULL,
	[CCL_Cuc] [int] NULL,
	[CCL_CedisCode] [varchar](1) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[CustItemPricesOpe]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[CustItemPricesOpe] AS

WITH PrevOrder AS
(
	SELECT CustOrder.Id AS OrderId,
		Address.CustBarCode AS CustBarCode,
		(CASE WHEN Address.WantBill = 1 THEN Address.CUC ELSE Route.COD_CucCCTH END) AS Cuc,
		CustOrder.ORD_BaseGrandTotal AS Total,
		CustOrder.ORD_BaseDiscountAmount AS GlobalDiscountAmount,
		CustOrder.ORD_TotalPaid AS TotalPaid,
		CustOrder.ORD_BaseSubtotal AS Subtotal,
		CustOrder.ORD_DiscountAmount AS OrderDiscountAmount,
		Bottler.BOT_DeliveryDate AS DeliveryDate,
		Address.WantBill,
		CAST(Bottler.BOT_RouteCode AS VARCHAR) AS RouteId,
		Cedis.REC_CedisIdOpecd AS IdCedis
	FROM dbo.CAT_OrderGeneral AS CustOrder
	JOIN dbo.CAT_BottlerData AS Bottler
		ON Bottler.BOT_OrderGeneralId = CustOrder.Id
	JOIN (
		SELECT DISTINCT
			ORE_BarCode AS CustBarCode, ORE_OrderGeneralId AS OrderId,
			ORE_CRMId AS CRMId, COALESCE(Customer.CCL_Cuc, 0) AS CUC, ORE_WantBill AS WantBill
		FROM dbo.CAT_OrderAddresses
		JOIN dbo.CAT_Customers AS Customer
			ON Customer.CCL_CRMId = CAT_OrderAddresses.ORE_CRMId
			AND Customer.CCL_CustomerType = 'CLIENTE_COCACOLA' /* Cliente coca-cola*/
	) AS Address
		ON Address.OrderId = CustOrder.Id
	JOIN dbo.CONF_Destinos AS Route
		ON Route.COD_RouteCodeOpecd = CAST(Bottler.BOT_RouteCode AS VARCHAR)
	JOIN dbo.REF_Cedis AS Cedis
		ON Cedis.REC_CedisNameCCHT = Bottler.BOT_CediName
	WHERE ORD_CreateFrom = 'COCACOLA' /* Provenientes del portal */
		AND ORD_StatusOrden = 'COMPLETADO' /* Ya surtidos (venta) */
)
, FinalPrevOrder AS ( 
	SELECT PrevOrder.IdCedis,
		PrevOrder.DeliveryDate,
		PrevOrder.OrderId,
		PrevOrder.Total,
		OrderDiscountAmount,
		--PrevOrder.CustBarCode,
		PrevOrder.Cuc,
		PrevDet.ItemId,
		PrevDet.Qty,
		PrevDet.ItemDiscount,
		PrevDet.BasePrice,
		PrevDet.Price, 
		CAST(PrevDet.ItemTotalWithDiscount/PrevDet.Qty AS DECIMAL(19,4)) AS ItemPriceWithDiscount,
		CAST(PrevDet.ItemTotalWithDiscount/PrevDet.GrandTotalItems AS DECIMAL(10,9)) AS ItemPercent,
		PrevDet.ItemTotal
	FROM PrevOrder AS PrevOrder
	JOIN (
		SELECT
			ORI_OrderGeneralId AS OrderId,
			ORI_Sku AS ItemId,
			ORI_Name AS Name,
			ORI_QtyOrdered AS Qty,
			ORI_Price AS Price,
			ORI_OriginalPrice AS BasePrice,
			ORI_DiscountAmount AS ItemDiscount,
			ORI_RowTotal AS ItemTotal,
			ORI_Applied_rule_ids AS RuleId,
			CAST((ORI_RowTotal - ORI_DiscountAmount) AS DECIMAL(19,4)) AS ItemTotalWithDiscount,
			SUM(CAST((ORI_RowTotal - ORI_DiscountAmount) AS DECIMAL(19,4)))	OVER (PARTITION BY ORI_OrderGeneralId) AS GrandTotalItems
		FROM dbo.CAT_OrderItems
	) AS PrevDet
		ON PrevOrder.OrderId = PrevDet.OrderId
)
, FinalNotGroupping AS (
	SELECT
		FinalOrder.IdCedis, 
		FinalOrder.DeliveryDate,
		FinalOrder.Cuc,
		--FinalOrder.CustBarCode,
		FinalOrder.ItemId,
		FinalOrder.Qty,  
		FinalOrder.BasePrice, 
		FinalOrder.ItemPercent*FinalOrder.Total AS NewItemTotal,
		FinalOrder.Total*FinalOrder.ItemPercent/FinalOrder.Qty AS NewPriceResp,
		BasePrice - FinalOrder.Total*FinalOrder.ItemPercent/FinalOrder.Qty AS NewDiscount
	FROM FinalPrevOrder AS FinalOrder
)
, FinalOrder AS (
	SELECT 
		IdCedis AS CedisId,
		DeliveryDate,
		Cuc,
		--CustBarCode,
		ItemId,
		CAST((ItemTotal/ItemQty) AS DECIMAL(19, 4)) AS Price, 
		CAST(ItemDiscount/ItemQty AS DECIMAL(19, 4)) AS Discount,
		CAST(ItemBasePrice/ItemQty AS DECIMAL(19, 4)) AS BasePrice,
		CAST(0 AS DECIMAL(19,4)) AS DiscountRate
	FROM (
		SELECT IdCedis,
			DeliveryDate,
			Cuc,
			--CustBarCode,
			ItemId,
			SUM(NewItemTotal) AS ItemTotal,
			SUM(NewDiscount*Qty) AS ItemDiscount, 
			SUM(BasePrice*Qty) AS ItemBasePrice,
			SUM(Qty) AS ItemQty
		FROM FinalNotGroupping
		GROUP BY IdCedis, DeliveryDate, Cuc, ItemId
		HAVING SUM(Qty) > 0
	) AS Prev
)
SELECT
	ROW_NUMBER() OVER(ORDER BY ItemId ASC) AS Row#,
	CedisId,
	DeliveryDate,
	CAST('' AS VARCHAR(30)) AS CustBarCode,
	Cuc,
	ItemId,
	Price, BasePrice, Discount, DiscountRate
FROM FinalOrder


GO
/****** Object:  View [dbo].[CustItemPricesWbc]    Script Date: 15/11/2018 10:44:11 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[CustItemPricesWbc] AS

--WITH PrevCustomer AS (
--	SELECT
--		REC_CedisIdOpecd AS CedisId,
--		CAST(CONVERT(DATE, CURRENT_TIMESTAMP) AS SMALLDATETIME) AS DeliveryDate,
-- 		CCL_CRMId AS CRMId,
--		CCL_Cuc AS CUC,
--		CCL_BarCode AS BarCode,
--		CCL_WBCId AS WBCId,
--		(CASE WHEN CCL_Cuc IS NOT NULL AND CCL_Cuc>0 THEN CAST(CCL_Cuc AS VARCHAR)
--		WHEN COALESCE(CCL_CustomerType, '') = 'CLIENTE_COCACOLA' THEN Destino.COD_CucCCTH
--		ELSE CAST(CCL_PriceList AS VARCHAR) END
--		) AS PriceListId,
--		CCL_RouteCode AS RouteId
--	FROM dbo.CAT_Customers AS Customer
--	JOIN dbo.CONF_Destinos AS Destino
--		ON Destino.COD_RouteCodeOpecd = Customer.CCL_RouteCode
--	JOIN dbo.REF_Cedis AS Cedi
--		ON Cedi.Id = Destino.COD_CedisRefId
--)
--, FinalPrevCustomer AS (
--	SELECT
--		CedisId,
--		DeliveryDate,
--		PriceListId AS PriceListCode,
--		BarCode AS CustBarCode,
--		RouteId,
--		PListDetail.priceListProductCode AS ItemId,
--		CAST(PListDetail.price AS DECIMAL(19, 4)) AS Price,
--		CAST(PListDetail.priceBase AS DECIMAL(19, 4)) AS BasePrice,
--		CAST(PListDetail.discount AS DECIMAL(19, 4)) AS Discount,
--		CAST(PListDetail.discountPercent AS DECIMAL(19, 4))AS DiscountRate
--	FROM PrevCustomer
--	JOIN dbo.BIT_PriceList AS PList
--		ON PList.priceListCode = PrevCustomer.PriceListId
--	JOIN dbo.BIT_PriceList_Detail AS PListDetail
--		ON PListDetail.priceListCode = PList.priceListCode
--	WHERE ISNUMERIC(PList.priceListCode) = 1
--) 
WITH PrevOrder AS (
	SELECT DISTINCT
		CustOrder.Id AS OrderId,
		Address.CustBarCode AS CustBarCode,
		--CAST(CustOrder.ORD_CustomerId AS VARCHAR) AS CustBarCode, /* QUITAR, SOLO PRUEBAS */
		CustOrder.ORD_GrandTotal AS Total,
		CustOrder.ORD_BaseDiscountAmount AS GlobalDiscountAmount,
		CustOrder.ORD_TotalPaid AS TotalPaid,
		CustOrder.ORD_BaseSubtotal AS Subtotal,
		CustOrder.ORD_DiscountAmount AS OrderDiscountAmount,
		Bottler.BOT_DeliveryDate AS DeliveryDate,
		CAST(Bottler.BOT_RouteCode AS VARCHAR) AS RouteId,
		Cedis.REC_CedisIdOpecd AS IdCedis,
		(CASE WHEN UPPER(CustOrder.ORD_StatusOrden) = 'ENVIADO' THEN 1 ELSE 0 END) AS Sended /* Validar que es el estatus correcto */
	FROM dbo.CAT_OrderGeneral AS CustOrder
	JOIN dbo.CAT_BottlerData AS Bottler
		ON Bottler.BOT_OrderGeneralId = CustOrder.Id
	JOIN (
		SELECT DISTINCT
			ORE_BarCode AS CustBarCode, ORE_OrderGeneralId AS OrderId,
			ORE_CRMId AS CRMId, COALESCE(Customer.CCL_Cuc, 0) AS CUC
		FROM dbo.CAT_OrderAddresses
		JOIN dbo.CAT_Customers AS Customer
			ON Customer.CCL_CRMId = CAT_OrderAddresses.ORE_CRMId
			AND Customer.CCL_CustomerType = 'CLIENTE_COCACOLA'
	) AS Address
		ON Address.OrderId = CustOrder.Id
	JOIN dbo.CONF_Destinos AS Destinos
		ON Destinos.COD_RouteCodeOpecd = CAST(Bottler.BOT_RouteCode AS VARCHAR)
	JOIN dbo.REF_Cedis AS Cedis
		ON Cedis.REC_CedisNameCCHT = Bottler.BOT_CediName
	WHERE ORD_CreateFrom = 'COCACOLA'
)
, FinalPrevOrder AS ( 
	SELECT PrevOrder.IdCedis,
		PrevOrder.DeliveryDate,
		PrevOrder.OrderId,
		PrevOrder.Total,
		PrevOrder.CustBarCode,
		PrevDet.ItemId,
		PrevDet.Qty,
		PrevDet.Price, 
		PrevDet.BasePrice,
		PrevDet.ItemDiscount,
		PrevDet.ItemTotal,
		PrevDet.GrandTotalItems,
		ItemTotalWithDiscount,
		CAST(PrevDet.ItemTotalWithDiscount/PrevDet.Qty AS DECIMAL(19,4)) AS ItemPriceWithDiscount,
		CAST(PrevDet.ItemTotalWithDiscount/PrevDet.GrandTotalItems AS DECIMAL(10,9)) AS ItemPercent
	FROM PrevOrder AS PrevOrder
	JOIN (
		SELECT
			ORI_OrderGeneralId AS OrderId,
			ORI_Sku AS ItemId,
			ORI_Name AS Name,
			ORI_QtyOrdered AS Qty,
			ORI_Price AS Price,
			ORI_OriginalPrice AS BasePrice,
			ORI_DiscountAmount AS ItemDiscount,
			ORI_RowTotal AS ItemTotal,
			ORI_Applied_rule_ids AS RuleId,
			CAST((ORI_RowTotal - ORI_DiscountAmount) AS DECIMAL(19,4)) AS ItemTotalWithDiscount,
			SUM(CAST((ORI_RowTotal - ORI_DiscountAmount) AS DECIMAL(19,4)))	OVER (PARTITION BY ORI_OrderGeneralId) AS GrandTotalItems
		FROM dbo.CAT_OrderItems
	) AS PrevDet
		ON PrevOrder.OrderId = PrevDet.OrderId
	WHERE PrevOrder.Sended = 1
)
, FinalNotGroupping AS (
	SELECT
		FinalOrder.IdCedis,
		FinalOrder.DeliveryDate,
		FinalOrder.CustBarCode,
		FinalOrder.ItemId,
		FinalOrder.Qty, 
		FinalOrder.ItemTotalWithDiscount AS ItemTotal,
		FinalOrder.BasePrice, 
		FinalOrder.Total*FinalOrder.ItemPercent/FinalOrder.Qty AS NewPrice,
		FinalOrder.Total*FinalOrder.ItemPercent AS NewItemTotal,
		BasePrice - FinalOrder.Total*FinalOrder.ItemPercent/FinalOrder.Qty AS NewDiscount
	FROM FinalPrevOrder AS FinalOrder
)
, FinalOrder AS
(
	SELECT 
		IdCedis,
		DeliveryDate,
		CustBarCode,
		ItemId,
		CAST(ItemTotal/ItemQty AS DECIMAL(19, 4)) AS Price, 
		CAST(ItemDiscount/ItemQty AS DECIMAL(19, 4)) AS Discount,
		CAST(ItemBasePrice/ItemQty AS DECIMAL(19, 4)) AS BasePrice,
		CAST(0 AS DECIMAL(19,4)) AS DiscountRate
	FROM (
		SELECT IdCedis,
			DeliveryDate,
			CustBarCode,
			ItemId,
			SUM(NewItemTotal) AS ItemTotal,
			SUM(NewDiscount*Qty) AS ItemDiscount, 
			SUM(BasePrice*Qty) AS ItemBasePrice,
			SUM(Qty) AS ItemQty
		FROM FinalNotGroupping
		GROUP BY IdCedis, DeliveryDate, CustBarCode, ItemId
		HAVING SUM(Qty) > 0
	) AS Prev
)
SELECT
	--COALESCE(FinalOrder.IdCedis, FinalCustomer.CedisId) AS CedisId,
	--COALESCE(FinalOrder.DeliveryDate, FinalCustomer.DeliveryDate) AS DeliveryDate,
	--COALESCE(FinalOrder.CustBarCode, FinalCustomer.CustBarCode) AS CustBarCode,
	--COALESCE(FinalOrder.ItemId, FinalCustomer.ItemId) AS ItemId,
	--COALESCE(FinalOrder.Price, FinalCustomer.Price) AS Price,
	--COALESCE(FinalOrder.BasePrice, FinalCustomer.BasePrice) AS BasePrice,
	--COALESCE(FinalOrder.Discount, FinalCustomer.Discount) AS Discount,
	--COALESCE(FinalOrder.DiscountRate, FinalCustomer.DiscountRate) AS DiscountRate,
	--(CASE WHEN FinalOrder.ItemId IS NULL THEN 0 ELSE 1 END) AS ModifiedByOrders
	ROW_NUMBER() OVER(ORDER BY ItemId ASC) AS Row# ,
	FinalOrder.IdCedis,
	FinalOrder.DeliveryDate,
	FinalOrder.CustBarCode, 
	FinalOrder.ItemId,
	FinalOrder.Price, 
	FinalOrder.BasePrice,
	FinalOrder.Discount, 
	FinalOrder.DiscountRate, 
	(CASE WHEN FinalOrder.ItemId IS NULL THEN 0 ELSE 1 END) AS ModifiedByOrders
FROM FinalOrder
--FULL OUTER JOIN FinalPrevCustomer FinalCustomer
--	ON FinalCustomer.CustBarCode = FinalOrder.CustBarCode
--	AND FinalCustomer.ItemId = FinalOrder.ItemId
--	AND FinalCustomer.DeliveryDate IN (
--		FinalOrder.DeliveryDate,
--		FinalCustomer.DeliveryDate
--	)


GO
ALTER TABLE [dbo].[BIT_PriceList] ADD  CONSTRAINT [DF_BIT_PriceList_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BIT_PriceList] ADD  CONSTRAINT [DF_BIT_PriceList_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedOn]
GO
ALTER TABLE [dbo].[BIT_PriceList_Detail] ADD  CONSTRAINT [DF_BIT_PriceList_Detail_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BIT_PriceList_Detail] ADD  CONSTRAINT [DF_BIT_PriceList_Detail_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedOn]
GO
ALTER TABLE [dbo].[CAT_BottlerData] ADD  DEFAULT ((0)) FOR [BOT_Trip]
GO
ALTER TABLE [dbo].[CAT_Customers] ADD  DEFAULT ('') FOR [CCL_CCTHId]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_BaseDiscountAmount]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_BaseGrandTotal]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_BaseShippingAmount]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_BaseShippingTaxAmount]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_BaseSubtotal]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_BaseTaxAmount]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_BaseTotalPaid]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_BaseTotalRefunded]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_DiscountAmount]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_GrandTotal]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_ShippingAmount]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_ShippingTaxAmount]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_StoreToOrderRate]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_Subtotal]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_TaxAmount]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_TotalPaid]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_TotalQtyOrdered]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_TotalRefunded]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_BaseShippingDiscountAmount]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_BaseSubtotalInclTax]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_BaseTotalDue]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_ShippingDiscountAmount]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_SubtotalInclTax]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_TotalDue]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_ShippingInclTax]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_BaseCustomerBalanceAmount]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_CustomerBalanceAmount]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_BaseGiftCardsAmount]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_GiftCardsAmount]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_BaseRewardCurrencyAmount]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT ((0.0)) FOR [ORD_RewardCurrencyAmount]
GO
ALTER TABLE [dbo].[CAT_OrderGeneral] ADD  DEFAULT (getdate()) FOR [ORD_CreateDateInter]
GO
ALTER TABLE [dbo].[CAT_OrderItems] ADD  DEFAULT ((0.0)) FOR [ORI_QtyCanceled]
GO
ALTER TABLE [dbo].[CAT_OrderItems] ADD  DEFAULT ((0.0)) FOR [ORI_QtyInvoiced]
GO
ALTER TABLE [dbo].[CAT_OrderItems] ADD  DEFAULT ((0.0)) FOR [ORI_QtyOrdered]
GO
ALTER TABLE [dbo].[CAT_OrderItems] ADD  DEFAULT ((0.0)) FOR [ORI_QtyRefunded]
GO
ALTER TABLE [dbo].[CAT_OrderItems] ADD  DEFAULT ((0.0)) FOR [ORI_QtyShipped]
GO
ALTER TABLE [dbo].[CAT_OrderItems] ADD  DEFAULT ((0.0)) FOR [ORI_Price]
GO
ALTER TABLE [dbo].[CAT_OrderItems] ADD  DEFAULT ((0.0)) FOR [ORI_OriginalPrice]
GO
ALTER TABLE [dbo].[CAT_OrderItems] ADD  DEFAULT ((0.0)) FOR [ORI_DiscountAmount]
GO
ALTER TABLE [dbo].[CAT_OrderItems] ADD  DEFAULT ((0.0)) FOR [ORI_RowTotal]
GO
ALTER TABLE [dbo].[CAT_OrderItems] ADD  DEFAULT ((0.0)) FOR [ORI_PriceInclTax]
GO
ALTER TABLE [dbo].[CAT_OrderItems] ADD  DEFAULT ((0.0)) FOR [ORI_RowTotalInclTax]
GO
ALTER TABLE [dbo].[CAT_OrderItems] ADD  DEFAULT ((0)) FOR [ORI_InterId]
GO
ALTER TABLE [dbo].[CAT_Visits] ADD  DEFAULT ((0)) FOR [CTV_Monday]
GO
ALTER TABLE [dbo].[CAT_Visits] ADD  DEFAULT ((0)) FOR [CTV_Tuesday]
GO
ALTER TABLE [dbo].[CAT_Visits] ADD  DEFAULT ((0)) FOR [CTV_Wednesday]
GO
ALTER TABLE [dbo].[CAT_Visits] ADD  DEFAULT ((0)) FOR [CTV_Thursday]
GO
ALTER TABLE [dbo].[CAT_Visits] ADD  DEFAULT ((0)) FOR [CTV_Friday]
GO
ALTER TABLE [dbo].[CAT_Visits] ADD  DEFAULT ((0)) FOR [CTV_Saturday]
GO
ALTER TABLE [dbo].[CAT_Visits] ADD  DEFAULT ((0)) FOR [CTV_Sunday]
GO
ALTER TABLE [dbo].[CONF_Destinos] ADD  DEFAULT ((0)) FOR [COD_CucCCTH]
GO
ALTER TABLE [dbo].[CONF_ProcesosAutomaticos] ADD  DEFAULT (getdate()) FOR [CPA_Modificado]
GO
ALTER TABLE [dbo].[CONF_Routes] ADD  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[DET_Sales] ADD  DEFAULT ((0)) FOR [DSS_Precio]
GO
ALTER TABLE [dbo].[DET_Sales] ADD  DEFAULT ((0)) FOR [DSS_PrecioBase]
GO
ALTER TABLE [dbo].[DET_Sales] ADD  DEFAULT ((0)) FOR [DSS_Descuento]
GO
ALTER TABLE [dbo].[DET_Sales] ADD  DEFAULT ((0)) FOR [DSS_PorcentajeDescuento]
GO
ALTER TABLE [dbo].[ENC_Sales] ADD  DEFAULT (getdate()) FOR [ESS_FechaCreacion]
GO
ALTER TABLE [dbo].[ENC_Sales] ADD  DEFAULT ((0)) FOR [ESS_CustomerCCTH]
GO
ALTER TABLE [dbo].[ENC_Sales] ADD  DEFAULT ('') FOR [ESS_Barcode]
GO
ALTER TABLE [dbo].[CAT_BottlerData]  WITH CHECK ADD  CONSTRAINT [FK_CAT_BottlerData_CAT_OrderGeneral] FOREIGN KEY([BOT_OrderGeneralId])
REFERENCES [dbo].[CAT_OrderGeneral] ([Id])
GO
ALTER TABLE [dbo].[CAT_BottlerData] CHECK CONSTRAINT [FK_CAT_BottlerData_CAT_OrderGeneral]
GO
ALTER TABLE [dbo].[CAT_OrderAddresses]  WITH CHECK ADD  CONSTRAINT [FK_CAT_OrderAddresses_CAT_OrderGeneral] FOREIGN KEY([ORE_OrderGeneralId])
REFERENCES [dbo].[CAT_OrderGeneral] ([Id])
GO
ALTER TABLE [dbo].[CAT_OrderAddresses] CHECK CONSTRAINT [FK_CAT_OrderAddresses_CAT_OrderGeneral]
GO
ALTER TABLE [dbo].[CAT_OrderComments]  WITH CHECK ADD  CONSTRAINT [FK_CAT_OrderComments_CAT_OrderGeneral] FOREIGN KEY([OCM_OrderGeneralId])
REFERENCES [dbo].[CAT_OrderGeneral] ([Id])
GO
ALTER TABLE [dbo].[CAT_OrderComments] CHECK CONSTRAINT [FK_CAT_OrderComments_CAT_OrderGeneral]
GO
ALTER TABLE [dbo].[CAT_OrderItems]  WITH CHECK ADD  CONSTRAINT [FK_CAT_OrderItems_CAT_OrderGeneral] FOREIGN KEY([ORI_OrderGeneralId])
REFERENCES [dbo].[CAT_OrderGeneral] ([Id])
GO
ALTER TABLE [dbo].[CAT_OrderItems] CHECK CONSTRAINT [FK_CAT_OrderItems_CAT_OrderGeneral]
GO
ALTER TABLE [dbo].[CAT_Visits]  WITH CHECK ADD  CONSTRAINT [FK_CAT_CustomerId_CAT_Customers] FOREIGN KEY([CTV_CustomerId])
REFERENCES [dbo].[CAT_Customers] ([Id])
GO
ALTER TABLE [dbo].[CAT_Visits] CHECK CONSTRAINT [FK_CAT_CustomerId_CAT_Customers]
GO
ALTER TABLE [dbo].[CONF_Destinos]  WITH CHECK ADD  CONSTRAINT [FK_CONF_Destinos_REF_Cedis] FOREIGN KEY([COD_CedisRefId])
REFERENCES [dbo].[REF_Cedis] ([Id])
GO
ALTER TABLE [dbo].[CONF_Destinos] CHECK CONSTRAINT [FK_CONF_Destinos_REF_Cedis]
GO
ALTER TABLE [dbo].[DET_Log]  WITH CHECK ADD FOREIGN KEY([DLO_LogId])
REFERENCES [dbo].[ENC_Log] ([ELO_Id])
GO
ALTER TABLE [dbo].[DET_Log]  WITH CHECK ADD FOREIGN KEY([DLO_ProcessId])
REFERENCES [dbo].[CAT_Process] ([CPR_Id])
GO
ALTER TABLE [dbo].[DET_Promotions]  WITH CHECK ADD FOREIGN KEY([DEP_SaleId])
REFERENCES [dbo].[ENC_Sales] ([Id])
GO
ALTER TABLE [dbo].[DET_Sales]  WITH CHECK ADD FOREIGN KEY([DSS_SaleId])
REFERENCES [dbo].[ENC_Sales] ([Id])
GO
ALTER TABLE [dbo].[ENC_Log]  WITH CHECK ADD FOREIGN KEY([ELO_ProcessId])
REFERENCES [dbo].[CAT_Process] ([CPR_Id])
GO
ALTER TABLE [dbo].[REF_TripItems]  WITH CHECK ADD FOREIGN KEY([TIT_TripId])
REFERENCES [dbo].[ENC_Trips] ([Id])
GO
ALTER TABLE [dbo].[REF_UserCedis]  WITH CHECK ADD FOREIGN KEY([CedisId])
REFERENCES [dbo].[REF_Cedis] ([Id])
GO
ALTER TABLE [dbo].[REF_UserCedis]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[CAT_Users] ([Id])
GO
