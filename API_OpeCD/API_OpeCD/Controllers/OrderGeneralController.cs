﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Http;
using API_OpeCD.Models;
using Newtonsoft.Json;
using OpeCDLib.Entities;
using OpeCDLib.Services;
using System.Net;
using System.Web.Http.Description;
using API_OpeCD.Utils;
using System.Globalization;
using API_OpeCD.Repository.IntegrationsApi;
using OpeCDLib.Error;

namespace API_OpeCD.Controllers
{

    public class OrderGeneralController : ApiController
    {
        private readonly InventoryRepository _inventoryRepository = new InventoryRepository();

        [HttpGet]
        [Route("api/OrderGeneral/GetValidation")]
        [ResponseType(typeof(List<DeliveryOrderConfirmedLine>))]
        public IHttpActionResult GetValidation([FromUri] string ids, int cedisId)
        {
            try
            {
                var cedisConnectionString = new CedisConnectionUtil().GetConnectionStringByCedis(cedisId);
                var deliveryOrderService = new DeliveryOrderService(cedisConnectionString);
                var strArray = ids.Split(',');
                var numArray = new long[strArray.Length];
                for (var index = 0; index < strArray.Length; ++index)
                    numArray[index] = long.Parse(strArray[index]);

                var confirmed = deliveryOrderService.ValidateConfirmation(new List<long>(numArray));
                if (confirmed == null && deliveryOrderService.ErrorList.Any())
                    return Content(HttpStatusCode.InternalServerError, new { ErrorList = deliveryOrderService.ErrorList });
                return Ok(confirmed);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpGet]
        [Route("api/OrderGeneral/GetConfirmation")]
        [ResponseType(typeof(List<DeliveryOrderConfirmedLine>))]
        public IHttpActionResult GetConfirmation([FromUri] string ids, [FromUri]int cedisId)
        {
            try
            {
                var cedisConnectionString = new CedisConnectionUtil().GetConnectionStringByCedis(cedisId);
                var deliveryOrderService = new DeliveryOrderService(cedisConnectionString);
                var strArray = ids.Split(',');
                var numArray = new long[strArray.Length];
                for (var index = 0; index < strArray.Length; ++index)
                    numArray[index] = long.Parse(strArray[index]);

                var confirmed = deliveryOrderService.GetConfirmedLines(new List<long>(numArray));
                if (confirmed == null && deliveryOrderService.ErrorList.Any())
                    return Content(HttpStatusCode.InternalServerError, new { ErrorList = deliveryOrderService.ErrorList });
                return Ok(confirmed);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        //[HttpGet]
        //[Route("api/OrderGeneral/GetConfirmation")]
        //[ResponseType(typeof(List<DeliveryOrderConfirmedLine>))]
        //public IHttpActionResult GetConfirmation([FromUri] int route, [FromUri]int cedisId)
        //{
        //    try
        //    {
        //        if (!ModelState.IsValid)
        //            return BadRequest(ModelState);
        //        var cedisConnectionString = new CedisConnectionUtil().GetConnectionStringByCedis(cedisId);
        //        var deliveryOrderService = new DeliveryOrderService(cedisConnectionString);
        //        var orderConfirmedLineList = deliveryOrderService.GetConfirmedLines(route);
        //        if (orderConfirmedLineList == null && deliveryOrderService.ErrorList.Any())
        //            return Content(HttpStatusCode.InternalServerError, new { ErrorList = deliveryOrderService.ErrorList });
        //        return Ok(orderConfirmedLineList);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Content(HttpStatusCode.InternalServerError, ex.Message);
        //    }

        //}

        [HttpPost]
        [Route("api/OrderGeneral")]
        public IHttpActionResult Post([FromBody] List<OrderGeneralModels> json, [FromUri]int cedisId)
        {
            try
            {
                var cedisConnectionString = new CedisConnectionUtil().GetConnectionStringByCedis(cedisId);
                var orderService = new DeliveryOrderService(cedisConnectionString);
                var deliveryOrderModelsList = new List<DeliveryOrderModels>();
                var deliveryOrderDetailList = new List<DeliveryOrderItem>();
                var orderResponseModels = new List<OrderResponseModels>();
                foreach (var orderGeneralModels in json)
                {
                    var deliveryOrderModels = new DeliveryOrderModels();
                    var deliveryOrder = new DeliveryOrder();

                    deliveryOrder.CustomerOpeId = orderGeneralModels.OrderAddresses.First().CustomerId != null ? Convert.ToInt32(orderGeneralModels.OrderAddresses.First().CustomerId) : 0;
                    deliveryOrder.NumLines = 0;
                    deliveryOrder.DeliveryOrderStatus = orderGeneralModels.Status;
                    deliveryOrder.DiscountAmount = orderGeneralModels.DiscountAmount;
                    deliveryOrder.DeliveryDate = DateTime.Parse(orderGeneralModels.BottlerData.DeliveryDate.Substring(0, 10));
                    deliveryOrder.DateDeliveryOrder = DateTime.Parse(orderGeneralModels.CreatedAt.Substring(0, 10));
                    deliveryOrder.RouteCode = short.Parse(orderGeneralModels.BottlerData.RouteCode);
                    deliveryOrder.StatusSync = 0;
                    deliveryOrder.Subtotal = 1;
                    deliveryOrder.PaymentMethod = orderGeneralModels.PaymentMethod;
                    foreach (var orderItem in orderGeneralModels.OrderItems)
                    {
                        var flag = false;
                        var deliveryOrderDetail = new DeliveryOrderItem
                        {
                            ItemId = orderItem.Sku,
                            DiscountAmount = orderItem.DiscountAmount,
                            QtyDeliveryOrdered = Convert.ToInt32(orderItem.QtyOrdered),
                            OriginalPrice = orderItem.OriginalPrice,
                            RuleId = orderGeneralModels.AppliedRuleIds,
                            Price = Convert.ToDecimal(orderItem.Price),
                            SubOrder = 1
                        };
                        foreach (var t in deliveryOrderDetailList)
                        {
                            if (t.ItemId != deliveryOrderDetail.ItemId) continue;
                            t.DiscountAmount += deliveryOrderDetail.DiscountAmount;
                            t.BottlePrice += deliveryOrderDetail.BottlePrice;
                            t.QtyDeliveryOrdered += deliveryOrderDetail.QtyDeliveryOrdered;
                            flag = true;
                            break;
                        }
                        if (!flag)
                            deliveryOrderDetailList.Add(deliveryOrderDetail);
                    }
                    foreach (var deliveryOrderDetail in deliveryOrderDetailList)
                        deliveryOrder.DeliveryOrderDetail.Add(deliveryOrderDetail);
                    deliveryOrderModels.WantBill = orderGeneralModels.OrderAddresses.Count(orderAddress => orderAddress.WantBill.Equals("1")) == 1 ? "1" : "0";
                    deliveryOrderModels.DeliveryOrder = deliveryOrder;
                    deliveryOrderDetailList = new List<DeliveryOrderItem>();
                    if (string.IsNullOrEmpty(deliveryOrderModels.WantBill))
                    {
                        if (deliveryOrderModelsList.Count > 0)
                        {
                            var flag = false;
                            foreach (var t in deliveryOrderModelsList)
                            {
                                if (t.DeliveryOrder.CustomerOpeId != deliveryOrder.CustomerOpeId ||
                                    t.DeliveryOrder.RouteCode != deliveryOrder.RouteCode ||
                                    !string.IsNullOrEmpty(t.WantBill)) continue;
                                flag = true;
                                t.DeliveryOrder.DiscountAmount += deliveryOrder.DiscountAmount;
                                t.DeliveryOrder.Subtotal += 1;
                                deliveryOrderDetailList.AddRange(t.DeliveryOrder.DeliveryOrderDetail);

                                foreach (var deliveryOrderDetail1 in deliveryOrder.DeliveryOrderDetail)
                                {
                                    var flag2 = false;
                                    foreach (var deliveryOrderDetail2 in deliveryOrderDetailList)
                                    {
                                        if (deliveryOrderDetail2.ItemId != deliveryOrderDetail1.ItemId) continue;
                                        deliveryOrderDetail1.DiscountAmount += deliveryOrderDetail2.DiscountAmount;
                                        deliveryOrderDetail1.BottlePrice += deliveryOrderDetail2.BottlePrice;
                                        deliveryOrderDetail1.QtyDeliveryOrdered += deliveryOrderDetail2.QtyDeliveryOrdered;
                                        flag2 = true;
                                        break;
                                    }
                                    if (!flag2)
                                        deliveryOrderDetailList.Add(deliveryOrderDetail1);
                                }
                                t.DeliveryOrder.DeliveryOrderDetail.Clear();
                                foreach (var deliveryOrderDetail in deliveryOrderDetailList)
                                    t.DeliveryOrder.DeliveryOrderDetail.Add(deliveryOrderDetail);
                            }

                            if (!flag)
                                deliveryOrderModelsList.Add(deliveryOrderModels);
                        }
                        else
                            deliveryOrderModelsList.Add(deliveryOrderModels);
                    }
                    else
                        deliveryOrderModelsList.Add(deliveryOrderModels);

                    var orderResponse = new OrderResponseModels();
                    orderResponse.DeliveryOrderId = orderService.Add(deliveryOrderModelsList.Last().DeliveryOrder);
                    orderResponse.ErrorItemList = new List<ErrorItem>(orderService.ErrorList);
                    orderResponse.Id = orderGeneralModels.InterId;
                    orderResponseModels.Add(orderResponse);
                    orderService.ErrorList.Clear();
                }

                return Content(HttpStatusCode.Created, orderResponseModels);
                
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpGet]
        [Route("api/orderGeneral/{deliveryOrderId}")]
        [ResponseType(typeof(DeliveryOrder))]
        public IHttpActionResult GetOrder(long deliveryOrderId, int cedisId)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                var cedisConnectionString = new CedisConnectionUtil().GetConnectionStringByCedis(cedisId);
                var deliveryOrderService = new DeliveryOrderService(cedisConnectionString);
                var deliveryOrder = deliveryOrderService.Get(deliveryOrderId);
                if (deliveryOrder == null && deliveryOrderService.ErrorList.Any())
                    return Content(HttpStatusCode.InternalServerError, new { ErrorList = deliveryOrderService.ErrorList });
                return Ok(deliveryOrder);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }

        }

        [HttpGet]
        [Route("api/orderGeneral/TripInventory")]
        [ResponseType(typeof(List<Trip>))]
        public IHttpActionResult GetTripInventory(int cedisId, int route)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                var cedisConnectionString = new CedisConnectionUtil().GetConnectionStringByCedis(cedisId);
                var deliveryOrderService = new DeliveryOrderService(cedisConnectionString);
                var response = deliveryOrderService.GetTripInventory(route);
                if (response == null) {
                    if (deliveryOrderService.ErrorList.Any())
                        return Ok(new { Trips = new List<Trip>(), ErrorList = deliveryOrderService.ErrorList });
                    else
                        return Ok(new List<Trip>());
                }
                return Ok(response);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }

        }

        [HttpPost]
        [Route("api/Inventario/TripInventory")]
        [ResponseType(typeof(TripInventoryResponseModels))]
        public IHttpActionResult Get(TripInventoryRequestModels request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var tripsToUpdate = new List<CedisTrips>();
                var opeInventory = new List<CedisTrips>();

                foreach (var cedisRoute in request.CedisRoutes)
                {
                    var cedisConnectionString = new CedisConnectionUtil().GetConnectionStringByCedis(cedisRoute.CedisId);
                    var deliveryOrderService = new DeliveryOrderService(cedisConnectionString);

                    var cedisTrips = new CedisTrips();
                    cedisTrips.CedisId = cedisRoute.CedisId;
                    cedisTrips.TripList = deliveryOrderService.GetTripInventory(cedisRoute.Route);
                    if (cedisTrips.TripList != null && cedisTrips.TripList.Any())
                        opeInventory.Add(cedisTrips);
                }

                foreach (var cedis in opeInventory)
                {
                    var cedisTripEntity = new CedisTrips();
                    cedisTripEntity.CedisId = cedis.CedisId;
                    foreach (var trip in cedis.TripList)
                    {
                        var intermediaTripInventory = _inventoryRepository.Get(cedis.CedisId, trip.Route, trip.DeliveryDate, false).SingleOrDefault(trp => trp.Id == trip.Id);
                        if (intermediaTripInventory == null){
                            cedisTripEntity.TripList.Add(trip);
                            continue;
                        }
                        var existsDifferenceBetweenTrips = (from confirmedItems in trip.ConfirmedItems
                                                            where !intermediaTripInventory.ConfirmedItems.Any(x => x.ItemId == confirmedItems.ItemId && x.Qty == confirmedItems.Qty)
                                                            select confirmedItems).ToList()
                                                            .Any();
                        if (existsDifferenceBetweenTrips)
                            cedisTripEntity.TripList.Add(trip);
                    }
                    tripsToUpdate.Add(cedisTripEntity);
                }
                return Ok(new TripInventoryResponseModels() { CedisTrips = tripsToUpdate });
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}