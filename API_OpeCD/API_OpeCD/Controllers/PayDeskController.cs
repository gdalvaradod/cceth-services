﻿using API_OpeCD.Utils;
using OpeCDLib.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace API_OpeCD.Controllers
{
    public class PayDeskController : ApiController
    {
        private PayDeskService _payDeskService;

        [HttpGet]
        [Route("api/PayDesk")]
        [ResponseType(typeof(DateTime))]
        public IHttpActionResult GetPayDesk([FromUri]int cedisId) {
            try {
                var cedisConnection = new CedisConnectionUtil().GetConnectionStringByCedis(cedisId);
                _payDeskService = new PayDeskService(cedisConnection);
                if(_payDeskService.ErrorList.Any())
                    return Content(HttpStatusCode.InternalServerError, _payDeskService.ErrorList);
                var payDeskDate = _payDeskService.PayDeskDate();
                return Ok(payDeskDate);
            }
            catch (Exception ex) {

                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

    }
}
