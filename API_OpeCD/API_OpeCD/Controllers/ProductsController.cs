﻿using API_OpeCD.Models;
using API_OpeCD.Utils;
using OpeCDLib.Entities;
using OpeCDLib.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace API_OpeCD.Controllers
{
    public class ProductsController : ApiController
    {
        private ProductService _productService;

        [HttpGet]
        [Route("api/products")]
        [ResponseType(typeof(List<Product>))]
        public IHttpActionResult Get([FromUri]int cedisId)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                var cedisConnectionString = new CedisConnectionUtil().GetConnectionStringByCedis(cedisId);
                _productService = new ProductService(cedisConnectionString);
                var cedisProductList = _productService.GetAll();
                if(cedisProductList == null && _productService.ErrorList.Any())
                    return Content(HttpStatusCode.InternalServerError, new { ErrorList = _productService.ErrorList });
                return Ok(cedisProductList);
            }catch(Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        
    }
}
