﻿using API_OpeCD.Mappers;
using API_OpeCD.Models;
using API_OpeCD.Models.Requests;
using API_OpeCD.Models.Responses;
using API_OpeCD.Utils;
using OpeCDLib.Entities;
using OpeCDLib.Error;
using OpeCDLib.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using API_OpeCD.Models.WBC;
using API_OpeCD.Repository;

namespace API_OpeCD.Controllers
{
    public class SalesController : ApiController
    {
        private SaleService _saleService;
        private SaleMapper _saleMapper = new SaleMapper();
        private SaleRepository _saleRepository = new SaleRepository(Settings.Api_Integraciones);
        private PrecioClienteCocaRepository _precioClienteRepository = new PrecioClienteCocaRepository();
        private CustomerRepository _customerRepository = new CustomerRepository();

        [HttpPost]
        [Route("api/sales")]
        [ResponseType(typeof(List<PostSalesReponse>))]
        public IHttpActionResult PostSales(List<WbcSale> sales, [FromUri]int cedisId)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                var cedisConnectionString = new CedisConnectionUtil().GetConnectionStringByCedis(cedisId);
                _saleService = new SaleService(cedisConnectionString);
                _saleService.SystemId = 1;
                var responseList = new List<PostSalesReponse>();
                foreach (var sale in sales)
                {
                    var opeSale = _saleMapper.Map(sale);
                    if(sale.CustomerCCTH)
                        SetPriceToProducts(opeSale, cedisId);
                    var response = new PostSalesReponse();
                    response.Success = _saleService.Add(opeSale);
                    response.Sale = sale;
                    if (_saleService.ErrorList.Any())
                        _saleService.ErrorList.ForEach(error => response.ErrorDescriptions.Add(error.ErrorDescription));
                    _saleService.ErrorList.Clear();
                    responseList.Add(response);
                }
                return Ok(responseList);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }

        }

        [HttpGet]
        [Route("api/sales")]
        [ResponseType(typeof(List<Sale>))]
        public IHttpActionResult GetIntegrationSales(int cedisId, int routeId, DateTime deliveryDate)
        {
            try
            {
                return Ok(_saleRepository.Find(cedisId, routeId, deliveryDate));
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        private void SetPriceToProducts(Sale sale, int cedisId)
        {
            var customer = _customerRepository.FindByCuc(sale.CustomerOpeId);
            if (customer == null)
                throw new Exception($"No existe en intermedia el cliente con el cuc {sale.CustomerOpeId}");
            var customerPriceList = _precioClienteRepository.Find(cedisId, customer.BarCode, sale.DeliveryDate);
            if (!customerPriceList.Any())
                return;
            foreach (var product in sale.SaleDetail)
            {
                var productPriceInfo = customerPriceList.SingleOrDefault(item => item.ItemId == Convert.ToInt32(product.ItemId));
                product.Price = (decimal)productPriceInfo.Price;
                product.BasePrice = (decimal)productPriceInfo.BasePrice;
                product.DiscountAmount = (decimal)productPriceInfo.DiscountAmount;
                product.DiscountRate = (decimal)productPriceInfo.DiscountRate;
            }
           
        }
    }
}
