﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using API_OpeCD.Utils;
using OpeCDLib.Entities;
using OpeCDLib.Services;

namespace API_OpeCD.Controllers
{
    public class WorkingDaysController : ApiController
    {
        private PayDeskService _payDeskService;

        [HttpGet]
        [Route("api/workingDays")]
        public IHttpActionResult Get(int cedisId)
        {
            try
            {
                var cedisConnection = new CedisConnectionUtil().GetConnectionStringByCedis(cedisId);
                _payDeskService = new PayDeskService(cedisConnection);
                if (_payDeskService.ErrorList.Any())
                    return Content(HttpStatusCode.InternalServerError, _payDeskService.ErrorList);
                
                var januaryFirst = new DateTime(DateTime.UtcNow.Year, 1, 1);
                var december31 = new DateTime(DateTime.UtcNow.Year, 12, 31);
                var workingDays = _payDeskService.WorkingDays(januaryFirst, december31);
                return Content(HttpStatusCode.OK, workingDays);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

    }
}