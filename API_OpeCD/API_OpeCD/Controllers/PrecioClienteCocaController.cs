﻿using System;
using System.Net;
using System.Web.Http;
using API_OpeCD.Repository;

namespace API_OpeCD.Controllers
{
    public class PrecioClienteCocaController : ApiController
    {
        private readonly PrecioClienteCocaRepository _precioRepository = new PrecioClienteCocaRepository();

        // GET: PrecioClienteCoca
        [HttpGet]
        [Route("api/PrecioClienteCoca")]
        public IHttpActionResult GetPrecioClienteCoca(int? cedisId = null, string barcode = "", DateTime? deliveryDate = null)
        {
            try
            {
                var precios = _precioRepository.Find(cedisId, barcode, deliveryDate);
                return Ok(precios);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

    }
}