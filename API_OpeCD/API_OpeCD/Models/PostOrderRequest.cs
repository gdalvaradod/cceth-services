﻿using OpeCDLib.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API_OpeCD.Models
{
    public class PostOrderRequest
    {
        [Required]
        public DeliveryOrder DeliveryOrder{ get; set; }
        public bool CreateWarehouseNotification { get; set; }
    }
}