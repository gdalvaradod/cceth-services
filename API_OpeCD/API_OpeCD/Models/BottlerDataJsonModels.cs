﻿using Newtonsoft.Json;

namespace API_OpeCD.Models
{
    public class BottlerDataJsonModels
    {
        [JsonProperty("route_id")]
        public int RouteId { get; set; }

        [JsonProperty("website")]
        public string Website { get; set; }

        [JsonProperty("store")]
        public string Store { get; set; }

        [JsonProperty("storeview")]
        public string Storeview { get; set; }

        [JsonProperty("postalcode")]
        public int Postalcode { get; set; }

        [JsonProperty("deliveryco")]
        public string Deliveryco { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("settlement")]
        public string Settlement { get; set; }

        [JsonProperty("delivery_date")]
        public string DeliveryDate { get; set; }

        [JsonProperty("route_code")]
        public string RouteCode { get; set; }
    }
}