﻿namespace API_OpeCD.Models.PrecioClienteCoca
{
    public class PrecioClienteCoca
    {
        public int CedisId { get; set; }
        public string DeliveryDate { get; set; }
        public string CustBarCode { get; set; }
        public string Cuc { get; set; }
        public int? ItemId { get; set; }
        public decimal? Price { get; set; }
        public decimal? BasePrice { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? DiscountRate { get; set; }
    }
}