﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_OpeCD.Models
{
    public class Biservidor
    {
        public int IdBiservidor { get; set; }
        public int PuntoVta { get; set; }
        public string  Server { get; set; }
        public string DbName { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
    }
}