﻿using System;
using Newtonsoft.Json;

namespace API_OpeCD.Models
{
    public class OrderItemJsonModels
    {
        [JsonProperty("item_id")]
        public int ItemId { get; set; }

        [JsonProperty("parent_item_id")]
        public int ParentItemId { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("qty_canceled")]
        public Decimal QtyCanceled { get; set; }

        [JsonProperty("qty_invoiced")]
        public Decimal QtyInvoiced { get; set; }

        [JsonProperty("qty_ordered")]
        public Decimal QtyOrdered { get; set; }

        [JsonProperty("qty_refunded")]
        public Decimal QtyRefunded { get; set; }

        [JsonProperty("qty_shipped")]
        public Decimal QtyShipped { get; set; }

        [JsonProperty("price")]
        public Decimal Price { get; set; }

        [JsonProperty("original_price")]
        public Decimal OriginalPrice { get; set; }

        [JsonProperty("discount_amount")]
        public Decimal DiscountAmount { get; set; }

        [JsonProperty("row_total")]
        public Decimal RowTotal { get; set; }

        [JsonProperty("price_incl_tax")]
        public Decimal? PriceInclTax { get; set; }

        [JsonProperty("row_total_incl_tax")]
        public Decimal? RowTotalInclTax { get; set; }
    }
}