﻿using API_OpeCD.Models.Requests;
using OpeCDLib.Entities;
using OpeCDLib.Error;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API_OpeCD.Models.WBC;

namespace API_OpeCD.Models.Responses
{
    public class PostSalesReponse
    {
        public WbcSale Sale { get; set; }

        public List<string> ErrorDescriptions { get; set; }
        public bool Success { get; set; }

        public PostSalesReponse()
        {
            ErrorDescriptions = new List<string>();
        }
    }
}