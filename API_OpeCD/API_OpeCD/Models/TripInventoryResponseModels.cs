﻿using OpeCDLib.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_OpeCD.Models
{
    public class TripInventoryResponseModels
    {
        public List<CedisTrips> CedisTrips { get; set; }
        public TripInventoryResponseModels()
        {
            CedisTrips = new List<Models.CedisTrips>();
        }
    }

    public class CedisTrips
    {
        public int CedisId { get; set; }
        public List<Trip> TripList { get; set; }

        public CedisTrips()
        {
            TripList = new List<Trip>();
        }
    }
}