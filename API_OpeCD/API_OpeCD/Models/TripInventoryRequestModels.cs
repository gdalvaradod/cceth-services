﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_OpeCD.Models
{
    public class TripInventoryRequestModels
    {
        public List<CedisRoute> CedisRoutes { get; set; }
    }

    public class CedisRoute
    {
        public int CedisId { get; set; }
        public int Route { get; set; }
    }
}