﻿using System.Collections.Generic;
using OpeCDLib.Error;

namespace API_OpeCD.Models
{
    public class OrderResponseModels
    {
        //[JsonProperty("id")]
        public int Id { get; set; }

        //[JsonProperty("DeliveryOrderId")]
        public long DeliveryOrderId { get; set; }

        //[JsonProperty("increment_id")]
        public int Trip { get; set; }

        public List<ProductoModels> Productos { get; set; }

        //[JsonProperty("ErrorItemList")]
        public List<ErrorItem> ErrorItemList { get; set; }
    }
}