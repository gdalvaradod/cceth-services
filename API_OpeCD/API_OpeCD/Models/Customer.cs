﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_OpeCD.Models
{
    public class Customer
    {
        public int Cuc { get; set; }
        public string BarCode { get; set; }
        public string Name { get; set; }
    }
}