﻿using System.Collections.Generic;
using Newtonsoft.Json;
using OpeCDLib.Error;
using OpeCDLib.Entities;

namespace API_OpeCD.Models
{
    public class OrderConfirmedLineResponseModels
    {
        [JsonProperty("DeliveryOrderId")]
        public long DeliveryOrderId { get; set; }

        [JsonProperty("CustomerId")]
        public int CustomerId { get; set; }

        [JsonProperty("Trip")]
        public int Trip { get; set; }

        [JsonProperty("ConfirmedItemList")]
        public List<ItemQty> ConfirmedItemList { get; set; }

        [JsonProperty("ErrorItemList")]
        public List<ErrorItem> ErrorItemList { get; set; }
    }
}