﻿using System.Collections.Generic;
using Newtonsoft.Json;
using OpeCDLib.Error;

namespace API_OpeCD.Models
{
    public class OrderConfirmedResponseModels
    {
        [JsonProperty("DeliveryOrderId")]
        public long DeliveryOrderId { get; set; }

        [JsonProperty("IsConfirmed")]
        public bool IsConfirmed { get; set; }

        [JsonProperty("ErrorItemList")]
        public List<ErrorItem> ErrorItemList { get; set; }
    }
}