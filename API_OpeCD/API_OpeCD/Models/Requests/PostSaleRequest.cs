﻿using OpeCDLib.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API_OpeCD.Models.WBC;

namespace API_OpeCD.Models.Requests
{
    public class PostSaleRequest
    {
        public int CustomerOpeId { get; set; }
        public DateTime DeliveryDate { get; set; }
        public long DeliveryOrderId { get; set; }
        public int RouteCode { get; set; }
        public short Trip { get; set; }
        public List<SaleItem> SaleDetail { get; set; }
        public List<SalePromotion> SalePromotions { get; set; }
        public PostSaleRequest()
        {
            SaleDetail = new List<SaleItem>();
            SalePromotions = new List<SalePromotion>();
        }
    }
}