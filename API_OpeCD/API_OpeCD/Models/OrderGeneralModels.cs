﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace API_OpeCD.Models
{
    public class OrderGeneralModels
    {
        [JsonProperty("entity_id")]
        public int EntityId { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("coupon_code")]
        public string CouponCode { get; set; }

        [JsonProperty("InterID")]
        public int InterId { get; set; }

        [JsonProperty("customer_id")]
        public int? CustomerId { get; set; }

        [JsonProperty("shipping_description")]
        public string ShippingDescription { get; set; }

        [JsonProperty("base_discount_amount")]
        public Decimal BaseDiscountAmount { get; set; }

        [JsonProperty("base_grand_total")]
        public Decimal BaseGrandTotal { get; set; }

        [JsonProperty("base_shipping_amount")]
        public Decimal BaseShippingAmount { get; set; }

        [JsonProperty("base_shipping_tax_amount")]
        public Decimal BaseShippingTaxAmount { get; set; }

        [JsonProperty("base_subtotal")]
        public Decimal BaseSubtotal { get; set; }

        [JsonProperty("base_tax_amount")]
        public Decimal BaseTaxAmount { get; set; }

        [JsonProperty("base_total_paid")]
        public Decimal? BaseTotalPaid { get; set; }

        [JsonProperty("base_total_refunded")]
        public Decimal? BaseTotalRefunded { get; set; }

        [JsonProperty("discount_amount")]
        public Decimal DiscountAmount { get; set; }

        [JsonProperty("grand_total")]
        public Decimal GrandTotal { get; set; }

        [JsonProperty("shipping_amount")]
        public Decimal ShippingAmount { get; set; }

        [JsonProperty("shipping_tax_amount")]
        public Decimal ShippingTaxAmount { get; set; }

        [JsonProperty("store_to_order_rate")]
        public Decimal StoreToOrderRate { get; set; }

        [JsonProperty("subtotal")]
        public Decimal Subtotal { get; set; }

        [JsonProperty("tax_amount")]
        public Decimal TaxAmount { get; set; }

        [JsonProperty("total_paid")]
        public Decimal? TotalPaid { get; set; }

        [JsonProperty("total_qty_ordered")]
        public Decimal? TotalQtyOrdered { get; set; }

        [JsonProperty("total_refunded")]
        public Decimal? TotalRefunded { get; set; }

        [JsonProperty("base_shipping_discount_amount")]
        public Decimal BaseShippingDiscountAmount { get; set; }

        [JsonProperty("base_subtotal_incl_tax")]
        public Decimal BaseSubtotalInclTax { get; set; }

        [JsonProperty("base_total_due")]
        public Decimal BaseTotalDue { get; set; }

        [JsonProperty("shipping_discount_amount")]
        public Decimal ShippingDiscountAmount { get; set; }

        [JsonProperty("subtotal_incl_tax")]
        public Decimal SubtotalInclTax { get; set; }

        [JsonProperty("total_due")]
        public Decimal TotalDue { get; set; }

        [JsonProperty("increment_id")]
        public string IncrementId { get; set; }

        [JsonProperty("applied_rule_ids")]
        public string AppliedRuleIds { get; set; }

        [JsonProperty("base_currency_code")]
        public string BaseCurrencyCode { get; set; }

        [JsonProperty("customer_email")]
        public string CustomerEmail { get; set; }

        [JsonProperty("discount_description")]
        public string DiscountDescription { get; set; }

        [JsonProperty("remote_ip")]
        public string RemoteIp { get; set; }

        [JsonProperty("store_currency_code")]
        public string StoreCurrencyCode { get; set; }

        [JsonProperty("store_name")]
        public string StoreName { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("total_item_count")]
        public short TotalItemCount { get; set; }

        [JsonProperty("shipping_incl_tax")]
        public Decimal ShippingInclTax { get; set; }

        [JsonProperty("base_customer_balance_amount")]
        public Decimal? BaseCustomerBalanceAmount { get; set; }

        [JsonProperty("customer_balance_amount")]
        public Decimal? CustomerBalanceAmount { get; set; }

        [JsonProperty("base_gift_cards_amount")]
        public Decimal BaseGiftCardsAmount { get; set; }

        [JsonProperty("gift_cards_amount")]
        public Decimal GiftCardsAmount { get; set; }

        [JsonProperty("reward_points_balance")]
        public int? RewardPointBalance { get; set; }

        [JsonProperty("base_reward_currency_amount")]
        public Decimal? BaseRewardCurrencyAmount { get; set; }

        [JsonProperty("reward_currency_amount")]
        public Decimal? RewardCurrencyAmount { get; set; }

        [JsonProperty("payment_method")]
        public string PaymentMethod { get; set; }

        [JsonProperty("opecdid")]
        public long OpecdId { get; set; }

        [JsonProperty("comments")]
        public string Comments { get; set; }

        [JsonProperty("status_orden")]
        public string StatusOrden { get; set; }

        [JsonProperty("create_from")]
        public string CreateFrom { get; set; }

        [JsonProperty("bottler_data")]
        public BottlerDataJsonModels BottlerData { get; set; }

        [JsonProperty("addresses")]
        public List<OrderAddressesJsonModels> OrderAddresses { get; set; }

        [JsonProperty("order_items")]
        public List<OrderItemJsonModels> OrderItems { get; set; }
    }
}