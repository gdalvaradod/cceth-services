﻿using OpeCDLib.Entities;

namespace API_OpeCD.Models
{
    public class DeliveryOrderModels
    {
        public int Id { get; set; }

        public string WantBill { get; set; }

        public DeliveryOrder DeliveryOrder { get; set; }
    }
}