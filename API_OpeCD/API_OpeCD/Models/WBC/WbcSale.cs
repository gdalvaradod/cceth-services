﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_OpeCD.Models.WBC
{
    public class WbcSale
    {
        public int Id { get; set; }
        public int Codope { get; set; }
        public int Cuc { get; set; }
        public int WbcSaleId { get; set; }
        public bool Facturado { get; set; }
        public int RazonDev { get; set; }
        public int CedisId { get; set; }
        public int RouteCode { get; set; }
        public string Code { get; set; }
        public short Trip { get; set; }
        public bool CardPayment { get; set; }
        public decimal CardAmount { get; set; }
        public bool CustomerCCTH { get; set; }
        public DateTime DeliveryDate { get; set; }
        public List<SaleProduct> SaleProducts { get; set; }
        public List<SalePromotion> SalePromotions { get; set; }
    }
}