﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_OpeCD.Models.WBC
{
    public class SalePromotion
    {
        public int Product { get; set; }
        public int Quantity { get; set; }
        public int Type { get; set; }
        public decimal Price { get; set; }
        public decimal BasePrice { get; set; }
        public decimal Discount { get; set; }
        public decimal DiscountRate { get; set; }
    }
}