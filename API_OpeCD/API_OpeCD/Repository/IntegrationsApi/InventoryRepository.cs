﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API_OpeCD.Utils;
using OpeCDLib.Entities;

namespace API_OpeCD.Repository.IntegrationsApi
{
    public class InventoryRepository
    {

        public List<Trip> Get(int cedisId, int routeId, DateTime deliveryDate, bool withSales = true)
        {
            var restClient = RestClientUtil<Trip>.GetInstance(Settings.Api_Integraciones);
            var uri = $"api/inventario?cedisId={cedisId}&routeId={routeId}&deliveryDate={deliveryDate:yyyy-MM-dd}&withSales={withSales}";
            return restClient.Get(uri);
        }
    }
}