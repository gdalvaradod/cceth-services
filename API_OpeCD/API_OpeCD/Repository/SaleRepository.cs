﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API_OpeCD.Utils;
using OpeCDLib.Entities;

namespace API_OpeCD.Repository
{
    public class SaleRepository
    {
        private RestClientUtil<Sale> _restClient;

        public SaleRepository(string baseUrl)
        {
            _restClient = RestClientUtil<Sale>.GetInstance(baseUrl);
        }

        public List<Sale> Find(int cedisId, int routeId, DateTime deliveryDate)
        {
            return _restClient.Get(string.Format("api/VentasWBC/IntegrationSales?cedisId={0}&routeId={1}&deliveryDate={2}", cedisId, routeId, deliveryDate));
        }
    }
}