﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API_OpeCD.Mappers;
using API_OpeCD.Models;
using API_OpeCD.Models.Entity;

namespace API_OpeCD.Repository
{
    public class CustomerRepository
    {
        private readonly CCTH_INTERMEDIAEntities _db = new CCTH_INTERMEDIAEntities();
        private readonly CustomerMapper _mapper = new CustomerMapper();

        public Customer FindByCuc(int cuc)
        {
            var catCustomers = _db.CAT_Customers.Where(customr => customr.CCL_Cuc == cuc);
            if(!catCustomers.Any())
                return null;
            return _mapper.Map(catCustomers.First(), new Customer());
            
        }
    }
}