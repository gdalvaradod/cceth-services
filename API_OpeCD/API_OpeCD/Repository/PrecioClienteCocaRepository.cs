﻿using System;
using System.Collections.Generic;
using System.Linq;
using API_OpeCD.Mappers;
using API_OpeCD.Models.Entity;
using API_OpeCD.Models.PrecioClienteCoca;

namespace API_OpeCD.Repository
{
    public class PrecioClienteCocaRepository
    {
        private readonly CCTH_INTERMEDIAEntities _context = new CCTH_INTERMEDIAEntities();
        private readonly PrecioClienteCocaMapper _mapper = new PrecioClienteCocaMapper();

        public List<PrecioClienteCoca> Find(int? cedisId = null, string barcode = "", DateTime? deliveryDate = null)
        {
            var preciosQuery = from precios in _context.CustItemPricesOpe
                            select precios;

            if (cedisId != null)
                preciosQuery = preciosQuery.Where(pre => pre.CedisId == cedisId);
            if (!string.IsNullOrEmpty(barcode))
                preciosQuery = preciosQuery.Where(pre => pre.Cuc.ToString() == barcode);
            if (deliveryDate != null)
                preciosQuery = preciosQuery.Where(pre => pre.DeliveryDate == deliveryDate);
            
            return _mapper.Map(preciosQuery.ToList());
        }
    }
}