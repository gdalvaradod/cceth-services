﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace API_OpeCD
{
    public static class Settings
    {
        public static string Api_Integraciones { get { return ConfigurationManager.AppSettings["INTEGRATION_API_URL"]; }}
    }
}