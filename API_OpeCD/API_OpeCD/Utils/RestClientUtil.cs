﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;

namespace API_OpeCD.Utils
{
    public class RestClientUtil<T>
        where T : class
    {
        private static RestClientUtil<T> _restClientUtil;
        private RestClient _restClient;

        private RestClientUtil(string baseUrl)
        {
            _restClient = new RestClient(baseUrl);
        }

        public static RestClientUtil<T> GetInstance(string baseUrl)
        {
            if (_restClientUtil == null)
                _restClientUtil = new RestClientUtil<T>(baseUrl);
            _restClientUtil.SetBaseUrl(baseUrl);
            return _restClientUtil;
        }

        public void SetBaseUrl(string baseUrl)
        {
            _restClient.BaseUrl = new Uri(baseUrl);
        }

        public string GetBaseUrl()
        {
            return _restClient.BaseUrl.ToString();
        }

        public List<T> Get(string uri)
        {
            var request = new RestRequest(uri, Method.GET);
            var response = _restClient.Execute<List<T>>(request);
            return response.Data;
        }
    }
}