﻿using API_OpeCD.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace API_OpeCD.Utils
{
    public class CedisConnectionUtil
    {

        private string _opecdConnection = ConfigurationManager.ConnectionStrings["OPECDServidoresCnx"].ConnectionString;
        public string GetConnectionStringByCedis(int cedisId)
        {
            string connectionStringFormat = "Data Source = {0}; Initial Catalog = {1}; user id = {2}; password = {3}";
            #region Getting cedis connection data from db
            SqlConnection sqlConnection = new SqlConnection(_opecdConnection);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            cmd.CommandText = "SELECT * FROM biservidores WHERE puntovta = " + cedisId;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection;

            sqlConnection.Open();

            reader = cmd.ExecuteReader();
            // Data is accessible through the DataReader object here.
            if (!reader.HasRows)
                throw new InvalidOperationException("No existen datos de conexion para el cedis con el identificador " + cedisId);
            reader.Read();
            Biservidor biservidor = new Biservidor();
            biservidor.Server = reader["server"].ToString();
            biservidor.DbName = reader["dbname"].ToString();
            biservidor.Usuario = reader["usuario"].ToString();
            biservidor.Password = reader["password"].ToString();

            sqlConnection.Close();
            #endregion
            connectionStringFormat = string.Format(connectionStringFormat, biservidor.Server, biservidor.DbName, biservidor.Usuario, biservidor.Password);
            return connectionStringFormat;
            //return "Data Source = 10.20.128.154; Initial Catalog = opepte_t006104; user id = sa; password = desarrollo";
        }
    }
}