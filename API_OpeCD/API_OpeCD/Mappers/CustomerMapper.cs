﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API_OpeCD.Models;
using API_OpeCD.Models.Entity;

namespace API_OpeCD.Mappers
{
    public class CustomerMapper
    {
        public Customer Map(CAT_Customers catCustomer, Customer customer)
        {
            customer.Cuc = (int)catCustomer.CCL_Cuc;
            customer.BarCode = catCustomer.CCL_BarCode;
            customer.Name = catCustomer.CCL_Name;
            return customer;
        }

        public List<Customer> Map(List<CAT_Customers> catCustomers)
        {
            var customers = new List<Customer>();
            catCustomers.ForEach(catCustomer => customers.Add(Map(catCustomer, new Customer())));
            return customers;
        }
    }
}