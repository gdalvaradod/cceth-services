﻿
using System.Collections.Generic;
using API_OpeCD.Models.Entity;
using API_OpeCD.Models.PrecioClienteCoca;

namespace API_OpeCD.Mappers
{
    public class PrecioClienteCocaMapper
    {
        private static void Map(CustItemPricesOpe custPrice, PrecioClienteCoca precioCliente)
        {
            precioCliente.CedisId = custPrice.CedisId;
            precioCliente.CustBarCode = custPrice.CustBarCode;
            precioCliente.Cuc = custPrice.Cuc.ToString();
            if (custPrice.DeliveryDate != null)
                precioCliente.DeliveryDate = custPrice.DeliveryDate.Value.ToString("yyyy-MM-dd");
            precioCliente.ItemId = custPrice.ItemId;
            precioCliente.Price = custPrice.Price;
            precioCliente.BasePrice = custPrice.BasePrice;
            precioCliente.DiscountAmount = custPrice.Discount;
            precioCliente.DiscountRate = custPrice.DiscountRate;
        }

        public List<PrecioClienteCoca> Map(List<CustItemPricesOpe> precioCliente)
        {
            var preciosList = new List<PrecioClienteCoca>();
            foreach (var item in precioCliente)
            {
                var preClient = new PrecioClienteCoca();
                Map(item, preClient);
                preciosList.Add(preClient);
            }
            
            return preciosList;
        }
    }
}