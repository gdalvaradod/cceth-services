﻿using OpeCDLib.Entities;
using System;
using System.Collections.Generic;
using API_OpeCD.Models.WBC;

namespace API_OpeCD.Mappers
{
    public class SaleMapper
    {
        public Sale Map(WbcSale wbcSale)
        {
            var sale = new Sale();
            sale.DeliveryOrderId = 0;
            sale.SaleDetail = Map(wbcSale.SaleProducts);
            sale.PromotionDetail = Map(wbcSale.SalePromotions);
            sale.CustomerOpeId = wbcSale.Cuc;
            sale.DeliveryDate = new DateTime(wbcSale.DeliveryDate.Year, wbcSale.DeliveryDate.Month, wbcSale.DeliveryDate.Day, 0, 0, 0);
            sale.RouteCode = wbcSale.RouteCode;
            sale.Trip = wbcSale.Trip;
            sale.Precedence = wbcSale.CustomerCCTH;
            sale.CardAmount = wbcSale.CardAmount;
            sale.CardPayment = wbcSale.CardPayment;
            return sale;
        }

        private PromotionItem Map(SalePromotion salePromotion)
        {
            var promotionItem = new PromotionItem();
            promotionItem.ItemId = salePromotion.Product.ToString();
            promotionItem.PromotionId = Convert.ToByte(salePromotion.Type);
            promotionItem.Qty = salePromotion.Quantity;
            return promotionItem;

        }
        private List<PromotionItem> Map(List<SalePromotion> salePromotions)
        {
            var promotionItems = new List<PromotionItem>();
            salePromotions.ForEach(salePromotion => promotionItems.Add(Map(salePromotion)));
            return promotionItems;
        }

        private List<SaleItem> Map(List<SaleProduct> saleProducts)
        {
            var saleItems = new List<SaleItem>();
            saleProducts.ForEach(saleProduct =>
                saleItems.Add(new SaleItem {
                    ItemId = saleProduct.Product.ToString(),
                    Qty = saleProduct.Quantity,
                    Price = saleProduct.Price,
                    BasePrice = saleProduct.BasePrice,
                    DiscountAmount = saleProduct.Discount
                }));
            return saleItems;
        }
    }
}