﻿using System;

namespace CCETH091703.Base
{
    public class ApiIntegracionesCnx
    {
        public static string GetOrdenGeneral(int status,string cedi, string ruta)
        {
            return "/api/OrdenOpeCd/GetOrders?Status=" + status+"&cedis="+cedi+"&ruta="+ruta;
        }

        public static string PutOrderGeneral(int accion)
        {
            return "/api/OrdenOpeCd/PutOrder/" + accion;
        }

        public static string GetConfigAuto(int cediId, int ruta)
        {
            return "/api/OrdenOpeCd/GetConfigAuto?cedis=" + cediId + "&route=" + ruta;
        }

        public static string PutStatusAuto(int status, int cediId, int ruta)
        {
            return "/api/OrdenOpeCd/PutStatusAuto?Status="+ status +"&cedi=" + cediId + "&route=" + ruta;
        }

        public static string GetCedisRutasInventarioWbc()
        {
            return "/api/OrdenWBC/CedisInfo";
        }
        public static string TripInventoryIntegra()
        {
            return "/api/inventario";
        }
        public static string GetEstadoJornada(int cedisId, int route, string deliveryDate)
        {
            return "/api/jornadasFinalizadas?cedisId=" + cedisId + "&routeId=" + route + "&deliveryDate=" + deliveryDate;
        }

        public static string GetTripInventoryIntegra(int cedid, string routecode, DateTime deliverydate, bool withsales = true)
        {
            return "/api/inventario?cedisID=" + cedid + "&routeID=" + routecode + "&deliveryDate=" +
                   deliverydate.ToString("yyyy-MM-dd") + "&withsales=" + withsales;
        }

        public static string GetSalesIntegra(DateTime deliveryDate,int cedisId, int route)
        {
            return "/api/VentasWBC/IntegrationSales?cedisId=" + cedisId + "&routeId=" + route + "&deliveryDate=" +
                   deliveryDate.ToString("yyyy-MM-dd"); 
        }

        public static string GetClienteGenerico(int branchId, int routeId)
        {
            return "/api/OrdenWBC/GetCucGeneric?cedis=" + branchId + "&route=" + routeId;
        }

        public static string PostLogBitacora()
        {
            return "/api/Log";
        }

        public static string PostLogBitacoraDetail()
        {
            return "/api/LogDetail";
        }

        public static string GetLogBitacora(int cedisId, int ruta, int? viaje, string deliveryDate, int estatus, bool groupedByCedis)
        {
            const string cad = "/api/Log";
            var filtros = "";
            if (string.IsNullOrEmpty(filtros))
            {
                filtros += "?groupedByCedis=" + groupedByCedis;
            }
            else
            {
                filtros += "&groupedByCedis=" + groupedByCedis;
            }
            if (string.IsNullOrEmpty(filtros))
            {
                if (cedisId > 0)
                {
                    filtros += "?cedisId=" + cedisId;
                }
            }
            else
            {
                if (cedisId > 0)
                {
                    filtros += "&cedisId=" + cedisId;
                }
            }
            if (string.IsNullOrEmpty(filtros))
            {
                if (ruta > 0)
                {
                    filtros += "?routeId=" + ruta;
                }
            }
            else
            {
                if (ruta > 0)
                {
                    filtros += "&routeId=" + ruta;
                }
            }
            if (string.IsNullOrEmpty(filtros))
            {
                if (viaje > 0)
                {
                    filtros += "?trip=" + viaje;
                }
            }
            else
            {
                if (viaje > 0)
                {
                    filtros += "&trip=" + viaje;
                }
            }
            if (string.IsNullOrEmpty(filtros))
            {
                if (!string.IsNullOrEmpty(deliveryDate))
                {
                    filtros += "?deliveryDate=" + deliveryDate;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(deliveryDate))
                {
                    filtros += "&deliveryDate=" + deliveryDate;
                }
            }
            if (string.IsNullOrEmpty(filtros))
            {
                if (estatus > 0)
                {
                    filtros += "?processSatus=" + estatus;
                }
            }
            else
            {
                if (estatus > 0)
                {
                    filtros += "&processSatus=" + estatus;
                }
            }
            return cad + filtros;
        }

        public static string PutLogBitacora(int logBitacoraId)
        {
            return "/api/Log/" + logBitacoraId;
        }

        public static string GetCustomerRelation(int cedi, string crmid = "", string barcode = "", int cuc = 0)
        {
            crmid = crmid ?? "";
            barcode = barcode ?? "";
            
            const string cad = "/api/Customer/GetSearchFilterRelation";
            var filtros = "?cedi=" + cedi;
            filtros =filtros+ (crmid != "" ? "&crmid=" + crmid:"");
            filtros = filtros + (barcode != "" ? "&barcode=" + barcode : "");
            filtros = filtros + (cuc != 0 ? "&cuc=" + cuc : "");
            return cad + filtros;
        }

        public static string GetPrecioClienteCoca(int cedi, string barcode, string deliveryDate)
        {
            var filtros = "?cedisId=" + cedi;
            filtros = filtros + (barcode != "" ? "&barcode=" + barcode : "");
            filtros = filtros + (deliveryDate != "" ? "&deliveryDate=" + deliveryDate : "");
            return "/api/PrecioClienteCoca" + filtros;
        }

        public static string GetOrdenGeneralByCliente(string barcode)
        {
            return "/api/OrdenOpeCd/GetOrderCliente/" + barcode;
        }

        public static string PostCancelUpdateOrderInventory()
        {
            return "/api/Inventario/CancelUpdateOrderInventory";
        }
    }
}
