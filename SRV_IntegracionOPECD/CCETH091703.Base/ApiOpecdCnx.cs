﻿namespace CCETH091703.Base
{
    public class ApiOpecdCnx
    {
        //public static string GetOrderGeneral(int code, int cedisId)
        //{
        //    return string.Concat("/api/OrderGeneral/", code, "&cedisId=", cedisId);
        //}

        public static string GetValidation(string ids, int? cedisId)
        {
            return string.Concat("/api/OrderGeneral/GetValidation?ids=", ids, "&cedisId=", cedisId);
        }

        //public static string GetConfirmation(int route, int? cedisId)
        //{
        //    return string.Concat("/api/OrderGeneral/GetConfirmation?route=", route, "&cedisId=", cedisId);
        //}

        public static string GetConfirmation(string ids, int? cedisId)
        {
            return string.Concat("/api/OrderGeneral/GetConfirmation?ids=", ids, "&cedisId=", cedisId);
        }

        public static string PostOrderGeneral(int? cedisId)
        {
            return "/api/OrderGeneral?cedisId=" + cedisId;
        }

        public static string PostSalesOpeCd(int cedisId)
        {
            return "/api/Sales?cedisId=" + cedisId;
        }

        public static string TripInventory()
        {
            return "/api/Inventario/TripInventory";
        }

    }
}
