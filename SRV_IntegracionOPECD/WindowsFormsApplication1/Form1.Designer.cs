﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_pedidos = new System.Windows.Forms.Button();
            this.btn_confirmados = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnAutomaticos = new System.Windows.Forms.Button();
            this.btnInventarios = new System.Windows.Forms.Button();
            this.btnVentas = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_pedidos
            // 
            this.btn_pedidos.Location = new System.Drawing.Point(12, 258);
            this.btn_pedidos.Name = "btn_pedidos";
            this.btn_pedidos.Size = new System.Drawing.Size(75, 23);
            this.btn_pedidos.TabIndex = 0;
            this.btn_pedidos.Text = "Pedidos";
            this.btn_pedidos.UseVisualStyleBackColor = true;
            this.btn_pedidos.Click += new System.EventHandler(this.btn_pedidos_Click);
            // 
            // btn_confirmados
            // 
            this.btn_confirmados.Location = new System.Drawing.Point(93, 258);
            this.btn_confirmados.Name = "btn_confirmados";
            this.btn_confirmados.Size = new System.Drawing.Size(75, 23);
            this.btn_confirmados.TabIndex = 1;
            this.btn_confirmados.Text = "Confirmados";
            this.btn_confirmados.UseVisualStyleBackColor = true;
            this.btn_confirmados.Click += new System.EventHandler(this.btn_confirmados_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(425, 240);
            this.textBox1.TabIndex = 2;
            // 
            // btnAutomaticos
            // 
            this.btnAutomaticos.Location = new System.Drawing.Point(174, 258);
            this.btnAutomaticos.Name = "btnAutomaticos";
            this.btnAutomaticos.Size = new System.Drawing.Size(75, 23);
            this.btnAutomaticos.TabIndex = 3;
            this.btnAutomaticos.Text = "Automaticos";
            this.btnAutomaticos.UseVisualStyleBackColor = true;
            this.btnAutomaticos.Click += new System.EventHandler(this.btnAutomaticos_Click);
            // 
            // btnInventarios
            // 
            this.btnInventarios.Location = new System.Drawing.Point(256, 258);
            this.btnInventarios.Name = "btnInventarios";
            this.btnInventarios.Size = new System.Drawing.Size(99, 37);
            this.btnInventarios.TabIndex = 4;
            this.btnInventarios.Text = "Invetarios ope-Integra";
            this.btnInventarios.UseVisualStyleBackColor = true;
            this.btnInventarios.Click += new System.EventHandler(this.btnInventarios_Click);
            // 
            // btnVentas
            // 
            this.btnVentas.Location = new System.Drawing.Point(362, 258);
            this.btnVentas.Name = "btnVentas";
            this.btnVentas.Size = new System.Drawing.Size(75, 23);
            this.btnVentas.TabIndex = 5;
            this.btnVentas.Text = "Ventas";
            this.btnVentas.UseVisualStyleBackColor = true;
            this.btnVentas.Click += new System.EventHandler(this.btnVentas_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 318);
            this.Controls.Add(this.btnVentas);
            this.Controls.Add(this.btnInventarios);
            this.Controls.Add(this.btnAutomaticos);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btn_confirmados);
            this.Controls.Add(this.btn_pedidos);
            this.Name = "Form1";
            this.Text = "Servicio IntegracionOPE";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_pedidos;
        private System.Windows.Forms.Button btn_confirmados;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnAutomaticos;
        private System.Windows.Forms.Button btnInventarios;
        private System.Windows.Forms.Button btnVentas;
    }
}

