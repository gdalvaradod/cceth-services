﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Windows.Forms;
using CCETH091703.Model.BR;
using System.Configuration;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using CCETH091703.Model.Services;
using GrupoGitLib.Model.Objects.Enum;
using Newtonsoft.Json;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            //IntegracionCtlr.ActualizaConfig();

            LogServicioBr.EscribirLog(AppSettings()["UsandoApisLog"],
                DateTime.Now.ToString(CultureInfo.InvariantCulture) + Environment.NewLine +
                ": Api_Integracion=" + AppSettings()["URL_INTEGRACION_API"] + Environment.NewLine +
                ": Api_OPE=" + AppSettings()["URL_OPE_API"] + Environment.NewLine);
        }

        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        private void btn_pedidos_Click(object sender, EventArgs e)
        {
            try
            {
                String cedi = "97";
                string ruta = "450";
                LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Consultando Pedido.\n", true);
                var resultGet = OrderIntegracionCtrl.GetOrder(1,cedi,ruta);
                if (resultGet.ResultType != ResultTypes.Success)
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + resultGet.ResultType + "_ConsultarPedidos: " + resultGet.Mensaje + ".\n", true);
                    return;
                }
                var orderGeneralList = resultGet.Objeto;
                var pedidosGroup = (from m in orderGeneralList
                    where m.BottlerData.CediId.HasValue 
                    //&& m.BottlerData.DeliveryDate.Substring(0,10) == DateTime.Now.ToString("dd/MM/yyyy")
                    group m by m.BottlerData.CediId);
                foreach (var item in pedidosGroup)
                {
                    EnviarPedidos(JsonConvert.SerializeObject(item.ToList()), item.Key);
                }
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": ConsultaPedidos: " + ex.Message + ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        private static async void EnviarPedidos(string json, int? cedisId)
        {
            await Task.Run(() =>
            {
                LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Guardando Pedidos de Cedis: " + cedisId + ".\n", true);
                var getPedidos = OrderOpecdCtrl.PostOrder(json, cedisId);
                if (getPedidos.ResultType == ResultTypes.Success)
                {
                    getPedidos.Objeto.RemoveAll(x => x.DeliveryOrderId <= 0);
                    LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Guardando pedidos en Integracion.\n", true);
                    var resultViaje = OrderIntegracionCtrl.PutOrder(JsonConvert.SerializeObject(getPedidos.Objeto), 1);
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + resultViaje.ResultType + "_ConsultarPedidos: " + resultViaje.Mensaje + ".\n", true);

                    if (resultViaje.ResultType == ResultTypes.Success)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Pedidos publicados.\n", true);
                    }
                    else
                    {
                        LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Problema al guardar pedidos en Integración.\n", true);
                    }
                }
                else
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + getPedidos.ResultType + "_ConsultarPedidos: " + getPedidos.Mensaje + ".\n", true);
                }
            });
        }

        private void btn_confirmados_Click(object sender, EventArgs e)
        {
            try
            {
                var cedi = "97";
                var ruta = "450";
                var resultGet = OrderIntegracionCtrl.GetOrder(2,cedi,ruta);
                if (resultGet.ResultType != ResultTypes.Success)
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + resultGet.ResultType + "_ConsultarConfirmados: " + resultGet.Mensaje + ".\n", true);
                    return;
                }
                var objeto = resultGet.Objeto;
                var pedidos = (from m in objeto
                               where m.BottlerData.CediId.HasValue
                               group m by m.BottlerData.CediId).ToList();
                foreach (var current in pedidos)
                {
                    var confirmation = OrderOpecdCtrl.GetConfirmation(JsonConvert.SerializeObject(current.ToList()), current.Key);
                    if (confirmation.ResultType == ResultTypes.Success)
                    {
                        var res = OrderIntegracionCtrl.PutOrder(JsonConvert.SerializeObject(confirmation.Objeto), 0);
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + res.ResultType + "_ValidarConfirmados: " + res.Mensaje + ".\n", true);
                    }
                    else
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + confirmation.ResultType + "_ConsultarConfirmados: " + confirmation.Mensaje + ".\n", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": ConsultaConfirmados: " + ex.Message + ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        private void btnAutomaticos_Click(object sender, EventArgs e)
        {
            try
            {
                //var result = OrderIntegracionCtrl.ObtenCedisRutasIntegracion();
                //if (result.ResultType == ResultTypes.Error || result.Objeto == null || result.Objeto.Count == 0)
                //{
                //    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                //        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                //        ": Error_ObtenCedisRutasIntegracion: no se obtuvieron Cedis-Ruta de Integración.\n", true);
                //    return;
                //}
                //foreach (var cedi in result.Objeto)
                //{
                //    var resultGet = OrderIntegracionCtrl.GetConfigAuto(cedi.Cedi);
                //    if (resultGet.ResultType != ResultTypes.Success)
                //    {
                //        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                //            DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + resultGet.ResultType +
                //            "_ConsultarConfiguracion: " + resultGet.Mensaje + ".\n", true);
                //        return;
                //    }

                //    var res = OrderIntegracionCtrl.PutStatusAuto(JsonConvert.SerializeObject(resultGet.Objeto), cedi.Cedi);
                //    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                //        DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + res.ResultType +
                //        "_ActualizarStatus: " + res.Mensaje + ".\n", true);
                //}

                var result = OrderIntegracionCtrl.ObtenCedisRutasIntegracion();
                if (result.ResultType == ResultTypes.Error || result.Objeto == null || result.Objeto.Count == 0)
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Error_ObtenCedisRutasIntegracion: no se obtuvieron Cedis-Ruta de Integración.\n", true);
                    return;
                }
                foreach (var cedi in result.Objeto)
                {
                    foreach (var cediDestino in cedi.Destinos)
                    {
                        var resultGet = OrderIntegracionCtrl.GetConfigAuto(cedi.Cedi, cediDestino.RouteCodeOpecd);
                        if (resultGet.ResultType != ResultTypes.Success)
                        {
                            if (resultGet.ResultType == ResultTypes.Error)
                            {
                                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                   DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + resultGet.ResultType +
                                   "_ConsultarConfiguracion: " + resultGet.Mensaje + ".\n", true);
                            }
                            continue;
                        }
                        var configuracionAutomatica = resultGet.Objeto;

                        var horaActualiza = TimeSpan.Parse(configuracionAutomatica.Hora);

                        if (TimeSpan.Parse(DateTime.Now.ToString("HH:mm")).Ticks < horaActualiza.Ticks) continue;

                        var res = OrderIntegracionCtrl.PutStatusAuto(JsonConvert.SerializeObject(configuracionAutomatica), cedi.Cedi, cediDestino.RouteCodeOpecd);
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + res.ResultType +
                            "_ActualizarStatus: " + res.Mensaje + ".\n", true);

                        OrderIntegracionCtrl.EnviaPedidosOpe(cedi.Cedi, cediDestino.RouteCodeOpecd);
                    }
                }
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": ConfirmacionAutomatica: " + ex.Message + ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        private void btnInventarios_Click(object sender, EventArgs e)
        {
            try
            {
                //EnviarInventariosAsync();
                //InventoryCtrl.ProcesaInventariosOpeIntegraAllAsync();
                InventoryCtrl.ProcesaInventariosOpeIntegra(97,"469");

            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": ConsultaPedidos: " + ex.Message + ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        private Task _taskInventarios;
        protected async void EnviarInventariosAsync()
        {
            if (_taskInventarios != null)
            {
                if (!_taskInventarios.IsCompleted) return;
                LogServicioBr.EscribirLog(AppSettings()["InventoryLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Termino proceso de inventarios Integra-Wbc", true);  // *** Monitor Customer Log
                await (_taskInventarios = Task.Run(() =>
                {
                    try
                    {
                        InventoryCtrl.ProcesaInventariosOpeIntegraAllAsync();
                    }
                    catch (Exception ex)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_ConsultarVentas",
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""),
                            true);
                        Console.WriteLine(ex);
                    }

                }));
            }
            else
            {
                await (_taskInventarios = Task.Run(() =>
                {
                    try
                    {
                        InventoryCtrl.ProcesaInventariosOpeIntegraAllAsync();
                    }
                    catch (Exception ex)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_ConsultarVentas",
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""),
                            true);
                        Console.WriteLine(ex);
                    }
                }));
            }
        }

        private void btnVentas_Click(object sender, EventArgs e)
        {
            try
            {
                var result = VentasCtrl.EnviarVentasAll(97, 508, new StringBuilder(""));
                textBox1.Text = result.Mensaje;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }
    }
}