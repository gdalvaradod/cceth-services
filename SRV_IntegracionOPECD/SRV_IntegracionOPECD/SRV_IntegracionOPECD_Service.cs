﻿using CCETH091703.Model.BR;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Timers;
using Timer = System.Timers.Timer;
using CCETH091703.Model.Services;
using GrupoGitLib.Model.Objects.Enum;
using Newtonsoft.Json;

namespace SRV_IntegracionOPECD
{
    public partial class SRV_IntegracionOPECD_Service : ServiceBase
    {
        //private ConfiguracionAutomatica _configuracionAutomatica;
        //private static Task _taskPedidos;
        //private static Task _taskConfirmados;
        //private Timer _tmrPedidos;
        //private Timer _tmrConfirmados;
        private Timer _tmrActualizarPreparados;
        private Timer _tmrInventarios;
        
        public SRV_IntegracionOPECD_Service()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                IntegracionCtlr.ActualizaConfig();

                LogServicioBr.EscribirLog(AppSettings()["UsandoApisLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + Environment.NewLine +
                    ": Api_Integracion=" + AppSettings()["URL_INTEGRACION_API"] + Environment.NewLine +
                    ": Api_OPE=" + AppSettings()["URL_OPE_API"] + Environment.NewLine);


                //IniciaTimerPedidos();
                //IniciaTimerConfirmados();
                IniciaTimerPreparados();
                IniciaTimerInventarios();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": OnStart: " + ex.Message + ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        protected override void OnStop()
        {
            try
            {
                //StopTimerPedidos();
                //StopTimerConfirmados();
                StopTimerPreparados();
                StopTimerInventarios();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": OnStop: " + ex.Message + ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        //#region TimerInventarios
        protected void IniciaTimerInventarios()
        {
            try
            {
                _tmrInventarios = new Timer { Interval = int.Parse(AppSettings()["InventarioTimer"]) };
                _tmrInventarios.Elapsed += OnTimerInventarios;
                _tmrInventarios.Start();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": ConsultaConfirmados: " + ex.Message + ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        protected void StopTimerInventarios()
        {
            _tmrInventarios.Stop();
            _tmrInventarios = null;
        }

        protected void OnTimerInventarios(object sender, ElapsedEventArgs args)
        {
            try
            {
                EnviarInventariosAsync();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": ConsultaConfirmados: " + ex.Message + ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        private Task _taskInventarios;
        protected async void EnviarInventariosAsync()
        {
            if (_taskInventarios != null)
            {
                if (!_taskInventarios.IsCompleted) return;
                LogServicioBr.EscribirLog(AppSettings()["InventoryLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Termino proceso de Inventarios Ope-Integra", true);  // *** Monitor Customer Log
                await (_taskInventarios = Task.Run(() =>
                {
                    try
                    {
                        InventoryCtrl.ProcesaInventariosOpeIntegraAllAsync();
                    }
                    catch (Exception ex)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_ConsultarVentas",
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""),
                            true);
                        Console.WriteLine(ex);
                    }

                }));
            }
            else
            {
                await (_taskInventarios = Task.Run(() =>
                {
                    try
                    {
                        InventoryCtrl.ProcesaInventariosOpeIntegraAllAsync();
                    }
                    catch (Exception ex)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_ConsultarVentas",
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""),
                            true);
                        Console.WriteLine(ex);
                    }
                }));
            }
        }
        //#Endregion  


        #region TimerPedidos

        //protected void IniciaTimerPedidos()
        //{
        //    try
        //    {
        //        _tmrPedidos = new Timer { Interval = int.Parse(AppSettings()["PedidosTimer"]) };
        //        _tmrPedidos.Elapsed += OnTimerPedidos;
        //        _tmrPedidos.Start();
        //    }
        //    catch (Exception ex)
        //    {
        //        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
        //            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
        //            ": ConsultaPedidos: " + ex.Message + ".\n" + ex.StackTrace + ".\n", true);
        //    }
        //}

        //protected void StopTimerPedidos()
        //{
        //    _tmrPedidos.Stop();
        //    _tmrPedidos = null;
        //}

        ////protected void OnTimerPedidos(object sender, ElapsedEventArgs args)
        ////{
        ////    try
        ////    {
        ////        ConsultarPedidos();
        ////        //var resultGet = OrderIntegracionCtrl.GetOrder(1);
        ////        //if (resultGet.ResultType == ResultTypes.Error)
        ////        //{
        ////        //    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + resultGet.ResultType + "_ConsultarPedidos: " + resultGet.Mensaje + ".\n", true);
        ////        //    return;
        ////        //}
        ////        //var orderGeneralList = resultGet.Objeto;
        ////        //var pedidosGroup = (from m in orderGeneralList
        ////        //                    where m.BottlerData.CediId.HasValue && m.BottlerData.DeliveryDate.Substring(0, 10) == DateTime.Now.ToString("dd/MM/yyyy")
        ////        //                    group m by m.BottlerData.CediId).ToList();
        ////        //foreach (var item in pedidosGroup)
        ////        //{
        ////        //    EnviarPedidos(JsonConvert.SerializeObject(item.ToList()), item.Key);
        ////        //}
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
        ////            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
        ////            ": ConsultaPedidos: " + ex.Message + ".\n" + ex.StackTrace + ".\n", true);
        ////    }
        ////}

        ////private static async void ConsultarPedidos()
        ////{
        ////    if (_taskPedidos != null)
        ////    {
        ////        if (_taskPedidos.IsCompleted)
        ////        {
        ////            await (
        ////                _taskPedidos = Task.Run(() =>
        ////                {
        ////                    try
        ////                    {


        ////                        LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Consultando Pedido.\n", true);
        ////                        var resultGet = OrderIntegracionCtrl.GetOrder(1);
        ////                        if (resultGet.ResultType == ResultTypes.Error)
        ////                        {
        ////                            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + resultGet.ResultType + "_ConsultarPedidos: " + resultGet.Mensaje + ".\n", true);
        ////                            return;
        ////                        }
        ////                        if (resultGet.Objeto == null) return;
        ////                        var orderGeneralList = resultGet.Objeto;
        ////                        var pedidosGroup = (from m in orderGeneralList
        ////                                            where m.BottlerData.CediId.HasValue && m.BottlerData.DeliveryDate.Substring(0, 10) == DateTime.Now.ToString("dd/MM/yyyy")
        ////                                            group m by m.BottlerData.CediId);
        ////                        foreach (var item in pedidosGroup)
        ////                        {
        ////                            EnviarPedidos(JsonConvert.SerializeObject(item.ToList()), item.Key);
        ////                        }
        ////                    }
        ////                    catch (Exception ex)
        ////                    {
        ////                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
        ////                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " +
        ////                            ex.Message.Replace(",", ""),
        ////                            true);
        ////                    }
        ////                })
        ////            );
        ////        }
        ////    }
        ////    else
        ////    {
        ////        await (
        ////            _taskPedidos = Task.Run(() =>
        ////            {
        ////                try
        ////                {
        ////                    LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Consultando Pedido.\n", true);
        ////                    var resultGet = OrderIntegracionCtrl.GetOrder(1);
        ////                    if (resultGet.ResultType == ResultTypes.Error)
        ////                    {
        ////                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + resultGet.ResultType + "_ConsultarPedidos: " + resultGet.Mensaje + ".\n", true);
        ////                        return;
        ////                    }
        ////                    if (resultGet.Objeto == null) return;
        ////                    var orderGeneralList = resultGet.Objeto;
        ////                    var pedidosGroup = (from m in orderGeneralList
        ////                                        where m.BottlerData.CediId.HasValue && m.BottlerData.DeliveryDate.Substring(0, 10) == DateTime.Now.ToString("dd/MM/yyyy")
        ////                                        group m by m.BottlerData.CediId);
        ////                    foreach (var item in pedidosGroup)
        ////                    {
        ////                        EnviarPedidos(JsonConvert.SerializeObject(item.ToList()), item.Key);
        ////                    }
        ////                }
        ////                catch (Exception ex)
        ////                {
        ////                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
        ////                        DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + ex.Message.Replace(",", ""),
        ////                        true);
        ////                }
        ////            })
        ////        );
        ////    }
        ////}

        ////private static async void EnviarPedidos(string json, int? cedisId)
        ////{
        ////    await Task.Run(() =>
        ////    {
        ////        LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Guardando Pedidos de Cedis: " + cedisId + ".\n", true);
        ////        var getPedidos = OrderOpecdCtrl.PostOrder(json, cedisId);
        ////        switch (getPedidos.ResultType)
        ////        {
        ////            case ResultTypes.Success:
        ////                LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Guardando pedidos en Integracion.\n", true);
        ////                var resultViaje = OrderIntegracionCtrl.PutOrder(JsonConvert.SerializeObject(getPedidos.Objeto), 1);
        ////                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + resultViaje.ResultType + "_ActualizarPedidosOpeId: " + resultViaje.Mensaje + ".\n", true);
        ////                if (resultViaje.ResultType == ResultTypes.Success)
        ////                {
        ////                    LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Pedidos publicados.\n", true);
        ////                }
        ////                else
        ////                {
        ////                    LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Problema al guardar pedidos en Integración.\n", true);
        ////                }
        ////                break;
        ////            case ResultTypes.Error:
        ////                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + getPedidos.ResultType + "_EnviarPedidosOpe: " + getPedidos.Mensaje + ".\n", true);
        ////                break;
        ////            case ResultTypes.Warning:
        ////                break;
        ////        }
        ////    });
        ////}

        #endregion

        #region TimerConfirmados

        //protected void IniciaTimerConfirmados()
        //{
        //    try
        //    {
        //        _tmrConfirmados = new Timer { Interval = int.Parse(AppSettings()["ConfirmadosTimer"]) };
        //        _tmrConfirmados.Elapsed += OnTimerConfirmados;
        //        _tmrConfirmados.Start();
        //    }
        //    catch (Exception ex)
        //    {
        //        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
        //            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
        //            ": ConsultaConfirmados: " + ex.Message + ".\n" + ex.StackTrace + ".\n", true);
        //    }
        //}

        //protected void StopTimerConfirmados()
        //{
        //    _tmrConfirmados.Stop();
        //    _tmrConfirmados = null;
        //}

        //protected void OnTimerConfirmados(object sender, ElapsedEventArgs args)
        //{
        //    try
        //    {
        //        ConsultarConfirmados();
        //    }
        //    catch (Exception ex)
        //    {
        //        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
        //            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
        //            ": ConsultaConfirmados: " + ex.Message + ".\n" + ex.StackTrace + ".\n", true);
        //    }
        //}

        //private static async void ConsultarConfirmados()
        //{
        //    if (_taskConfirmados != null)
        //    {
        //        if (_taskConfirmados.IsCompleted)
        //        {
        //            await (
        //                _taskConfirmados = Task.Run(() =>
        //                {
        //                    try
        //                    {
        //                        var resultGet = OrderIntegracionCtrl.GetOrder(2);
        //                        if (resultGet.ResultType == ResultTypes.Error)
        //                        {
        //                            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + resultGet.ResultType + "_ConsultarConfirmados: " + resultGet.Mensaje + ".\n", true);
        //                            return;
        //                        }
        //                        if (resultGet.Objeto == null) return;
        //                        var objeto = resultGet.Objeto;
        //                        var pedidos = (from m in objeto
        //                                       where m.BottlerData.CediId.HasValue
        //                                       group m by m.BottlerData.CediId).ToList();
        //                        foreach (var current in pedidos)
        //                        {
        //                            var confirmation = OrderOpecdCtrl.GetConfirmation(JsonConvert.SerializeObject(current.ToList()), current.Key);
        //                            switch (confirmation.ResultType)
        //                            {
        //                                case ResultTypes.Success:
        //                                    var res = OrderIntegracionCtrl.PutOrder(JsonConvert.SerializeObject(confirmation.Objeto), 0);
        //                                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + res.ResultType + "_ValidarConfirmados: " + res.Mensaje + ".\n", true);
        //                                    break;
        //                                case ResultTypes.Error:
        //                                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + confirmation.ResultType + "_ConsultarConfirmados: " + confirmation.Mensaje + ".\n", true);
        //                                    break;
        //                                case ResultTypes.Warning:
        //                                    break;
        //                            }
        //                        }
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
        //                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " +
        //                            ex.Message.Replace(",", ""),
        //                            true);
        //                    }
        //                })
        //            );
        //        }
        //    }
        //    else
        //    {
        //        await (
        //            _taskConfirmados = Task.Run(() =>
        //            {
        //                try
        //                {
        //                    var resultGet = OrderIntegracionCtrl.GetOrder(2);
        //                    if (resultGet.ResultType == ResultTypes.Error)
        //                    {
        //                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + resultGet.ResultType + "_ConsultarConfirmados: " + resultGet.Mensaje + ".\n", true);
        //                        return;
        //                    }
        //                    if (resultGet.Objeto == null) return;
        //                    var objeto = resultGet.Objeto;
        //                    var pedidos = (from m in objeto
        //                                   where m.BottlerData.CediId.HasValue
        //                                   group m by m.BottlerData.CediId).ToList();
        //                    foreach (var current in pedidos)
        //                    {
        //                        var confirmation = OrderOpecdCtrl.GetConfirmation(JsonConvert.SerializeObject(current.ToList()), current.Key);
        //                        switch (confirmation.ResultType)
        //                        {
        //                            case ResultTypes.Success:
        //                                var res = OrderIntegracionCtrl.PutOrder(JsonConvert.SerializeObject(confirmation.Objeto), 0);
        //                                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + res.ResultType + "_ValidarConfirmados: " + res.Mensaje + ".\n", true);
        //                                break;
        //                            case ResultTypes.Error:
        //                                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + confirmation.ResultType + "_ConsultarConfirmados: " + confirmation.Mensaje + ".\n", true);
        //                                break;
        //                            case ResultTypes.Warning:
        //                                break;
        //                        }
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
        //                        DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + ex.Message.Replace(",", ""),
        //                        true);
        //                }
        //            })
        //        );
        //    }
        //}
        #endregion

        #region TimerPreparados

        protected void IniciaTimerPreparados()
        {
            try
            {
                while (DateTime.Now.Second > 0)
                {
                }
                _tmrActualizarPreparados = new Timer { Interval = 60000 };
                _tmrActualizarPreparados.Elapsed += OnTimerPreparados;
                _tmrActualizarPreparados.Start();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": ActualizaPreparados: " + ex.Message + ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        protected void StopTimerPreparados()
        {
            _tmrActualizarPreparados.Stop();
            _tmrActualizarPreparados = null;
        }

        protected void OnTimerPreparados(object sender, ElapsedEventArgs args)
        {
            try
            {
                var result = OrderIntegracionCtrl.ObtenCedisRutasIntegracion();
                if (result.ResultType == ResultTypes.Error || result.Objeto == null || result.Objeto.Count == 0)
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Error_ObtenCedisRutasIntegracion: no se obtuvieron Cedis-Ruta de Integración.\n", true);
                    return;
                }
                foreach (var cedi in result.Objeto)
                {
                    //if (_configuracionAutomatica == null)
                    //{
                        //var resultGet = OrderIntegracionCtrl.GetConfigAuto(cedi.Cedi);
                        //if (resultGet.ResultType != ResultTypes.Success)
                        //{
                        //    if (resultGet.ResultType == ResultTypes.Error)
                        //    {
                        //        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        //           DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + resultGet.ResultType +
                        //           "_ConsultarConfiguracion: " + resultGet.Mensaje + ".\n", true);
                        //    }
                        //    return;
                        //}
                        //_configuracionAutomatica = resultGet.Objeto;
                    //}
                    //else
                    //{
                    foreach (var cediDestino in cedi.Destinos)
                    {
                        var resultGet = OrderIntegracionCtrl.GetConfigAuto(cedi.Cedi, cediDestino.RouteCodeOpecd);
                        if (resultGet.ResultType != ResultTypes.Success)
                        {
                            if (resultGet.ResultType == ResultTypes.Error)
                            {
                                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                   DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + resultGet.ResultType +
                                   "_ConsultarConfiguracion: " + resultGet.Mensaje + ".\n", true);
                            }
                            continue;
                        }
                        var configuracionAutomatica = resultGet.Objeto;

                        var horaActualiza = TimeSpan.Parse(configuracionAutomatica.Hora);

                        if (TimeSpan.Parse(DateTime.Now.ToString("HH:mm")).Ticks < horaActualiza.Ticks) continue;

                        var res = OrderIntegracionCtrl.PutStatusAuto(JsonConvert.SerializeObject(configuracionAutomatica), cedi.Cedi, cediDestino.RouteCodeOpecd);
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + res.ResultType +
                            "_ActualizarStatus: " + res.Mensaje + ".\n", true);
                        OrderIntegracionCtrl.EnviaPedidosOpe(cedi.Cedi, cediDestino.RouteCodeOpecd);
                        //var resultGet = OrderIntegracionCtrl.GetConfigAuto(cedi.Cedi);
                        //if (resultGet.ResultType != ResultTypes.Success)
                        //{
                        //    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        //        DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + resultGet.ResultType +
                        //        "_ConsultarConfiguracion: " + resultGet.Mensaje + ".\n", true);
                        //    return;
                        //}
                        //_configuracionAutomatica = resultGet.Objeto;
                    }

                }
                //}
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": ConfirmacionAutomatica: " + ex.Message + ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        #endregion

        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }
    }
}
