﻿using System.ServiceProcess;

namespace SRV_IntegracionOPECD
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main()
        {
            var servicesToRun = new ServiceBase[]
            {
                new SRV_IntegracionOPECD_Service()
            };
            ServiceBase.Run(servicesToRun);
        }
    }
}
