﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCETH091703.Model.Objects.BO
{
    public class CedisRutaLog
    {
        public int Cedi { get; set; }
        public List<Ruta> Log { get; set; }
        public class Ruta
        {
            public int RouteCodeOpecd { get; set; }
            private int _enable;
            public int? Enable
            {
                get { return _enable; }
                set { _enable = value ?? 1; }

            }
        }
    }
}
