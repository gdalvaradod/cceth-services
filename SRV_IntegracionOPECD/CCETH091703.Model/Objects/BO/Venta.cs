﻿using System.Collections.Generic;

namespace CCETH091703.Model.Objects.BO
{
    public class Venta
    {
        public List<VentasPromociones> Promociones { get; set; }
        public List<VentasProductos> ProductosVendidos { get; set; }
        public int Cuc { get; set; }
        public int CedisId { get; set; }
        public int RouteCode { get; set; }
        public int Trip { get; set; }
        public string Code { get; set; }
        public string DeliveryDate { get; set; }
        public bool CustomerCCTH { get; set; }
    }
}
