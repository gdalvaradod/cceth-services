﻿using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class OrderComments
    {
        [JsonProperty("entity_id")]
        public int EntityId { get; set; }

        [JsonProperty("parent_id")]
        public int ParentId { get; set; }

        [JsonProperty("is_customer_notified")]
        public string IsCustomerNotified { get; set; }

        [JsonProperty("is_visible_on_front")]
        public string IsVisibleOnFront { get; set; }

        [JsonProperty("comment")]
        public string Comment { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("entity_name")]
        public string EntityName { get; set; }

        [JsonProperty("store_id")]
        public string StoreId { get; set; }
    }
}
