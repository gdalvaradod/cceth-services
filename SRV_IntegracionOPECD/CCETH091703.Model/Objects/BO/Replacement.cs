﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class Replacement
    {
        public Replacement(int repid, decimal repam, string cod)
        {
            ReplacementId = repid;
            Amount = repam;
            Code = cod;
        }

        public Replacement() { }

        [JsonProperty("ReplacementId")]
        public int ReplacementId { get; set; }
        [JsonProperty("Code")]
        public string Code { get; set; }
        //[JsonProperty("Name")]
        //public string Name { get; set; }
        //[JsonProperty("Status")]
        //public bool Status { get; set; }
        [JsonProperty("Amount")]
        public decimal Amount { get; set; }

        ////Datos devueltos por el Stored
        //public string Replacement_Code { get; set; }



    }
}
