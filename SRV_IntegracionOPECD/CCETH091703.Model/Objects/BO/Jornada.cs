﻿using System;

namespace CCETH091703.Model.Objects.BO
{
    public class Jornada
    {
        public Jornada(int cedis, int ruta, DateTime date)
        {
            CedisIdOpeCd = cedis;
            Route = ruta;
            DeliveryDate = date;
        }

        public int CedisIdOpeCd { get; set; }
        public int Route { get; set; }
        public DateTime DeliveryDate { get; set; }
    }
}
