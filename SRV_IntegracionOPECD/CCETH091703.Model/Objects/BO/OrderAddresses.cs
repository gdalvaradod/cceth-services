﻿using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class OrderAddresses
    {
        [JsonProperty("entity_id")]
        public int EntityId { get; set; }

        [JsonProperty("parent_id")]
        public int ParentId { get; set; }

        [JsonProperty("customer_address_id")]
        public int? CustomerAddressId { get; set; }

        [JsonProperty("quote_address_id")]
        public int? QuoteAddressId { get; set; }

        [JsonProperty("region_id")]
        public int? RegionId { get; set; }

        [JsonProperty("fax")]
        public int? Fax { get; set; }

        [JsonProperty("region")]
        public string Region { get; set; }

        [JsonProperty("postcode")]
        public string Postcode { get; set; }

        [JsonProperty("lastname")]
        public string Lastname { get; set; }

        [JsonProperty("street")]
        public string Street { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("telephone")]
        public string Telephone { get; set; }

        [JsonProperty("country_id")]
        public string CountryId { get; set; }

        [JsonProperty("firstname")]
        public string Firstname { get; set; }

        [JsonProperty("address_type")]
        public string AddressType { get; set; }

        [JsonProperty("prefix")]
        public string Prefix { get; set; }

        [JsonProperty("middlename")]
        public string Middlename { get; set; }

        [JsonProperty("suffix")]
        public string Suffix { get; set; }

        [JsonProperty("company")]
        public string Company { get; set; }

        [JsonProperty("vat_id")]
        public string VatId { get; set; }

        [JsonProperty("vat_is_valid")]
        public string VatIsValid { get; set; }

        [JsonProperty("vat_request_id")]
        public string VatRequestId { get; set; }

        [JsonProperty("vat_request_date")]
        public string VatRequestDate { get; set; }

        [JsonProperty("vat_request_success")]
        public string VatRequestSuccess { get; set; }

        [JsonProperty("giftregistry_item_id")]
        public string GiftregistryItemId { get; set; }

        [JsonProperty("want_bill")]
        public string WantBill { get; set; }

        [JsonProperty("rfc")]
        public string Rfc { get; set; }

        [JsonProperty("billing_name")]
        public string BillingName { get; set; }

        [JsonProperty("number")]
        public string Number { get; set; }

        [JsonProperty("number_int")]
        public string NumberInt { get; set; }

        [JsonProperty("neighborhood")]
        public string Neighborhood { get; set; }

        [JsonProperty("municipality")]
        public string Municipality { get; set; }

        [JsonProperty("references")]
        public string References { get; set; }

        [JsonProperty("alias")]
        public string Alias { get; set; }

        [JsonProperty("payment_method")]
        public string PaymentMethod { get; set; }

        [JsonProperty("customer_id")]
        public string CustomerId { get; set; }

    }
}
