﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class OrderResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("Trip")]
        public string Trip { get; set; }

        [JsonProperty("DeliveryOrderId")]
        public long DeliveryOrderId { get; set; }

        [JsonProperty("Products")]
        public List<Product> Product { get; set; }

        [JsonProperty("ConfirmedItems")]
        public List<Product> ProductList { get; set; }

        [JsonProperty("ErrorItemList")]
        public List<ErrorItem> ErrorItemList { get; set; }
    }
}
