﻿namespace CCETH091703.Model.Objects.BO
{
    public class Product
    {
        public string ItemId { get; set; }
        public int Qty { get; set; }
        public string Code => ItemId;
        public int Quantity => Qty;
        public int Type { get; set; }
        public decimal Amount { get; set; }
    }
}
