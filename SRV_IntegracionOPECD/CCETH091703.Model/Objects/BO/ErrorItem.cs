﻿using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class ErrorItem
    {
        [JsonProperty("ErrorNum")]
        public int ErrorNum { get; set; }

        [JsonProperty("ErrorType")]
        public string ErrorType { get; set; }

        [JsonProperty("ErrorDescription")]
        public string ErrorDescription { get; set; }
    }
}
