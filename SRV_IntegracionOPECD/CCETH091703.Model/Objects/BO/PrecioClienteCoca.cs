﻿namespace CCETH091703.Model.Objects.BO
{
    public class PrecioClienteCoca
    {
        public int CedisId { get; set; }
        public string DeliveryDate { get; set; }
        public string CustBarCode { get; set; }
        public int ItemId { get; set; }
        public decimal Price { get; set; }
        public decimal BasePrice { get; set; }
        public decimal Discount { get; set; }
        public decimal DiscountPercent { get; set; }
    }
}
