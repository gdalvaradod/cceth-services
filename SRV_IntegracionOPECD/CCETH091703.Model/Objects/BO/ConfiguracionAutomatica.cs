﻿using System;

namespace CCETH091703.Model.Objects.BO
{
    public class ConfiguracionAutomatica
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Sistema { get; set; }
        public string Hora { get; set; }
        public int Cedis { get; set; }
        public DateTime Modificado { get; set; }
        public int? Status { get; set; }
    }
}
