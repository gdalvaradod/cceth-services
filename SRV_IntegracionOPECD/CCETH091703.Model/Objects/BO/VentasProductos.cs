﻿namespace CCETH091703.Model.Objects.BO
{
    public class VentasProductos
    {
        public int Product { get; set; }
        public int Quantity { get; set; }
        public int CreditAmount { get; set; }
        public decimal Price { get; set; }
        public decimal BasePrice { get; set; }
        public decimal Discount { get; set; }
        public decimal DiscountRate { get; set; }
    }
}
