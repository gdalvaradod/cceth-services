﻿using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class BottlerData
    {
        [JsonProperty("route_id")]
        public int RouteId { get; set; }

        [JsonProperty("website")]
        public string Website { get; set; }

        [JsonProperty("store")]
        public string Store { get; set; }

        [JsonProperty("storeview")]
        public string Storeview { get; set; }

        [JsonProperty("postalcode")]
        public string Postalcode { get; set; }

        [JsonProperty("deliveryco")]
        public string Deliveryco { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("settlement")]
        public string Settlement { get; set; }

        [JsonProperty("delivery_date")]
        public string DeliveryDate { get; set; }

        [JsonProperty("route_code")]
        public string RouteCode { get; set; }

        [JsonProperty("cedi_name")]
        public string CediName { get; set; }
        [JsonProperty("CEDI_ID")]
        public int? CediId { get; set; }
    }
}
