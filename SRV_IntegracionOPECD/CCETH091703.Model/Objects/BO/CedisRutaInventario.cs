﻿using System.Collections.Generic;

namespace CCETH091703.Model.Objects.BO
{
    public class CedisRutaInventario
    {
        public int Cedi { get; set; }
        public List<Ruta> Destinos { get; set; }
        public class Ruta
        {
            public int RouteCodeOpecd { get; set; }
        }
    }
}
