﻿using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class ProcesosSistema
    {
        [JsonProperty("Id")]
        public int Id { get; set; }
        [JsonProperty("Description")]
        public string Description { get; set; }
    }
}
