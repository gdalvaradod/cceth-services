﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Configuration;
using System.Globalization;
using System.Text;
using CCETH091703.Base;
using CCETH091703.Model.BR;
using CCETH091703.Model.DataAccess;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;
using Newtonsoft.Json;

namespace CCETH091703.Model.Services
{
    public static class VentasCtrl
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        public static Result<List<GetSalesResponse>> PostVentasOpeCd(List<Ventas> ventas, int cedisId)
        {
            var result = new Result<List<GetSalesResponse>>();
            try
            {
                var helper = new VentasHelper();
                result =
                    helper.PostVentasOpeCd(
                        AppSettings()["URL_OPE_API"] + ApiOpecdCnx.PostSalesOpeCd(cedisId),
                        int.Parse(AppSettings()["TimeOutCnx"]), JsonConvert.SerializeObject(ventas));
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_VentasCtrl.PostVentasOpeCd: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }

            return result;
        }

        public static Result<object> EnviarVentasAll(int branchCode, int route,StringBuilder logDictionary)
        {
            var resultGral = new Result<object>();
            try
            {
                var date = DateTime.Today;
                //var date = new DateTime(2019, 04, 10);
                // Consultar jornada Finalizada

                logDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                              ": Consultando Jornada Finalizada");
                
                var resultjornada = InventoryCtrl.GetJornadaCerrada(branchCode, route, date);
                if (resultjornada.ResultType == ResultTypes.Error)
                {
                    resultGral.ResultType = resultjornada.ResultType;
                    resultGral.Mensaje = resultjornada.Mensaje;
                    logDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                ": problema al consultar estado de la Jornada.");
                    return resultGral;
                }

                if (resultjornada.Objeto == null || resultjornada.Objeto.Count == 0)
                {
                    resultGral.ResultType = ResultTypes.Success;
                    resultGral.Mensaje = "";
                    logDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                ": Jornada no Finalizada.");
                    return resultGral;
                }

                var inventoryHelper = new InventoryHelper();
                logDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                         ": Obteniendo Inventarios de Integracion.");
                var inventarios =
                    inventoryHelper.TripInventoryIntegracion(
                        AppSettings()["URL_INTEGRACION_API"] +
                        ApiIntegracionesCnx.GetTripInventoryIntegra(branchCode, route.ToString(), date, false),
                        int.Parse(AppSettings()["TimeOutCnx"]));
                if (inventarios.ResultType != ResultTypes.Success)
                {
                    resultGral.ResultType = inventarios.ResultType;
                    resultGral.Mensaje = inventarios.Mensaje;
                    logDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Problema al obtener Inventarios de Integracion.");
                    return resultGral;
                }

                var resultLogBit = LogBitacoraCtrl.GetLogBitacora(branchCode, route, 0, date.ToString("yyyy-MM-dd"), 0);

                if (resultLogBit.ResultType == ResultTypes.Success)
                {
                    var bandTerminado = resultLogBit.Objeto.Any(logbitacora => logbitacora.CedisId == branchCode && logbitacora.RouteId == route && logbitacora.ProcessStatus == 2);
                    if (bandTerminado)
                    {
                        resultGral.ResultType = ResultTypes.Warning;
                        resultGral.Mensaje = "";
                        logDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                    ": Ventas Finalizadas.");
                        return resultGral;
                    }
                }

                logDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                         ": Obteniendo Ventas para el para BranchCode : " +
                                branchCode + ", route : " + route + ", fecha : " + date + ".");
                var ventasHelper = new VentasHelper();
                
                var resultVentas =
                    ventasHelper.GetVentasIntegracion(AppSettings()["URL_INTEGRACION_API"] +
                                                      ApiIntegracionesCnx.GetSalesIntegra(
                                                          date, branchCode, route),
                        int.Parse(AppSettings()["TimeOutCnx"]));

                var customerHelper = new CustomerHelper();
                var resultCustomerIdGen = customerHelper.GetClienteGenerico(
                    AppSettings()["URL_INTEGRACION_API"] +
                    ApiIntegracionesCnx.GetClienteGenerico(branchCode, route),
                    int.Parse(AppSettings()["TimeOutCnx"]));
                if (resultCustomerIdGen.ResultType != ResultTypes.Success)
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Error_CustomerHelper.GetClienteGenerico: No se obtuvo Cliente Genérico para BranchCode : " +
                        branchCode + ", route : " + route, true);
                //    continue;
                }

                foreach (var viaje in inventarios.Objeto)
                {
                    if (viaje.Route != route) continue;
                    var resultLogBitacora = LogBitacoraCtrl.GetLogBitacora(branchCode, route, viaje.Id, date.ToString("yyyy-MM-dd"), 0);
                    if (resultVentas.ResultType != ResultTypes.Success || resultVentas.Objeto == null ||
                        resultVentas.Objeto.Count == 0)
                    {
                        if (resultVentas.ResultType == ResultTypes.Error)
                        {
                            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                ": Error_VentasCtrl.GetVentas: No se obtuvieron ventas para BranchCode : " +
                                branchCode +
                                ", route : " + route + ", fecha : " + date + ",viaje : " + viaje.Id + ".", true);

                            if (resultLogBitacora.ResultType != ResultTypes.Success) continue;
                            {
                                foreach (var logbitacora in resultLogBitacora.Objeto)
                                {
                                    if (logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 11))
                                    {
                                        InventoryCtrl.ActualizaBitacora(logbitacora, 11, 3, true, "No se pudieron obtener las ventas de Integracion." + resultVentas.Mensaje);
                                    }
                                }
                            }
                            logDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                  ": Problema al obtener Ventas.");
                        }
                        else
                        {
                            logDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                  "No se pudieron obtener las ventas de Integracion." + resultVentas.Mensaje);
                        }
                        resultGral.ResultType = resultVentas.ResultType;
                        resultGral.Mensaje = resultVentas.Mensaje;
                        logDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                    "No se pudieron obtener las ventas de Integracion." + resultVentas.Mensaje);
                        return resultGral;
                        //continue;
                    }
                    if (resultLogBitacora.ResultType != ResultTypes.Success) continue;
                    {
                        foreach (var logbitacora in resultLogBitacora.Objeto)
                        {
                            if (logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 11))
                            {
                                InventoryCtrl.ActualizaBitacora(logbitacora, 11, 1, false, "Se obtuvieron correctamente las ventas de Integracion.");
                                logDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                      ": Se obtuvieron correctamente las ventas de Integracion.");
                            }
                            else if (logbitacora.LogBitacoraDetail.Find(x => x.Process.Id == 11 && x.Success) == null)
                            {
                                InventoryCtrl.ActualizaBitacora(logbitacora, 11, 1, false, "Se obtuvieron correctamente las ventas de Integracion.");
                                logDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                      ": Se obtuvieron correctamente las ventas de Integracion.");
                            }
                        }
                    }
                }

                foreach (var ventaviaje in resultVentas.Objeto)
                {
                    var facturado = false;
                    var resultOrdenesCliente = OrderIntegracionCtrl.GetOrderByCliente(ventaviaje.BarCode);
                    if (resultOrdenesCliente.ResultType == ResultTypes.Success)
                    {
                        facturado =
                            resultOrdenesCliente.Objeto.Any(x => x.OrderAddresses.Any(y => y.WantBill == "1"));
                    }

                    var resultGetCustomerRel =
                        CustomerHelper.GetCustomerRelation(
                            AppSettings()["URL_INTEGRACION_API"] +
                            ApiIntegracionesCnx.GetCustomerRelation(branchCode, barcode: ventaviaje.BarCode),
                            int.Parse(AppSettings()["TimeOutCnx"]));
                    if (resultGetCustomerRel.ResultType != ResultTypes.Success) continue;
                    if (resultGetCustomerRel.Objeto[0].CustomerType == "CLIENTE_COCACOLA" && facturado)
                    {
                        ventaviaje.Cuc = resultGetCustomerRel.Objeto[0].Cuc;
                    }
                    else if (resultGetCustomerRel.Objeto[0].CustomerType == "CLIENTE_COCACOLA" && !facturado)
                    {
                        ventaviaje.Cuc = resultCustomerIdGen.Objeto.CucCCTH;
                    }
                    else if (resultGetCustomerRel.Objeto[0].CustomerType != "CLIENTE_COCACOLA" && facturado)
                    {
                        ventaviaje.Cuc = resultGetCustomerRel.Objeto[0].Cuc;
                    }
                    else if (resultGetCustomerRel.Objeto[0].CustomerType != "CLIENTE_COCACOLA" && !facturado)
                    {
                        ventaviaje.Cuc = resultCustomerIdGen.Objeto.Cuc;
                    }

                    //if (!ventaviaje.CustomerCCTH) continue;
                    //var precioCoca = PrecioClienteCocaCtrl.GetPrecioClienteCoca(branchCode, ventaviaje.Cuc.ToString(),
                    //    ventaviaje.DeliveryDate);
                    //if (precioCoca.ResultType != ResultTypes.Success) continue;
                    //foreach (var t1 in ventaviaje.SaleProducts)
                    //{
                    //    foreach (var t in precioCoca.Objeto)
                    //    {
                    //        if (t1.Product != t.ItemId) continue;
                    //        t1.BasePrice = t.BasePrice;
                    //        t1.Price = t.Price;
                    //        t1.Discount = t.Discount;
                    //        t1.DiscountRate = t.DiscountPercent;
                    //    }
                    //}
                }
                var ventasList = new List<Ventas>();

                var ventas = (from m in resultVentas.Objeto
                              where m.Cuc != 0
                              group m by m.Cuc).ToList();
                foreach (var t1 in ventas.ToList())
                {
                    if (ventasList.Count > 0)
                    {
                        Ventas venta = null;
                        foreach (var orderitem in t1.ToList())
                        {
                            if (venta == null)
                            {
                                venta = orderitem;
                                continue;
                            }

                            foreach (var t3 in orderitem.SaleProducts)
                            {
                                var band1 = false;
                                foreach (var t2 in venta.SaleProducts)
                                {
                                    if (t3.Product != t2.Product)
                                        continue;
                                    t2.Quantity += t3.Quantity;
                                    t2.CreditAmount += t3.CreditAmount;
                                    band1 = true;
                                }

                                if (!band1)
                                {
                                    venta.SaleProducts.Add(t3);
                                }
                            }

                            foreach (var t3 in orderitem.SalePromotions)
                            {
                                var band1 = false;
                                foreach (var t2 in venta.SalePromotions)
                                {
                                    if (t3.Product != t2.Product || t3.Type != t2.Type)
                                        continue;
                                    t2.Quantity += t3.Quantity;
                                    band1 = true;
                                }

                                if (!band1)
                                {
                                    venta.SalePromotions.Add(t3);
                                }
                            }
                        }

                        ventasList.Add(venta);
                    }
                    else
                    {
                        Ventas venta = null;
                        foreach (var orderitem in t1.ToList())
                        {
                            if (venta == null)
                            {
                                venta = orderitem;
                                continue;
                            }

                            foreach (var t3 in orderitem.SaleProducts)
                            {
                                var band = false;
                                foreach (var t2 in venta.SaleProducts)
                                {
                                    if (t3.Product != t2.Product)
                                        continue;
                                    t2.Quantity += t3.Quantity;
                                    t2.CreditAmount += t3.CreditAmount;
                                    band = true;
                                }

                                if (!band)
                                {
                                    venta.SaleProducts.Add(t3);
                                }
                            }

                            foreach (var t3 in orderitem.SalePromotions)
                            {
                                var band = false;
                                foreach (var t2 in venta.SalePromotions)
                                {
                                    if (t3.Product != t2.Product || t3.Type != t2.Type)
                                        continue;
                                    t2.Quantity += t3.Quantity;
                                    band = true;
                                }

                                if (!band)
                                {
                                    venta.SalePromotions.Add(t3);
                                }
                            }
                        }

                        ventasList.Add(venta);
                    }
                }
                //foreach (var viaje in inventarios.Objeto)
                //{
                //    if (viaje.Route != route) continue;
                //    var resultLogBitacora = LogBitacoraCtrl.GetLogBitacora(branchCode, route, viaje.Id, date.ToString("yyyy-MM-dd"), 0);

                //    //var resultVentas = GetVentas(service, branchCode, route, date, viaje.Id);
                //    //var ventasHelper = new VentasHelper();
                //    //LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                //    //     ": Obteniendo Ventas para el viaje:"+viaje.Id+".");

                //    //var resultVentas =
                //    //    ventasHelper.GetVentasIntegracion(AppSettings()["URL_INTEGRACION_API"] +
                //    //                                      ApiIntegracionesCnx.GetSalesIntegra(
                //    //                                          date, branchCode, route),
                //    //        int.Parse(AppSettings()["TimeOutCnx"]));
                //    if (resultVentas.ResultType != ResultTypes.Success || resultVentas.Objeto == null ||
                //        resultVentas.Objeto.Count == 0)
                //    {
                //        if (resultVentas.ResultType == ResultTypes.Error)
                //        {
                //            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                //                DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                //                ": Error_VentasCtrl.GetVentas: No se obtuvieron ventas para BranchCode : " +
                //                branchCode +
                //                ", route : " + route + ", fecha : " + date + ",viaje : " + viaje.Id + ".", true);

                //            if (resultLogBitacora.ResultType != ResultTypes.Success) continue;
                //            {
                //                foreach (var logbitacora in resultLogBitacora.Objeto)
                //                {
                //                    if (logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 11))
                //                    {
                //                        InventoryCtrl.ActualizaBitacora(logbitacora, 11, 3, true, "No se pudieron obtener las ventas de Integracion." + resultVentas.Mensaje);
                //                    }
                //                }
                //            }
                //            LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                //                  ": Problema al obtener Ventas.");
                //        }
                //        else
                //        {
                //            LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                //                  ": No se obtuvieron Ventas.");
                //        }

                //        continue;
                //    }
                //    if (resultLogBitacora.ResultType != ResultTypes.Success) continue;
                //    {
                //        foreach (var logbitacora in resultLogBitacora.Objeto)
                //        {
                //            if (logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 11))
                //            {
                //                InventoryCtrl.ActualizaBitacora(logbitacora, 11, 1, false, "Se obtuvieron correctamente las ventas de Integracion.");
                //                LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                //                      ": Se obtuvieron correctamente las ventas de Integracion.");
                //            }
                //            else if (logbitacora.LogBitacoraDetail.Find(x => x.Process.Id == 11 && x.Success) == null)
                //            {
                //                InventoryCtrl.ActualizaBitacora(logbitacora, 11, 1, false, "Se obtuvieron correctamente las ventas de Integracion.");
                //                LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                //                      ": Se obtuvieron correctamente las ventas de Integracion.");
                //            }
                //        }
                //    }

                //    //var customerHelper = new CustomerHelper();
                //    //var resultCustomerIdGen = customerHelper.GetClienteGenerico(
                //    //    AppSettings()["URL_INTEGRACION_API"] +
                //    //    ApiIntegracionesCnx.GetClienteGenerico(branchCode, route),
                //    //    int.Parse(AppSettings()["TimeOutCnx"]));
                //    //if (resultCustomerIdGen.ResultType != ResultTypes.Success)
                //    //{
                //    //    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                //    //        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                //    //        ": Error_CustomerHelper.GetClienteGenerico: No se obtuvo Cliente Genérico para BranchCode : " +
                //    //        branchCode + ", route : " + route, true);
                //    //    continue;
                //    //}

                //    //foreach (var ventaviaje in resultVentas.Objeto)
                //    //{
                //    //    var facturado = false;
                //    //    var resultOrdenesCliente = OrderIntegracionCtrl.GetOrderByCliente(ventaviaje.BarCode);
                //    //    if (resultOrdenesCliente.ResultType == ResultTypes.Success)
                //    //    {
                //    //        facturado =
                //    //            resultOrdenesCliente.Objeto.Any(x => x.OrderAddresses.Any(y => y.WantBill == "1"));
                //    //    }

                //    //    var resultGetCustomerRel =
                //    //        CustomerHelper.GetCustomerRelation(
                //    //            AppSettings()["URL_INTEGRACION_API"] +
                //    //            ApiIntegracionesCnx.GetCustomerRelation(branchCode, barcode: ventaviaje.BarCode),
                //    //            int.Parse(AppSettings()["TimeOutCnx"]));
                //    //    if (resultGetCustomerRel.ResultType != ResultTypes.Success) continue;
                //    //    if (resultGetCustomerRel.Objeto[0].CustomerType == "CLIENTE_COCACOLA" && facturado)
                //    //    {
                //    //        ventaviaje.Cuc = resultGetCustomerRel.Objeto[0].Cuc;
                //    //    }
                //    //    else if (resultGetCustomerRel.Objeto[0].CustomerType == "CLIENTE_COCACOLA" && !facturado)
                //    //    {
                //    //        ventaviaje.Cuc = resultCustomerIdGen.Objeto.CucCCTH;
                //    //    }
                //    //    else if (resultGetCustomerRel.Objeto[0].CustomerType != "CLIENTE_COCACOLA" && facturado)
                //    //    {
                //    //        ventaviaje.Cuc = resultGetCustomerRel.Objeto[0].Cuc;
                //    //    }
                //    //    else if (resultGetCustomerRel.Objeto[0].CustomerType != "CLIENTE_COCACOLA" && !facturado)
                //    //    {
                //    //        ventaviaje.Cuc = resultCustomerIdGen.Objeto.Cuc;
                //    //    }

                //    //    //if (!ventaviaje.CustomerCCTH) continue;
                //    //    var precioCoca = PrecioClienteCocaCtrl.GetPrecioClienteCoca(branchCode, ventaviaje.Cuc.ToString(),
                //    //        ventaviaje.DeliveryDate);
                //    //    if (precioCoca.ResultType != ResultTypes.Success) continue;
                //    //    foreach (var t1 in ventaviaje.SaleProducts)
                //    //    {
                //    //        foreach (var t in precioCoca.Objeto)
                //    //        {
                //    //            if (t1.Product != t.ItemId) continue;
                //    //            t1.BasePrice = t.BasePrice;
                //    //            t1.Price = t.Price;
                //    //            t1.Discount = t.Discount;
                //    //            t1.DiscountRate = t.DiscountPercent;
                //    //        }
                //    //    }
                //    //}

                //    //var ventas = (from m in resultVentas.Objeto
                //    //              where m.Cuc != 0
                //    //              group m by m.Cuc).ToList();
                //    //foreach (var t1 in ventas.ToList())
                //    //{
                //    //    if (ventasList.Count > 0)
                //    //    {
                //    //        Ventas venta = null;
                //    //        foreach (var orderitem in t1.ToList())
                //    //        {
                //    //            if (venta == null)
                //    //            {
                //    //                venta = orderitem;
                //    //                continue;
                //    //            }

                //    //            foreach (var t3 in orderitem.SaleProducts)
                //    //            {
                //    //                var band1 = false;
                //    //                foreach (var t2 in venta.SaleProducts)
                //    //                {
                //    //                    if (t3.Product != t2.Product)
                //    //                        continue;
                //    //                    t2.Quantity += t3.Quantity;
                //    //                    t2.CreditAmount += t3.CreditAmount;
                //    //                    band1 = true;
                //    //                }

                //    //                if (!band1)
                //    //                {
                //    //                    venta.SaleProducts.Add(t3);
                //    //                }
                //    //            }

                //    //            foreach (var t3 in orderitem.SalePromotions)
                //    //            {
                //    //                var band1 = false;
                //    //                foreach (var t2 in venta.SalePromotions)
                //    //                {
                //    //                    if (t3.Product != t2.Product || t3.Type != t2.Type)
                //    //                        continue;
                //    //                    t2.Quantity += t3.Quantity;
                //    //                    band1 = true;
                //    //                }

                //    //                if (!band1)
                //    //                {
                //    //                    venta.SalePromotions.Add(t3);
                //    //                }
                //    //            }
                //    //        }

                //    //        ventasList.Add(venta);
                //    //    }
                //    //    else
                //    //    {
                //    //        Ventas venta = null;
                //    //        foreach (var orderitem in t1.ToList())
                //    //        {
                //    //            if (venta == null)
                //    //            {
                //    //                venta = orderitem;
                //    //                continue;
                //    //            }

                //    //            foreach (var t3 in orderitem.SaleProducts)
                //    //            {
                //    //                var band = false;
                //    //                foreach (var t2 in venta.SaleProducts)
                //    //                {
                //    //                    if (t3.Product != t2.Product)
                //    //                        continue;
                //    //                    t2.Quantity += t3.Quantity;
                //    //                    t2.CreditAmount += t3.CreditAmount;
                //    //                    band = true;
                //    //                }

                //    //                if (!band)
                //    //                {
                //    //                    venta.SaleProducts.Add(t3);
                //    //                }
                //    //            }

                //    //            foreach (var t3 in orderitem.SalePromotions)
                //    //            {
                //    //                var band = false;
                //    //                foreach (var t2 in venta.SalePromotions)
                //    //                {
                //    //                    if (t3.Product != t2.Product || t3.Type != t2.Type)
                //    //                        continue;
                //    //                    t2.Quantity += t3.Quantity;
                //    //                    band = true;
                //    //                }

                //    //                if (!band)
                //    //                {
                //    //                    venta.SalePromotions.Add(t3);
                //    //                }
                //    //            }
                //    //        }

                //    //        ventasList.Add(venta);
                //    //    }
                //    //}
                //}

                var ventasListProrrat = Prorrateo(inventarios.Objeto, ventasList);

                foreach (var ventase in ventasListProrrat)
                {
                    var precioCoca = PrecioClienteCocaCtrl.GetPrecioClienteCoca(branchCode, ventase.Cuc.ToString(),
                            ventase.DeliveryDate);
                    if (precioCoca.ResultType != ResultTypes.Success) continue;
                    foreach (var t1 in ventase.SaleProducts)
                    {
                        foreach (var t in precioCoca.Objeto)
                        {
                            if (t1.Product != t.ItemId) continue;
                            t1.BasePrice = t.BasePrice;
                            t1.Price = t.Price;
                            t1.Discount = t.Discount;
                            t1.DiscountRate = t.DiscountPercent;
                        }
                    }
                }
                

                logDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Publicando ventas en OpeCD.");
                //foreach (var t1 in ventasListProrrat)
                //{
                //    if (t1.SaleProducts.Count <= 0) continue;
                //    var ventasProductosList = new List<VentasProductos>();
                //    foreach (var t in t1.SaleProducts)
                //    {
                //        if (t.Price == 0)
                //        {
                //            var ventasPromociones = new VentasPromociones
                //            {
                //                Product = t.Product,
                //                Quantity = t.Quantity,
                //                Type = 1
                //            };
                //            t1.SalePromotions.Add(ventasPromociones);
                //            ventasProductosList.Add(t);
                //        }
                //        t.BasePrice =
                //            decimal.Round(t.BasePrice, 2);
                //        t.Price =
                //            decimal.Round(t.Price, 2);
                //        t.Discount =
                //            decimal.Round(t.Discount, 2);
                //        t.DiscountRate =
                //            decimal.Round(t.DiscountRate, 2);
                //    }
                //    foreach (var t2 in ventasProductosList)
                //    {
                //        foreach (var t in t1.SaleProducts)
                //        {
                //            if (t2.Product != t.Product) continue;
                //            t1.SaleProducts.Remove(t);
                //            break;
                //        }
                //    }
                //}
                var result1 = PostVentasOpeCd(ventasListProrrat, branchCode);
                if (result1.Objeto == null)
                {
                    resultGral.Mensaje = DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                ": Error_VentasCtrl.EnviarVentasAll: No se obtuvo respuesta de la dll de apiopecd: " + branchCode +
                                ". " + result1.Mensaje;
                    resultGral.ResultType = result1.ResultType;
                    return resultGral;
                }
                var bandError = false;
                foreach (var t in result1.Objeto)
                {
                    var resultLogBitacora = LogBitacoraCtrl.GetLogBitacora(branchCode, route, 0, date.ToString("yyyy-MM-dd"), 0);
                    if (t.Success)
                    {
                        if (resultLogBitacora.ResultType != ResultTypes.Success) continue;
                        {
                            foreach (var logbitacora in resultLogBitacora.Objeto)
                            {
                                if (!(logbitacora.Trip > 0)) continue;
                                if (logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 12))
                                {
                                    InventoryCtrl.ActualizaBitacora(logbitacora, 12, bandError ? 3 : 2, bandError, bandError ? "No se pudieron guardar las ventas en OPE." + Environment.NewLine + resultGral.Mensaje : "La venta se guardo correctamente en OPE.");
                                    logDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                                                                     (bandError ? ": No se pudieron guardar las ventas en OPE." : ": La venta se guardo correctamente en OPE."));
                                }
                                else if (logbitacora.LogBitacoraDetail.Find(x => x.Process.Id == 12 && x.Success) == null)
                                {
                                    InventoryCtrl.ActualizaBitacora(logbitacora, 12, bandError ? 3 : 2, bandError, bandError ? "No se pudieron guardar las ventas en OPE." + Environment.NewLine + resultGral.Mensaje : "La venta se guardo correctamente en OPE.");
                                    logDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                                                                     (bandError ? ": No se pudieron guardar las ventas en OPE." : ": La venta se guardo correctamente en OPE."));
                                }
                            }
                        }
                        continue;
                    }
                    if (t.ErrorDescriptions.Count > 0)
                    {
                        bandError = true;
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " +
                            ResultTypes.Error +
                            "_VentasCtrl.PostVentasOpeCd: No se guardo la venta para el Cedis: " + branchCode +
                            ", Ruta:" + t.Sale.RouteCode + ", Cliente: " + t.Sale.CustomerOpeId + ". " +
                            JsonConvert.SerializeObject(t.ErrorDescriptions), true);

                        resultGral.Mensaje += DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " +
                            ResultTypes.Error +
                            "_VentasCtrl.PostVentasOpeCd: No se guardo la venta para el Cedis: " + branchCode +
                            ", Ruta:" + t.Sale.RouteCode + ", Cliente: " + t.Sale.CustomerOpeId + ". " +
                            JsonConvert.SerializeObject(t.ErrorDescriptions) +Environment.NewLine;
                        resultGral.ResultType = result1.ResultType;
                    }
                    if (resultLogBitacora.ResultType != ResultTypes.Success) continue;
                    {
                        foreach (var logbitacora in resultLogBitacora.Objeto)
                        {
                            if (!(logbitacora.Trip > 0)) continue;
                            if (logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 12))
                            {
                                InventoryCtrl.ActualizaBitacora(logbitacora, 12, bandError ? 3 : 2, bandError, bandError ? "No se pudieron guardar las ventas en OPE." + Environment.NewLine + resultGral.Mensaje : "La venta se guardo correctamente en OPE.");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_VentasCtrl.EnviarVentasAll: " +
                    ex.Message +
                    "."+Environment.NewLine + ex.StackTrace + ".", true);
                Console.WriteLine(ex);
            }

            return resultGral;
        }

        #region Prorrateos

        private static List<Ventas> Prorrateo(List<InventoryCtrl.Trip> viajes, List<Ventas> ventasTotales)
        {
            var ventasPorCliente = AgruparVentasPorCliente(ventasTotales);
            var ventasProrrateo = new List<Ventas>();
            foreach (var viaje in viajes)
            {
                foreach (var producto in viaje.ConfirmedItems)
                {
                    var cantidadProducto = producto.Qty;
                    var vtasPorCliente = ventasPorCliente.Where(vta => vta.ProductosVendidos.Any(prod => prod.Product == Convert.ToInt32(producto.ItemId) && prod.Quantity > 0)).ToList();
                    Ventas ventaProrrateo;
                    if (vtasPorCliente.Any())
                    {
                        foreach (var vtaCliente in vtasPorCliente)
                        {
                            if (cantidadProducto == 0)
                            {
                                break;
                            }
                            ventaProrrateo = new Ventas();
                            var productoVendido = vtaCliente.ProductosVendidos.Find(prodVendido => prodVendido.Product == Convert.ToInt32(producto.ItemId));
                            if (productoVendido == null) continue;
                            var diferencia = productoVendido.Quantity - cantidadProducto;
                            if (diferencia > 0)
                            {
                                AgregarVentaProducto(Convert.ToInt32(producto.ItemId), cantidadProducto, ventaProrrateo.SaleProducts, productoVendido.Price, productoVendido.BasePrice, productoVendido.Discount, productoVendido.DiscountRate);
                                cantidadProducto = 0;
                                productoVendido.Quantity = diferencia;
                            }
                            if (diferencia == 0)
                            {
                                AgregarVentaProducto(Convert.ToInt32(producto.ItemId), cantidadProducto, ventaProrrateo.SaleProducts, productoVendido.Price, productoVendido.BasePrice, productoVendido.Discount, productoVendido.DiscountRate);
                                cantidadProducto = 0;
                                vtaCliente.ProductosVendidos.Remove(productoVendido);
                            }
                            if (diferencia < 0)
                            {
                                cantidadProducto = productoVendido.Quantity > 0 ? cantidadProducto - productoVendido.Quantity : cantidadProducto;
                                if (productoVendido.Quantity > 0)
                                    AgregarVentaProducto(Convert.ToInt32(producto.ItemId), productoVendido.Quantity, ventaProrrateo.SaleProducts, productoVendido.Price, productoVendido.BasePrice, productoVendido.Discount, productoVendido.DiscountRate);
                                vtaCliente.ProductosVendidos.Remove(productoVendido);
                            }

                            ventaProrrateo.Cuc = vtaCliente.Cuc;
                            ventaProrrateo.Trip = viaje.Id;
                            ventaProrrateo.DeliveryDate = vtaCliente.DeliveryDate;
                            ventaProrrateo.CedisId = vtaCliente.CedisId;
                            ventaProrrateo.RouteCode = vtaCliente.RouteCode;
                            ventaProrrateo.CustomerCCTH = vtaCliente.CustomerCCTH;
                            ventasProrrateo.Add(ventaProrrateo);
                        }
                    }
                    if (cantidadProducto == 0)
                        continue;
                    var vttasPorCliente = ventasPorCliente.Where(vta => vta.Promociones.Any(prod => prod.Product == Convert.ToInt32(producto.ItemId) && prod.Quantity > 0)).ToList();
                    if (!vttasPorCliente.Any())
                        continue;
                    foreach (var vttaPorCliente in vttasPorCliente)
                    {
                        if (cantidadProducto == 0)
                        {
                            break;
                        }
                        var promociones = vttaPorCliente.Promociones.Where(prom => prom.Product == Convert.ToInt32(producto.ItemId));
                        if (!promociones.Any()) continue;
                        var promocionesVacias = new List<VentasPromociones>();
                        foreach (var promocion in promociones)
                        {
                            if (cantidadProducto == 0)
                            {
                                break;
                            }
                            ventaProrrateo = new Ventas();
                            var diferencia = promocion.Quantity - cantidadProducto;
                            if (diferencia > 0)
                            {
                                AgregarPromocion(Convert.ToInt32(producto.ItemId), cantidadProducto, ventaProrrateo.SalePromotions, promocion.Type);
                                cantidadProducto = 0;
                                promocion.Quantity = diferencia;
                            }
                            if (diferencia == 0)
                            {
                                AgregarPromocion(Convert.ToInt32(producto.ItemId), cantidadProducto, ventaProrrateo.SalePromotions, promocion.Type);
                                cantidadProducto = 0;
                                promocionesVacias.Add(promocion);
                                //vttaPorCliente.Promociones.Remove(promocion);
                            }
                            if (diferencia < 0)
                            {
                                cantidadProducto = promocion.Quantity > 0 ? cantidadProducto - promocion.Quantity : cantidadProducto;
                                if (promocion.Quantity > 0)
                                    AgregarPromocion(Convert.ToInt32(producto.ItemId), promocion.Quantity, ventaProrrateo.SalePromotions, promocion.Type);
                                promocionesVacias.Add(promocion);
                                //vttaPorCliente.Promociones.Remove(promocion);
                            }
                            ventaProrrateo.Cuc = vttaPorCliente.Cuc;
                            ventaProrrateo.Trip = viaje.Id;
                            ventaProrrateo.DeliveryDate = vttaPorCliente.DeliveryDate;
                            ventaProrrateo.CedisId = vttaPorCliente.CedisId;
                            ventaProrrateo.RouteCode = vttaPorCliente.RouteCode;
                            ventaProrrateo.CustomerCCTH = vttaPorCliente.CustomerCCTH;
                            ventasProrrateo.Add(ventaProrrateo);

                        }
                        promocionesVacias.ForEach(promocionVacia => vttaPorCliente.Promociones.Remove(promocionVacia));
                    }
                }
            }
            return AgruparVentas(ventasProrrateo);
        }

        private static List<Ventas> AgruparVentas(List<Ventas> ventas)
        {
            var listaVentas = new List<Ventas>();
            foreach (var gViaje in ventas.GroupBy(vta => vta.Trip))
            {
                foreach (var gFecha in gViaje.GroupBy(vta => vta.DeliveryDate))
                {
                    foreach (var gRuta in gFecha.GroupBy(vta => vta.RouteCode))
                    {
                        foreach (var gCuc in gRuta.GroupBy(vta => vta.Cuc))
                        {
                            var venta = new Ventas();
                            venta.Cuc = gCuc.First().Cuc;
                            venta.Trip = gCuc.First().Trip;
                            venta.DeliveryDate = gCuc.First().DeliveryDate;
                            venta.CedisId = gCuc.First().CedisId;
                            venta.RouteCode = gCuc.First().RouteCode;
                            venta.CustomerCCTH = gCuc.First().CustomerCCTH;
                            var vtasProductos = new List<VentasProductos>();
                            gCuc.ToList().ForEach(vta => vtasProductos.AddRange(vta.SaleProducts));
                            vtasProductos.ForEach(vtaProducto => AgregarVentaProducto(vtaProducto.Product, vtaProducto.Quantity, venta.SaleProducts, vtaProducto.Price, vtaProducto.BasePrice, vtaProducto.Discount, vtaProducto.DiscountRate));

                            var vtasPromociones = new List<VentasPromociones>();
                            gCuc.ToList().ForEach(vta => vtasPromociones.AddRange(vta.SalePromotions));
                            vtasPromociones.ForEach(vtaPromocion => AgregarPromocion(vtaPromocion.Product, vtaPromocion.Quantity, venta.SalePromotions, vtaPromocion.Type));

                            listaVentas.Add(venta);
                        }
                    }
                }
            }
            return listaVentas;
        }

        private static void AgregarPromocion(int productoId, int cantidad, List<VentasPromociones> ventasPromociones, int tipoPromocion)
        {
            var productoEquivalente = ventasPromociones.Find(promocion =>
                promocion.Product == productoId);
            if (productoEquivalente != null)
                productoEquivalente.Quantity += cantidad;
            else
                ventasPromociones.Add(new VentasPromociones
                {
                    Quantity = cantidad,
                    Product = productoId,
                    Type = tipoPromocion
                });
        }
        private static void AgregarVentaProducto(int productoId, int cantidad, List<VentasProductos> ventasProductos, decimal price, decimal pricebase, decimal discount, decimal discountrate)
        {
            var productoEquivalente = ventasProductos.Find(ventaProducto =>
                ventaProducto.Product == productoId);
            if (productoEquivalente != null)
                productoEquivalente.Quantity += cantidad;
            else
                ventasProductos.Add(new VentasProductos
                {
                    Quantity = cantidad,
                    Product = productoId,
                    BasePrice = pricebase,
                    Price = price,
                    DiscountRate = discountrate,
                    Discount = discount
                });
        }
        private static List<Venta> AgruparVentasPorCliente(List<Ventas> ventas)
        {
            var ventasAgrupadas = ventas.GroupBy(venta => venta.Cuc);
            return ventasAgrupadas.Select(grupoVentas => new Venta
            {
                Cuc = grupoVentas.First().Cuc,
                ProductosVendidos = ObtenerProductosVendidos(grupoVentas.ToList()),
                Promociones = ObtenerPromociones(grupoVentas.ToList()),
                CedisId = grupoVentas.First().CedisId,
                RouteCode = grupoVentas.First().RouteCode,
                Trip = grupoVentas.First().Trip,
                DeliveryDate = grupoVentas.First().DeliveryDate,
                Code = grupoVentas.First().Code,
                CustomerCCTH = grupoVentas.First().CustomerCCTH
            }).ToList();
        }
        private static List<VentasProductos> ObtenerProductosVendidos(List<Ventas> ventas)
        {
            var acumuladoProductosVendidos = new List<VentasProductos>();
            ventas.ForEach(venta => acumuladoProductosVendidos.AddRange(venta.SaleProducts));
            var productosVendidos = new List<VentasProductos>();
            foreach (var grupo in acumuladoProductosVendidos.GroupBy(prod => prod.Product))
            {
                var productoDistinto = new VentasProductos { Product = grupo.First().Product };
                grupo.ToList().ForEach(prod => productoDistinto.Quantity += prod.Quantity);
                grupo.ToList().ForEach(prod => productoDistinto.BasePrice = prod.BasePrice);
                grupo.ToList().ForEach(prod => productoDistinto.Price = prod.Price);
                grupo.ToList().ForEach(prod => productoDistinto.DiscountRate = prod.DiscountRate);
                grupo.ToList().ForEach(prod => productoDistinto.Discount = prod.Discount);

                productosVendidos.Add(productoDistinto);
            }
            return productosVendidos;
        }
        private static List<VentasPromociones> ObtenerPromociones(List<Ventas> ventas)
        {
            var acumuladoPromociones = new List<VentasPromociones>();
            ventas.ForEach(venta => acumuladoPromociones.AddRange(venta.SalePromotions));
            var promociones = new List<VentasPromociones>();
            foreach (var grupo in acumuladoPromociones.GroupBy(prod => prod.Product))
            {
                var agrupadosPorTipo = grupo.ToList().GroupBy(promo => promo.Type);
                foreach (var grupoPromocion in agrupadosPorTipo)
                {
                    var promocion = new VentasPromociones
                    {
                        Product = grupo.First().Product,
                        Type = grupoPromocion.First().Type
                    };
                    grupoPromocion.ToList().ForEach(prom => promocion.Quantity += prom.Quantity);
                    promociones.Add(promocion);
                }
            }
            return promociones;
        }
        #endregion
    }
}
