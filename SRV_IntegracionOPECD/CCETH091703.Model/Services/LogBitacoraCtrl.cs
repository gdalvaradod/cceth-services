﻿using CCETH091703.Base;
using CCETH091703.Model.BR;
using CCETH091703.Model.DataAccess;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;

namespace CCETH091703.Model.Services
{
    public class LogBitacoraCtrl
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        public static Result<List<LogBitacora>> GetLogBitacora(int cedisId, int ruta, int viaje, string deliveryDate, int estatus)
        {
            var result = new Result<List<LogBitacora>>();
            try
            {
                var helper = new LogBitacoraHelper();
                result = helper.GetLogBitacora(AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.GetLogBitacora(cedisId, ruta, viaje, deliveryDate, estatus, false),
                    int.Parse(AppSettings()["TimeOutCnx"]));
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_LogBitacoraCtrl.GetLogBitacora: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + ".";
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<List<CedisRutaLog>> GetLogBitacoraAgrupado(int cedisId, int ruta, int? viaje, string deliveryDate, int estatus)
        {
            var result = new Result<List<CedisRutaLog>>();
            try
            {
                var helper = new LogBitacoraHelper();
                result = helper.GetLogBitacoraAgrupadoCedis(AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.GetLogBitacora(cedisId, ruta, viaje, deliveryDate, estatus, true),
                    int.Parse(AppSettings()["TimeOutCnx"]));
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_LogBitacoraCtrl.GetLogBitacora: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<LogBitacora> PostEnvioLogBitacora(LogBitacora logBitacora)
        {
            var result = new Result<LogBitacora>();
            try
            {
                var helper = new LogBitacoraHelper();
                result = helper.PostEnvioLogBitacora(AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.PostLogBitacora(),
                    int.Parse(AppSettings()["TimeOutCnx"]), logBitacora);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_LogBitacoraCtrl.PostEnvioLogBitacora: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<LogBitacora> PutEnvioLogBitacora(LogBitacora logBitacora)
        {
            var result = new Result<LogBitacora>();
            try
            {
                var helper = new LogBitacoraHelper();
                result = helper.PutEnvioLogBitacora(AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.PutLogBitacora(logBitacora.Id),
                    int.Parse(AppSettings()["TimeOutCnx"]), logBitacora);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_LogBitacoraCtrl.PutEnvioLogBitacora: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<LogBitacoraDetail> PostEnvioLogBitacoraDetail(LogBitacoraDetail logBitacoraDetail)
        {
            var result = new Result<LogBitacoraDetail>();
            try
            {
                var helper = new LogBitacoraHelper();
                result = helper.PostEnvioLogBitacoraDetail(AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.PostLogBitacoraDetail(),
                    int.Parse(AppSettings()["TimeOutCnx"]), logBitacoraDetail);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_LogBitacoraCtrl.PostEnvioLogBitacoraDetail: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }
    }
}
