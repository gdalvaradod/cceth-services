﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using CCETH091703.Base;
using CCETH091703.Model.BR;
using CCETH091703.Model.DataAccess;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;
using CCETH091703.Model.Objects.BO;
using Newtonsoft.Json;

namespace CCETH091703.Model.Services
{
    public static class InventoryCtrl
    {
        
        private static Task _t;
        #region ClasesAuxiliares

        // Clases de auxiliares para uso local
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        //public class DeliveryIdReq
        //{
        //    public int DeliveryId { get; set; }
        //}

        //public class EnvioCabecera
        //{
        //    public int CustomerId { get; set; }
        //    public int InventoryId { get; set; }
        //    public string Code { get; set; }
        //}

        public class TripInventoryRequest
        {
            public List<CedisRouteDate> CedisRoutes { get; set; }
        }

        public class CedisRouteDate
        {
            public int CedisId { get; set; }
            public int Route { get; set; }
            public DateTime DeliveryDate { get; set; }
        }

        public class CedisTripList
        {
            public List<CedisTrip> CedisTrips { get; set; }
        }

        public class CedisTrip
        {
            public int CedisId { get; set; }
            public List<Trip> TripList { get; set; }
        }

        public class Trip
        {
            public DateTime DeliveryDate { get; set; }
            public int Route { get; set; }
            public int Id { get; set; }
            public List<Item> ConfirmedItems { get; set; }
        }

        public class Item
        {
            public string ItemId { get; set; }
            public int Qty { get; set; }
            public int Amount => Qty;
            public string ProductId { get; set; }
            public decimal Price = 0;

            public int WbcId
            {
                get { return int.Parse(string.IsNullOrEmpty(ProductId) ? "0" : ProductId); }
                set { ProductId = value.ToString(); }
            }
        }

        //public class InventoryRequest
        //{
        //    public int Userid { get; set; }
        //    public string Code { get; set; }
        //    public int Order { get; set; }
        //    public string Date { get; set; }
        //    public int State { get; set; }
        //}



        #endregion

        public static Result<CedisTripList> TripInventory(TripInventoryRequest tripInvReq)
        {
            var result = new Result<CedisTripList>();
            try
            {
                var helper = new InventoryHelper();
                result = helper.TripInventory(AppSettings()["URL_OPE_API"] + ApiOpecdCnx.TripInventory(),
                    int.Parse(AppSettings()["TimeOutCnx"]), tripInvReq);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_InventoryCtrl.TrupInventory: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + ".";
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static void ProcesaInventariosOpeIntegraAllAsync()
        {
            try
            {
                //if (_t != null)
                //{
                //    if (!_t.IsCompleted)
                //    {
                //        return _t;
                //    }
                //    else
                //    {
                //        LogServicioBr.EscribirLog(AppSettings()["InventoryLog"],
                //            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                //            ": Termino proceso de inventarios Ope-Integra", true);  // *** Monitor Customer Log
                //    }
                //}
                LogServicioBr.EscribirLog(AppSettings()["InventoryLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Inicia proceso de Inventarios Ope-Integra.", true); // *** Monitor Customer Log

                var inventoryHelper = new InventoryHelper();
                var result =
                    inventoryHelper.ObtenCedisRutasIntegracion(
                        AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.GetCedisRutasInventarioWbc(),
                        int.Parse(AppSettings()["timeOutCnx"]));
                if (result.ResultType != ResultTypes.Success || result.Objeto == null || result.Objeto.Count == 0)
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Error_InventoryCtrl.ProcesaInventariosOpeIntegraAll: no se obtuvieron Cedis-Ruta de Integración.", true);
                    return;
                }
               
                var listTasks = new List<Task>(5);
                foreach (var itemCediRuta in result.Objeto)
                {
                    for (var i = 0; i < itemCediRuta.Destinos.Count; i++)
                    {
                        var i1 = i;
                        listTasks.Add(Task.Run(() =>
                        {
                            Console.WriteLine(itemCediRuta.Cedi + " : " + itemCediRuta.Destinos[i1].RouteCodeOpecd);
                            ProcesaInventariosOpeIntegra(itemCediRuta.Cedi,
                                itemCediRuta.Destinos[i1].RouteCodeOpecd.ToString());

                        }));
                        if (listTasks.Count != 1 && i < itemCediRuta.Destinos.Count - 1) continue;
                        _t = Task.WhenAll(listTasks.ToArray());
                        do
                        {

                        } while (!_t.IsCompleted);
                        listTasks.Clear();
                    }
                }
                //var x = 0;
                //_t = Task.WhenAll((from cedi in result.Objeto where cedi.Destinos.Count != 0 from destino in cedi.Destinos select Task.Run(() => { ProcesaInventariosOpeIntegra(cedi.Cedi, destino.RouteCodeOpecd.ToString()); })).ToArray());
                //do
                //{
                //    x++;
                //    Console.WriteLine("Procesando " + x);
                //} while (!_t.IsCompleted);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_InventoryCtrl.ProcesaInventariosOpeIntegraWbcAll: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
            }
        }

        private static void CancelUpdateOrderInventory(int branchCode, int route, DateTime date)
        {
            try
            {
                var inventoryHelper = new InventoryHelper();
                var inventarios =
                       inventoryHelper.TripInventoryIntegracion(
                           AppSettings()["URL_INTEGRACION_API"] +
                           ApiIntegracionesCnx.GetTripInventoryIntegra(branchCode, route.ToString(), date, false),
                           int.Parse(AppSettings()["TimeOutCnx"]));
                if (inventarios.ResultType != ResultTypes.Success) return;
                if (inventarios.Objeto.Count <= 0) return;
                var cedisTripObj = new CedisTrip
                {
                    CedisId = branchCode,
                    TripList = inventarios.Objeto
                };

                var cad = JsonConvert.SerializeObject(cedisTripObj);
                var result2 =
                    inventoryHelper.CancelUpdateOrderInventory(
                        AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.PostCancelUpdateOrderInventory(),
                        int.Parse(AppSettings()["TimeOutCnx"]), cedisTripObj);
                Console.WriteLine(result2.Mensaje);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void ProcesaInventariosOpeIntegra(int cedi, string ruta)
         {
            try
            {
                if (ProcesoCompleto(cedi, int.Parse(ruta)))
                {
                    return;
                }

                CancelUpdateOrderInventory(cedi, int.Parse(ruta), DateTime.Today);
                StringBuilder LogDictionary = new StringBuilder("");

                LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture)
                    + ": Procesando cedi:" + cedi + " ruta:" + ruta + Environment.NewLine);

                //LogServicioBr.EscribirLog(AppSettings()["InventoryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Consultando Pedido.\n", true);
                //LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                //                                            ": Consultando Pedidos.");

                //Result<List<OrderGeneral>> resultGet = OrderIntegracionCtrl.GetOrder(1,cedi.ToString(),ruta);
                //if (resultGet.ResultType != ResultTypes.Error)
                //{
                //    if (resultGet.ResultType == ResultTypes.Success)
                //    {
                //        if (resultGet.Objeto != null)
                //        {
                //            List<OrderGeneral> orderGeneralList = resultGet.Objeto;
                //            List<OrderGeneral> orderGeneralListConsolidada = new List<OrderGeneral>();

                //            var pedidosGroup = (from m in orderGeneralList
                //                group m by m.BottlerData.DeliveryDate.Substring(0, 10));


                //            foreach (var grupofecha in pedidosGroup)
                //            {
                //                OrderGeneral OrderGenerica = new OrderGeneral();
                //                bool hayOrdenGenerica = false;
                //                List<OrderItem> temp = new List<OrderItem>();
                //                foreach (OrderGeneral item in grupofecha) //pedidosGroup)
                //                {
                //                    //determinar la direccion de billing del Item
                //                    int billaddr;
                //                    for (billaddr = 0; billaddr < item.OrderAddresses.Count; billaddr++)
                //                    {
                //                        if (item.OrderAddresses[billaddr].AddressType == "billing")
                //                        {
                //                            break;
                //                        }
                //                    }

                //                    if (billaddr >= item.OrderAddresses.Count)
                //                    {
                //                        // error, no se encontro direccion para billing
                //                    }
                //                    else
                //                    {
                //                        if (item.OrderAddresses[billaddr].WantBill == "0")
                //                        {
                //                            OrderGenerica = item;

                //                            // anexar aqui informacion de la orden genérica

                //                            hayOrdenGenerica = true;

                //                            foreach (var itemOrderItem in item.OrderItems)
                //                            {
                //                                var pos = temp.FindIndex(m =>
                //                                    m.Sku == itemOrderItem.Sku);
                //                                if (pos == -1)
                //                                {
                //                                    temp.Add(itemOrderItem);
                //                                }
                //                                else
                //                                {
                //                                   temp[pos].QtyCanceled +=
                //                                        itemOrderItem.QtyCanceled;
                //                                    temp[pos].QtyOrdered +=
                //                                        itemOrderItem.QtyOrdered;
                //                                    temp[pos].QtyInvoiced +=
                //                                        itemOrderItem.QtyInvoiced;
                //                                    temp[pos].QtyRefunded +=
                //                                        itemOrderItem.QtyRefunded;
                //                                    temp[pos].QtyShipped +=
                //                                        itemOrderItem.QtyShipped;
                //                                    temp[pos].RowTotal +=
                //                                        itemOrderItem.RowTotal;
                //                                    temp[pos].RowTotalInclTax +=
                //                                        itemOrderItem.RowTotalInclTax;
                //                                }
                //                            }     
                //                        }
                //                        else
                //                        {
                //                            orderGeneralListConsolidada.Add(item);
                //                        }
                //                    }
                //                }

                //                if (hayOrdenGenerica)
                //                {
                //                    var customerHelper = new CustomerHelper();
                //                    var resultCustomerIdGen = customerHelper.GetClienteGenerico(
                //                        AppSettings()["URL_INTEGRACION_API"] +
                //                        ApiIntegracionesCnx.GetClienteGenerico(cedi, int.Parse(ruta)),
                //                        int.Parse(AppSettings()["TimeOutCnx"]));
                //                    if (resultCustomerIdGen.ResultType != ResultTypes.Success)
                //                    {
                //                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                //                            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                //                            ": Error_CustomerHelper.GetClienteGenerico: No se obtuvo Cliente Genérico para BranchCode : " +
                //                            cedi + ", route : " + ruta, true);
                //                        continue;
                //                    }
                //                    OrderGenerica.OrderItems = temp;
                //                    OrderGenerica.OrderAddresses[0].CustomerId = resultCustomerIdGen.Objeto.CucCCTH.ToString();
                //                    OrderGenerica.OrderAddresses[1].CustomerId = resultCustomerIdGen.Objeto.CucCCTH.ToString();
                //                    orderGeneralListConsolidada.Add(OrderGenerica);
                //                }
                //                //EnviarPedidos(JsonConvert.SerializeObject(item.ToList()), item.Key);
                //                EnviarPedidos(JsonConvert.SerializeObject(orderGeneralListConsolidada), cedi);
                //            }
                //        }
                //    }
                //}
                //else
                //{
                //    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                //        DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + resultGet.ResultType +
                //        "_ConsultarPedidos: " + resultGet.Mensaje + ".\n", true);
                //}
                //LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                //      ": Se consultaron pedidos.");

                LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                      ": Se consultan pedidos confirmados.");

                Result<List<OrderGeneral>> resultGet = OrderIntegracionCtrl.GetOrder(2,cedi.ToString(),ruta);
                if (resultGet.ResultType != ResultTypes.Error)
                {
                    if (resultGet.ResultType == ResultTypes.Success)
                    {
                        if (resultGet.Objeto != null)
                        {
                            var objeto = resultGet.Objeto;
                            var pedidos = (from m in objeto
                                where m.BottlerData.CediId.HasValue
                                group m by m.BottlerData.CediId).ToList();
                            foreach (var current in pedidos)
                            {
                                var confirmation = OrderOpecdCtrl.GetConfirmation(
                                    JsonConvert.SerializeObject(current.ToList()), current.Key);
                                switch (confirmation.ResultType)
                                {
                                    case ResultTypes.Success:
                                        var res = OrderIntegracionCtrl.PutOrder(
                                            JsonConvert.SerializeObject(confirmation.Objeto), 0);
                                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + res.ResultType +
                                            "_ValidarConfirmados: " + res.Mensaje + ".\n", true);
                                        break;
                                    case ResultTypes.Error:
                                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " +
                                            confirmation.ResultType +
                                            "_ConsultarConfirmados: " + confirmation.Mensaje + ".\n", true);
                                        break;
                                    case ResultTypes.Warning:
                                        break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + resultGet.ResultType +
                        "_ConsultarConfirmados: " + resultGet.Mensaje + ".\n", true);
                }
                LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                     ": Se consultaron pedidos confirmados.");

                LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                     ": Se consultan Inventarios de Ope a Integra.");

                var resultEnvioOpeIntegra = EnvioInventario_Ope_Integra(cedi, ruta,LogDictionary);
                if (resultEnvioOpeIntegra.ResultType != ResultTypes.Success)
                {
                    if (resultEnvioOpeIntegra.ResultType == ResultTypes.Error)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                            ": Error_InventoryCtrl.EnvioInventario_Ope_Integra: No se envio el inventario de ope a integra para cedis:" +
                            cedi + ", ruta:" + ruta + "."+Environment.NewLine + resultEnvioOpeIntegra.Mensaje, true);
                        
                        LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                            ": Se genero un error al consultar inventarios.");
                        LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                            ": Termina Proceso en cedi:" + cedi + " ruta:" + ruta);
                        LogServicioBr.EscribirLog(AppSettings()["InventoryLog"],
                            LogDictionary + Environment.NewLine, true); // *** Monitor Customer Log
                        LogDictionary.Clear();
          

                        return;

                    }
                    LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                                ": No hubo inventarios para procesar.");
                }


                LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Inicia envio de ventas de Integra a Ope");

                var resultVentas = VentasCtrl.EnviarVentasAll(cedi, int.Parse(ruta), LogDictionary);
                switch (resultVentas.ResultType)
                {
                    case ResultTypes.Success:
                        LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                           ": Termina Envio de Ventas de Integra a Ope.");
                        break;
                    case ResultTypes.Error:
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                            ": Error_InventoryCtrl.EnviarVentasAll: Se generaron errores en la ventas de cedis:" +
                            cedi + ", ruta:" + ruta + "| |" + resultVentas.Mensaje, true);
                        break;
                    case ResultTypes.Warning:
                        LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                            ": Termina Envio de Ventas de Integra a Ope.");
                        break;
                }

                
                LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                         ": Termina Proceso para cedi:" + cedi + " ruta:" + ruta);
                LogServicioBr.EscribirLog(AppSettings()["InventoryLog"],
                    LogDictionary+Environment.NewLine , true); // *** Monitor Customer Log
                LogDictionary.Clear();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }    
        }

        //private static async void EnviarPedidos(string json, int? cedisId)
        //{
        //    try
        //    {
        //        await Task.Run(() =>
        //        {
        //            LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Guardando Pedidos de Cedis: " + cedisId + ".\n", true);
        //            var getPedidos = OrderOpecdCtrl.PostOrder(json, cedisId);
        //            if (getPedidos.ResultType == ResultTypes.Success)
        //            {
        //                getPedidos.Objeto.RemoveAll(x => x.DeliveryOrderId <= 0);
        //                LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Guardando pedidos en Integracion.\n", true);
        //                var resultViaje = OrderIntegracionCtrl.PutOrder(JsonConvert.SerializeObject(getPedidos.Objeto), 1);
        //                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + resultViaje.ResultType + "_ConsultarPedidos: " + resultViaje.Mensaje + ".\n", true);

        //                if (resultViaje.ResultType != ResultTypes.Error)
        //                {
        //                    LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Pedidos publicados.\n", true);
        //                }
        //                else
        //                {
        //                    LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Problema al guardar pedidos en Integración.\n", true);
        //                }
        //            }
        //            else
        //            {
        //                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + getPedidos.ResultType + "_ConsultarPedidos: " + getPedidos.Mensaje + ".\n", true);
        //            }
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_ConsultarPedidos: " + ex.Message + ".\n", true);
        //    }
        //}

        public static Result<Inventory> EnvioInventario_Ope_Integra(int cediid,
            string routeCode,StringBuilder LogDictionary)
        {
            var result = new Result<Inventory> { Objeto = new Inventory() };

            try
            {
                var resultLogBitacora = LogBitacoraCtrl.GetLogBitacora(cediid, int.Parse(routeCode), 0, DateTime.Now.ToString("yyyy-MM-dd"), 0);
                var tripInvReq = new TripInventoryRequest
                {
                    CedisRoutes = new List<CedisRouteDate>()
                };
                var cedroute = new CedisRouteDate
                {
                    CedisId = cediid,
                    Route = int.Parse(routeCode)
                };
                tripInvReq.CedisRoutes.Add(cedroute);

                var resultTripInventory = TripInventory(tripInvReq);
                if (resultTripInventory.ResultType == ResultTypes.Success)
                {
                    if (resultTripInventory.Objeto.CedisTrips != null && resultTripInventory.Objeto.CedisTrips.Count > 0)
                    {
                        foreach (var ceditrip in resultTripInventory.Objeto.CedisTrips)
                        {
                            var cedisTripObj = new CedisTrip
                            {
                                CedisId = ceditrip.CedisId,
                                TripList = new List<Trip>()
                            };
                            if (ceditrip.TripList.Count == 0)
                            {
                                result.ResultType = ResultTypes.Warning;
                                result.Mensaje = "No hay inventarios para procesar.";
                                return result;
                            }
                            foreach (var trip in ceditrip.TripList)
                            {
                                if (resultLogBitacora.ResultType != ResultTypes.Success)
                                {
                                    InsertaBitacora(cediid, int.Parse(routeCode), trip.Id,
                                        trip.DeliveryDate.ToString("yyyy-MM-dd"), 3, 1, false,
                                        "El inventario se consulto correctamente en OPE.");
                                    resultLogBitacora = LogBitacoraCtrl.GetLogBitacora(cediid, int.Parse(routeCode), 0, DateTime.Now.ToString("yyyy-MM-dd"), 0);
                                    continue;
                                }
                                foreach (var logbitacora in resultLogBitacora.Objeto)
                                {
                                    if (logbitacora.Trip == trip.Id && logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 3))
                                    {
                                        ActualizaBitacora(logbitacora, 3, 1, false, "El inventario se consulto correctamente en OPE.");
                                        LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                               "El inventario se consulto correctamente en OPE.");
                                    }
                                    else if (logbitacora.Trip == trip.Id && logbitacora.LogBitacoraDetail.Find(x => x.Process.Id == 3 && x.Success) == null)
                                    {
                                        ActualizaBitacora(logbitacora, 3, 1, false, "El inventario se consulto correctamente en OPE.");
                                        LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                               "El inventario se consulto correctamente en OPE.");
                                    }
                                }
                                if (resultLogBitacora.Objeto.All(x => x.Trip != trip.Id))
                                {
                                    InsertaBitacora(cediid, int.Parse(routeCode), trip.Id,
                                     trip.DeliveryDate.ToString("yyyy-MM-dd"), 3, 1, false,
                                     "El inventario se consulto correctamente en OPE.");
                                }
                                var bandTerminado = resultLogBitacora.Objeto.Any(logbitacora => logbitacora.RouteId == trip.Route && logbitacora.Trip == trip.Id && logbitacora.ProcessStatus == 2);
                                if (!bandTerminado)
                                {
                                    cedisTripObj.TripList.Add(trip);
                                }
                            }

                            if (cedisTripObj.TripList.Count == 0)
                            {
                                cedisTripObj = ceditrip;
                            }

                            //var items = new List<Item>();
                            //if (cedisTripObj.TripList.Count > 0)
                            //{
                            //    foreach (var trip in cedisTripObj.TripList)
                            //    {
                            //        items.AddRange(trip.ConfirmedItems);
                            //    }
                            //}
                            //else if (cedisTripObj.TripList.Count == 0 && resultLogBitacora.ResultType == ResultTypes.Success)
                            //{
                            //    result.ResultType = ResultTypes.Success;
                            //    return result;
                            //}
                            //else
                            //{
                            //    foreach (var trip in ceditrip.TripList)
                            //    {
                            //        items.AddRange(trip.ConfirmedItems);
                            //    }
                            //    cedisTripObj = ceditrip;
                            //}

                            //var itemIds = items.Select(item => item.ItemId).Distinct().ToList();
                            //var catItems = new List<Item>();
                            //itemIds.ForEach(itemId =>
                            //    catItems.Add(items.First(i => i.ItemId == itemId)));
                            //foreach (var productcode in itemIds)
                            //{
                            //    var result3 = ProductCtrl.GetProductWbc(productcode, token);
                            //    if (result3.ResultType != ResultTypes.Success) continue;
                            //    var item = catItems.Single(catItem => catItem.ItemId == productcode);
                            //    item.WbcId = result3.Objeto[0].ProductId;
                            //}
                            resultLogBitacora = LogBitacoraCtrl.GetLogBitacora(cediid, int.Parse(routeCode), 0, DateTime.Now.ToString("yyyy-MM-dd"), 0);
                            var inventoryHelper = new InventoryHelper();
                            var result2 =
                                inventoryHelper.PublicaInventarioIntegracion(
                                    AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.TripInventoryIntegra(),
                                    int.Parse(AppSettings()["TimeOutCnx"]), cedisTripObj);
                            if (result2.ResultType == ResultTypes.Success)
                            {
                                foreach (var item in cedisTripObj.TripList)
                                {
                                    if (resultLogBitacora.ResultType != ResultTypes.Success) continue;
                                    foreach (var logbitacora in resultLogBitacora.Objeto)
                                    {
                                        if (logbitacora.RouteId != item.Route || logbitacora.Trip != item.Id ||
                                            logbitacora.ProcessStatus == 2) continue;
                                        if (logbitacora.Trip == item.Id && logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 4))
                                        {
                                            ActualizaBitacora(logbitacora, 4, 1, false, "El inventario se inserto correctamente en integracion.");
                                            LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                                                        ": El inventario se inserto correctamente en integracion.");
                                            resultLogBitacora = LogBitacoraCtrl.GetLogBitacora(cediid, int.Parse(routeCode), 0, DateTime.Now.ToString("yyyy-MM-dd"), 0);
                                        }
                                        else if (logbitacora.Trip == item.Id && logbitacora.LogBitacoraDetail.Find(x => x.Process.Id == 4 && x.Success) == null)
                                        {
                                            ActualizaBitacora(logbitacora, 4, 1, false, "El inventario se inserto correctamente en integracion.");
                                            LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                                                               "El inventario se inserto correctamente en integracion.");
                                            resultLogBitacora = LogBitacoraCtrl.GetLogBitacora(cediid, int.Parse(routeCode), 0, DateTime.Now.ToString("yyyy-MM-dd"), 0);
                                        }
                                    }
                                }
                                continue;
                            }
                            result.ResultType = result2.ResultType;
                            result.Mensaje += result2.Mensaje + Environment.NewLine;
                            foreach (var item in ceditrip.TripList)
                            {
                                if (resultLogBitacora.ResultType != ResultTypes.Success) continue;
                                foreach (var logbitacora in resultLogBitacora.Objeto)
                                {
                                    if (logbitacora.RouteId != item.Route || logbitacora.Trip != item.Id ||
                                        logbitacora.DeliveryDate != item.DeliveryDate.ToString("yyyy-MM-dd") ||
                                        logbitacora.ProcessStatus == 2) continue;
                                    if (logbitacora.Trip == item.Id && logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 4))
                                    {
                                        ActualizaBitacora(logbitacora, 4, 3, true,
                                            "No se insertaron los inventarios en Intermedia");
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        result.ResultType = ResultTypes.Warning;
                        result.Mensaje = "No se obtuvieron Inventarios de Ope";
                        InsertaBitacora(cediid, int.Parse(routeCode), null, DateTime.Now.ToString("yyyy-MM-dd"), 3, 3, true, result.Mensaje);
                    }
                }
                else
                {
                    result.ResultType = ResultTypes.Warning;
                    result.Mensaje = "No se obtuvieron Inventarios de Ope";
                    InsertaBitacora(cediid, int.Parse(routeCode), null, DateTime.Now.ToString("yyyy-MM-dd"), 3, 3, true, result.Mensaje);
                }
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_InventoryCtrl.EnvioInventario_Ope_Integra: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".",
                    true);
                result.Objeto = null;
                result.Mensaje = "No se obtuvieron Inventarios de Ope. " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".";
                result.ResultType = ResultTypes.Error;
                InsertaBitacora(cediid, int.Parse(routeCode), null, DateTime.Now.ToString("yyyy-MM-dd"), 3, 3, true, result.Mensaje);
            }

            return result;
        }

        public static Result<List<Jornada>> GetJornadaCerrada(int cedisId, int route, DateTime deliveryDate)
        {
            try
            {
                var inventarioHelper = new InventoryHelper();
                return inventarioHelper.GetJornadaCerrada(
                    AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.GetEstadoJornada(cedisId, route, deliveryDate.ToString("yyyy-MM-dd") ),
                    int.Parse(AppSettings()["TimeOutCnx"]));
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_OrderIntegracionCtrl.GetOrder: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                return new Result<List<Jornada>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = new List<Jornada>(),
                    Mensaje = ex.Message
                };
            }
        }

        public static void InsertaBitacora(int cedis, int ruta, int? viaje, string deliveryDate, int proceso, int estatus, bool error, string errorDescription)
        {
            try
            {
                var logBitacora = new LogBitacora
                {
                    CedisId = cedis,
                    RouteId = ruta,
                    ProcessStatus = estatus,
                    Trip = viaje,
                    Process = new ProcesosSistema {Id = proceso},
                    DeliveryDate = deliveryDate
                };
                var resultLogBitacora = LogBitacoraCtrl.PostEnvioLogBitacora(logBitacora);
                if (resultLogBitacora.ResultType != ResultTypes.Success) return;
                var logBitacoraDetail = new LogBitacoraDetail
                {
                    LogId = resultLogBitacora.Objeto.Id,
                    Process = new ProcesosSistema {Id = proceso},
                    Success = !error,
                    ErrorDescription = errorDescription
                };
                var resultLogBitacoraDetail = LogBitacoraCtrl.PostEnvioLogBitacoraDetail(logBitacoraDetail);
            }
            catch (Exception)
            {
                //ignore
            }
        }

        public static void ActualizaBitacora(LogBitacora logBitacora, int proceso, int estatus, bool error, string errorDescription)
        {
            try
            {
                logBitacora.ProcessStatus = estatus;
                logBitacora.Process = new ProcesosSistema { Id = proceso };
                var resultLogBitacora = LogBitacoraCtrl.PutEnvioLogBitacora(logBitacora);
                if (resultLogBitacora.ResultType != ResultTypes.Success) return;
                var logBitacoraDetail = new LogBitacoraDetail
                {
                    LogId = logBitacora.Id,
                    Process = new ProcesosSistema {Id = proceso},
                    Success = !error,
                    ErrorDescription = errorDescription
                };
                var resultLogBitacoraDetail = LogBitacoraCtrl.PostEnvioLogBitacoraDetail(logBitacoraDetail);
            }
            catch (Exception)
            {
                // ignored
            }
        }

        private static bool ProcesoCompleto(int cedi, int ruta)
        {
            var logBit=LogBitacoraCtrl.GetLogBitacora(cedi, ruta,0, DateTime.Today.ToString("yyyy-MM-dd"),0);
            return logBit.ResultType == ResultTypes.Success && logBit.Objeto.All(x => x.ProcessStatus == 2);
        }
    }
}
