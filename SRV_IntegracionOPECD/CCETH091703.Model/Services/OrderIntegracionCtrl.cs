﻿using GrupoGitLib.Model.Objects.BO;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using CCETH091703.Base;
using CCETH091703.Model.BR;
using CCETH091703.Model.DataAccess;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;
using Newtonsoft.Json;

namespace CCETH091703.Model.Services
{
    public class OrderIntegracionCtrl
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        public static Result<List<OrderGeneral>> GetOrder(int status, string cedis, string ruta)
        {
            try
            {
                var orderIntegracionHelper = new OrderIntegracionHelper();
                return orderIntegracionHelper.GetOrder(
                    AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.GetOrdenGeneral(status,cedis,ruta),
                    int.Parse(AppSettings()["TimeOutCnx"]));
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_OrderIntegracionCtrl.GetOrder: " + ex.Message + "." + Environment.NewLine + ex.StackTrace + ".", true);
                return new Result<List<OrderGeneral>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = new List<OrderGeneral>(),
                    Mensaje = ex.Message
                };
            }
        }

        public static Result<List<OrderGeneral>> GetOrderByCliente(string barcode)
        {
            try
            {
                var orderIntegracionHelper = new OrderIntegracionHelper();
                return orderIntegracionHelper.GetOrderByCliente(
                    AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.GetOrdenGeneralByCliente(barcode),
                    int.Parse(AppSettings()["TimeOutCnx"]));
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_OrderIntegracionCtrl.GetOrderByCliente: " + ex.Message + "." + Environment.NewLine + ex.StackTrace + ".", true);
                return new Result<List<OrderGeneral>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = new List<OrderGeneral>(),
                    Mensaje = ex.Message
                };
            }
        }

        public static Result<string> PutOrder(string json, int accion)
        {
            var result = new Result<string>();
            try
            {
                var orderIntegracionHelper = new OrderIntegracionHelper();
                return orderIntegracionHelper.PutOrder(
                    AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.PutOrderGeneral(accion),
                    int.Parse(AppSettings()["TimeOutCnx"]), json);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_OrderIntegracionCtrl.PutOrder: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
            }
            return result;
        }

        public static Result<ConfiguracionAutomatica> GetConfigAuto(int cediId, int ruta)
        {
            try
            {
                var orderIntegracionHelper = new OrderIntegracionHelper();
                return orderIntegracionHelper.GetConfigAuto(
                    AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.GetConfigAuto(cediId, ruta),
                    int.Parse(AppSettings()["TimeOutCnx"]));
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_OrderIntegracionCtrl.GetConfigAuto: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                return new Result<ConfiguracionAutomatica>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = new ConfiguracionAutomatica(),
                    Mensaje = ex.Message
                };
            }
        }

        public static Result<List<CedisRutaInventario>> ObtenCedisRutasIntegracion()
        {
            try
            {
                var orderIntegracionHelper = new OrderIntegracionHelper();
                return orderIntegracionHelper.ObtenCedisRutasIntegracion(
                    AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.GetCedisRutasInventarioWbc(),
                    int.Parse(AppSettings()["TimeOutCnx"]));
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_OrderIntegracionCtrl.GetConfigAuto: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                return new Result<List<CedisRutaInventario>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = new List<CedisRutaInventario>(),
                    Mensaje = ex.Message
                };
            }
        }

        public static void EnviaPedidosOpe(int cedi, int ruta)
        {
            try
            {
                LogServicioBr.EscribirLog(AppSettings()["InventoryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Consultando Pedido.\n", true);
                //LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                //                                            ": Consultando Pedidos.");

                var resultGet = GetOrder(1, cedi.ToString(), ruta.ToString());
                if (resultGet.ResultType != ResultTypes.Error)
                {
                    if (resultGet.ResultType != ResultTypes.Success) return;
                    if (resultGet.Objeto == null) return;
                    var orderGeneralList = resultGet.Objeto;
                    var orderGeneralListConsolidada = new List<OrderGeneral>();

                    var pedidosGroup = (from m in orderGeneralList
                        group m by m.BottlerData.DeliveryDate.Substring(0, 10));


                    foreach (var grupofecha in pedidosGroup)
                    {
                        var orderGenerica = new OrderGeneral();
                        var hayOrdenGenerica = false;
                        var temp = new List<OrderItem>();
                        foreach (var item in grupofecha) //pedidosGroup)
                        {
                            //determinar la direccion de billing del Item
                            //int billaddr;
                            //for (billaddr = 0; billaddr < item.OrderAddresses.Count; billaddr++)
                            //{
                            //    if (item.OrderAddresses[billaddr].AddressType == "billing")
                            //    {
                            //        break;
                            //    }
                            //}

                            //if (billaddr >= item.OrderAddresses.Count)
                            //{
                            //    // error, no se encontro direccion para billing
                            //}
                            //else
                            //{
                                if (item.OrderAddresses.Any(x => x.WantBill == "0"))
                                {
                                    orderGenerica = item;

                                    // anexar aqui informacion de la orden genérica

                                    hayOrdenGenerica = true;

                                    foreach (var itemOrderItem in item.OrderItems)
                                    {
                                        var pos = temp.FindIndex(m =>
                                            m.Sku == itemOrderItem.Sku);
                                        if (pos == -1)
                                        {
                                            temp.Add(itemOrderItem);
                                        }
                                        else
                                        {
                                            temp[pos].QtyCanceled +=
                                                itemOrderItem.QtyCanceled;
                                            temp[pos].QtyOrdered +=
                                                itemOrderItem.QtyOrdered;
                                            temp[pos].QtyInvoiced +=
                                                itemOrderItem.QtyInvoiced;
                                            temp[pos].QtyRefunded +=
                                                itemOrderItem.QtyRefunded;
                                            temp[pos].QtyShipped +=
                                                itemOrderItem.QtyShipped;
                                            temp[pos].RowTotal +=
                                                itemOrderItem.RowTotal;
                                            temp[pos].RowTotalInclTax +=
                                                itemOrderItem.RowTotalInclTax;
                                        }
                                    }
                                }
                                else
                                {
                                    orderGeneralListConsolidada.Add(item);
                                }
                            //}
                        }

                        if (hayOrdenGenerica)
                        {
                            var customerHelper = new CustomerHelper();
                            var resultCustomerIdGen = customerHelper.GetClienteGenerico(
                                AppSettings()["URL_INTEGRACION_API"] +
                                ApiIntegracionesCnx.GetClienteGenerico(cedi, ruta),
                                int.Parse(AppSettings()["TimeOutCnx"]));
                            if (resultCustomerIdGen.ResultType != ResultTypes.Success)
                            {
                                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                    ": Error_CustomerHelper.GetClienteGenerico: No se obtuvo Cliente Genérico para BranchCode : " +
                                    cedi + ", route : " + ruta, true);
                                continue;
                            }
                            orderGenerica.OrderItems = temp;
                            foreach (var t in orderGenerica.OrderAddresses)
                            {
                                t.CustomerId = resultCustomerIdGen.Objeto.Cuc.ToString();
                            }
                            
                            orderGeneralListConsolidada.Add(orderGenerica);
                        }
                        //EnviarPedidos(JsonConvert.SerializeObject(item.ToList()), item.Key);
                        EnviarPedidos(JsonConvert.SerializeObject(orderGeneralListConsolidada), cedi);
                    }
                }
                else
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + resultGet.ResultType +
                        "_ConsultarPedidos: " + resultGet.Mensaje + ".\n", true);
                }
                //LogDictionary.AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                  //    ": Se consultaron pedidos.");

            }
            catch (Exception e)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": ERROR_ConsultarPedidos: " + e.Message + ".\n", true);
            }
        }

        private static async void EnviarPedidos(string json, int? cedisId)
        {
            try
            {
                await Task.Run(() =>
                {
                    LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Guardando Pedidos de Cedis: " + cedisId + ".\n", true);
                    var getPedidos = OrderOpecdCtrl.PostOrder(json, cedisId);
                    if (getPedidos.ResultType == ResultTypes.Success)
                    {
                        getPedidos.Objeto.RemoveAll(x => x.DeliveryOrderId <= 0);
                        LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Guardando pedidos en Integracion.\n", true);
                        var resultViaje = PutOrder(JsonConvert.SerializeObject(getPedidos.Objeto), 1);
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + resultViaje.ResultType + "_ConsultarPedidos: " + resultViaje.Mensaje + ".\n", true);

                        if (resultViaje.ResultType != ResultTypes.Error)
                        {
                            LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Pedidos publicados.\n", true);
                        }
                        else
                        {
                            LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Problema al guardar pedidos en Integración.\n", true);
                        }
                    }
                    else
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + getPedidos.ResultType + "_ConsultarPedidos: " + getPedidos.Mensaje + ".\n", true);
                    }
                });
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_ConsultarPedidos: " + ex.Message + ".\n", true);
            }
        }

        public static Result<string> PutStatusAuto(string json, int cediId, int ruta)
        {
            var result = new Result<string>();
            try
            {
                var orderIntegracionHelper = new OrderIntegracionHelper();
                return orderIntegracionHelper.PutStatusAuto(
                    AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.PutStatusAuto(2,cediId, ruta),
                    int.Parse(AppSettings()["TimeOutCnx"]), json);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_OrderIntegracionCtrl.PutOrder: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
            }
            return result;
        }
    }
}
