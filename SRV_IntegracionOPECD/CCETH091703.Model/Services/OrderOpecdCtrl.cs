﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.Linq;
using CCETH091703.Base;
using CCETH091703.Model.BR;
using CCETH091703.Model.DataAccess;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;

namespace CCETH091703.Model.Services
{
    public class OrderOpecdCtrl
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        public static Result<List<OrderResponse>> PostOrder(string json, int? cedisId)
        {
            var result = new Result<List<OrderResponse>>();

            try
            {
                var orderOpecdHelper = new OrderOpecdHelper();
                result = orderOpecdHelper.PostOrder(
                    AppSettings()["URL_OPE_API"] + ApiOpecdCnx.PostOrderGeneral(cedisId),
                    int.Parse(AppSettings()["TimeOutCnx"]), json);
                if (result.ResultType == ResultTypes.Success)
                {
                    if (result.Objeto[0].ErrorItemList.Count > 0)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_OrderOpecdCtrl.PostOrder: " +
                            result.Objeto[0].ErrorItemList[0].ErrorDescription, true);
                    }
                }
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_OrderOpecdCtrl.PostOrder: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.ResultType = ResultTypes.Error;
                result.Objeto = null;
                result.Mensaje = ex.Message;
            }
            return result;
        }

        //private static string CreateCadIds(IEnumerable<long> list)
        //{
        //    var str = list.Aggregate("", (current, key) => current + key + ",");
        //    return str.Substring(0, str.Length - 1);
        //}

        //public Result<List<OrderResponse>> GetValidation(string json, int? cedisId)
        //{
        //    var longList = new List<long>();
        //    try
        //    {
        //        var source = JArray.Parse(json);
        //        longList.AddRange(
        //            source.Select(key => key.ToString())
        //                .Select(JsonConvert.DeserializeObject<OrderGeneral>)
        //                .Select(order => order.OpecdId));
        //    }
        //    catch (Exception ex)
        //    {
        //        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
        //            DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""), true);
        //    }
        //    return API_OpeCDGetValidation(CreateCadIds(longList), cedisId);
        //}

        //private static Result<List<OrderResponse>> API_OpeCDGetValidation(string ids, int? cedisId)
        //{
        //    var result = new Result<List<OrderResponse>>();
        //    try
        //    {
        //        return
        //            MetodosApi.Get<List<OrderResponse>>(
        //                AppSettings()["URL_OPE_API"] + ApiOpecdCnx.GetValidation(ids, cedisId),
        //                int.Parse(AppSettings()["TimeOutCnx"]));
        //    }
        //    catch (WebException ex)
        //    {
        //        result.ResultType = ResultTypes.Error;
        //        result.Objeto = null;
        //        result.Mensaje = ex.Message;
        //        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
        //            DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""), true);
        //    }
        //    return result;
        //}

       



        public static Result<List<OrderResponse>> GetConfirmation(string json, int? cedisId)
        {
            try
            {
                var orderOpecdHelper = new OrderOpecdHelper();
                return orderOpecdHelper.GetConfirmation(AppSettings()["URL_OPE_API"],
                    int.Parse(AppSettings()["TimeOutCnx"]), json, cedisId);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_OrderOpecdCtrl.GetConfirmation: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                return new Result<List<OrderResponse>>();
            }
        }
    }
}
