﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using CCETH091703.Base;
using CCETH091703.Model.BR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;

namespace CCETH091703.Model.DataAccess
{
    public class OrderOpecdHelper
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        public Result<List<OrderResponse>> PostOrder(string urlApi, int timeOut, string json)
        {
            var result = new Result<List<OrderResponse>>();

            try
            {
                result = Metodos.Conexion_Post<List<OrderResponse>>(urlApi, timeOut, json);
                if (result.ResultType == ResultTypes.Success)
                {
                    if (result.Objeto[0].ErrorItemList.Count > 0)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_ApiOpecd.PostOrder: " + result.Objeto[0].ErrorItemList[0].ErrorDescription, true);
                    }
                }
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_OrderOpecdHelper.PostOrder: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.ResultType = ResultTypes.Error;
                result.Objeto = null;
                result.Mensaje = ex.Message;
            }
            return result;
        }

        private static string CreateCadIds(IEnumerable<long> list)
        {
            var str = list.Aggregate("", (current, key) => current + key + ",");
            return str.Substring(0, str.Length - 1);
        }

        public Result<List<OrderResponse>> GetValidation(string json, int? cedisId)
        {
            var longList = new List<long>();
            try
            {
                var source = JArray.Parse(json);
                longList.AddRange(source.Select(key => key.ToString()).Select(JsonConvert.DeserializeObject<OrderGeneral>).Select(order => order.OpecdId));
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""), true);
            }
            return API_OpeCDGetValidation(CreateCadIds(longList), cedisId);
        }

        private static Result<List<OrderResponse>> API_OpeCDGetValidation(string ids, int? cedisId)
        {
            var result = new Result<List<OrderResponse>>();
            try
            {
                return Metodos.Conexion_Get<List<OrderResponse>>(AppSettings()["URL_OPE_API"] + ApiOpecdCnx.GetValidation(ids, cedisId), int.Parse(AppSettings()["TimeOutCnx"]));
            }
            catch (WebException ex)
            {
                result.ResultType = ResultTypes.Error;
                result.Objeto = null;
                result.Mensaje = ex.Message;
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"], DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""), true);
            }
            return result;
        }

        public Result<List<OrderResponse>> GetConfirmation(string urlApi, int timeOut, string json, int? cedisId)
        {
            var opeList = new List<long>();
            var idList = new List<string>();
            var idsList = "";
            try
            {
                var source = JArray.Parse(json);
                opeList.AddRange(
                    source.Select(key => key.ToString())
                        .Select(JsonConvert.DeserializeObject<OrderGeneral>)
                        .Select(order => order.OpecdId));
                idList.AddRange(
                    source.Select(key => key.ToString())
                        .Select(JsonConvert.DeserializeObject<OrderGeneral>)
                        .Select(order => order.InterId));
                idsList = idList.Aggregate(idsList, (current, t) => current + ("," + t));
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_OrderOpecdHelper.GetConfirmation: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
            }
            return API_OpeCDGetConfirmation(urlApi, timeOut, CreateCadIds(opeList), idsList.Substring(1), cedisId);
        }

        private static Result<List<OrderResponse>> API_OpeCDGetConfirmation(string urlApi, int timeOut, string idsOpe, string idsInter, int? cedisId)
        {
            var result = new Result<List<OrderResponse>>();
            try
            {
                result = Metodos.Conexion_Get<List<OrderResponse>>(urlApi + ApiOpecdCnx.GetConfirmation(idsOpe, cedisId), timeOut);
                if (result.ResultType == ResultTypes.Success)
                {
                    result.Objeto[0].Id = idsInter;
                }
                
                return result;
            }
            catch (WebException ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_OrderOpecdHelper.API_OpeCDGetConfirmation: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.ResultType = ResultTypes.Error;
                result.Objeto = null;
                result.Mensaje = ex.Message;
            }
            return result;
        }
    }
}
