﻿using System;
using System.Collections.Generic;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;

namespace CCETH091703.Model.DataAccess
{
    internal class CustomerHelper
    {
        public Result<Destinos> GetClienteGenerico(string urlApi, int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<Destinos>(urlApi, timeOut);
            }
            catch (Exception e)
            {
                var result = new Result<Destinos>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public static Result<List<CustomerRelation>> GetCustomerRelation(string urlApi,int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<List<CustomerRelation>>(urlApi, timeOut);
            }
            catch (Exception e)
            {
                var result = new Result<List<CustomerRelation>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                return result;
            }
        }
    }
}
