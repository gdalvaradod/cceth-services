﻿using System;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;
using Newtonsoft.Json;
using CCETH091703.Model.Services;
using System.Collections.Generic;
using CCETH091703.Model.BR;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using CCETH091703.Model.Objects.BO;

namespace CCETH091703.Model.DataAccess
{

    public class InventoryHelper
    {

        #region Ope

        public Result<InventoryCtrl.CedisTripList> TripInventory(string urlApi, int timeOut, InventoryCtrl.TripInventoryRequest tripInvReq)
        {
            try
            {
                
                return Metodos.Conexion_Post<InventoryCtrl.CedisTripList>(urlApi, timeOut, JsonConvert.SerializeObject(tripInvReq));
            }
            catch (Exception e)
            {
                var result = new Result<InventoryCtrl.CedisTripList>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        #endregion
        #region Integracion

        public Result<bool> PublicaInventarioIntegracion(string urlApi, int timeOut, InventoryCtrl.CedisTrip ceditrips)
        {
            try
            {
                return Metodos.Conexion_Post<bool>(urlApi, timeOut, JsonConvert.SerializeObject(ceditrips));
            }
            catch (Exception e)
            {
                var result = new Result<bool>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = false,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<bool> CancelUpdateOrderInventory(string urlApi, int timeOut, InventoryCtrl.CedisTrip ceditrips)
        {
            try
            {
                return Metodos.Conexion_Post<bool>(urlApi, timeOut, JsonConvert.SerializeObject(ceditrips));
            }
            catch (Exception e)
            {
                var result = new Result<bool>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = false,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<List<CedisRutaInventario>> ObtenCedisRutasIntegracion(string urlApi, int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<List<CedisRutaInventario>>(urlApi, timeOut);
            }
            catch (Exception e)
            {
                var result = new Result<List<CedisRutaInventario>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<List<InventoryCtrl.Trip>> TripInventoryIntegracion(string urlApi, int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<List<InventoryCtrl.Trip>>(urlApi, timeOut);
            }
            catch (Exception e)
            {
                var result = new Result<List<InventoryCtrl.Trip>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        #endregion
        public class CedisRutaInventario
        {
            public int Cedi { get; set; }
            public List<Ruta> Destinos { get; set; }
            public class Ruta
            {
                public int RouteCodeOpecd { get; set; }
                private int _enable;
                public int? Enable
                {
                    get { return _enable; }
                    set { _enable = value ?? 1; }

                }
            }
        }

        public Result<List<Jornada>> GetJornadaCerrada(string urlApi, int timeOut)
        {
            var result = new Result<List<Jornada>>();
            try
            {
                return Metodos.Conexion_Get<List<Jornada>>(urlApi, timeOut);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_VentasHelper.PostJornadaCerrada: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.ResultType = ResultTypes.Error;
                result.Objeto = new List<Jornada>();
                result.Mensaje = ex.Message;
            }
            return result;
        }

        #region Atributos
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }
        #endregion
    }
}
