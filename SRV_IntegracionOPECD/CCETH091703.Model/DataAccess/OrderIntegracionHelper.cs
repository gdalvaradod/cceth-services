﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using CCETH091703.Model.BR;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;

namespace CCETH091703.Model.DataAccess
{
    public class OrderIntegracionHelper
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        public Result<List<OrderGeneral>> GetOrder(string urlApi, int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<List<OrderGeneral>>(urlApi, timeOut);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_OrderIntegracionHelper.GetOrder: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                return new Result<List<OrderGeneral>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = new List<OrderGeneral>(),
                    Mensaje = ex.Message
                };
            }
        }

        public Result<List<OrderGeneral>> GetOrderByCliente(string urlApi, int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<List<OrderGeneral>>(urlApi, timeOut);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_OrderIntegracionHelper.GetOrderByCliente: " + ex.Message + "." + Environment.NewLine + ex.StackTrace + ".", true);
                return new Result<List<OrderGeneral>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = new List<OrderGeneral>(),
                    Mensaje = ex.Message
                };
            }
        }

        public Result<string> PutOrder(string urlApi, int timeOut, string json)
        {
            var result = new Result<string>();
            try
            {
                return Metodos.Conexion_Put<string>(urlApi, timeOut, json);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_OrderIntegracionHelper.PutOrder: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
            }
            return result;
        }

        public Result<ConfiguracionAutomatica> GetConfigAuto(string urlApi, int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<ConfiguracionAutomatica>(urlApi, timeOut);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_OrderIntegracionHelper.GetConfigAuto: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                return new Result<ConfiguracionAutomatica>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = new ConfiguracionAutomatica(),
                    Mensaje = ex.Message
                };
            }
        }

        public Result<string> PutStatusAuto(string urlApi, int timeOut, string json)
        {
            var result = new Result<string>();
            try
            {
                return Metodos.Conexion_Put<string>(urlApi, timeOut, json);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_OrderIntegracionHelper.PutStatusAuto: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
            }
            return result;
        }

        public Result<List<CedisRutaInventario>> ObtenCedisRutasIntegracion(string urlApi, int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<List<CedisRutaInventario>>(urlApi, timeOut);
            }
            catch (Exception e)
            {
                var result = new Result<List<CedisRutaInventario>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }
    }
}