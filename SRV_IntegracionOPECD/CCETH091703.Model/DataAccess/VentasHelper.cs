﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCETH091703.Model.BR;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;

namespace CCETH091703.Model.DataAccess
{
    class VentasHelper
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }


        public Result<List<GetSalesResponse>> PostVentasOpeCd(string urlApi, int timeOut, string json)
        {
            var result = new Result<List<GetSalesResponse>>();
            try
            {
                return Metodos.Conexion_Post<List<GetSalesResponse>>(urlApi, timeOut, json);
            }
            catch (Exception ex)
            {
                result.ResultType = ResultTypes.Error;
                result.Objeto = null;
                result.Mensaje = ex.Message;
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_VentasHelper.PostVentasOpeCd: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
            }
            return result;
        }

        public Result<List<Ventas>> GetVentasIntegracion(string urlApi, int timeOut)
        {
            var result = new Result<List<Ventas>>();
            try
            {
                return Metodos.Conexion_Get<List<Ventas>>(urlApi, timeOut);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_VentasHelper.PostJornadaCerrada: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.ResultType = ResultTypes.Error;
                result.Objeto=null;
                result.Mensaje = ex.Message;
            }
            return result;
        }

    }
}
