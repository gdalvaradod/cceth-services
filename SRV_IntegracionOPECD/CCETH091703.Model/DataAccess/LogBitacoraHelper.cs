﻿using CCETH091703.Model.Objects.BO;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using CCETH091703.Model.Services;

namespace CCETH091703.Model.DataAccess
{
    public class LogBitacoraHelper
    {
        public Result<List<LogBitacora>> GetLogBitacora(string urlApi, int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<List<LogBitacora>>(urlApi, timeOut);
            }
            catch (Exception e)
            {
                var result = new Result<List<LogBitacora>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<List<CedisRutaLog>> GetLogBitacoraAgrupadoCedis(string urlApi, int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<List<CedisRutaLog>>(urlApi, timeOut);
            }
            catch (Exception e)
            {
                var result = new Result<List<CedisRutaLog>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<LogBitacora> PostEnvioLogBitacora(string urlApi, int timeOut, LogBitacora logBitacora)
        {
            try
            {
                var cad = JsonConvert.SerializeObject(logBitacora);
                return Metodos.Conexion_Post<LogBitacora>(urlApi, timeOut, JsonConvert.SerializeObject(logBitacora));
            }
            catch (Exception e)
            {
                var result = new Result<LogBitacora>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<LogBitacora> PutEnvioLogBitacora(string urlApi, int timeOut, LogBitacora logBitacora)
        {
            try
            {
                var cad = JsonConvert.SerializeObject(logBitacora);
                return Metodos.Conexion_Put<LogBitacora>(urlApi, timeOut, JsonConvert.SerializeObject(logBitacora));
            }
            catch (Exception e)
            {
                var result = new Result<LogBitacora>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<LogBitacoraDetail> PostEnvioLogBitacoraDetail(string urlApi, int timeOut, LogBitacoraDetail logBitacoraDetail)
        {
            try
            {
                return Metodos.Conexion_Post<LogBitacoraDetail>(urlApi, timeOut, JsonConvert.SerializeObject(logBitacoraDetail));
            }
            catch (Exception e)
            {
                var result = new Result<LogBitacoraDetail>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }
    }
}
