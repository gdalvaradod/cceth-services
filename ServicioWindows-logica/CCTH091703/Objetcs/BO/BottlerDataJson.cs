﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCTH091703.Objetcs.BO
{
    public class BottlerDataJson
    {

        public BottlerDataJson() { }

        //[JsonProperty("bottler_id")]
        //public int BOTTLER_ID { get; set; }

        //[JsonProperty("cedi_id")]
        //public int CEDI_ID { get; set; }

        //[JsonProperty("cedi_name")]
        //public string CEDI_NAME { get; set; }

        [JsonProperty("route_id")]
        public int ROUTE_ID { get; set; }

        [JsonProperty("website")]
        public string WEBSITE { get; set; }

        [JsonProperty("store")]
        public string STORE { get; set; }

        [JsonProperty("storeview")]
        public string STOREVIEW { get; set; }

        [JsonProperty("postalcode")]
        public int POSTALCODE { get; set; }

        [JsonProperty("deliveryco")]
        public string DELIVERYCO { get; set; }

        [JsonProperty("status")]
        public int STATUS { get; set; }

        [JsonProperty("settlement")]
        public string SETTLEMENT { get; set; }

        [JsonProperty("delivery_date")]
        public string DELIVERY_DATE { get; set; }

        [JsonProperty("route_code")]
        public string ROUTE_CODE { get; set; }

        [JsonProperty("cedi_name")]
        public string CEDI_NAME { get; set; }
        // [JsonProperty("id")]
        // public int ORDER_GENERAL_ID { get; set; }

        // [JsonProperty("id")]
        // public int ID { get; set; }

    }
}
