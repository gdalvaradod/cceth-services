﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCTH091703.Objetcs.BO
{
    public class OrderCommentsJson
    {
        public OrderCommentsJson() { }

        [JsonProperty("entity_id")]
        public int ENTITY_ID { get; set; }

        [JsonProperty("parent_id")]
        public int PARENT_ID { get; set; }

        [JsonProperty("is_customer_notified")]
        public string IS_CUSTOMER_NOTIFIED { get; set; }

        [JsonProperty("is_visible_on_front")]
        public string IS_VISIBLE_ON_FRONT { get; set; }

        [JsonProperty("comment")]
        public string COMMENT { get; set; }

        [JsonProperty("status")]
        public string STATUS { get; set; }

        [JsonProperty("created_at")]
        public string CREATED_AT { get; set; }

        [JsonProperty("entity_name")]
        public string ENTITY_NAME { get; set; }

        [JsonProperty("store_id")]
        public string STORE_ID { get; set; }
    }
}
