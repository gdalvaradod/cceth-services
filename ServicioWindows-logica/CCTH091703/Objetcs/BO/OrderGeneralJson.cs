﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCTH091703.Objetcs.BO
{
    public class OrderGeneralJson
    {
        public OrderGeneralJson() { }

        //[JsonProperty("id")]
        //public int ID { get; set; }

        [JsonProperty("entity_id")]
        public int ENTITY_ID { get; set; }

        [JsonProperty("status")]
        public string STATUS { get; set; }

        [JsonProperty("coupon_code")]
        public string COUPON_CODE { get; set; }

        [JsonProperty("customer_id")]
        public int? CUSTOMER_ID { get; set; }

        [JsonProperty("shipping_description")]
        public string SHIPPING_DESCRIPTION { get; set; }

        [JsonProperty("base_discount_amount")]
        public decimal BASE_DISCOUNT_AMOUNT { get; set; }

        [JsonProperty("base_grand_total")]
        public decimal BASE_GRAND_TOTAL { get; set; }

        [JsonProperty("base_shipping_amount")]
        public decimal BASE_SHIPPING_AMOUNT { get; set; }

        [JsonProperty("base_shipping_tax_amount")]
        public decimal BASE_SHIPPING_TAX_AMOUNT { get; set; }

        [JsonProperty("base_subtotal")]
        public decimal BASE_SUBTOTAL { get; set; }

        [JsonProperty("base_tax_amount")]
        public decimal BASE_TAX_AMOUNT { get; set; }

        [JsonProperty("base_total_paid")]
        public decimal? BASE_TOTAL_PAID { get; set; }

        [JsonProperty("base_total_refunded")]
        public decimal? BASE_TOTAL_REFUNDED { get; set; }

        [JsonProperty("discount_amount")]
        public decimal DISCOUNT_AMOUNT { get; set; }

        [JsonProperty("grand_total")]
        public decimal GRAND_TOTAL { get; set; }

        [JsonProperty("shipping_amount")]
        public decimal SHIPPING_AMOUNT { get; set; }

        [JsonProperty("shipping_tax_amount")]
        public decimal SHIPPING_TAX_AMOUNT { get; set; }

        [JsonProperty("store_to_order_rate")]
        public decimal STORE_TO_ORDER_RATE { get; set; }

        [JsonProperty("subtotal")]
        public decimal SUBTOTAL { get; set; }

        [JsonProperty("tax_amount")]
        public decimal TAX_AMOUNT { get; set; }

        [JsonProperty("total_paid")]
        public decimal? TOTAL_PAID { get; set; }

        [JsonProperty("total_qty_ordered")]
        public Nullable<decimal> TOTAL_QTY_ORDERED { get; set; }

        [JsonProperty("total_refunded")]
        public decimal? TOTAL_REFUNDED { get; set; }

        [JsonProperty("base_shipping_discount_amount")]
        public decimal BASE_SHIPPING_DISCOUNT_AMOUNT { get; set; }

        [JsonProperty("base_subtotal_incl_tax")]
        public decimal BASE_SUBTOTAL_INCL_TAX { get; set; }

        [JsonProperty("base_total_due")]
        public decimal BASE_TOTAL_DUE { get; set; }

        [JsonProperty("shipping_discount_amount")]
        public decimal SHIPPING_DISCOUNT_AMOUNT { get; set; }

        [JsonProperty("subtotal_incl_tax")]
        public decimal SUBTOTAL_INCL_TAX { get; set; }

        [JsonProperty("total_due")]
        public decimal TOTAL_DUE { get; set; }

        [JsonProperty("increment_id")]
        public string INCREMENT_ID { get; set; }

        [JsonProperty("applied_rule_ids")]
        public string APPLIED_RULE_IDS { get; set; }

        [JsonProperty("base_currency_code")]
        public string BASE_CURRENCY_CODE { get; set; }

        [JsonProperty("customer_email")]
        public string CUSTOMER_EMAIL { get; set; }

        [JsonProperty("discount_description")]
        public string DISCOUNT_DESCRIPTION { get; set; }

        [JsonProperty("remote_ip")]
        public string REMOTE_IP { get; set; }

        [JsonProperty("store_currency_code")]
        public string STORE_CURRENCY_CODE { get; set; }

        [JsonProperty("store_name")]
        public string STORE_NAME { get; set; }

        [JsonProperty("created_at")]
        public string CREATED_AT { get; set; }

        [JsonProperty("total_item_count")]
        public short TOTAL_ITEM_COUNT { get; set; }

        [JsonProperty("shipping_incl_tax")]
        public decimal SHIPPING_INCL_TAX { get; set; }

        [JsonProperty("base_customer_balance_amount")]
        public decimal? BASE_CUSTOMER_BALANCE_AMOUNT { get; set; }

        [JsonProperty("customer_balance_amount")]
        public decimal? CUSTOMER_BALANCE_AMOUNT { get; set; }

        [JsonProperty("base_gift_cards_amount")]
        public decimal BASE_GIFT_CARDS_AMOUNT { get; set; }

        [JsonProperty("gift_cards_amount")]
        public decimal GIFT_CARDS_AMOUNT { get; set; }

        [JsonProperty("reward_points_balance")]
        public int? REWARD_POINT_BALANCE { get; set; }

        [JsonProperty("base_reward_currency_amount")]
        public decimal? BASE_REWARD_CURRENCY_AMOUNT { get; set; }

        [JsonProperty("reward_currency_amount")]
        public decimal? REWARD_CURRENCY_AMOUNT { get; set; }

        [JsonProperty("payment_method")]
        public string PAYMENT_METHOD { get; set; }

        // [JsonProperty("CC_TYPE")]//-----------
        // public string CC_TYPE { get; set; }

        [JsonProperty("bottler_data")]
        public BottlerDataJson BOTTLER_DATA { get; set; }

        [JsonProperty("addresses")]
        public List<OrderAddressesJson> ORDER_ADDRESSES { get; set; }

        [JsonProperty("order_items")]
        public List<OrderItemJson> ORDER_ITEMS { get; set; }

        [JsonProperty("order_comments")]
        public List<OrderItemJson> ORDER_COMMENTS { get; set; }
    }
}
