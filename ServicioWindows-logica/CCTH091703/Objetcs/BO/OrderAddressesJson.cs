﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCTH091703.Objetcs.BO
{
    public class OrderAddressesJson
    {
        public OrderAddressesJson() { }

        // [JsonProperty("id")]
        // public int ID { get; set; }

        [JsonProperty("entity_id")]
        public int ENTITY_ID { get; set; }

        [JsonProperty("parent_id")]
        public int PARENT_ID { get; set; }

        [JsonProperty("customer_address_id")]
        public int? CUSTOMER_ADDRESS_ID { get; set; }

        [JsonProperty("quote_address_id")]
        public int? QUOTE_ADDRESS_ID { get; set; }

        [JsonProperty("region_id")]
        public int? REGION_ID { get; set; }

        [JsonProperty("fax")]
        public int? FAX { get; set; }

        [JsonProperty("region")]
        public string REGION { get; set; }

        [JsonProperty("postcode")]
        public string POSTCODE { get; set; }

        [JsonProperty("lastname")]
        public string LASTNAME { get; set; }

        [JsonProperty("street")]
        public string STREET { get; set; }

        [JsonProperty("city")]
        public string CITY { get; set; }

        [JsonProperty("email")]
        public string EMAIL { get; set; }

        [JsonProperty("telephone")]
        public string TELEPHONE { get; set; }

        [JsonProperty("country_id")]
        public string COUNTRY_ID { get; set; }

        [JsonProperty("firstname")]
        public string FIRSTNAME { get; set; }

        [JsonProperty("address_type")]
        public string ADDRESS_TYPE { get; set; }

        [JsonProperty("prefix")]
        public string PREFIX { get; set; }

        [JsonProperty("middlename")]
        public string MIDDLENAME { get; set; }

        [JsonProperty("suffix")]
        public string SUFFIX { get; set; }

        [JsonProperty("company")]
        public string COMPANY { get; set; }

        [JsonProperty("vat_id")]
        public string VAT_ID { get; set; }

        [JsonProperty("vat_is_valid")]
        public string VAT_IS_VALID { get; set; }

        [JsonProperty("vat_request_id")]
        public string VAT_REQUEST_ID { get; set; }

        [JsonProperty("vat_request_date")]
        public string VAT_REQUEST_DATE { get; set; }

        [JsonProperty("vat_request_success")]
        public string VAT_REQUEST_SUCCESS { get; set; }

        [JsonProperty("giftregistry_item_id")]
        public string GIFTREGISTRY_ITEM_ID { get; set; }

        [JsonProperty("want_bill")]
        public string WANT_BILL { get; set; }

        [JsonProperty("rfc")]
        public string RFC { get; set; }

        [JsonProperty("billing_name")]
        public string BILLING_NAME { get; set; }

        [JsonProperty("number")]
        public string NUMBER { get; set; }

        [JsonProperty("number_int")]
        public string NUMBER_INT { get; set; }

        [JsonProperty("neighborhood")]
        public string NEIGHBORHOOD { get; set; }

        [JsonProperty("municipality")]
        public string MUNICIPALITY { get; set; }

        [JsonProperty("references")]
        public string REFERENCES { get; set; }

        [JsonProperty("alias")]
        public string ALIAS { get; set; }

        [JsonProperty("payment_method")]
        public string PAYMENT_METHOD { get; set; }

        [JsonProperty("customer_id")]
        public string CUSTOMER_ID { get; set; }

    }
}
