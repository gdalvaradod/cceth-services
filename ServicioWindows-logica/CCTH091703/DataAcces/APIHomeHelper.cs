﻿using CCTH091703.Objetcs.BO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CCTH091703.DataAcces
{
    public class APIHomeHelper
    {
        public string GetOrder()
        {
            string cad = "";
            try {
                //HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create("https://devko.coca-colaentuhogar.com/rest/V1/bottler/orders");
                //webrequest.Method = "GET";
                //webrequest.KeepAlive = false;
                //webrequest.Timeout = 5000;
                //webrequest.Headers.Add("Authorization", "Bearer poqod6gxqbuxoncfa7q5pl75ui61yf1y");
                //webrequest.ContentType = "application/json";

                //using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                //    if ((int)responsePr.StatusCode == 200)
                //    {
                //        using (StreamReader reader = new StreamReader(responsePr.GetResponseStream()))
                //        {
                //            cad = reader.ReadToEnd();
                //            reader.Close();
                //            reader.Dispose();
                //        }
                //    }
                //    else
                //    {
                //        cad = responsePr.ContentType;
                //    }

                string path = @"C:\Users\jorda\Desktop\GrupoGit\CCTH091703\app\json\APIHomeTEST.json";
                using (StreamReader reader = new StreamReader(path))
                {
                    cad = reader.ReadToEnd();
                    reader.Close();
                    reader.Dispose();
                }
            }
            catch (WebException e)
            {
                cad = e.Message;
            }
            return cad;
        }
        public string PostOrder(string JSON)
        {
            JArray array = JArray.Parse(JSON);
            List<OrderGeneralJson> ListOrder = new List<OrderGeneralJson>();
            foreach (var key in array) {
                var temp = key.First();
                var item = temp.First();
                OrderGeneralJson order = JsonConvert.DeserializeObject<OrderGeneralJson>(item.ToString());
                ListOrder.Add(order);
            }
            //string output = JsonConvert.SerializeObject(ListOrder);
            return APIIntegracionesPost(CreateJson(ListOrder));
        }
        private string APIIntegracionesPost(string json) {
            string resp = "";
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create("http://localhost:56382/API/Orden");
            webrequest.Method = "POST";
            webrequest.KeepAlive = false;
            webrequest.Timeout = 50000;
            webrequest.ContentType = "application/json";

            byte[] byteArray = Encoding.UTF8.GetBytes(json.ToString());
            webrequest.ContentLength = byteArray.Length;

            using (var streamWriter = new StreamWriter(webrequest.GetRequestStream()))
            {
                streamWriter.Write(json.ToString());
                streamWriter.Flush();
                streamWriter.Close();
            }

            try
            {
                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        resp = responsePr.ContentType;
                    }
                    else
                    {
                        resp = responsePr.ContentType;
                    }

            }
            catch (WebException e)
            {
                resp = e.Message;
            }
            return resp;
        }
        private string APIHomePost(string json)       
        {
            string resp = "";
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create("http://localhost:56382/API/Orden");
            webrequest.Method = "POST";
            webrequest.KeepAlive = false;
            webrequest.Timeout = 50000;
            webrequest.ContentType = "application/json";

            byte[] byteArray = Encoding.UTF8.GetBytes(json.ToString());
            webrequest.ContentLength = byteArray.Length;

            using (var streamWriter = new StreamWriter(webrequest.GetRequestStream()))
            {
                streamWriter.Write(json.ToString());
                streamWriter.Flush();
                streamWriter.Close();
            }

            try
            {
                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 200)
                    {
                        resp = responsePr.ContentType;
                    }
                    else
                    {
                        resp = responsePr.ContentType;
                    }

            }
            catch (WebException e)
            {
                resp = e.Message;
            }
            return resp;
        }
        private string CreateJson(List<OrderGeneralJson> ListOrder)
        {
            JArray json = new JArray();
            JObject Row = null;

            foreach (var key in ListOrder)
            {
                Row = JObject.FromObject(key);
                json.Add(Row);
            }
            string arry = json. ToString();
            return arry;
        }
    }
}
