﻿namespace CCETH091703.Base
{
    public class ApiCrmCnx
    {
        //Search
        public static string GetCustomer(string code)
        {
            return "/Api/Customer?id=" + code;
        }

        //Search
        public static string GetCustomerAll(string distributionCenterCode, string routeCode, string name, string barcode,
            string cuc, string postcode, string email, string createdon, string modifiedon)
        {
            const string cad = "/api/Customer?";
            var filtros = "active=1";
            if (!string.IsNullOrEmpty(distributionCenterCode))
            {
                filtros += "&distributionCenterCode=" + distributionCenterCode;
            }
            if (string.IsNullOrEmpty(filtros))
            {
                if (!string.IsNullOrEmpty(routeCode))
                {
                    filtros += "routeCode=" + routeCode;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(routeCode))
                {
                    filtros += "&routeCode=" + routeCode;
                }
            }
            if (string.IsNullOrEmpty(filtros))
            {
                if (!string.IsNullOrEmpty(name))
                {
                    filtros += "name=" + name;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(name))
                {
                    filtros += "&name=" + name;
                }
            }
            if (string.IsNullOrEmpty(filtros))
            {
                if (!string.IsNullOrEmpty(barcode))
                {
                    filtros += "barcode=" + barcode;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(barcode))
                {
                    filtros += "&barcode=" + barcode;
                }
            }
            if (string.IsNullOrEmpty(filtros))
            {
                if (!string.IsNullOrEmpty(cuc))
                {
                    filtros += "CUC=" + cuc;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(cuc))
                {
                    filtros += "&CUC=" + cuc;
                }
            }
            if (string.IsNullOrEmpty(filtros))
            {
                if (!string.IsNullOrEmpty(postcode))
                {
                    filtros += "postcode=" + postcode;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(postcode))
                {
                    filtros += "&postcode=" + postcode;
                }
            }
            if (string.IsNullOrEmpty(filtros))
            {
                if (!string.IsNullOrEmpty(email))
                {
                    filtros += "email=" + email;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(email))
                {
                    filtros += "&email=" + email;
                }
            }
            if (string.IsNullOrEmpty(filtros))
            {
                if (!string.IsNullOrEmpty(createdon))
                {
                    filtros += "createdOn=" + createdon;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(createdon))
                {
                    filtros += "&createdOn=" + createdon;
                }
            }
            if (string.IsNullOrEmpty(filtros))
            {
                if (!string.IsNullOrEmpty(modifiedon))
                {
                    filtros += "modifiedOn=" + modifiedon;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(modifiedon))
                {
                    filtros += "&modifiedOn=" + modifiedon;
                }
            }
            return cad + filtros;
        }
    }
}