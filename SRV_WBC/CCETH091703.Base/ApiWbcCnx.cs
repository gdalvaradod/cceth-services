﻿using System;

namespace CCETH091703.Base
{
    public class ApiWbcCnx
    {
        //Authenticate
        public static string PostToken()
        {
            return "/Token";
        }

        //Search
        public static string GetBranch(int? companyid, string code, string name, DateTime? syncDate)
        {
            const string cad = "/Branches?";
            var filtros = "";
            if (companyid != null)
            {
                filtros += "&companyid=" + companyid;
            }
            if (!string.IsNullOrEmpty(code))
            {
                filtros += "&code=" + code;
            }
            if (!string.IsNullOrEmpty(name))
            {
                filtros += "&code=" + name;
            }
            if (syncDate != null)
            {
                filtros += "&syncDate=" + syncDate;
            }
            filtros = filtros == "" ? "" : filtros.Remove(0, 1);
            return cad + filtros;
        }

        //Search
        public static string GetRoute(int branchId, string code)
        {
            return "/Routes?branchId=" + branchId + "&code=" + code;
        }

        //Search
        public static string PostRoute()
        {
            return "/Routes";
        }

        //Search
        public static string GetUser(string code, int branchId)
        {
            return "/Users?code=" + code + "&branchId=" + branchId;
        }

        //Search
        public static string GetCustomer(string code)
        {
            return "/Customers?code=" + code;
        }

        //Create
        public static string PostCustomer()
        {
            return "/Customers";
        }

        //Update
        public static string PutCustomer(string customerid)
        {
            return "/Customers/" + customerid;
        }

        //Search
        public static string GetCustomerVisit(string customerid)
        {
            return "/Customers/" + customerid + "/Visits/";
        }

        //Create
        public static string PostCustomerVisit(string customerid)
        {
            return "/Customers/" + customerid + "/Visits/";
        }

        //Update
        public static string PutCustomerVisit(int customerid, int routeId, int day, int visitType)
        {
            return "/Customers/" + customerid + "/Visits/" + routeId + "/" + day + "/" + visitType;
        }

        //Search
        public static string GetCustomerPrice(string customerid)
        {
            return "/Customers/" + customerid + "/PriceList/";
        }

        //Update
        public static string PutCustomerPrice(int customerid, long priceList)
        {
            return "/Customers/" + customerid + "/PriceList/" + priceList;
        }

        //Create
        public static string PostPriceList()
        {
            return "/PriceLists/";
        }

        //Update
        public static string PutPriceList(long pricelistid)
        {
            return "/PriceLists/" + pricelistid;
        }

        //Search
        public static string GetPriceList(long priceListId)
        {
            return "/PriceLists/" + priceListId;
        }

        //Search
        public static string GetPriceListProduct(long priceListId)
        {
            return "/PriceLists/" + priceListId + "/Products/";
        }

        //Update
        public static string PutPriceListProduct(long priceListId, int productId)
        {
            return "/PriceLists/" + priceListId + "/Products/" + productId;
        }

        //Delete
        public static string DeletePriceListProduct(long priceListId, int productId)
        {
            return "/PriceLists/" + priceListId + "/Products/" + productId;
        }

        //Search
        public static string GetProduct(string code)
        {
            return "/Products?code=" + code;
        }

        //Create
        public static string PostProduct()
        {
            return "/Products";
        }

        //Search
        public static string GetReplacement(string code)
        {
            return "/Replacements?code=" + code;
        }

        //Search
        public static string GetPromotion(string code)
        {
            return "/Promotions?code=" + code;
        }

        //Create
        public static string PostInventory()
        {
            return "/Inventories";
        }

        //Create
        public static string PostInventoryDetails(int inventoryId)
        {
            return "/Inventories/" + inventoryId + "/Products";
        }

        //Complete POST
        public static string ConfirmInventory(int inventoryId)
        {
            return "/Inventories/" + inventoryId + "/confirm";
        }

        //Complete GET
        public static string GetConfirmInventory(int inventoryId)
        {
            return "/Inventories/" + inventoryId + "/confirm";
        }

        //Complete DELETE
        public static string DeleteConfirmInventory(int inventoryId)
        {
            return "/Inventories/" + inventoryId + "/confirm";
        }

        public static string GetProductsInventory(int inventoryId)
        {
            return "/Inventories/" + inventoryId + "/products";
        }

        //Create
        public static string PostDelivery()
        {
            return "/Deliveries";
        }

        //Create
        public static string PostDeliveryProduct(int deliveryId)
        {
            return "/Deliveries/" + deliveryId + "/Products";
        }

        //Create
        public static string PostDeliveryReplacement(int deliveryId)
        {
            return "/Deliveries/" + deliveryId + "/Replacements";
        }

        //Create
        public static string PostDeliveryPromotion()
        {
            return "/Deliveries/Promotions";
        }

        //Create
        public static string PostDeliveryPromotionDetails(int promotionDeliveryId)
        {
            return "/Deliveries/Promotions/" + promotionDeliveryId + "/Details";
        }

        public static string GetPriceList(int code, int branchId)
        {
            return "/PriceLists?Code=" + code + "&branchId=" + branchId;
        }
    }
}