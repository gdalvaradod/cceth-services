﻿using System;

namespace CCETH091703.Base
{
    public class ApiIntegracionesCnx
    {
        public static string ActualizaClientesIntegraciones()
        {
            return "/api/OrdenWBC/PutCustomers";
        }

        //public static string GetOrdersWbc()
        //{
        //    return "/api/OrdenWBC/GetOrders";
        //}

        public static string GetOrdersWbc(string cedis,string ruta)
        {
            return "/api/OrdenWBC/GetOrdersWbc?cedis="+cedis+"&ruta="+ruta;
        }

        public static string GetCedisRutasInventarioWbc()
        {
            return "/api/OrdenWBC/CedisInfo";
        }

        public static string GetProductsAll(int cedisId)
        {
            return "/API/AdminProduct/GetProducts?cedisId=" + cedisId;
        }

        public static string PutProducts()
        {
            return "/API/AdminProduct/PutProducts";
        }

        public static string PostSalesIntegracion(int cedisId)
        {
            return "/api/VentasWBC/IntegrationSales?cedisId=" + cedisId;
        }

        public static string Viaje()
        {
            return "/api/Viaje";
        }

        public static string TripInventoryIntegra()
        {
            return "/api/inventario";
        }

        public static string GetTripInventoryIntegra(int cedid, string routecode, DateTime deliverydate,bool withsales=true)
        {
            return "/api/inventario?cedisID=" + cedid + "&routeID=" + routecode + "&deliveryDate=" +
                   deliverydate.ToString("yyyy-MM-dd")+"&withsales="+withsales;
        }

        public static string GetClienteGenerico(int branchId, int routeId)
        {
            return "/api/OrdenWBC/GetCucGeneric?cedis=" + branchId + "&route=" + routeId;
        }

        public static string GetClientesCediRuta(int branchId, int routeId)
        {
            return "/api/OrdenWBC/GetListCustomers?cedi=" + branchId + "&route=" + routeId;
        }

        public static string GetEstadoJornada(int cedisId, int route, string deliveryDate)
        {
            return "/api/jornadasFinalizadas?cedisId=" + cedisId + "&routeId=" + route + "&deliveryDate=" + deliveryDate;
        }

        public static string PostEstadoJornada()
        {
            return "/api/jornadasFinalizadas";
        }
        public static string PostLogBitacora()
        {
            return "/api/Log";
        }

        public static string PostLogBitacoraDetail()
        {
            return "/api/LogDetail";
        }

        public static string GetLogBitacora(int cedisId, int ruta, int? viaje, string deliveryDate, int estatus, bool groupedByCedis)
        {
            const string cad = "/api/Log";
            var filtros = "";
            if (string.IsNullOrEmpty(filtros))
            {
                filtros += "?groupedByCedis=" + groupedByCedis;
            }
            else
            {
                filtros += "&groupedByCedis=" + groupedByCedis;
            }
            if (string.IsNullOrEmpty(filtros))
            {
                if (cedisId > 0)
                {
                    filtros += "?cedisId=" + cedisId;
                }
            }
            else
            {
                if (cedisId > 0)
                {
                    filtros += "&cedisId=" + cedisId;
                }
            }
            if (string.IsNullOrEmpty(filtros))
            {
                if (ruta > 0)
                {
                    filtros += "?routeId=" + ruta;
                }
            }
            else
            {
                if (ruta > 0)
                {
                    filtros += "&routeId=" + ruta;
                }
            }
            if (string.IsNullOrEmpty(filtros))
            {
                if (viaje > 0)
                {
                    filtros += "?trip=" + viaje;
                }
            }
            else
            {
                if (viaje > 0)
                {
                    filtros += "&trip=" + viaje;
                }
            }
            if (string.IsNullOrEmpty(filtros))
            {
                if (!string.IsNullOrEmpty(deliveryDate))
                {
                    filtros += "?deliveryDate=" + deliveryDate;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(deliveryDate))
                {
                    filtros += "&deliveryDate=" + deliveryDate;
                }
            }
            if (string.IsNullOrEmpty(filtros))
            {
                if (estatus > 0)
                {
                    filtros += "?processSatus=" + estatus;
                }
            }
            else
            {
                if (estatus > 0)
                {
                    filtros += "&processSatus=" + estatus;
                }
            }
            return cad + filtros;
        }

        public static string PutLogBitacora(int logBitacoraId)
        {
            return "/api/Log/" + logBitacoraId;
        }

        public static string GetCustomerRelation(int cedi, string crmid = "", string barcode = "", int cuc = 0)
        {
            crmid = crmid ?? "";
            barcode = barcode ?? "";

            var filtros = "?cedi=" + cedi;
            filtros = filtros + (crmid != "" ? "&crmid=" + crmid : "");
            filtros = filtros + (barcode != "" ? "&barcode=" + barcode : "");
            filtros = filtros + (cuc != 0 ? "&cuc=" + cuc : "");
            return "/api/Customer/GetSearchFilterRelation" + filtros;
        }

        public static string GetPrecioClienteCoca(int cedi, string barcode, string deliveryDate)
        {
            var filtros = "?cedisId=" + cedi;
            filtros = filtros + (barcode != "" ? "&barcode=" + barcode : "");
            filtros = filtros + (deliveryDate != "" ? "&deliveryDate=" + deliveryDate : "");
            return "/api/PrecioClienteCoca" + filtros;
        }
    }
}