﻿namespace CCETH091703.Model.Objects.Enum
{
    public enum EnumEstatus
    {
        EN_PROCESO,
        ENVIADO,
        FALLO,
        PENDIENTE
    }
}
