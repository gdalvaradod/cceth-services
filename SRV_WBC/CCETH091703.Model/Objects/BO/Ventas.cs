﻿using System.Collections.Generic;

namespace CCETH091703.Model.Objects.BO
{
    public class Ventas
    {
        public int Id { get; set; }
        public int Codope { get; set; }
        public int Cuc { get; set; }
        public string Code { get; set; }
        public int WbcSaleId { get; set; }
        public bool Facturado { get; set; }
        public int RazonDev { get; set; }
        public int CedisId { get; set; }
        public int RouteCode { get; set; }
        public int Trip { get; set; }
        public int CustomerOpeId { get; set; }
        public string DeliveryDate { get; set; }
        public bool CustomerCCTH { get; set; }
        public List<VentasProductos> SaleProducts { get; set; }
        public List<VentasPromociones> SalePromotions { get; set; }

        public Ventas()
        {
            SaleProducts = new List<VentasProductos>();
            SalePromotions = new List<VentasPromociones>();
        }
    }

    public class PostSalesResponse
    {
        public Ventas Sale { get; set; }
        public bool Success { get; set; }
        public List<string> ErrorDescriptions { get; set; }
    }
}
