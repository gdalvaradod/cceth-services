﻿namespace CCETH091703.Model.Objects.BO
{
    public class VentasProductos
    {
        public int Product { get; set; }
        public int Quantity { get; set; }
        public int CreditAmount { get; set; }
    }
}
