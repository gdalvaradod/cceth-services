﻿using System;

namespace CCETH091703.Model.Objects.BO
{
    public class PriceList
    {
        public long PriceListId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int BranchId { get; set; }
        public string ValidityStart { get; set; }
        public string ValidityEnd { get; set; }
        public bool Master { get; set; }
    }
}