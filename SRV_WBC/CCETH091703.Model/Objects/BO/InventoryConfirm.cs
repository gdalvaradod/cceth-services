﻿namespace CCETH091703.Model.Objects.BO
{
    public class InventoryConfirm
    {
        public int ProductsAmount { get; set; }
        public int DeliveriesAmount { get; set; }
        public int ReplacementsAmount { get; set; }
        public int ImplementsAmount { get; set; }
    }
}
