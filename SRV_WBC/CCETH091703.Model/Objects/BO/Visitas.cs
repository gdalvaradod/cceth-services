﻿namespace CCETH091703.Model.Objects.BO
{
    public class Visitas
    {
        public int BinnacleVisitId { get; set; }
        public int BranchCode { get; set; }
        public int CustomerCode { get; set; }
        public int RouteCode { get; set; }
        public int UserCode { get; set; }
    }
}
