﻿using System;

namespace CCETH091703.Model.Objects.BO
{
    public class Customer
    {
        public string Id { get; set; }
        public int? CustomerId { get; set; }
        public string Name { get; set; }
        public string Cuc { get; set; }
        public string BarCode { get; set; }
        public long PriceList { get; set; }
        public int Sequence { get; set; }
        public string DistributionCenter { get; set; }
        public string RouteCode { get; set; }
        public string ManagerId { get; set; }
        public string Manager => string.IsNullOrEmpty(ManagerId) ? "0" : ManagerId;
        public string PhysicalAddress { get; set; }
        public string Street { get; set; }
        public string IndoorNumber { get; set; }
        public string OutdoorNumber { get; set; }
        public string Intersection1 { get; set; }
        public string Intersection2 { get; set; }
        public string CountryId { get; set; }
        public string Country => string.IsNullOrEmpty(CountryId) ? "0" : CountryId;
        public string StateId { get; set; }
        public string State => string.IsNullOrEmpty(StateId) ? "0" : StateId;
        public string CityId { get; set; }
        public string City => string.IsNullOrEmpty(CityId) ? "0" : CityId;
        public string NeighborhoodId { get; set; }
        public string Neighborhood => string.IsNullOrEmpty(NeighborhoodId) ? "0" : NeighborhoodId;
        public bool ClientType { get; set; }
        public bool CustomerVarious { get; set; }
        public string CustomerType { get; set; }
        public string Email { get; set; }
        public string PostCode { get; set; }
        public string Telephone { get; set; }
        private string _latitude;
        public string Latitude
        {
            get { return _latitude; }
            set { _latitude = value ?? ""; }
        }
        private string _longitude;
        public string Longitude
        {
            get { return _longitude;}
            set { _longitude = value ?? ""; }
        }
        public string CreatedOn { get; set; }
        public string ModifiedOn { get; set; }
        public string Contact => Name;
        public string Address => PhysicalAddress;
        public string Description => "";
        public string Code => Cuc;
        public string CrmId { get; set; }
        public string CrmCreateOn { get; set; }
        public Visit Visit { get; set; }
        
    }

    public class Visit
    {
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }
        public bool Sunday { get; set; }

        public int NumericFormat
        {
            get
            {
                int _NumericFormat = 0;
                if (Sunday) _NumericFormat |= 1;
                if (Monday) _NumericFormat |= 2;
                if (Tuesday) _NumericFormat |= 4;
                if (Wednesday) _NumericFormat |= 8;
                if (Thursday) _NumericFormat |= 16;
                if (Friday) _NumericFormat |= 32;
                if (Saturday) _NumericFormat |= 64;
                return _NumericFormat;
            }
            set
            {
                Sunday = (value & 1) > 0;
                Monday = (value & 2) > 0;
                Tuesday = (value & 4) > 0;
                Wednesday = (value & 8) > 0;
                Thursday = (value & 16) > 0;
                Friday = (value & 32) > 0;
                Saturday = (value & 64) > 0;
            }
        }

        private bool this[int index] => (NumericFormat >> index & 1) == 1;


        public DateTime ProximaVisita(DateTime fecha)
        {
            int diasem = (int)fecha.DayOfWeek;
            for (int x = diasem + 1; x < diasem + 8; x++)
            {
                if (this[x % 7])
                {
                    return fecha.AddDays(x - diasem);
                }
            }
            return fecha;
        }

    }

    public class CustomerWbc
    {

        public string CustomerId { get; set; }
        public string Contact { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }

        public bool Equals(CustomerWbc other)
        {
            return other != null && 
                   CustomerId + Contact + Address + Description + Email + Code + Name+ Active ==
                   other.CustomerId + other.Contact + other.Address + other.Description + other.Email +
                   other.Code + other.Name+other.Active;
        }
    }

    public class CustomerVisitWbc
    {
        public CustomerVisitWbc(int cid, int routeId, int day, int visitType, int order, bool act)
        {
            CustomerId = cid;
            RouteId = routeId;
            Day = day;
            VisitType = visitType;
            Order = order;
            Active = act;
        }

        public int CustomerId { get; set; }
        public int RouteId { get; set; }
        public int Day { get; set; }
        public int VisitType { get; set; }
        public int Order { get; set; }
        public bool Active { get; set; }
    }

    public class CustomerPriceListWbc
    {
        public CustomerPriceListWbc(int cid, long pid, bool act)
        {
            CustomerId = cid;
            PriceListId = pid;
            Active = act;
        }

        public int CustomerId { get; set; }
        public long PriceListId { get; set; }
        public bool Active { get; set; }
    }
}

