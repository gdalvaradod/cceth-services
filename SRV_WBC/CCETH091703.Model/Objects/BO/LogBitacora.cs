﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class LogBitacora
    {
        public LogBitacora()
        {
            Process = new ProcesosSistema();
            LogBitacoraDetail = new List<LogBitacoraDetail>();
        }

        [JsonProperty("Id")]
        public int Id { get; set; }
        [JsonProperty("DeliveryDate")]
        public string DeliveryDate { get; set; }
        [JsonProperty("CedisId")]
        public int CedisId { get; set; }
        [JsonProperty("RouteId")]
        public int RouteId { get; set; }
        [JsonProperty("Trip")]
        public int? Trip { get; set; }
        [JsonProperty("Process")]
        public ProcesosSistema Process { get; set; }
        [JsonProperty("ProcessStatus")]
        public int ProcessStatus { get; set; }
        [JsonProperty("LogDetail")]
        public List<LogBitacoraDetail> LogBitacoraDetail { get; set; }
    }
}
