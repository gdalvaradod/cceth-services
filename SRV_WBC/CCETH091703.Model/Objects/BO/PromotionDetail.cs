﻿using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class PromotionDetail
    {
        [JsonProperty("ProductId")]
        public int ProductId { get; set; }
        [JsonProperty("Amount")]
        public int Amount { get; set; }

        public string ProductCode { get; set; }
    }
}
