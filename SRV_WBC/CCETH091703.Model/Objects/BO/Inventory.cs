﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace CCETH091703.Model.Objects.BO
{
    public class Inventory
    {
        public int InventoryId { get; set; }
        public int UserId { get; set; }
        [JsonProperty("Code")]
        public string Code { get; set; }
        [JsonProperty("Date")]
        public string Date { get; set; }
 
        public int Order { get; set; }
        [JsonProperty("State")]
        public int State { get; set; }


        [JsonProperty("Active")]
        public bool Active { get; set; }
        
        
        [JsonProperty("Products")]
        public List<Product> Products { get; set; }
        [JsonProperty("Replacements")]
        public List<Replacement> Replacements { get; set; }
        [JsonProperty("Implements")]
        public List<Implement> Implements { get; set; }
        [JsonProperty("Deliveries")]
        public List<Product> Deliveries { get; set; }

        public int ProductsAmount
        {
            get
            {
                return (Products ?? new List<Product>()).Sum(product => (int) product.Amount);
            }
        }
        public int ReplacementsAmount
        {
            get
            {
                return (Replacements ?? new List<Replacement>()).Sum(replacement => (int) replacement.Amount);
            }
        }
        public int ImplementsAmount
        {
            get
            {
                return (Implements ?? new List<Implement>()).Sum(implement => implement.Amount);
            }
        }
        public int DeliveriesAmount
        {
            get
            {
                return (Deliveries ?? new List<Product>()).Sum(product => (int) product.Amount);
            }
        }
    }
}
