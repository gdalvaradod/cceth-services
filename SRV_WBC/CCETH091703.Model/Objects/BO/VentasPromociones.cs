﻿namespace CCETH091703.Model.Objects.BO
{
    public class VentasPromociones
    {
        public int Product { get; set; }
        public int Quantity { get; set; }
        public int Type { get; set; }
    }
}
