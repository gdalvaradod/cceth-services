﻿using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class User
    {
        [JsonProperty("UserId")]
        public int UserId { get; set; }
        [JsonProperty("Code")]
        public string Code { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("Type")]
        public string Type { get; set; }
    }
}
