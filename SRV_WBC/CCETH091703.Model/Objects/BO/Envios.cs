﻿using CCETH091703.Model.Objects.Enum;
using System;

namespace CCETH091703.Model.Objects.BO
{
    [Serializable]
    public class Envios
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string ConfirmationDate { get; set; }
        public string FechaCreacion { get; set; }
        public string FechaActualizacion { get; set; }
        public EnumEstatus Estatus { get; set; }
        public int Intentos { get; set; }
        public int TripNumber { get; set; }
        public int Sucursal { get; set; }
        public int Cliente { get; set; }
        public string TripDate { get; set; }
        public override string ToString()
        {
            return "Envio: id=" + Id + ",Descripcion=" + Descripcion + ",Estatus=" + Estatus+",Intentos="+Intentos;
        }
    }
}
