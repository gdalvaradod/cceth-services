﻿using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class Product
    {
        public Product(int pid, decimal am, int credam,string cod)
        {
            ProductId = pid;
            Amount = am;
            CreditAmount = credam;
            Code = cod;
            Price = 0;
        }

        //Datos enviados al API
        [JsonProperty("ProductId")]
        public int ProductId { get; set; }

        [JsonProperty("WBCId")]
        //public int WbcId => ProductId > 0 ? ProductId : WbcId;
        public int WbcId { get; set; }

        [JsonProperty("Code")]
        public string Code { get; set; }

        //Datos devueltos por el Stored
        //public string Product_Code { get; set; }
        public decimal Amount { get; set; }
        public decimal Price { get; set; }
        //public int Cash_Amount { get; set; }
        public int CreditAmount { get; set; }

        //Datos Product Integraciones
        [JsonProperty("Id")]
        public int Id { get; set; }
        [JsonProperty("CodigoProducto")]
        public string CodigoProducto { get; set; }
        [JsonProperty("Nombre")]
        public string Nombre { get; set; }
        [JsonProperty("Ruta")]
        public int Ruta { get; set; }
        [JsonProperty("Cedis")]
        public int Cedis { get; set; }
        [JsonProperty("Status")]
        public int Status { get; set; }
        //[JsonProperty("WBCId")]
        //public int WbcId { get; set; }
        [JsonProperty("Tipo")]
        public int Tipo { get; set; }
        [JsonProperty("CreateDate")]
        public string CreateDate { get; set; }
    }
}
