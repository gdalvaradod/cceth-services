﻿using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class Branch
    {
        [JsonProperty("BranchId")]
        public int BranchId { get; set; }
        [JsonProperty("Code")]
        public string Code { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
    }
}
