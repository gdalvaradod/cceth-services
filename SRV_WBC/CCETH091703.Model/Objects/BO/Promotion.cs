﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CCETH091703.Model.Objects.BO
{
    public class Promotion
    {
        [JsonProperty("PromotionId")]
        public int PromotionId { get; set; }
        [JsonProperty("Code")]
        public string Code { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("ValidityStart")]
        public string ValidityStart { get; set; }
        [JsonProperty("ValidityEnd")]
        public string ValidityEnd { get; set; }
        [JsonProperty("Type")]
        public int Type { get; set; }
        [JsonProperty("Status")]
        public bool Status { get; set; }
        [JsonProperty("Details")]
        public List<PromotionDetail> Details { get; set; }

        public string PromotionCode { get; set; }
    }
}
