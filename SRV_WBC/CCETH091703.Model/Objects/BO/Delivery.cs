﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace CCETH091703.Model.Objects.BO
{
    public class Delivery
    {
        [JsonProperty("increment_id")]
        public string IncrementId { get; set; }

        [JsonProperty("OPeID")]
        public string OpeId { get; set; }

        [JsonProperty("inventoryid")]
        public string InventoryId { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("Bottler_Data")]
        public BottlerDataJson BottlerData { get; set; }

        [JsonProperty("Create_From")]
        public string CreateFrom { get; set; }

        [JsonProperty("addresses")]
        public List<Address> Addresses { get; set; }
        
        [JsonProperty("order_items")]
        public List<OrderItem> OrderItems { get; set; }

        [JsonProperty("Products")]
        public List<Product> Products { get; set; }

        [JsonProperty("Replacements")]
        public List<Replacement> Replacements { get; set; }

        public void GeneraProductosReplacements()
        {
            foreach (var ordItem in OrderItems) 
            {
                //  remover para publicar
                //ordItem.Tipo = 1;

                switch(ordItem.Tipo)
                {
                    case 1:
                    case 2:
                        Products.Add(new Product(ordItem.WbcId, ordItem.QtyOrdered, 0,ordItem.ItemId.ToString()));
                        break;
                    case 4:
                        Replacements.Add(new Replacement(ordItem.WbcId, ordItem.QtyOrdered,ordItem.ItemId.ToString()));
                        break;
                }
            }
        }

        public int DeliveriesAmount
        {
            get
            {
                return (Products ?? new List<Product>()).Sum(product => (int)product.Amount);
            }
        }

        //public int DeliveryId { get; set; }
        //public int CustomerId { get; set; }
        //[JsonProperty("ReferenceCustomerCode")]
        //public int ReferenceCustomerCode { get; set; }
        //[JsonProperty("DeliveryNumber")]
        //public int DeliveryNumber { get; set; }
        //[JsonProperty("DeliveryCode")]
        //public string DeliveryCode { get; set; }
        //[JsonProperty("salenote")]
        //public int SaleNote { get; set; }
        //[JsonProperty("InventoryId")]
        //public int InventoryId { get; set; }
        //[JsonProperty("Products")]
        //public List<Product> Products { get; set; }
        //[JsonProperty("Replacements")]
        //public List<Replacement> Replacements { get; set; }
        //[JsonProperty("Promotions")]
        //public List<Promotion> Promotions { get; set; }
        //[JsonProperty("ReferenceCliente")]
        //public int ReferenceCliente { get; set; }

        public class OrderItem
        {
            public OrderItem(int itemid, decimal qty, int wbcid, int prodttype, int sku)
            {
                ItemId = itemid;
                QtyOrdered = qty;
                WbcId = wbcid;
                Tipo = prodttype;
                Sku = sku;
            }
            [JsonProperty("item_id")]
            public int ItemId { get; set; }
            [JsonProperty("sku")]
            public int Sku { get; set; }
            [JsonProperty("qty_ordered")]
            public decimal QtyOrdered { get; set; }
            [JsonProperty("WBCId")]
            public int WbcId { get; set; }
            [JsonProperty("PRODUCTTYPE")]
            public int Tipo { get; set; }
        }

        public class Address
        {
            public Address(string custid, int wanabill,string wbcid)
            {
                CustomerId = custid;
                WantBill = wanabill;
                WbcId = wbcid;
            }

            [JsonProperty("customer_id")]
            public string CustomerId { get; set; }

            [JsonProperty("BarCode")]
            public string Barcode { get; set; }

            [JsonProperty("CRM_Id")]
            public string CrmId { get; set; }

            [JsonProperty("want_bill")]
            public int WantBill { get; set; }

            public string WbcId { get; set; }
        }


    }
}
