﻿using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class Route
    {
        [JsonProperty("routeId")]
        public int RouteId { get; set; }
        [JsonProperty("branchId")]
        public int BranchId { get; set; }
        [JsonProperty("Code")]
        public string Code { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
    }
}
