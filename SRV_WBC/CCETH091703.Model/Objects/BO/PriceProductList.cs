﻿namespace CCETH091703.Model.Objects.BO
{
    public class PriceProductList
    {
        public decimal Price { get; set; }
        public int ProductId { get; set; }
        public decimal BasePrice { get; set; }
        public decimal Discount { get; set; }
        public decimal DiscountPercent { get; set; }
    }
}
