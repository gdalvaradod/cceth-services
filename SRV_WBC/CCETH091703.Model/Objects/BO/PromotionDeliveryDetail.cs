﻿using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class PromotionDeliveryDetail
    {
        [JsonProperty("productId")]
        public int ProductId { get; set; }
        [JsonProperty("deliveryPromotionId")]
        public int DeliveryPromotionId { get; set; }
        [JsonProperty("deliveryId")]
        public int DeliveryId { get; set; }
        [JsonProperty("amount")]
        public string Amount { get; set; }
        [JsonProperty("price")]
        public string Price { get; set; }
        [JsonProperty("import")]
        public string Import { get; set; }
        [JsonProperty("isGift")]
        public bool IsGift { get; set; }
    }
}
