﻿using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class PromotionDelivery
    {
        [JsonProperty("promotionId")]
        public int PromotionId { get; set; }
        [JsonProperty("deliveryPromotionId")]
        public int DeliveryPromotionId { get; set; }
        [JsonProperty("deliveryId")]
        public int DeliveryId { get; set; }
        [JsonProperty("amount")]
        public string Amount { get; set; }
    }
}
