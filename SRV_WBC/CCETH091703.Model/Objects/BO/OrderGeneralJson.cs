﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CCETH091703.Model.Objects.BO
{
    public class OrderGeneralJson
    {
        [JsonProperty("entity_id")]
        public int EntityId { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("coupon_code")]
        public string CouponCode { get; set; }

        [JsonProperty("customer_id")]
        public int? CustomerId { get; set; }

        [JsonProperty("shipping_description")]
        public string ShippingDescription { get; set; }

        [JsonProperty("base_discount_amount")]
        public decimal BaseDiscountAmount { get; set; }

        [JsonProperty("base_grand_total")]
        public decimal BaseGrandTotal { get; set; }

        [JsonProperty("base_shipping_amount")]
        public decimal BaseShippingAmount { get; set; }

        [JsonProperty("base_shipping_tax_amount")]
        public decimal BaseShippingTaxAmount { get; set; }

        [JsonProperty("base_subtotal")]
        public decimal BaseSubtotal { get; set; }

        [JsonProperty("base_tax_amount")]
        public decimal BaseTaxAmount { get; set; }

        [JsonProperty("base_total_paid")]
        public decimal? BaseTotalPaid { get; set; }

        [JsonProperty("base_total_refunded")]
        public decimal? BaseTotalRefunded { get; set; }

        [JsonProperty("discount_amount")]
        public decimal DiscountAmount { get; set; }

        [JsonProperty("grand_total")]
        public decimal GrandTotal { get; set; }

        [JsonProperty("shipping_amount")]
        public decimal ShippingAmount { get; set; }

        [JsonProperty("shipping_tax_amount")]
        public decimal ShippingTaxAmount { get; set; }

        [JsonProperty("store_to_order_rate")]
        public decimal StoreToOrderRate { get; set; }

        [JsonProperty("subtotal")]
        public decimal Subtotal { get; set; }

        [JsonProperty("tax_amount")]
        public decimal TaxAmount { get; set; }

        [JsonProperty("total_paid")]
        public decimal? TotalPaid { get; set; }

        [JsonProperty("total_qty_ordered")]
        public decimal? TotalQtyOrdered { get; set; }

        [JsonProperty("total_refunded")]
        public decimal? TotalRefunded { get; set; }

        [JsonProperty("base_shipping_discount_amount")]
        public decimal BaseShippingDiscountAmount { get; set; }

        [JsonProperty("base_subtotal_incl_tax")]
        public decimal BaseSubtotalInclTax { get; set; }

        [JsonProperty("base_total_due")]
        public decimal BaseTotalDue { get; set; }

        [JsonProperty("shipping_discount_amount")]
        public decimal ShippingDiscountAmount { get; set; }

        [JsonProperty("subtotal_incl_tax")]
        public decimal SubtotalInclTax { get; set; }

        [JsonProperty("total_due")]
        public decimal TotalDue { get; set; }

        [JsonProperty("increment_id")]
        public string IncrementId { get; set; }

        [JsonProperty("applied_rule_ids")]
        public string AppliedRuleIds { get; set; }

        [JsonProperty("base_currency_code")]
        public string BaseCurrencyCode { get; set; }

        [JsonProperty("customer_email")]
        public string CustomerEmail { get; set; }

        [JsonProperty("discount_description")]
        public string DiscountDescription { get; set; }

        [JsonProperty("remote_ip")]
        public string RemoteIp { get; set; }

        [JsonProperty("store_currency_code")]
        public string StoreCurrencyCode { get; set; }

        [JsonProperty("store_name")]
        public string StoreName { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("total_item_count")]
        public short TotalItemCount { get; set; }

        [JsonProperty("shipping_incl_tax")]
        public decimal ShippingInclTax { get; set; }

        [JsonProperty("base_customer_balance_amount")]
        public decimal? BaseCustomerBalanceAmount { get; set; }

        [JsonProperty("customer_balance_amount")]
        public decimal? CustomerBalanceAmount { get; set; }

        [JsonProperty("base_gift_cards_amount")]
        public decimal BaseGiftCardsAmount { get; set; }

        [JsonProperty("gift_cards_amount")]
        public decimal GiftCardsAmount { get; set; }

        [JsonProperty("reward_points_balance")]
        public int? RewardPointBalance { get; set; }

        [JsonProperty("base_reward_currency_amount")]
        public decimal? BaseRewardCurrencyAmount { get; set; }

        [JsonProperty("reward_currency_amount")]
        public decimal? RewardCurrencyAmount { get; set; }

        [JsonProperty("payment_method")]
        public string PaymentMethod { get; set; }

        [JsonProperty("bottler_data")]
        public BottlerDataJson BottlerData { get; set; }

        [JsonProperty("addresses")]
        public List<OrderAddressesJson> OrderAddresses { get; set; }

        [JsonProperty("order_items")]
        public List<OrderItemJson> OrderItems { get; set; }

        [JsonProperty("order_comments")]
        public List<OrderItemJson> OrderComments { get; set; }
    }
}
