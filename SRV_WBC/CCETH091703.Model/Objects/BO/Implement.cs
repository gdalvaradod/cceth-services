﻿using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class Implement
    {
        public int Amount { get; set; }
        public string ImplementCode { get; set; }
        public int ImplementId { get; set; }

        //Datos enviados al API
        [JsonProperty("ProductId")]
        public int ProductId { get; set; }
        [JsonProperty("CategoryId")]
        public int CategoryId { get; set; }
        [JsonProperty("BillingDataId")]
        public int BillingDataId { get; set; }
        [JsonProperty("BottleId")]
        public int BottleId { get; set; }
        [JsonProperty("Code")]
        public string Code { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("IsReturnable")]
        public bool IsReturnable { get; set; }
        [JsonProperty("Factor")]
        public long Factor { get; set; }
        [JsonProperty("Pieces")]
        public long Pieces { get; set; }
        [JsonProperty("Status")]
        public bool Status { get; set; }
        [JsonProperty("Type")]
        public int Type { get; set; }
    }
}
