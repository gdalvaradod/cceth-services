﻿using System;

namespace CCETH091703.Model.Objects.BO
{
    public class ViajeJornada
    {
        public int NumeroViaje { get; set; }
        public bool ViajeFinalizado { get; set; }
        public int Ruta { get; set; }
        public DateTime? InicioJornada { get; set; }
        public DateTime? FinJornada { get; set; }
        public bool JornadaFinalizada { get; set; }
    }
}
