﻿using Newtonsoft.Json;

namespace CCETH091703.Model.Objects.BO
{
    public class LogBitacoraDetail
    {
        [JsonProperty("Id")]
        public int Id { get; set; }
        [JsonProperty("LogId")]
        public int LogId { get; set; }
        [JsonProperty("Process")]
        public ProcesosSistema Process { get; set; }
        [JsonProperty("ErrorDescription")]
        public string ErrorDescription { get; set; }
        [JsonProperty("Success")]
        public bool Success { get; set; }
    }
}
