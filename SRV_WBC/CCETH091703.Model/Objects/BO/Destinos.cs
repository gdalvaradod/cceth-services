﻿namespace CCETH091703.Model.Objects.BO
{
    public class Destinos
    {
        public int Id { get; set; }
        public string RouteCodeCCHT { get; set; }
        public int RouteCodeOpecd { get; set; }
        public string Nombre { get; set; }
        public int? Manager { get; set; }
        public int Cuc { get; set; }
        public int CucCCTH { get; set; }
        public int Cedis { get; set; }
        public int? Enable { get; set; }
    }
}
