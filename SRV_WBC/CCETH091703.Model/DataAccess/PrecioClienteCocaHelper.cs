﻿using System;
using System.Collections.Generic;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;

namespace CCETH091703.Model.DataAccess
{
    public class PrecioClienteCocaHelper
    {
        public Result<List<PrecioClienteCoca>> GetPrecioClienteCoca(string url, int timeOut, string token)
        {
            try
            {
                return Metodos.Conexion_Get<List<PrecioClienteCoca>>(url, timeOut, token);
            }
            catch (Exception e)
            {
                var result = new Result<List<PrecioClienteCoca>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }
    }
}
