﻿using System;
using System.Collections.Generic;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;
using Newtonsoft.Json;

namespace CCETH091703.Model.DataAccess
{
    public class ProductHelper
    {

        public Result<List<Product>> GetProductAllIntegra(string urlApi, int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<List<Product>>(urlApi, timeOut);
            }
            catch (Exception e)
            {
                var result = new Result<List<Product>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<bool> PutProductoIntegra(string urlApi, int timeOut, List<Product> products)
        {
            try
            {
                return Metodos.Conexion_Put<bool>(urlApi, timeOut, JsonConvert.SerializeObject(products));
            }
            catch (Exception e)
            {
                var result = new Result<bool>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = false,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<List<Product>> GetProductWbc(string urlApi, int timeOut, string token)
        {
            try
            {
                return Metodos.Conexion_Get<List<Product>>(urlApi, timeOut, token);
            }
            catch (Exception e)
            {
                var result = new Result<List<Product>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }
    }
}