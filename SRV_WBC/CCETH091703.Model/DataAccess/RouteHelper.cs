﻿using System;
using System.Collections.Generic;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;
using Newtonsoft.Json;

namespace CCETH091703.Model.DataAccess
{
    public class RouteHelper
    {

        public Result<List<Route>> GetRouteWbc(string url, int timeOut, string token)
        {
            try
            {
                return Metodos.Conexion_Get<List<Route>>(url, timeOut, token);
            }
            catch (Exception e)
            {
                var result = new Result<List<Route>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<Route> PostRouteWbc(string urlApi, int timeOut, string token, Route route)
        {
            try
            {
                return Metodos.Conexion_Post<Route>(urlApi, timeOut, JsonConvert.SerializeObject(route), token);
            }
            catch (Exception e)
            {
                var result = new Result<Route>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }
    }
}