﻿using System;
using System.Collections.Generic;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib;
using Newtonsoft.Json;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;

namespace CCETH091703.Model.DataAccess
{
    public class CustomerHelper
    {
        #region CRM

        public Result<List<string>> GetClienteCrmAll(string url, int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<List<string>>(url, timeOut);
            }
            catch (Exception e)
            {
                var result = new Result<List<string>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<Customer> GetClienteCrmComplete(string url, int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<Customer>(url, timeOut);
            }
            catch (Exception e)
            {
                var result = new Result<Customer>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        #endregion

        #region WBC

        /**Metodos Clientes**/
        public Result<CustomerWbc> PostClienteWbc(string urlApi, int timeOut, string token, CustomerWbc customer)
        {
            try
            {
                return Metodos.Conexion_Post<CustomerWbc>(urlApi, timeOut, JsonConvert.SerializeObject(customer), token);
            }
            catch (Exception e)
            {
                var result = new Result<CustomerWbc>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<List<CustomerWbc>> GetClienteWbc(string urlApi, int timeOut, string token)
        {
            try
            {
                return Metodos.Conexion_Get<List<CustomerWbc>>(urlApi, timeOut, token);
            }
            catch (Exception e)
            {
                var result = new Result<List<CustomerWbc>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<string> PutClienteWbc(string urlApi, int timeOut, string token, CustomerWbc customer)
        {
            try
            {
                return Metodos.Conexion_Put<string>(urlApi, timeOut, JsonConvert.SerializeObject(customer), token);
            }
            catch (Exception e)
            {
                var result = new Result<string>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        /**Metodos Clientes Visitas**/
        public Result<CustomerVisitWbc> PostClienteVisitaWbc(string urlApi, int timeOut, string token,
            CustomerVisitWbc customer)
        {
            try
            {
                return Metodos.Conexion_Post<CustomerVisitWbc>(urlApi, timeOut, JsonConvert.SerializeObject(customer), token);
            }
            catch (Exception e)
            {
                var result = new Result<CustomerVisitWbc>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<List<CustomerVisitWbc>> GetClienteVisitaWbc(string urlApi, int timeOut, string token)
        {
            try
            {
                return Metodos.Conexion_Get<List<CustomerVisitWbc>>(urlApi, timeOut, token);
            }
            catch (Exception e)
            {
                var result = new Result<List<CustomerVisitWbc>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<string> PutClienteVisitaWbc(string urlApi, int timeOut, string token, CustomerVisitWbc customer)
        {
            try
            {
                return Metodos.Conexion_Put<string>(urlApi, timeOut, JsonConvert.SerializeObject(customer), token);
            }
            catch (Exception e)
            {
                var result = new Result<string>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        /**Metodos Clientes PreciosLista**/
        public Result<CustomerPriceListWbc> GetClientePriceWbc(string urlApi, int timeOut, string token)
        {
            try
            {
                return Metodos.Conexion_Get<CustomerPriceListWbc>(urlApi, timeOut, token);
            }
            catch (Exception e)
            {
                var result = new Result<CustomerPriceListWbc>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<List<PriceList>> GetPriceListWbc(string urlApi, int timeOut, string token)
        {
            try
            {
                return Metodos.Conexion_Get<List<PriceList>>(urlApi, timeOut, token);
            }
            catch (Exception e)
            {
                var result = new Result<List<PriceList>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<string> PutClientePrecioWbc(string urlApi, int timeOut, string token,
            CustomerPriceListWbc customer)
        {
            try
            {
                return Metodos.Conexion_Put<string>(urlApi, timeOut, JsonConvert.SerializeObject(customer), token);
            }
            catch (Exception e)
            {
                var result = new Result<string>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        #endregion

        #region Integraciones

        public Result<object> ActualizaClienteIntegraciones(string urlApi, int timeOut, List<Customer> customer)
        {
            try
            {
                return Metodos.Conexion_Put<object>(urlApi, timeOut, JsonConvert.SerializeObject(customer));
            }
            catch (Exception e)
            {
                var result = new Result<object>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<Destinos> GetClienteGenerico(string urlApi, int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<Destinos>(urlApi, timeOut);
            }
            catch (Exception e)
            {
                var result = new Result<Destinos>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<List<Customer>> GetClientesCediRutaInt(string url, int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<List<Customer>>(url, timeOut);
            }
            catch (Exception e)
            {
                var result = new Result<List<Customer>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public static Result<List<CustomerRelation>> GetCustomerRelation(string urlApi, int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<List<CustomerRelation>>(urlApi, timeOut);
            }
            catch (Exception e)
            {
                var result = new Result<List<CustomerRelation>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                return result;
            }
        }
        #endregion
    }
}