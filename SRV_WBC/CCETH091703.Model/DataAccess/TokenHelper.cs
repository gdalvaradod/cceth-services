﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using CCETH091703.Model.BR;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;

namespace CCETH091703.Model.DataAccess
{
    public class TokenHelper
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        public Result<Token> GetToken(string urlApi, int timeOut, string json)
        {
            var result = new Result<Token>();
            try
            {
                return Metodos.Conexion_Post<Token>(urlApi, timeOut, json);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + "No se pudo obtener el token del Api_Wbc: "+ Environment.NewLine + "Error_TokenHelper.GetToken: " + ex.Message +
                    "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.ResultType = ResultTypes.Error;
                result.Objeto = null;
                result.Mensaje = ex.Message;
            }
            return result;
        }
    }
}