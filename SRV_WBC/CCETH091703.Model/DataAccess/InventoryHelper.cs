﻿using System;
using System.Collections.Generic;
using System.Globalization;
using CCETH091703.Model.BR;
using CCETH091703.Model.Services;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;
using Newtonsoft.Json;
using CCETH091703.Model.Objects.BO;

namespace CCETH091703.Model.DataAccess
{
    public class InventoryHelper
    {
        #region Ope

        //public Result<DeliveryCtrl.TripInventory> TripInventory(string urlApi,int timeOut, DeliveryCtrl.TripInventoryRequest tripInvReq)
        //{
        //    try
        //    {
        //        return Metodos.Conexion_Post<DeliveryCtrl.TripInventory>(urlApi, timeOut, JsonConvert.SerializeObject(tripInvReq));
        //    }
        //    catch (Exception e)
        //    {
        //        var result = new Result<DeliveryCtrl.TripInventory>
        //        {
        //            ResultType = ResultTypes.Error,
        //            Objeto = null,
        //            Mensaje = e.Message
        //        };
        //        Console.WriteLine(e);
        //        return result;
        //    }
        //}
        
        #endregion

        #region Integracion

        //public Result<bool> PublicaInventarioIntegracion(string urlApi,int timeOut, DeliveryCtrl.CedisTrip ceditrips)
        //{
        //    try
        //    {
        //        return Metodos.Conexion_Post<bool>(urlApi, timeOut, JsonConvert.SerializeObject(ceditrips));
        //    }
        //    catch (Exception e)
        //    {
        //        var result = new Result<bool>
        //        {
        //            ResultType = ResultTypes.Error,
        //            Objeto = false,
        //            Mensaje = e.Message
        //        };
        //        Console.WriteLine(e);
        //        return result;
        //    }
        //}

        public Result<List<InventoryCtrl.CedisRutaInventario>> ObtenCedisRutasIntegracion(string urlApi,int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<List<InventoryCtrl.CedisRutaInventario> >(urlApi, timeOut);
            }
            catch (Exception e)
            {
                var result = new Result<List<InventoryCtrl.CedisRutaInventario>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<List<DeliveryCtrl.Trip>> TripInventoryIntegracion(string urlApi,int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<List<DeliveryCtrl.Trip>>(urlApi, timeOut);
            }
            catch (Exception e)
            {
                var result = new Result<List<DeliveryCtrl.Trip>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<List<Jornada>> GetJornadaCerrada(string urlApi, int timeOut)
        {
            var result = new Result<List<Jornada>>();
            try
            {
                return Metodos.Conexion_Get<List<Jornada>>(urlApi, timeOut);
            }
            catch (Exception ex)
            {
                result.ResultType = ResultTypes.Error;
                result.Objeto = new List<Jornada>();
                result.Mensaje = ex.Message;
            }
            return result;
        }

        #endregion

        #region WBC

        public Result<Inventory> PublicaInventarioWbc(string urlApi, int timeOut, DeliveryCtrl.InventoryRequest preinv, string token)
        {
            try
            {
                var cad = JsonConvert.SerializeObject(preinv);
                return Metodos.Conexion_Post<Inventory>(urlApi, timeOut, JsonConvert.SerializeObject(preinv),token);
            }
            catch (Exception e)
            {
                var result = new Result<Inventory>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }   
        }
        
        public Result<Product> PublicaProductosInventarioWbc(string urlApi,int timeOut, DeliveryCtrl.Item item, string token)
        {
            try
            {
                return Metodos.Conexion_Post<Product>(urlApi, timeOut, JsonConvert.SerializeObject(item), token);
            }
            catch (Exception e)
            {
                var result = new Result<Product>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<InventoryConfirm> ConsultarInventarioConfirmadoWbc(string urlApi, int timeOut, string token)
        {
            try
            {
                return Metodos.Conexion_Get<InventoryConfirm>(urlApi, timeOut, token);
            }
            catch (Exception e)
            {
                var result = new Result<InventoryConfirm>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<InventoryConfirm> EliminarInventarioConfirmadoWbc(string urlApi, int timeOut, string token)
        {
            try
            {
                return Metodos.Conexion_Delete<InventoryConfirm>(urlApi, timeOut, token);
            }
            catch (Exception e)
            {
                var result = new Result<InventoryConfirm>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<string> ConfirmarInventarioWbc(string urlApi,int timeOut,  string token, InventoryCtrl.Confirmacion o)
        {
            try
            {
                return Metodos.Conexion_Put<string>(urlApi, timeOut, JsonConvert.SerializeObject(o), token);
            }
            catch (Exception e)
            {
                var result = new Result<string>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<List<Product>> ConsultarProductosInventarioWbc(string urlApi, int timeOut, string token)
        {
            try
            {
                return Metodos.Conexion_Get<List<Product>>(urlApi, timeOut, token);
            }
            catch (Exception e)
            {
                var result = new Result<List<Product>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }
        #endregion      
    }
}