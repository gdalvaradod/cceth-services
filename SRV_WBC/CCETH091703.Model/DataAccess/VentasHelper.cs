﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.Linq;
using CCETH091703.Base;
using CCETH091703.Model.BR;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;
using OpeCDLib.Services;

namespace CCETH091703.Model.DataAccess
{
    public class VentasHelper
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        public Result<List<ViajeJornada>> GetViajes(DownloadService service, int branchCode, DateTime date)
        {
            var result = new Result<List<ViajeJornada>>();
            var viajesList = new List<ViajeJornada>();
            try
            {
                var journeys = service.ReadJourneys(branchCode, date); // consultando rutas iniciadas por en WBC
                foreach (var journey in journeys)
                {
                    viajesList.AddRange(journey.Viajes.Select(viaje => new ViajeJornada
                    {
                        NumeroViaje = viaje.Numero,
                        ViajeFinalizado = viaje.Finalizado,
                        Ruta = journey.Ruta,
                        InicioJornada = journey.Inicio,
                        FinJornada = journey.Fin,
                        JornadaFinalizada = journey.Finalizada
                    }));
                }

                if (viajesList.Count > 0)
                {
                    result.ResultType = ResultTypes.Success;
                    result.Objeto = viajesList;
                }
                else
                {
                    result.ResultType = ResultTypes.Warning;
                    result.Objeto = null;
                    result.Mensaje = "No se encontraron viajes.";
                }
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_VentasHelper.GetViajes: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.ResultType = ResultTypes.Error;
                result.Objeto = null;
                result.Mensaje = ex.Message;
            }

            return result;
        }

        public Result<List<Ventas>> GetVentas(DownloadService service, int branchCode, int route, DateTime date,
            int trip)
        {
            var result = new Result<List<Ventas>>();
            var ventasList = new List<Ventas>();
            try
            {
                var sales = service.DownloadSales(branchCode, route, date, trip, false);
                    // consultando ventas disponibles

                var customerHelper = new CustomerHelper();
                var resultCustomerIdGen = customerHelper.GetClienteGenerico(
                    AppSettings()["URL_INTEGRACION_API"] +
                    ApiIntegracionesCnx.GetClienteGenerico(branchCode, route),
                    int.Parse(AppSettings()["TimeOutCnx"]));

                foreach (var sale in sales)
                {
                    var ventas = new Ventas
                    {
                        Codope = sale.CodOpe,
                        Cuc = sale.CUC,
                        Code = sale.Code,
                        WbcSaleId = sale.WBCSaleId,
                        Facturado = sale.Facturado,
                        RazonDev = sale.RazonDev,
                        RouteCode = route,
                        Trip = trip,
                        DeliveryDate = date.ToString("s").Substring(0, 10),
                        CedisId = branchCode,
                        SaleProducts = new List<VentasProductos>(),
                        SalePromotions = new List<VentasPromociones>()
                    };

                    if (resultCustomerIdGen.ResultType == ResultTypes.Success)
                    {
                        if (ventas.Cuc == 0)
                        {
                            ventas.Cuc = resultCustomerIdGen.Objeto.Cuc;
                        }
                    }

                    foreach (var details in sale.Detalles)
                    {
                        var ventasPro = new VentasProductos
                        {
                            Product = details.Producto,
                            Quantity = details.Cantidad,
                            CreditAmount = details.CantidadCredito
                        };
                        ventas.SaleProducts.Add(ventasPro);
                    }
                    foreach (var details in sale.Promociones)
                    {
                        var ventasPro = new VentasPromociones()
                        {
                            Product = details.Producto,
                            Quantity = details.Cantidad,
                            Type = details.Tipo
                        };
                        ventas.SalePromotions.Add(ventasPro);
                    }
                    if (ventas.SalePromotions == null)
                    {
                        ventas.SalePromotions = new List<VentasPromociones>();
                    }
                    if (ventas.SaleProducts == null)
                    {
                        ventas.SaleProducts = new List<VentasProductos>();
                    }
                    ventasList.Add(ventas);
                }
                if (ventasList.Count > 0)
                {
                    result.ResultType = ResultTypes.Success;
                    result.Objeto = ventasList;
                }
                else
                {
                    result.ResultType = ResultTypes.Warning;
                    result.Objeto = null;
                    result.Mensaje = "No se encontraron Ventas";
                }
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_VentasHelper.GetVentas: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.ResultType = ResultTypes.Error;
                result.Objeto = null;
                result.Mensaje = ex.Message;
            }

            return result;
        }

        public Result<List<Visitas>> GetVisitas(DownloadService service, int branchCode, int route, DateTime date)
        {
            var result = new Result<List<Visitas>>();
            var visitasList = new List<Visitas>();
            try
            {
                var visits = service.DownloadVisit(branchCode, route, date); // consultando ventas disponibles

                visitasList.AddRange(visits.Select(visit => new Visitas
                {
                    BinnacleVisitId = visit.BinnacleVisitId,
                    BranchCode = visit.BranchCode,
                    CustomerCode = visit.CustomerCode,
                    RouteCode = visit.RouteCode,
                    UserCode = visit.UserCode,
                }));
                if (visitasList.Count > 0)
                {
                    result.ResultType = ResultTypes.Success;
                    result.Objeto = visitasList;
                }
                else
                {
                    result.ResultType = ResultTypes.Warning;
                    result.Objeto = null;
                    result.Mensaje = "No se encontraron Visitas";
                }
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_VentasHelper.GetVisitas: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.ResultType = ResultTypes.Error;
                result.Objeto = null;
                result.Mensaje = ex.Message;
            }
            return result;
        }

        public Result<bool> PostVentasIntegracion(string urlApi, int timeOut, string json)
        {
            var result = new Result<bool>();
            try
            {
                return Metodos.Conexion_Post<bool>(urlApi, timeOut, json);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_VentasHelper.PostVentasIntegracion: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.ResultType = ResultTypes.Error;
                result.Objeto = false;
                result.Mensaje = ex.Message;
            }
            return result;
        }

        public Result<Jornada> PostJornadaCerrada(string urlApi, int timeOut,string json)
        {
            var result = new Result<Jornada>();
            try
            {
                return Metodos.Conexion_Post<Jornada>(urlApi,timeOut,json);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_VentasHelper.PostJornadaCerrada: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.ResultType = ResultTypes.Error;
                result.Objeto = null;
                result.Mensaje = ex.Message;
            }
            return result;
        }
    }
}