﻿using System;
using System.Collections.Generic;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;
using Newtonsoft.Json;

namespace CCETH091703.Model.DataAccess
{
    public class PromotionHelper
    {
        public Result<List<Promotion>> GetPromotionWbc(string urlApi, int timeOut, string token)
        {
            try
            {
                return Metodos.Conexion_Get<List<Promotion>>(urlApi, timeOut, token);
            }
            catch (Exception e)
            {
                var result = new Result<List<Promotion>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<PromotionDelivery> PostPromotionDeliveryWbc(string urlApi, int timeOut, string token, PromotionDelivery promotions)
        {
            try
            {
                return Metodos.Conexion_Post<PromotionDelivery>(urlApi, timeOut, JsonConvert.SerializeObject(promotions), token);
            }
            catch (Exception e)
            {
                var result = new Result<PromotionDelivery>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<PromotionDeliveryDetail> PostPromotionDeliveryDetailsWbc(string urlApi, int timeOut, string token, PromotionDeliveryDetail promotions)
        {
            try
            {
                return Metodos.Conexion_Post<PromotionDeliveryDetail>(urlApi, timeOut, JsonConvert.SerializeObject(promotions), token);
            }
            catch (Exception e)
            {
                var result = new Result<PromotionDeliveryDetail>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }
    }
}
