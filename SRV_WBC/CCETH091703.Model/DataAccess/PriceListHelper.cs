﻿using System;
using System.Collections.Generic;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;
using Newtonsoft.Json;

namespace CCETH091703.Model.DataAccess
{
    public class PriceListHelper
    {
        public Result<PriceList> GetPriceList(string url, int timeOut, string token)
        {
            try
            {
                return Metodos.Conexion_Get<PriceList>(url, timeOut, token);
            }
            catch (Exception e)
            {
                var result = new Result<PriceList>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<PriceList> PostPriceList(string urlApi, int timeOut, string token, PriceList priceList)
        {
            try
            {
                return Metodos.Conexion_Post<PriceList>(urlApi, timeOut, JsonConvert.SerializeObject(priceList), token);
            }
            catch (Exception e)
            {
                var result = new Result<PriceList>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<PriceList> PutPriceList(string urlApi, int timeOut, string token, PriceList priceList)
        {
            try
            {
                return Metodos.Conexion_Put<PriceList>(urlApi, timeOut, JsonConvert.SerializeObject(priceList), token);
            }
            catch (Exception e)
            {
                var result = new Result<PriceList>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<List<PriceProductList>> GetPriceListProduct(string url, int timeOut, string token)
        {
            try
            {
                return Metodos.Conexion_Get<List<PriceProductList>>(url, timeOut, token);
            }
            catch (Exception e)
            {
                var result = new Result<List<PriceProductList>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<PriceProductList> PutPriceListProduct(string urlApi, int timeOut, string token, PriceProductList priceProductList)
        {
            try
            {
                return Metodos.Conexion_Put<PriceProductList>(urlApi, timeOut, JsonConvert.SerializeObject(priceProductList), token);
            }
            catch (Exception e)
            {
                var result = new Result<PriceProductList>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

        public Result<string> DeletePriceListProduct(string url, int timeOut, string token)
        {
            try
            {
                return Metodos.Conexion_Delete<string>(url, timeOut, token);
            }
            catch (Exception e)
            {
                var result = new Result<string>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = e.Message
                };
                Console.WriteLine(e);
                return result;
            }
        }

    }
}
