﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using CCETH091703.Model.BR;
using CCETH091703.Model.Objects.BO;
using CCETH091703.Model.Services;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;
using Newtonsoft.Json;

namespace CCETH091703.Model.DataAccess
{
    public class DeliveryHelper
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        #region Integraciones
        public Result<List<Delivery>> ConsultarPedidosIntegra(string url,int timeOut)
        {
            try
            {
                return Metodos.Conexion_Get<List<Delivery>>(url, timeOut);
            }
            catch (Exception ex)
            {
                var result = new Result<List<Delivery>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = ex.Message
                };
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_DeliveryHelper.ConsultarPedidosIntegra: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                return result;
            }
        }
        #endregion

        #region WBC

        public Result<List<Branch>> GetBranchesWbc(string urlApi,int timeOut, string token)
        {
            try
            {
                return Metodos.Conexion_Get<List<Branch>>(urlApi, timeOut, token);
            }
            catch (Exception ex)
            {
                var result = new Result<List<Branch>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = ex.Message
                };
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_DeliveryHelper.GetBranchesWbc: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                return result;
            }
        }

        public Result<List<User>> GetUserWbc(string urlApi, int timeOut, string token)
        {
            try
            {
                return Metodos.Conexion_Get<List<User>>(urlApi, timeOut, token);
            }
            catch (Exception ex)
            {
                var result = new Result<List<User>>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = ex.Message
                };
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_DeliveryHelper.GetBranchesWbc: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                return result;
            }
        }

        public Result<object> PublicaPedidoProductoWbc(string urlApi, int timeOut, string token, object o)
        {
            try
            {
                return Metodos.Conexion_Post<object>(urlApi, timeOut, JsonConvert.SerializeObject(o), token);
            }
            catch (Exception ex)
            {
                var result = new Result<object>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = ex.Message
                };
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_DeliveryHelper.PublicaPedidoProductoWbc: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                return result;
            }
        }

        public Result<object> PublicaPedidoReplacementWbc(string urlApi,int timeOut, string token, object o)
        {
            try
            {
                return Metodos.Conexion_Post<object>(urlApi, timeOut, JsonConvert.SerializeObject(o), token);
            }
            catch (Exception ex)
            {
                var result = new Result<object>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = ex.Message
                };
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_DeliveryHelper.PublicaPedidoReplacementWbc: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                return result;
            }
        }

        public Result<DeliveryCtrl.DeliveryIdReq> PublicaPedidoWbc(string urlApi,int timeOut, string token, DeliveryCtrl.EnvioCabecera pedido)
        {
            try
            {
                return Metodos.Conexion_Post<DeliveryCtrl.DeliveryIdReq>(urlApi, timeOut, JsonConvert.SerializeObject(pedido), token);
            }
            catch (Exception ex)
            {
                var result = new Result<DeliveryCtrl.DeliveryIdReq>
                {
                    ResultType = ResultTypes.Error,
                    Objeto = null,
                    Mensaje = ex.Message
                };
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_DeliveryHelper.PublicaPedidoWbc: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                return result;
            }
        }

        #endregion
    }
}