﻿using System;
using System.Collections.Generic;
using CCETH091703.Model.BR;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.Linq;
using CCETH091703.Base;
using CCETH091703.Model.DataAccess;
using CCETH091703.Model.Objects.BO;
using CCETH091703.Model.Services.Tools;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;
using Newtonsoft.Json;


namespace CCETH091703.Model.Services
{
    public static class DeliveryCtrl
    {
        #region ClasesAuxiliares

        // Clases de auxiliares para uso local
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        public class DeliveryIdReq
        {
            public int DeliveryId { get; set; }
        }

        public class EnvioCabecera
        {
            public int CustomerId { get; set; }
            public int InventoryId { get; set; }
            public string Code { get; set; }
        }

        public class TripInventoryRequest
        {
            public List<CedisRouteDate> CedisRoutes { get; set; }
        }

        public class CedisRouteDate
        {
            public int CedisId { get; set; }
            public int Route { get; set; }
            public DateTime DeliveryDate { get; set; }
        }

        public class TripInventory
        {
            public List<CedisTrip> CedisTrips { get; set; }
        }

        public class CedisTrip
        {
            public int CedisId { get; set; }
            public List<Trip> TripList { get; set; }
        }

        public class Trip
        {
            public DateTime DeliveryDate { get; set; }
            public int Route { get; set; }
            public int Id { get; set; }
            public List<Item> ConfirmedItems { get; set; }
        }

        public class Item
        {
            public string ItemId { get; set; }
            public int Qty { get; set; }
            public int Amount => Qty;
            public string ProductId { get; set; }
            public decimal Price = 0;

            public int WbcId
            {
                get { return int.Parse(string.IsNullOrEmpty(ProductId) ? "0" : ProductId); }
                set { ProductId = value.ToString(); }
            }
        }

        public class InventoryRequest
        {
            public int Userid { get; set; }
            public string Code { get; set; }
            public int Order { get; set; }
            public string Date { get; set; }
            public int State { get; set; }
        }


        #endregion

        #region Integracion
        public static Result<List<Delivery>> ConsultarPedidosIntegration(string cedis,string ruta)
        {
            var resultConsultarPedidos = new Result<List<Delivery>>();
            try
            {
                var deliveryHelper = new DeliveryHelper();
                resultConsultarPedidos =
                    deliveryHelper.ConsultarPedidosIntegra(
                        AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.GetOrdersWbc(cedis,ruta),
                        int.Parse(AppSettings()["TimeOutCnx"]));
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_DeliveryCtrl.ConsultarPedidosIntegration: " + ex.Message + Environment.NewLine + ex.StackTrace,
                    true);
                resultConsultarPedidos.Objeto = null;
                resultConsultarPedidos.Mensaje = ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine;
                resultConsultarPedidos.ResultType = ResultTypes.Error;
            }
            return resultConsultarPedidos;
        }

        #endregion

        #region wbc

        private static DateTime _diaActual=DateTime.Today.AddDays(-1);
        private static readonly List<string> PedidosCompletos=new List<string>();

        public static Result<List<Delivery>> ProcesaPedidos(string urlIntegrationApi, string urlOpeApi,
            string urlWbcApi,string token,string cedis,string ruta)
        {
            if (_diaActual != DateTime.Today)
            {
                PedidosCompletos.Clear();
                _diaActual=DateTime.Today;
            }


            Result<List<Delivery>> resultConsultarPedidosIntegration=new Result<List<Delivery>>();
            try
            {
                LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Iniciando proceso de Pedidos de Integracion a WBC.", true);
                resultConsultarPedidosIntegration = ConsultarPedidosIntegration(cedis,ruta);
                if (resultConsultarPedidosIntegration.ResultType == ResultTypes.Success)
                {
                    var pedidos = resultConsultarPedidosIntegration.Objeto;

                    // Eliminamos los pedidos que ya estén completos
                    for (int x=0;x<pedidos.Count;x++)
                    {
                        Delivery pedido = pedidos[x]; 
                        if (PedidosCompletos.Contains(pedido.IncrementId + ":" + pedido.BottlerData.TripId))
                        {
                            pedidos.Remove(pedido);
                            x--;
                        }
                    }

                    if (pedidos.Count > 0)
                    {
                  
                        var pedCedi = (from m in pedidos
                            where
                                m.BottlerData.Cediid !=0
                                && m.BottlerData.DeliveryDate.Substring(0, 10) == DateTime.Today.ToString("dd/MM/yyyy")
                            group m by m.BottlerData.Cediid); //.ToList();

                        foreach (var itemCedi in pedCedi)
                        {
                            var pedRoute = (from m in itemCedi
                                where m.BottlerData.Cediid == itemCedi.Key
                                group m by int.Parse(m.BottlerData.RouteCode)); //.ToList();

                            foreach (var itemRoute in pedRoute)
                            {

                                LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"],
                                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                    ": Enviando Inventarios de Integra a WBC para cedi:" + itemCedi.Key + "  ruta:" +
                                    itemRoute.Key + " de Integracion a WBC.", true);

                                var resultLogBitacora = LogBitacoraCtrl.GetLogBitacora(itemCedi.Key, itemRoute.Key, 0,
                                    DateTime.Today.ToString("yyyy-MM-dd"), 0);
                                int inventoryId = 0;

                                if (resultLogBitacora.ResultType == ResultTypes.Success)
                                {
                                    foreach (var logbitacora in resultLogBitacora.Objeto)
                                    {
                                        if (logbitacora.ProcessStatus == 2)
                                        {
                                            inventoryId = 0;
                                            continue;
                                        }

                                        if (logbitacora.Trip.Value ==
                                                int.Parse(itemRoute.ToList()[0].BottlerData.TripId) && logbitacora.LogBitacoraDetail.Any(x => x.Process.Id == 14 && x.Success))
                                        {
                                            continue;
                                        }
                                        // && !logbitacora.LogBitacoraDetail.Any(x => (x.Process.Id == 9) && x.Success)
                                        if (!logbitacora.LogBitacoraDetail.Any(x => x.Process.Id == 6 && x.Success))
                                            continue;
                                        {
                                            if (logbitacora.Trip.Value !=
                                                int.Parse(itemRoute.ToList().Find(x => x.BottlerData.DeliveryDate.Substring(0, 10) == DateTime.Today.ToString("dd/MM/yyyy")).BottlerData.TripId)) continue;
                                            var detailLog = logbitacora.LogBitacoraDetail.Find(x => x.Process.Id == 6);
                                            var cad = detailLog.ErrorDescription.Split(':');
                                            inventoryId = int.Parse(cad[1].Trim());
                                        }
                                    }
                                }

                                /*
                                var resultEnvioInventarioIntegraWbc = InventoryCtrl.EnvioInventario_Integra_Wbc(
                                    itemCedi.Key,
                                    itemRoute.Key.ToString(),
                                    DateTime.Parse(itemRoute.ToList()[0].BottlerData.DeliveryDate.Substring(0, 10)), token);
                                if (resultEnvioInventarioIntegraWbc.ResultType != ResultTypes.Success)
                                {
                                    if (resultEnvioInventarioIntegraWbc.ResultType == ResultTypes.Error)
                                    {
                                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                        ": Error_InventoryCtrl.EnvioInventario_Integra_Wbc: No se envio el inventario de integra a wbc para cedis:" +
                                        itemCedi.Key + ", ruta:" + itemRoute.Key + ", fecha: " + DateTime.Today + "."+Environment.NewLine + resultEnvioInventarioIntegraWbc.Mensaje, true);
                                        LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"],
                                            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                            ": No se envio el inventario de integra a wbc."+Environment.NewLine, true);
                                    }
                                    continue;
                                }
                                */

                                if (inventoryId != 0)
                                {
                                    LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"],
                                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                        ": Consolidando pedidos genéricos.", true);
                                    // consolidar pedidos genéricos
                                    var pedidositemRoute = itemRoute.ToList();
                                    //Delivery pedidoGenerico = new Delivery();
                                    //pedidoGenerico.Addresses = new List<Delivery.Address>();
                                    //pedidoGenerico.OrderItems = new List<Delivery.OrderItem>();
                                    //foreach (Delivery pedido in pedidositemRoute) //pedidos)
                                    //{
                                    //    if (pedido.Addresses[0].WantBill == 0 &&
                                    //        (string.IsNullOrEmpty(pedido.Addresses[0].CustomerId) ||
                                    //         pedido.Addresses[0].CustomerId == "0"))
                                    //    {
                                    //        pedidoGenerico.Addresses.Add(new Delivery.Address(
                                    //            pedido.Addresses[0].CustomerId,
                                    //            pedido.Addresses[0].WantBill,
                                    //            pedido.Addresses[0]
                                    //                .WbcId)); //  Customer_id = pedido.Addresses[0].Customer_id;
                                    //        pedidoGenerico.BottlerData = pedido.BottlerData;

                                    //        foreach (Delivery.OrderItem orderitem in pedido.OrderItems)
                                    //        {
                                    //            var z = pedidoGenerico.OrderItems.Find(
                                    //                x => x.ItemId == orderitem.ItemId);
                                    //            if (z != null)
                                    //            {
                                    //                z.QtyOrdered += orderitem.QtyOrdered;
                                    //            }
                                    //            else
                                    //            {
                                    //                pedidoGenerico.OrderItems.Add(orderitem);
                                    //            }
                                    //        }
                                    //    }

                                    //}

                                    //pedidositemRoute.RemoveAll(x =>
                                    //    x.Addresses[0].WantBill == 0 &&
                                    //    (string.IsNullOrEmpty(x.Addresses[0].CustomerId) ||
                                    //     x.Addresses[0].CustomerId == "0"));
                                    //pedidositemRoute.Add(pedidoGenerico);
                                    
                                    
                                    // fin consolidar pedidos genéricos
                                    LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"],
                                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                        ": Pedidos genéricos consolidados.", true);
                                    LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"],
                                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                        ": Procesando Pedido.", true);
                                    foreach (var pedido in pedidositemRoute) //itemRoute.ToList())
                                    {
                                        pedido.Products = new List<Product>();
                                        pedido.Replacements = new List<Replacement>();
                                        pedido.GeneraProductosReplacements();

                                        var envioCabecera = new EnvioCabecera
                                        {
                                            InventoryId = inventoryId
                                        };

                                        var x = CustomerCtrl.GetClienteWbc(token, pedido.Addresses[0].Barcode);

                                        if (x.ResultType != ResultTypes.Success)
                                        {
                                            continue;
                                        }

                                        envioCabecera.CustomerId =
                                            int.Parse(x.Objeto[0].CustomerId);

                                        var resultBranchId = InventoryCtrl.GetBranchWbc(itemCedi.Key, token);
                                        if (resultBranchId.ResultType != ResultTypes.Success)
                                        {
                                            continue;
                                        }

                                        var codigo = GeneraCode.Code(DateTime.Parse(
                                                pedido.BottlerData.DeliveryDate.Substring(0, 10)),
                                            resultBranchId.Objeto[0].BranchId.ToString(),
                                            itemRoute.Key.ToString(), pedido.BottlerData.TripId);

                                        envioCabecera.Code =
                                            codigo + "-" +
                                            pedido.Addresses[0].Barcode;

                                        var cust = new Customer
                                        {
                                            DistributionCenter = itemCedi.Key.ToString(),
                                            RouteCode = itemRoute.Key.ToString(),
                                            BarCode = pedido.Addresses[0].Barcode
                                        };
                                        var precioClienteCocaList = new List<PrecioClienteCoca>();
                                        if (pedido.CreateFrom.Contains("COCACOLA"))
                                        {
                                            precioClienteCocaList = CustomerCtrl.GenerarListaPrecioCustomerCcth(cust, token,
                                                int.Parse(x.Objeto[0].CustomerId), 0);
                                        }

                                        LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"],
                                            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                            ": Publicando pedido en WBC.", true);

                                        var resultPublicaPedidoWbc = PublicaPedidoWbc(token, envioCabecera);
                                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " +
                                            resultPublicaPedidoWbc.ResultType +
                                            "_DeliveryCtrl.PublicaPedidoWbc: " +
                                            (string.IsNullOrEmpty(resultPublicaPedidoWbc.Mensaje)
                                                ? JsonConvert.SerializeObject(resultPublicaPedidoWbc.Objeto)
                                                : resultPublicaPedidoWbc.Mensaje), true);

                                        if (resultPublicaPedidoWbc.ResultType == ResultTypes.Success)
                                        {
                                            resultLogBitacora = LogBitacoraCtrl.GetLogBitacora(itemCedi.Key, itemRoute.Key, 0, DateTime.Today.ToString("yyyy-MM-dd"), 0);
                                            if (resultLogBitacora.ResultType == ResultTypes.Success)
                                            {
                                                foreach (var logbitacora in resultLogBitacora.Objeto)
                                                {
                                                    if (logbitacora.Trip.Value.ToString() == pedido.BottlerData.TripId &&
                                                        logbitacora.LogBitacoraDetail.All(y => y.Process.Id != 14))
                                                    {
                                                        ActualizaBitacora(logbitacora, 14, 1, false,
                                                            "Se creo correctamente la entrega: " +
                                                            resultPublicaPedidoWbc.Objeto.DeliveryId);
                                                    }
                                                    else if (logbitacora.Trip.Value.ToString() == pedido.BottlerData.TripId &&
                                                             logbitacora.LogBitacoraDetail.Find(y => y.Process.Id == 14 && y.Success) == null)
                                                    {
                                                        ActualizaBitacora(logbitacora, 14, 1, false,
                                                            "Se creo correctamente la entrega: " +
                                                            resultPublicaPedidoWbc.Objeto.DeliveryId);
                                                    }
                                                }
                                            }

                                            var deliveryid = resultPublicaPedidoWbc.Objeto.DeliveryId;
                                            LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"],
                                                DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                ": Publicando productos del Pedido.", true);
                                            var promotionDeliveryId = 0;
                                            if (precioClienteCocaList?.FindAll(y => y.Price == 0).Count > 0)
                                            {
                                                var resultPromotion = PromotionCtrl.GetPromotionWbc("1", token);
                                                if (resultPromotion.ResultType == ResultTypes.Success)
                                                {
                                                    var amount = pedido.OrderItems.Sum(t1 => precioClienteCocaList.Where(t => t.Price == 0 && t.ItemId == t1.Sku).Sum(t => t1.QtyOrdered));
                                                    var promotionDelivery = new PromotionDelivery
                                                    {
                                                        DeliveryId = deliveryid,
                                                        PromotionId = resultPromotion.Objeto[0].PromotionId,
                                                        Amount = amount.ToString("##.###")
                                                    };
                                                    var resultPromotionDelivery = PromotionCtrl.PostPromotionDeliveryWbc(promotionDelivery, token);
                                                    if (resultPromotionDelivery.ResultType == ResultTypes.Success)
                                                    {
                                                        promotionDeliveryId =
                                                            resultPromotionDelivery.Objeto.DeliveryPromotionId;
                                                    }
                                                }
                                            }
                                            foreach (var producto in pedido.OrderItems)
                                            {
                                                if (producto.WbcId == 0)
                                                {
                                                    //buscar productid de WBC por medio de code
                                                    var resultGetProductWbc =
                                                        ProductCtrl.GetProductWbc(producto.Sku.ToString(), token);
                                                    if (resultGetProductWbc.ResultType == ResultTypes.Success)
                                                    {
                                                        producto.WbcId = resultGetProductWbc.Objeto[0].ProductId;
                                                    }
                                                    else
                                                    {
                                                        resultGetProductWbc.Mensaje =
                                                            "Producto " + producto.ItemId + " no obtuvo productId de WBC";
                                                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " +
                                                            resultGetProductWbc.ResultType +
                                                            "_DeliveryCtrl.ConfirmarInventario: " +
                                                            resultGetProductWbc.Mensaje, true);

                                                        continue;
                                                    }
                                                }
                                                var bandPromo = false;
                                                if (precioClienteCocaList?.FindAll(y => y.ItemId == producto.Sku && y.Price == 0).Count > 0)
                                                {
                                                    if (promotionDeliveryId > 0)
                                                    {
                                                        bandPromo = true;
                                                        var promotionDeliveryDetail =
                                                            new PromotionDeliveryDetail
                                                            {
                                                                DeliveryId = deliveryid,
                                                                DeliveryPromotionId =
                                                                    promotionDeliveryId,
                                                                Amount = decimal.Round(producto.QtyOrdered, 0).ToString(),
                                                                ProductId = producto.WbcId,
                                                                Price = "0",
                                                                Import = "0",
                                                                IsGift = true
                                                            };
                                                        var resultPromotionDeliveryDetail = PromotionCtrl.PostPromotionDeliveryDetailWbc(
                                                            promotionDeliveryId,
                                                            promotionDeliveryDetail, token);
                                                        if (resultPromotionDeliveryDetail.ResultType != ResultTypes.Success)
                                                        {
                                                            resultPromotionDeliveryDetail.Mensaje +=
                                                                Environment.NewLine + "No se pudo publicar el producto promocion " + producto.WbcId +
                                                                " en WBC";
                                                            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                                                DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " +
                                                                resultPromotionDeliveryDetail.ResultType +
                                                                "_DeliveryCtrl.DetallePromocion: " +
                                                                resultPromotionDeliveryDetail.Mensaje, true);
                                                        }
                                                    }
                                                }
                                                //promotionDeliveryId = 418808;
                                                if (bandPromo) continue;
                                                var resultPublicaPedidoProductoWbc = PublicaPedidoProductoWbc(token,
                                                    deliveryid.ToString(),
                                                    new
                                                    {
                                                        productid = producto.WbcId,
                                                        amount = (int)producto.QtyOrdered,
                                                        creditAmount = 0
                                                    });
                                                if (resultPublicaPedidoProductoWbc.ResultType == ResultTypes.Success)
                                                    continue;
                                                resultPublicaPedidoProductoWbc.Mensaje +=
                                                    Environment.NewLine + "No se pudo publicar el producto " + producto.WbcId +
                                                    " en WBC";
                                                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " +
                                                    resultPublicaPedidoProductoWbc.ResultType +
                                                    "_DeliveryCtrl.ConfirmarInventario: " +
                                                    resultPublicaPedidoProductoWbc.Mensaje, true);
                                            } // fin for productos

                                            LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"],
                                                DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                ": Terminó publicacion productos del Pedido.", true);

                                            LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"],
                                                DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                ": Generando Replacements para el pedido.", true);

                                            foreach (var replacement in pedido.Replacements)
                                            {
                                                if (replacement.ReplacementId == 0)
                                                {
                                                    //buscar productid de WBC por medio de code

                                                    var resultGetProductWbc =
                                                        ProductCtrl.GetProductWbc(replacement.Code, token);
                                                    if (resultGetProductWbc.ResultType == ResultTypes.Success)
                                                    {
                                                        replacement.ReplacementId =
                                                            resultGetProductWbc.Objeto[0].ProductId;
                                                    }
                                                    else
                                                    {
                                                        resultGetProductWbc.Mensaje =
                                                            "Replacement " + replacement.ReplacementId +
                                                            " no obtuvo productId de WBC";
                                                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " +
                                                            resultGetProductWbc.ResultType +
                                                            "_DeliveryCtrl.ConfirmarInventario: " +
                                                            resultGetProductWbc.Mensaje, true);
                                                        continue;
                                                    }
                                                }

                                                var resultPublicaPedidoReplacement =
                                                    PublicaPedidoReplacementWbc(token, deliveryid,
                                                        new
                                                        {
                                                            replacementId = replacement.ReplacementId,
                                                            amount = (int) replacement.Amount
                                                        });
                                                if (resultPublicaPedidoReplacement.ResultType != ResultTypes.Success)
                                                {

                                                    resultPublicaPedidoReplacement.Mensaje +=
                                                        Environment.NewLine+"No se pudo publicar el replacement " +
                                                        replacement.ReplacementId +
                                                        " en WBC";
                                                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                                        DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " +
                                                        resultPublicaPedidoReplacement.ResultType +
                                                        "_DeliveryCtrl.ConfirmarInventario: " +
                                                        resultPublicaPedidoReplacement.Mensaje, true);
                                                }
                                            }

                                            LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"],
                                                DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                ": Terminó generación de replacements.", true);

                                            //  Guardamos el pedido en la lista de Pedidos completos para el dia
                                                PedidosCompletos.Add(pedido.IncrementId + ":" + pedido.BottlerData.TripId);

                                        }
                                        else
                                        {
                                            PedidosCompletos.Clear();
                                            resultPublicaPedidoWbc.Mensaje += Environment.NewLine+"La cabecera del pedido " +
                                                                              JsonConvert.SerializeObject(
                                                                                  envioCabecera) +
                                                                              " no se envió a WBC";
                                            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                                DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " +
                                                resultPublicaPedidoWbc.ResultType +
                                                "_DeliveryCtrl.ConfirmarInventario: " + resultPublicaPedidoWbc.Mensaje,
                                                true);

                                            resultLogBitacora = LogBitacoraCtrl.GetLogBitacora(itemCedi.Key, itemRoute.Key, 0, DateTime.Today.ToString("yyyy-MM-dd"), 0);
                                            if (resultLogBitacora.ResultType == ResultTypes.Success)
                                            {
                                                foreach (var logbitacora in resultLogBitacora.Objeto)
                                                {
                                                    if (logbitacora.Trip.Value.ToString() == pedido.BottlerData.TripId &&
                                                        logbitacora.LogBitacoraDetail.All(y => y.Process.Id != 14))
                                                    {
                                                        ActualizaBitacora(logbitacora, 14, 3, true,
                                                            "No se creo correctamente la entrega");
                                                    }
                                                }
                                            }
                                        }
                                    } //fin for pedidos

                                    LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"],
                                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                        ": Confirmando Inventario.", true);

                                    if (resultConsultarPedidosIntegration.ResultType != ResultTypes.Success) continue;

                                    // --------------------------Fin pedido WBC --------------------
                                    LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"],
                                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                        ": Terminó proceso para el pedido.", true);
                                } // endif Envio inventarios Ope-Integra-WBC
                                else
                                {
                                    PedidosCompletos.Clear();
                                    resultConsultarPedidosIntegration.ResultType = ResultTypes.Warning;
                                    resultConsultarPedidosIntegration.Mensaje = "La entrega ya fue enviada o el inventario aun no existe en WBC.";
                                    LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"],
                                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                        ": La entrega ya fue enviada o el inventario aun no existe en WBC.", true);
                                }
                            }
                        }
                    }
                    else
                    {
                        PedidosCompletos.Clear();
                        resultConsultarPedidosIntegration.ResultType = ResultTypes.Warning;
                        resultConsultarPedidosIntegration.Mensaje = "No se leyeron pedidos de integración";
                        LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"],
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                            resultConsultarPedidosIntegration.Mensaje + ".", true);

                    }

                    // -------------------- Fin inventarios Wbc ---------------------
                }
                else
                {
                    resultConsultarPedidosIntegration.ResultType = ResultTypes.Warning;
                    resultConsultarPedidosIntegration.Mensaje = "No se leyeron pedidos de integración";
                }
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_DeliveryCtrl.ProcesaPedidos: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                resultConsultarPedidosIntegration.Objeto = null;
                resultConsultarPedidosIntegration.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                resultConsultarPedidosIntegration.ResultType = ResultTypes.Error;
            }
            finally
            {
                LogServicioBr.EscribirLog(AppSettings()["DeliveryLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Terminó proceso de Pedidos de Integracion a WBC."+Environment.NewLine, true);
            }
           
            return resultConsultarPedidosIntegration;
        }

        public static void ActualizaBitacora(LogBitacora logBitacora, int proceso, int estatus, bool error,
            string errorDescription)
        {
            try
            {
                logBitacora.ProcessStatus = estatus;
                logBitacora.Process = new ProcesosSistema { Id = proceso };
                var resultLogBitacora = LogBitacoraCtrl.PutEnvioLogBitacora(logBitacora);
                if (resultLogBitacora.ResultType != ResultTypes.Success) return;
                var logBitacoraDetail = new LogBitacoraDetail
                {
                    LogId = logBitacora.Id,
                    Process = new ProcesosSistema { Id = proceso },
                    Success = !error,
                    ErrorDescription = errorDescription
                };
                LogBitacoraCtrl.PostEnvioLogBitacoraDetail(logBitacoraDetail);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static Result<object> PublicaPedidoProductoWbc(string token, string deliveryid, object o)
        {
            var result = new Result<object>();
            try
            {
                var helper = new DeliveryHelper();
                result =
                    helper.PublicaPedidoProductoWbc(
                        AppSettings()["URL_WBC_API"] + ApiWbcCnx.PostDeliveryProduct(int.Parse(deliveryid)),
                        int.Parse(AppSettings()["TimeOutCnx"]), token, o);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_DeliveryCtrl.PublicaPedidoProductoWbc: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<object> PublicaPedidoReplacementWbc(string token, int deliveryid, object o)
        {
            var result = new Result<object>();
            try
            {
                var helper = new DeliveryHelper();
                result =
                    helper.PublicaPedidoReplacementWbc(
                        AppSettings()["URL_WBC_API"] + ApiWbcCnx.PostDeliveryReplacement(deliveryid),
                        int.Parse(AppSettings()["TimeOutCnx"]), token, o);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_DeliveryCtrl.PublicaPedidoReplacementWbc: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".",
                    true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<DeliveryIdReq> PublicaPedidoWbc(string token, EnvioCabecera pedido)
        {
            var result = new Result<DeliveryIdReq>();
            try
            {
                var helper = new DeliveryHelper();
                result = helper.PublicaPedidoWbc(AppSettings()["URL_WBC_API"] + ApiWbcCnx.PostDelivery(),
                    int.Parse(AppSettings()["TimeOutCnx"]), token, pedido);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_DeliveryCtrl.PublicaPedidoWbc: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        #endregion
    }
}