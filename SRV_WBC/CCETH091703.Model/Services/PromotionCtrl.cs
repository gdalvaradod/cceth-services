﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using CCETH091703.Base;
using CCETH091703.Model.BR;
using CCETH091703.Model.DataAccess;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;

namespace CCETH091703.Model.Services
{
    public class PromotionCtrl
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        public static Result<List<Promotion>> GetPromotionWbc(string promotionCode, string token)
        {
            var result = new Result<List<Promotion>>();
            try
            {
                var helper = new PromotionHelper();
                result = helper.GetPromotionWbc(AppSettings()["URL_WBC_API"] + ApiWbcCnx.GetPromotion(promotionCode),
                    int.Parse(AppSettings()["TimeOutCnx"]), token);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_PromotionCtrl.GetPromotionWbc: " +
                    ex.Message + "." + Environment.NewLine + ex.StackTrace + "." + Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "." + Environment.NewLine + ex.StackTrace + "." + Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<PromotionDelivery> PostPromotionDeliveryWbc(PromotionDelivery promotionDelivery, string token)
        {
            var result = new Result<PromotionDelivery>();
            try
            {
                var helper = new PromotionHelper();
                result = helper.PostPromotionDeliveryWbc(AppSettings()["URL_WBC_API"] + ApiWbcCnx.PostDeliveryPromotion(),
                    int.Parse(AppSettings()["TimeOutCnx"]), token, promotionDelivery);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_PromotionCtrl.PostPromotionDeliveryWbc: " +
                    ex.Message + "." + Environment.NewLine + ex.StackTrace + "." + Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "." + Environment.NewLine + ex.StackTrace + "." + Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<PromotionDeliveryDetail> PostPromotionDeliveryDetailWbc(int promotionDeliveryId, PromotionDeliveryDetail promotionDelivery, string token)
        {
            var result = new Result<PromotionDeliveryDetail>();
            try
            {
                var helper = new PromotionHelper();
                result = helper.PostPromotionDeliveryDetailsWbc(AppSettings()["URL_WBC_API"] + ApiWbcCnx.PostDeliveryPromotionDetails(promotionDeliveryId),
                    int.Parse(AppSettings()["TimeOutCnx"]), token, promotionDelivery);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_PromotionCtrl.PostPromotionDeliveryDetailWbc: " +
                    ex.Message + "." + Environment.NewLine + ex.StackTrace + "." + Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "." + Environment.NewLine + ex.StackTrace + "." + Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }
    }
}
