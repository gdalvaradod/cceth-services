﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.Linq;
using CCETH091703.Base;
using CCETH091703.Model.BR;
using CCETH091703.Model.DataAccess;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;
using System.Threading.Tasks;

namespace CCETH091703.Model.Services
{
    public static class ProductCtrl
    {
        private static Task _t;
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        #region Integra

        public static Result<List<Product>> GetProductAllIntegra(int cedisId)
        {
            var result = new Result<List<Product>>();
            try
            {
                var helper = new ProductHelper();
                result =
                    helper.GetProductAllIntegra(
                        AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.GetProductsAll(cedisId),
                        int.Parse(AppSettings()["TimeOutCnx"]));
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_ProductCtrl.GetProductAllIntegra: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }
        
        public static Result<Boolean> PutProductIntegra(List<Product> product)
        {
            var result = new Result<bool>();
            try
            {
                var helper = new ProductHelper();
                result =
                    helper.PutProductoIntegra(AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.PutProducts(),
                        int.Parse(AppSettings()["TimeOutCnx"]), product);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_ProductCtrl.PutProductIntegra: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.Objeto = false;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        #endregion

        #region WBC
        public static Result<List<Product>> GetProductWbc(string productCode, string token)
        {
            var result = new Result<List<Product>>();
            try
            {
                var helper = new ProductHelper();
                result = helper.GetProductWbc(AppSettings()["URL_WBC_API"] + ApiWbcCnx.GetProduct(productCode),
                    int.Parse(AppSettings()["TimeOutCnx"]), token);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_ProductCtrl.GetProductWbc: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        #endregion

        public static Task ProcesaProductosIntegraWbcAllAsync(Result<Token> resultToken)
        {
            try
            {
                if (_t != null)
                {
                    if (!_t.IsCompleted)
                    {
                        return _t;
                    }
                }
                var inventoryHelper = new InventoryHelper();
                var result =
                    inventoryHelper.ObtenCedisRutasIntegracion(
                        AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.GetCedisRutasInventarioWbc(),
                        int.Parse(AppSettings()["timeOutCnx"]));
                if (result.ResultType != ResultTypes.Success || result.Objeto == null || result.Objeto.Count == 0)
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Error_ProductCtrl.ProcesaProductosIntegraWbcAllAsync: no se obtuvieron Cedis-Ruta de Integración."+Environment.NewLine,
                        true);
                    return _t;
                }
                _t = Task.WhenAll((from cedi in result.Objeto
                                   where cedi.Cedi != 0
                                   select
                                   Task.Run(
                                       () =>
                                       {
                                           ProductosAll(cedi.Cedi, resultToken);
                                       }))
                    .ToArray());

            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_ProductCtrl.ProcesaProductosIntegraWbcAllAsync: " + ex.Message + "."+Environment.NewLine + ex.StackTrace +
                    "."+Environment.NewLine, true);
            }
            return _t;
        }

        public static void ProductosAll(int cedisId, Result<Token> resultToken)
        {
            try
            {
                var result = GetProductAllIntegra(cedisId);
                if (result.ResultType != ResultTypes.Success) return;
                var products = result.Objeto;
                products.RemoveAll(p => p.WbcId != 0);
                foreach (var t in products)
                {
                    if (t.ProductId != 0) continue;
                    var resultWbc = GetProductWbc(t.CodigoProducto, resultToken.Objeto.AccessToken);
                    if (resultWbc.ResultType == ResultTypes.Success)
                    {
                        t.ProductId = resultWbc.Objeto[0].ProductId;
                    }
                }
                products.RemoveAll(p => p.ProductId == 0);
                if (products.Count > 0)
                {
                    PutProductIntegra(products);
                }
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_ProductCtrl.ProductosAll: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
            }
        }
    }
}