﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using CCETH091703.Base;
using CCETH091703.Model.BR;
using CCETH091703.Model.DataAccess;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;

namespace CCETH091703.Model.Services
{
    public static class TokenCtrl
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        public static Result<Token> GetToken(string granttype, string username, string password)
        {
            var result = new Result<Token>();
            try
            {
                var helper = new TokenHelper();
                var json = "grant_type=" + granttype + "&username=" + username + "&password=" + password;
                result = helper.GetToken(AppSettings()["URL_WBC_API"] + ApiWbcCnx.PostToken(),
                    int.Parse(AppSettings()["TimeOutCnx"]), json);
                result.Objeto.Expires =
                    DateTime.Now.AddHours(TimeSpan.FromHours(double.Parse(result.Objeto.ExpiresIn)).Hours)
                        .ToString();
                if (result.ResultType != ResultTypes.Success)
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) + "Error al obtener token: " + result.ResultType +
                        "_TokenCtrl.GetToken: " + result.Mensaje + "."+Environment.NewLine, true);
                }
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_TokenCtrl.GetToken: " + ex.Message +
                    "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }
    }
}