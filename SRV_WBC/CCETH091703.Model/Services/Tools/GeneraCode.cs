﻿using System;

namespace CCETH091703.Model.Services.Tools
{
    public static class GeneraCode
    {
        public static string Code(DateTime fechaViaje, string branchId, string ruta, string viaje)
        {
            return fechaViaje.ToString("yyyy.MM.dd") + "-" + branchId + "-" + ruta + "-" + viaje;
        }
    }
}