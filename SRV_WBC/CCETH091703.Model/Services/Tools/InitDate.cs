﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using CCETH091703.Model.BR;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.Runtime.Serialization.Formatters.Binary;

namespace CCETH091703.Model.Services.Tools
{

    public static class InitDate
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        private static Dictionary<string, DateTime?> _fecha = new Dictionary<string, DateTime?>();
        private static bool changed = false;

        public static DateTime Get(String cedisruta)
        {
            try
            {
                return _fecha[cedisruta] ?? DateTime.Parse(AppSettings()["FechaClientes"]);
            }
            catch (Exception)
            {
                return DateTime.Parse(AppSettings()["FechaClientes"]);
            }
        }

        public static void Set(string cedisruta, DateTime fecha)
        {
            _fecha[cedisruta] = fecha;
            changed = true;
        }

        public static void Leer()
        {
            try
            {
                using (StreamReader _sr = new StreamReader(AppSettings()["RutaFisicaLog"] + "\\InitDate.txt"))
                {

                    string json = _sr.ReadToEnd();
                    _fecha = JsonConvert.DeserializeObject<Dictionary<string, DateTime?>>(json);
                }
            }
            catch (Exception)
            {
            }
        }

        public static void Guardar()
        {
            if (changed)
            {
                try
                {
                    using (StreamWriter _fs = new StreamWriter(AppSettings()["RutaFisicaLog"] + "\\InitDate.txt",
                        false))
                    {
                        _fs.Write(JsonConvert.SerializeObject(_fecha));
                    }

                    changed = false;
                }
                catch (Exception)
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Error al escribir InitDate.txt:", true);
                }
            }
        }
    }

   
}

