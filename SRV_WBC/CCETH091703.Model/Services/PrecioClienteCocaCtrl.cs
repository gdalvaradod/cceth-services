﻿using CCETH091703.Model.Objects.BO;
using GrupoGitLib.Model.Objects.BO;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using CCETH091703.Base;
using CCETH091703.Model.DataAccess;
using System.Globalization;
using CCETH091703.Model.BR;
using GrupoGitLib.Model.Objects.Enum;

namespace CCETH091703.Model.Services
{
    public class PrecioClienteCocaCtrl
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }
        public static Result<List<PrecioClienteCoca>> GetPrecioClienteCoca(int cediId, string barcode, string deliveryDate)
        {
            var result = new Result<List<PrecioClienteCoca>>();
            try
            {
                var helper = new PrecioClienteCocaHelper();
                result = helper.GetPrecioClienteCoca(AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.GetPrecioClienteCoca(cediId,barcode,deliveryDate),
                    int.Parse(AppSettings()["TimeOutCnx"]), "");
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_PrecioClienteCocaCtrl.GetPrecioClienteCoca: " + ex.Message +
                    "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }
    }
}
