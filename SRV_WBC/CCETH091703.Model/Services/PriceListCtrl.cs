﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using CCETH091703.Base;
using CCETH091703.Model.BR;
using CCETH091703.Model.DataAccess;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;

namespace CCETH091703.Model.Services
{
    public class PriceListCtrl
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        public static Result<PriceList> GetPriceList(long priceListId, string token)
        {
            var result = new Result<PriceList>();
            try
            {
                var helper = new PriceListHelper();
                result = helper.GetPriceList(AppSettings()["URL_WBC_API"] + ApiWbcCnx.GetPriceList(priceListId),
                    int.Parse(AppSettings()["TimeOutCnx"]), token);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_PriceListCtrl.GetPriceList: " + ex.Message +
                    "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<PriceList> PostPriceList(string token, PriceList priceList)
        {
            var result = new Result<PriceList>();
            try
            {
                var helper = new PriceListHelper();
                result = helper.PostPriceList(AppSettings()["URL_WBC_API"] + ApiWbcCnx.PostPriceList(),
                    int.Parse(AppSettings()["TimeOutCnx"]), token, priceList);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_PriceListCtrl.PostPriceList: " + ex.Message +
                    "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<PriceList> PutPriceList(string token, PriceList priceList, long pricelistid)
        {
            var result = new Result<PriceList>();
            try
            {
                var helper = new PriceListHelper();
                result = helper.PutPriceList(AppSettings()["URL_WBC_API"] + ApiWbcCnx.PutPriceList(pricelistid),
                    int.Parse(AppSettings()["TimeOutCnx"]), token, priceList);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_PriceListCtrl.PutPriceList: " + ex.Message +
                    "." + Environment.NewLine + ex.StackTrace + "." + Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "." + Environment.NewLine + ex.StackTrace + "." + Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<List<PriceProductList>> GetPriceListProduct(long priceListId, string token)
        {
            var result = new Result<List<PriceProductList>>();
            try
            {
                var helper = new PriceListHelper();
                result = helper.GetPriceListProduct(AppSettings()["URL_WBC_API"] + ApiWbcCnx.GetPriceListProduct(priceListId),
                    int.Parse(AppSettings()["TimeOutCnx"]), token);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_PriceListCtrl.GetPriceListProduct: " + ex.Message +
                    "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<PriceProductList> PutPriceListProduct(long priceListId, int productId, string token, PriceProductList priceProductList)
        {
            var result = new Result<PriceProductList>();
            try
            {
                var helper = new PriceListHelper();
                result = helper.PutPriceListProduct(AppSettings()["URL_WBC_API"] + ApiWbcCnx.PutPriceListProduct(priceListId, productId),
                    int.Parse(AppSettings()["TimeOutCnx"]), token, priceProductList);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_PriceListCtrl.PutPriceListProduct: " + ex.Message +
                    "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<string> DeletePriceListProduct(long priceListId, int productId, string token)
        {
            var result = new Result<string>();
            try
            {
                var helper = new PriceListHelper();
                result = helper.DeletePriceListProduct(AppSettings()["URL_WBC_API"] + ApiWbcCnx.DeletePriceListProduct(priceListId, productId),
                    int.Parse(AppSettings()["TimeOutCnx"]), token);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_PriceListCtrl.GetPriceListProduct: " + ex.Message +
                    "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }
    }
}
