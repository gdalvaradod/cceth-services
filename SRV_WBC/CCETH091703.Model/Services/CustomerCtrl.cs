﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using CCETH091703.Base;
using CCETH091703.Model.BR;
using CCETH091703.Model.DataAccess;
using CCETH091703.Model.Objects.BO;
using CCETH091703.Model.Services.Tools;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;


namespace CCETH091703.Model.Services
{
    public static class CustomerCtrl
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        #region Crm

        public static Result<List<string>> GetClientesCrmAll(string distributionCenterCode, string routeCode,
            string name, string barcode, string cuc, string postcode, string email, string createdon, string modifiedon)
        {
            var result = new Result<List<string>>();
            try
            {
                var helper = new CustomerHelper();
                result =
                    helper.GetClienteCrmAll(
                        AppSettings()["URL_CRM_API"] + ApiCrmCnx.GetCustomerAll(distributionCenterCode, routeCode, name,
                            barcode, cuc, postcode, email, createdon, modifiedon),
                        int.Parse(AppSettings()["TimeOutCnx"]));
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_CustomerCtrl.GetClientesCrmAll: " +
                    ex.Message + "."+ Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+ Environment.NewLine + ex.StackTrace + "."+ Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<Customer> GetClientesCrmComplete(string code)
        {
            var result = new Result<Customer>();
            try
            {
                var helper = new CustomerHelper();
                result = helper.GetClienteCrmComplete(AppSettings()["URL_CRM_API"] + ApiCrmCnx.GetCustomer(code),
                    int.Parse(AppSettings()["TimeOutCnx"]));
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_CustomerCtrl.GetClientesCrm:Complete " + ex.Message + "."+ Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+ Environment.NewLine + ex.StackTrace + "."+ Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        #endregion

        #region WBC

        /**Metodos Clientes**/

        public static Result<CustomerWbc> PostClienteWbc(string token, CustomerWbc customer)
        {
            var result = new Result<CustomerWbc>();
            try
            {
                var helper = new CustomerHelper();
                result = helper.PostClienteWbc(AppSettings()["URL_WBC_API"] + ApiWbcCnx.PostCustomer(),
                    int.Parse(AppSettings()["TimeOutCnx"]), token, customer);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_CustomerCtrl.PostClienteWbc: " +
                    ex.Message + "."+ Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+ Environment.NewLine + ex.StackTrace + "."+ Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<List<CustomerWbc>> GetClienteWbc(string token, string code)
        {
            var result = new Result<List<CustomerWbc>>();
            try
            {
                var helper = new CustomerHelper();
                result = helper.GetClienteWbc(AppSettings()["URL_WBC_API"] + ApiWbcCnx.GetCustomer(code),
                    int.Parse(AppSettings()["TimeOutCnx"]), token);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_CustomerCtrl.GetClienteWbc: " +
                    ex.Message + "."+ Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+ Environment.NewLine + ex.StackTrace + "."+ Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<string> PutClienteWbc(string token, CustomerWbc customer)
        {
            var result = new Result<string>();
            try
            {
                var helper = new CustomerHelper();
                result = helper.PutClienteWbc(
                    AppSettings()["URL_WBC_API"] + ApiWbcCnx.PutCustomer(customer.CustomerId),
                    int.Parse(AppSettings()["timeOutCnx"]), token, customer);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_CustomerCtrl.PutClienteWbc: " +
                    ex.Message + "."+ Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+ Environment.NewLine + ex.StackTrace + "."+ Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        /**Metodos Clientes Visitas**/

        public static Result<CustomerVisitWbc> PostClienteVisitaWbc(string token, CustomerVisitWbc customer)
        {
            var result = new Result<CustomerVisitWbc>();
            try
            {
                var helper = new CustomerHelper();
                result =
                    helper.PostClienteVisitaWbc(
                        AppSettings()["URL_WBC_API"] + ApiWbcCnx.PostCustomerVisit(customer.CustomerId.ToString()),
                        int.Parse(AppSettings()["TimeOutCnx"]), token, customer);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_CustomerCtrl.PostClienteVisitaWbc: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<List<CustomerVisitWbc>> GetClienteVisitaWbc(string token, string customerId)
        {
            var result = new Result<List<CustomerVisitWbc>>();
            try
            {
                var helper = new CustomerHelper();
                result =
                    helper.GetClienteVisitaWbc(AppSettings()["URL_WBC_API"] + ApiWbcCnx.GetCustomerVisit(customerId),
                        int.Parse(AppSettings()["TimeOutCnx"]), token);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_CustomerCtrl.GetClienteVisitaWbc: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<string> PutClienteVisitaWbc(string token, CustomerVisitWbc customer)
        {
            var result = new Result<string>();
            try
            {
                var helper = new CustomerHelper();
                result =
                    helper.PutClienteVisitaWbc(
                        AppSettings()["URL_WBC_API"] +
                        ApiWbcCnx.PutCustomerVisit(customer.CustomerId, customer.RouteId, customer.Day,
                            customer.VisitType), int.Parse(AppSettings()["TimeOutCnx"]), token, customer);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_CustomerCtrl.PutClienteVisitaWbc: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        /**Metodos Clientes Precios Lista**/

        public static Result<CustomerPriceListWbc> GetClientePrecioWbc(string token, string customerId)
        {
            var result = new Result<CustomerPriceListWbc>();
            try
            {
                var helper = new CustomerHelper();
                result = helper.GetClientePriceWbc(
                    AppSettings()["URL_WBC_API"] + ApiWbcCnx.GetCustomerPrice(customerId),
                    int.Parse(AppSettings()["TimeOutCnx"]), token);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_CustomerCtrl.GetClientePrecioWbc: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<string> PutClientePrecioWbc(string token, CustomerPriceListWbc customer)
        {
            var result = new Result<string>();
            try
            {
                var helper = new CustomerHelper();
                result =
                    helper.PutClientePrecioWbc(
                        AppSettings()["URL_WBC_API"] +
                        ApiWbcCnx.PutCustomerPrice(customer.CustomerId, customer.PriceListId),
                        int.Parse(AppSettings()["TimeOutCnx"]), token, customer);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_CustomerCtrl.PutClientePrecioWbc: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        #endregion

        #region Integraciones

        public static Result<List<CustomerRelation>> GetCustomerRelation(int cediId, string crmid, string barcode, int cuc)
        {
            var result = new Result<List<CustomerRelation>>();
            try
            {
                result =
                    CustomerHelper.GetCustomerRelation(
                        AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.GetCustomerRelation(cediId, crmid, barcode, cuc),
                        int.Parse(AppSettings()["TimeOutCnx"]));
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_CustomerCtrl.GetCustomerRelation: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<object> ActualizaClienteIntegraciones(List<Customer> customer)
        {
            var result = new Result<object>();
            try
            {
                var helper = new CustomerHelper();
                result =
                    helper.ActualizaClienteIntegraciones(
                        AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.ActualizaClientesIntegraciones(),
                        int.Parse(AppSettings()["TimeOutCnx"]), customer);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_CustomerCtrl.ActualizaClienteIntegraciones: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".",
                    true);
                result.Objeto = false;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        #endregion

        #region Acciones_Generales

        
        //private static Dictionary<string, List<Customer>> _retryCedisRouteList;

        public static void CustomerAll(Result<Token> resultToken)
        {
            try
            {
                LogServicioBr.EscribirLog(AppSettings()["CustomerLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Inicia proceso de Clientes de CRM - Intermedia - WBC.", true);  // *** Monitor Customer Log
                var inventoryHelper = new InventoryHelper();
                var resultCedisRutas =
                    inventoryHelper.ObtenCedisRutasIntegracion(
                        AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.GetCedisRutasInventarioWbc(),
                        int.Parse(AppSettings()["timeOutCnx"]));
                if (resultCedisRutas.ResultType != ResultTypes.Success || resultCedisRutas.Objeto == null)//|| resultCedisRutas.Objeto.Count == 0)
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Error_CustomerCtrl.CustomerAll: no se obtuvieron Cedis-Ruta de Integración.", true);
                    return;
                }
                //await(_t = Task.WhenAll((from cedi in resultCedisRutas.Objeto where cedi.Destinos.Count != 0 from destino in cedi.Destinos select Task.Run(() => { Customer(resultToken, cedi.Cedi, destino.RouteCodeOpecd.ToString(), fechainicial); })).ToArray()));

                if (!(resultCedisRutas.Objeto?.Count > 0))
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Error_CustomerCtrl.CustomerAll: No se encontraron Cedis.", true);
                    return;

                }
                foreach (var t1 in resultCedisRutas.Objeto)
                {
                    if (t1.Destinos == null || t1.Destinos.Count <= 0)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                            ": Error_CustomerCtrl.CustomerAll: No se encontraron rutas en el Cedis "+t1.Cedi+".", true);
                        continue;
                    }
                    foreach (var t in t1.Destinos)
                    {
                        if (t.Enable == 1)
                        {
                            Customer(resultToken, t1.Cedi,
                                t.RouteCodeOpecd.ToString());
                           
                        }
                    }
                }

                //await (_t = Task.WhenAll((from cedi in resultCedisRutas.Objeto where cedi.Destinos.Count != 0 from destino in cedi.Destinos select Task.Run(() => { Customer(resultToken, cedi.Cedi, destino.RouteCodeOpecd.ToString(), fechainicial); })).ToArray()));
                LogServicioBr.EscribirLog(AppSettings()["CustomerLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Finaliza proceso de Clientes de CRM - Intermedia - WBC."+Environment.NewLine, true); //*** Monitor Customer Log
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_CustomerCtrl.CustomerAll: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + ".",
                    true);
            }
        }

        public static void Customer(Result<Token> resultToken, int cedi,
            string ruta)
        {
           
            try
            {
                //if(_retryCedisRouteList==null) _retryCedisRouteList=new Dictionary<string, List<Customer>>();
                var clientesIds = new List<string>();
                var listaclientes = new List<Customer>();

                // obtener CUC generico para este Cedis-Ruta
                var customerHelper = new CustomerHelper();
                var resultCustomerIdGen = customerHelper.GetClienteGenerico(
                    AppSettings()["URL_INTEGRACION_API"] +
                    ApiIntegracionesCnx.GetClienteGenerico(cedi, int.Parse(ruta)),
                    int.Parse(AppSettings()["TimeOutCnx"]));
                if (resultCustomerIdGen.ResultType != ResultTypes.Success)
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Error_CustomerCtrl.Customer: No se obtuvo Cliente Genérico.", true);
                    return;
                }

                var primerodelista = "";

                InitDate.Leer();
                var entrando = true;
                do //repetir mientras el bloque sea de 100
                {

                    clientesIds.Clear();


                    var result = GetClientesCrmAll(cedi.ToString(), ruta, "", "", "", "",
                        "", "", InitDate.Get(cedi + ":" + ruta).ToString("yyyy-MM-ddTHH:mm:ss"));
                    if (result.ResultType != ResultTypes.Success)
                    {
                        if (result.ResultType == ResultTypes.Error)
                        {
                            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                ": Error_CustomerCtrl.Customer: No se pudieron obtener clientes de Crm." +
                                Environment.NewLine + "Para cedis:" +
                                cedi + ", ruta:" + ruta + "." + Environment.NewLine + result.Mensaje, true);
                        }

                        return; //break;
                    }

                    if (result.Objeto == null || result.Objeto.Count == 0)
                    {
                        break; //Salir del ciclo cuando ya no hay mas clientes para procesar en este cedis-ruta
                    }

                    if (entrando)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["CustomerLog"],
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                            ": Inicia proceso de Clientes para cedi:" + cedi + " ruta:" + ruta + ".",
                            true); // *** Monitor Customer Log
                        entrando = false;
                    }

                    LogServicioBr.EscribirLog(AppSettings()["CustomerLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Procesando bloque con " + result.Objeto.Count + " clientes con fecha de inicial en " +
                        InitDate.Get(cedi + ":" + ruta) + ".", true); // *** Monitor Customer Log

                    if (primerodelista.Equals(result.Objeto[0]))
                    {
                        LogServicioBr.EscribirLog(AppSettings()["CustomerLog"],
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                            ": cerrando proceso de Clientes por repeticion de cliente: " +
                            primerodelista + ".", true); // *** Monitor Customer Log
                        
                        // aumento de 1 segundo a la fecha
                        InitDate.Set(cedi + ":" + ruta, InitDate.Get(cedi + ":" + ruta).AddSeconds(1));
                        LogServicioBr.EscribirLog(AppSettings()["CustomerLog"],
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                            ": Terminó proceso de Clientes para cedi:" + cedi + " ruta:" + ruta + "." + Environment.NewLine,
                            true); // *** Monitor Customer Log
                        break; // Salir si repite los mismos datos
                    }

                    primerodelista = result.Objeto[0];

                    clientesIds = result.Objeto;

                    listaclientes.Clear();

                    foreach (var t in clientesIds)
                    {
                        //if (!t.Equals("cde914e8-e09a-e111-ba7e-005056977fbc"))
                        //{
                        //    continue;
                        //}
                        var resultClienteCrmComplete =
                            GetClientesCrmComplete(t.Trim());
                        if (resultClienteCrmComplete.ResultType != ResultTypes.Success)
                        {
                            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                ": Error_CustomerCtrl.Customer: No se pudo obtener la información del cliente " + t +
                                " de Crm." + Environment.NewLine + "Para cedis:" +
                                cedi + ", ruta:" + ruta + ".", true);
                            break;
                        }

                        resultClienteCrmComplete.Objeto.CrmId = resultClienteCrmComplete.Objeto.Id;
                        resultClienteCrmComplete.Objeto.CrmCreateOn = resultClienteCrmComplete.Objeto.CreatedOn;

                        ProcesaClienteWbc(resultToken, resultClienteCrmComplete.Objeto, listaclientes,
                            resultCustomerIdGen.Objeto.ToString());


                        InitDate.Set(cedi + ":" + ruta,
                            (DateTime.Parse(resultClienteCrmComplete.Objeto.ModifiedOn) >
                             InitDate.Get(cedi + ":" + ruta)
                                ? DateTime.Parse(resultClienteCrmComplete.Objeto.ModifiedOn)
                                : InitDate.Get(cedi + ":" + ruta)));
                    }

                    InitDate.Guardar();
                    var resultActCliInt = ActualizaClienteIntegraciones(listaclientes);
                    if (resultActCliInt.ResultType == ResultTypes.Success)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["CustomerLog"],
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                            ": pasaron " + listaclientes.Count + " clientes con fecha final en " +
                            InitDate.Get(cedi + ":" + ruta) + ".", true); // *** Monitor Customer Log

                    }
                } while (true); //(result.Objeto.Count == 100);
                InitDate.Guardar();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_CustomerCtrl.Customer: " + ex.Message + "." + Environment.NewLine + ex.StackTrace + ".",
                    true);
            }
           
        }

        private static void ProcesaClienteWbc(Result<Token> resultToken, Customer clienteCrmComplete,
            ICollection<Customer> listaclientes, string clientegenerico)
        {
            var customer = new CustomerWbc
            {
                CustomerId = clienteCrmComplete.CustomerId.ToString(),
                Contact = clienteCrmComplete.Contact,
                Address = clienteCrmComplete.PhysicalAddress,
                Description = clienteCrmComplete.Description,
                Email = clienteCrmComplete.Email,
                Latitude = clienteCrmComplete.Latitude,
                Longitude = clienteCrmComplete.Longitude,
                Code = clienteCrmComplete.BarCode,
                Name = clienteCrmComplete.Name,
                Active = clienteCrmComplete.ClientType
            };

            if (customer.Code == "")
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_CustomerCtrl.ProcesaClienteWbc: Cliente "+clienteCrmComplete.CrmId +" inactivo o sin Code",
                    true);
                return;
            }
            var resultCustomerWbc = GetClienteWbc(resultToken.Objeto.AccessToken,
                customer.Code);

            if (resultCustomerWbc.ResultType == ResultTypes.Success)
            {
                if (resultCustomerWbc.Objeto.Count == 0)
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Error_CustomerCtrl.ProcesaClienteWbc: Cliente " + clienteCrmComplete.CustomerId + " no encontrado en WBC",
                        true);
                    return;
                }
                
                var customerWbc = resultCustomerWbc.Objeto[0];
                
                customer.CustomerId = customerWbc.CustomerId;
                if (!customer.Equals(customerWbc))
                {
                    // actualizar put cliente en WBC
                    var cad = PutClienteWbc(resultToken.Objeto.AccessToken,
                        customer);
                    if (cad.ResultType != ResultTypes.Success)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                            ": Error_CustomerCtrl.ProcesaClienteWbc: no se pudo actualizar al cliente" +
                            clienteCrmComplete.CustomerId + " en WBC",
                            true);
                        return;
                    }
                }

                GenerarVisitasPrecio(clienteCrmComplete, resultToken.Objeto.AccessToken,
                    int.Parse(resultCustomerWbc.Objeto[0].CustomerId), 0);

                clienteCrmComplete.CrmId = clienteCrmComplete.Id;
                clienteCrmComplete.Id = "0";
                clienteCrmComplete.CustomerId = int.Parse(customer.CustomerId);
                if (string.IsNullOrEmpty(clienteCrmComplete.Cuc) || clienteCrmComplete.Cuc == "0") //si falta CUC usar Generico para est Cedis-Ruta
                {
                    clienteCrmComplete.Cuc = clientegenerico;
                }
                listaclientes.Add(clienteCrmComplete);
            }
            else
            {
                // Dar de alta al cliente en WBC

                var cad = PostClienteWbc(resultToken.Objeto.AccessToken, customer);
                if (cad.ResultType != ResultTypes.Success)
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Error_CustomerCtrl.ProcesaClienteWbc: no se pudo agregar al cliente" + clienteCrmComplete.CustomerId + " en WBC",
                        true);
                    return;
                }

                GenerarVisitasPrecio(clienteCrmComplete, resultToken.Objeto.AccessToken,
                    int.Parse(cad.Objeto.CustomerId), int.Parse(cad.Objeto.CustomerId));
                clienteCrmComplete.CustomerId = int.Parse(cad.Objeto.CustomerId);
                clienteCrmComplete.CrmId = clienteCrmComplete.Id;
                clienteCrmComplete.Id = "0";
                clienteCrmComplete.CustomerId = int.Parse(cad.Objeto.CustomerId);
                listaclientes.Add(clienteCrmComplete);
            }
        }

        public static void GenerarVisitasPrecio(Customer clienteCrmComplete, string token, int customerId, int customerIdPost)
        {
            try
            {
                var branchResult = InventoryCtrl.GetBranchWbc(
                                int.Parse(clienteCrmComplete.DistributionCenter),
                                token);
                if (branchResult.ResultType != ResultTypes.Success) return;
                var routeResult = RouteCtrl.GetRouteWbc(branchResult.Objeto[0].BranchId,
                    clienteCrmComplete.RouteCode, token);
                if (routeResult.ResultType != ResultTypes.Success) return;
                var clienteVisita = new List<CustomerVisitWbc>();

                for (int x = 1; x < 8; x++)
                {
                    if ((clienteCrmComplete.Visit.NumericFormat & (int) Math.Pow(2, x-1)) > 0)
                    {
                        clienteVisita.Add(new CustomerVisitWbc(customerId,
                            routeResult.Objeto[0].RouteId, x, 1, clienteCrmComplete.Sequence,
                            true));
                    }
                }

                var clienteVisitaResult =
                    GetClienteVisitaWbc(token, customerId.ToString());
                if (clienteVisitaResult.ResultType == ResultTypes.Success)
                {
                    clienteVisitaResult.Objeto.RemoveAll(t => t.RouteId != routeResult.Objeto[0].RouteId);
                    foreach (var t2 in clienteVisita)
                    {
                        var existe = false;
                        foreach (var t1 in clienteVisitaResult.Objeto)
                        {
                            if (!clienteVisita.Exists(t => t.Day == t1.Day))
                            {
                                t1.Active = false;
                                PutClienteVisitaWbc(token, t1);
                            }
                            if (t2.Day != t1.Day) continue;
                            existe = true;
                            PutClienteVisitaWbc(token, t2);
                        }
                       
                        if (!existe)
                        {
                            PostClienteVisitaWbc(token, t2);
                        }
                    }
                }
                else
                {
                    foreach (var t2 in clienteVisita)
                    {
                        PostClienteVisitaWbc(token, t2);
                    }
                }

                var tieneCuc = !clienteCrmComplete.Cuc.Equals("") && !clienteCrmComplete.Cuc.Equals("0");

                var customerPrice = ObtenerListaPreciosIdWbc((customerIdPost > 0 ? customerIdPost : customerId).ToString(),
                    int.Parse(branchResult.Objeto[0].Code), int.Parse(routeResult.Objeto[0].Code),
                    token, branchResult.Objeto[0].BranchId, tieneCuc);
                if (customerPrice == null) return;
                if (customerPrice.PriceListId != clienteCrmComplete.PriceList)
                {
                    customerPrice.PriceListId = clienteCrmComplete.PriceList;
                }
                PutClientePrecioWbc(token,
                    customerPrice);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_CustomerCtrl.GenerarVisitasPrecio: " + ex.Message + "."+ Environment.NewLine + ex.StackTrace + ".",
                    true);
            }
        }

        public static List<PrecioClienteCoca> GenerarListaPrecioCustomerCcth(Customer clienteCrmComplete, string token, int customerId, int customerIdPost)
        {
            try
            {
                var branchResult = InventoryCtrl.GetBranchWbc(
                                int.Parse(clienteCrmComplete.DistributionCenter),
                                token);
                if (branchResult.ResultType != ResultTypes.Success) return null;
                var routeResult = RouteCtrl.GetRouteWbc(branchResult.Objeto[0].BranchId,
                    clienteCrmComplete.RouteCode, token);
                if (routeResult.ResultType != ResultTypes.Success) return null;

                //var customerPrice = ObtenerListaPreciosIdWbc((customerIdPost > 0 ? customerIdPost : customerId).ToString(),
                //    int.Parse(branchResult.Objeto[0].Code), int.Parse(routeResult.Objeto[0].Code),
                //    token, branchResult.Objeto[0].BranchId, tieneCuc);

                var customerHelper = new CustomerHelper();
                // consultar precios en WBC por customerid  ok
                var resultCustomerPriceListWbc = customerHelper.GetClientePriceWbc(
                    AppSettings()["URL_WBC_API"] + ApiWbcCnx.GetCustomerPrice(customerId.ToString()),
                    int.Parse(AppSettings()["TimeOutCnx"]), token);

                CustomerPriceListWbc customerPrice = null;
                if (resultCustomerPriceListWbc.ResultType == ResultTypes.Success)
                {
                    customerPrice = new CustomerPriceListWbc(customerId,
                        resultCustomerPriceListWbc.Objeto.PriceListId, true);
                }

                List<PrecioClienteCoca> precioCocaList;
                if (customerPrice != null)
                {
                    var priceListProduct = PriceListCtrl.GetPriceListProduct(customerPrice.PriceListId, token);
                    if (priceListProduct.ResultType != ResultTypes.Success)
                    {
                        priceListProduct.Objeto = new List<PriceProductList>();
                    }
                    var resultPrecioCoca = PrecioClienteCocaCtrl.GetPrecioClienteCoca(int.Parse(clienteCrmComplete.DistributionCenter), clienteCrmComplete.BarCode,
                        DateTime.Now.ToString("yyyy-MM-dd"));
                    if (resultPrecioCoca.ResultType != ResultTypes.Success)
                    {
                        resultPrecioCoca.Objeto = new List<PrecioClienteCoca>();
                    }
                    
                    foreach (var item in resultPrecioCoca.Objeto)
                    {
                        var resultGetProductWbc = ProductCtrl.GetProductWbc(item.ItemId.ToString(), token);
                        if (resultGetProductWbc.ResultType == ResultTypes.Success)
                        {
                            item.ProductWbcId = resultGetProductWbc.Objeto[0].ProductId;
                        }

                        priceListProduct.Objeto.RemoveAll(x => x.ProductId == item.ProductWbcId);
                    }
                    foreach (var item in priceListProduct.Objeto)
                    {
                        PriceListCtrl.DeletePriceListProduct(customerPrice.PriceListId, item.ProductId,
                                token);
                    }
                    foreach (var item in resultPrecioCoca.Objeto)
                    {
                        var priceProductList = new PriceProductList
                        {
                            Price = item.Price,
                            BasePrice = item.BasePrice,
                            Discount = item.Discount,
                            DiscountPercent = item.DiscountPercent
                        };
                       
                        PriceListCtrl.PutPriceListProduct(customerPrice.PriceListId, item.ProductWbcId, token,
                            priceProductList);
                    }
                    var priceList = new PriceList
                    {
                        BranchId = branchResult.Objeto[0].BranchId,
                        Master = false,
                        ValidityStart = DateTime.Now.ToString("s"),
                        ValidityEnd = DateTime.Now.AddDays(1).ToString("s"),
                        Code = customerId.ToString(),
                        Name = "Lista " + customerId
                    };
                    PriceListCtrl.PutPriceList(token, priceList, customerPrice.PriceListId);
                    PutClientePrecioWbc(token,
                        customerPrice);
                    precioCocaList = resultPrecioCoca.Objeto;
                }
                else
                {
                    var priceList = new PriceList
                    {
                        BranchId = branchResult.Objeto[0].BranchId,
                        Master = false,
                        ValidityStart = DateTime.Now.ToString("s"),
                        ValidityEnd = DateTime.Now.AddDays(1).ToString("s"),
                        Code = customerId.ToString(),
                        Name = "Lista " + customerId
                    };
                    var resultPriceList = PriceListCtrl.PostPriceList(token, priceList);
                    if (resultPriceList.ResultType != ResultTypes.Success) return null;
                    var resultPrecioCoca = PrecioClienteCocaCtrl.GetPrecioClienteCoca(int.Parse(clienteCrmComplete.DistributionCenter), clienteCrmComplete.BarCode,
                        DateTime.Now.ToString("yyyy-MM-dd"));
                    if (resultPrecioCoca.ResultType != ResultTypes.Success) return null;
                    foreach (var item in resultPrecioCoca.Objeto)
                    {
                        var priceProductList = new PriceProductList
                        {
                            Price = item.Price,
                            BasePrice = item.BasePrice,
                            Discount = item.Discount,
                            DiscountPercent = item.DiscountPercent
                        };
                        PriceListCtrl.PutPriceListProduct(resultPriceList.Objeto.PriceListId,item.ItemId, token, priceProductList);
                    }
                    customerPrice = new CustomerPriceListWbc(customerId, resultPriceList.Objeto.PriceListId, true);
                    PutClientePrecioWbc(token,
                        customerPrice);
                    precioCocaList = resultPrecioCoca.Objeto;
                }
                return precioCocaList;
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_CustomerCtrl.GenerarVisitasPrecio: " + ex.Message + "."+ Environment.NewLine + ex.StackTrace + ".",true);
                return null;
            }
        }

        private static CustomerPriceListWbc ObtenerListaPreciosIdWbc(string customerId, int branchId, int routeId,
            string token, int branchIdWbc, bool tieneCuc)
        {
            try
            {
                var customerHelper = new CustomerHelper();
                // consultar precios en WBC por customerid  ok
                var resultCustomerPriceListWbc = customerHelper.GetClientePriceWbc(
                    AppSettings()["URL_WBC_API"] + ApiWbcCnx.GetCustomerPrice(customerId),
                    int.Parse(AppSettings()["TimeOutCnx"]), token);
               
                if (resultCustomerPriceListWbc.ResultType == ResultTypes.Success)
                {
                    if (tieneCuc)
                    {
                        return null;
                    }
                    return new CustomerPriceListWbc(int.Parse(customerId),
                        resultCustomerPriceListWbc.Objeto.PriceListId, true);
                }
                // si no hay lista de precios,  traer generica 
                // obtener el cuc del cliente generico, 
                var resultCustomerIdGen = customerHelper.GetClienteGenerico(
                    AppSettings()["URL_INTEGRACION_API"] +
                    ApiIntegracionesCnx.GetClienteGenerico(branchId, routeId),
                    int.Parse(AppSettings()["TimeOutCnx"]));
                if (resultCustomerIdGen.ResultType != ResultTypes.Success) return null;
                if (resultCustomerIdGen.Objeto.Cuc == 0) return null;
                //Obtener lista de precios generica de WBC
                var resulPriceListWbc =
                    customerHelper.GetPriceListWbc(
                        AppSettings()["URL_WBC_API"] + ApiWbcCnx.GetPriceList(resultCustomerIdGen.Objeto.Cuc, branchIdWbc),
                        int.Parse(AppSettings()["TimeOutCnx"]), token);
                if (resulPriceListWbc.ResultType != ResultTypes.Success) return null;
                var lista = resulPriceListWbc.Objeto.Find(x => x.BranchId == branchId);
                if (lista != null)
                {
                    return new CustomerPriceListWbc(int.Parse(customerId),
                        lista.PriceListId, true);
                }
                return null;
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_CustomerCtrl.ObtenerListaPreciosIdWbc: " + ex.Message + "."+ Environment.NewLine + ex.StackTrace + ".",true);
                return null;
            }
        }

        public static Result<List<Customer>> GetClientesCediRutaInt(int cedis, int ruta)
        {
            var result = new Result<List<Customer>>();
            try
            {
                var helper = new CustomerHelper();
                result = helper.GetClientesCediRutaInt(AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.GetClientesCediRuta(cedis, ruta),
                    int.Parse(AppSettings()["TimeOutCnx"]));
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_CustomerCtrl.GetClientesCrm:Complete " + ex.Message + "."+ Environment.NewLine + ex.StackTrace + ".", true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+ Environment.NewLine + ex.StackTrace + "."+ Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        #endregion
    }
}