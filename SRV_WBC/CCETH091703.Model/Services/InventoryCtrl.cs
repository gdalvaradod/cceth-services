﻿using CCETH091703.Model.DataAccess;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using CCETH091703.Model.BR;
using CCETH091703.Base;
using CCETH091703.Model.Objects.BO;
using CCETH091703.Model.Services.Tools;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;
using Newtonsoft.Json;
using OpeCDLib.Services;


namespace CCETH091703.Model.Services
{
    public static class InventoryCtrl
    {
        private static Task _t;
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        
        #region WBC

        public static Result<List<Branch>> GetBranchWbc(int branchcode, string token)
        {
            var result = new Result<List<Branch>>();
            try
            {
                var helper = new DeliveryHelper();
                result =
                    helper.GetBranchesWbc(
                        AppSettings()["URL_WBC_API"] + ApiWbcCnx.GetBranch(null, branchcode.ToString(), null, null),
                        int.Parse(AppSettings()["TimeOutCnx"]), token);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_InventoryCtrl.GetBranchWbc: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<List<User>> GetUserWbc(int cedirutaCedisid, string cedirutaCediscode, string token)
        {
            var result = new Result<List<User>>();
            try
            {
                var helper = new DeliveryHelper();
                result =
                    helper.GetUserWbc(
                        AppSettings()["URL_WBC_API"] + ApiWbcCnx.GetUser(cedirutaCediscode, cedirutaCedisid),
                        int.Parse(AppSettings()["TimeOutCnx"]), token);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_InventoryCtrl.GetUserWbc: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<List<Product>> ConsultarProductosInventarioWbc(int inventoryId, string token)
        {
            var result = new Result<List<Product>>();
            try
            {
                var inventoryHelper = new InventoryHelper();
                result = inventoryHelper.ConsultarProductosInventarioWbc(
                    AppSettings()["URL_WBC_API"] + ApiWbcCnx.GetConfirmInventory(inventoryId),
                    int.Parse(AppSettings()["TimeOutCnx"]), token);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_InventoryCtrl.ConsultarProductosInventarioWbc: " + ex.Message + "."+Environment.NewLine + ex.StackTrace +
                    "."+Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<InventoryConfirm> ConsultarInventarioConfirmadoWbc(int inventoryId, string token)
        {
            var result = new Result<InventoryConfirm>();
            try
            {
                var inventoryHelper = new InventoryHelper();
                result = inventoryHelper.ConsultarInventarioConfirmadoWbc(
                    AppSettings()["URL_WBC_API"] + ApiWbcCnx.GetConfirmInventory(inventoryId),
                    int.Parse(AppSettings()["TimeOutCnx"]), token);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_InventoryCtrl.ConsultarInventarioConfirmadoWbc: " + ex.Message + "."+Environment.NewLine + ex.StackTrace +
                    "."+Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<InventoryConfirm> EliminarInventarioConfirmadoWbc(int inventoryId, string token)
        {
            var result = new Result<InventoryConfirm>();
            try
            {
                var inventoryHelper = new InventoryHelper();
                result = inventoryHelper.EliminarInventarioConfirmadoWbc(
                    AppSettings()["URL_WBC_API"] + ApiWbcCnx.DeleteConfirmInventory(inventoryId),
                    int.Parse(AppSettings()["TimeOutCnx"]), token);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_InventoryCtrl.EliminarInventarioConfirmadoWbc: " + ex.Message + "."+Environment.NewLine + ex.StackTrace +
                    "."+Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<string> ConfirmarInventarioWbc(int inventoryId,
            Confirmacion o, string token)
        {
            var result = new Result<string>();
            try
            {
                var inventoryHelper = new InventoryHelper();
                result = inventoryHelper.ConfirmarInventarioWbc(
                    AppSettings()["URL_WBC_API"] + ApiWbcCnx.ConfirmInventory(inventoryId),
                    int.Parse(AppSettings()["TimeOutCnx"]), token, o);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_InventoryCtrl.ConfirmarInventarioWbc: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<Product> EnviaProductosInventario(DeliveryCtrl.Item item, int inventoryId, string token)
        {
            var result = new Result<Product>();
            try
            {
                var helper = new InventoryHelper();
                result =
                    helper.PublicaProductosInventarioWbc(
                        AppSettings()["URL_WBC_API"] + ApiWbcCnx.PostInventoryDetails(inventoryId),
                        int.Parse(AppSettings()["TimeOutCnx"]), item, token);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_InventoryCtrl.EnviaProductosInventario: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine,
                    true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        #endregion

        public static void ProcesaInventariosIntegraWbcAllAsync(Result<Token> resultToken)
        {
            try
            {
                //if (_t != null)
                //{
                //    if (!_t.IsCompleted)
                //    {
                //        return _t;
                //    }
                //    else
                //    {
                //        LogServicioBr.EscribirLog(AppSettings()["InventoryLog"],
                //            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                //            ": Termino proceso de inventarios Integra-Wbc", true);  // *** Monitor Customer Log
                //    }
                //}

                LogServicioBr.EscribirLog(AppSettings()["InventoryLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Inicia proceso de Inventarios Integra-Wbc", true); // *** Monitor Customer Log

                var inventoryHelper = new InventoryHelper();
                var result =
                    inventoryHelper.ObtenCedisRutasIntegracion(
                        AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.GetCedisRutasInventarioWbc(),
                        int.Parse(AppSettings()["timeOutCnx"]));
                if (result.ResultType != ResultTypes.Success || result.Objeto == null || result.Objeto.Count == 0)
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Error_InventoryCtrl.ProcesaInventariosIntegraWbcAll: no se obtuvieron Cedis-Ruta de Integración."+Environment.NewLine,
                        true);
                    return;
                }

                var listTasks = new List<Task>(5);
                foreach (var itemCediRuta in result.Objeto)
                {
                    for (var i = 0; i < itemCediRuta.Destinos.Count; i++)
                    {
                        var i1 = i;
                        listTasks.Add(Task.Factory.StartNew(() => ProcesaInventariosIntegraWbc(itemCediRuta.Cedi, itemCediRuta.Destinos[i1].RouteCodeOpecd.ToString(), resultToken)));
                        if (listTasks.Count != 5 && i < itemCediRuta.Destinos.Count - 1) continue;
                        _t = Task.WhenAll(listTasks.ToArray());
                        do
                        {
                            
                        } while (!_t.IsCompleted);
                        listTasks.Clear();
                    }
                }
               
                //_t = Task.WhenAll((from cedi in result.Objeto
                //        where cedi.Destinos.Count != 0
                //        from destino in cedi.Destinos
                //        select
                //        Task.Run(
                //            () =>
                //            {
                //                ProcesaInventariosIntegraWbc(cedi.Cedi, destino.RouteCodeOpecd.ToString(), resultToken);
                //            }))
                //    .ToArray());

            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_InventoryCtrl.ProcesaInventariosIntegraWbcAll: " + ex.Message + "."+Environment.NewLine + ex.StackTrace +
                    "."+Environment.NewLine, true);
            }
            //return _t;
        }

        //private static void FinHebra(int cedi, string ruta,string punto)
        //{
        //    //remover bitacora hebras
        //    LogServicioBr.EscribirLog("Hebras",
        //        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
        //        ": Termina Hebra para inventarios de Cedi:" + cedi + " ruta:" + ruta + " en "+punto, false);
        //}


        public static void ProcesaInventariosIntegraWbc(int cedi, string ruta, Result<Token> resultToken)
        {
            
            var logDictionary = "";
            if (EsJornadaCerrada(cedi, int.Parse(ruta)))
            {
                return;
            }
            try
            {
                logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                   ": procesando cedi:" + cedi + " ruta:" + ruta + Environment.NewLine);
               
                logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                   ": Procesando Ventas." + Environment.NewLine);
                var resultVentas = VentasCtrl.EnviarVentasAll(cedi, int.Parse(ruta), logDictionary);
                if (resultVentas.ResultType != ResultTypes.Success)
                {
                    if (resultVentas.ResultType == ResultTypes.Error)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                            ": Error_InventoryCtrl.EnviarVentasAll: Se generaron errores en la ventas de cedis:" +
                            cedi + ", ruta:" + ruta + "."+Environment.NewLine + resultVentas.Mensaje, true);
                        logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                  ": Problema al Procesar Ventas." + Environment.NewLine);
                        return;
                    }
                }
                logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                  ": Termina proceso de Ventas." + Environment.NewLine);
                logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                  ": Enviando inventario de Integra a WBC." + Environment.NewLine);
                var resultEnvioIntegraWbc = EnvioInventario_Integra_Wbc(cedi, ruta, DateTime.Today,
                    resultToken.Objeto.AccessToken, logDictionary);
                if (resultEnvioIntegraWbc.ResultType != ResultTypes.Success)
                {
                    if (resultEnvioIntegraWbc.ResultType == ResultTypes.Error)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                            ": Error_InventoryCtrl.EnvioInventario_Integra_Wbc: No se envio el inventario de integra a wbc para cedis:" +
                            cedi + ", ruta:" + ruta + ", fecha: " + DateTime.Today + "."+Environment.NewLine +
                            resultEnvioIntegraWbc.Mensaje, true);
                    }
                    return;
                }
                var resultLogBitacora = LogBitacoraCtrl.GetLogBitacora(cedi, int.Parse(ruta), 0, DateTime.Now.ToString("yyyy-MM-dd"), 0);
                var inventoryId = ReconfirmarInventario(cedi, int.Parse(ruta), resultToken.Objeto.AccessToken);
                if (inventoryId > 0)
                {
                    var resultInvConfirmado = ConsultarProductosInventarioWbc(inventoryId,
                        resultToken.Objeto.AccessToken);
                    if (resultInvConfirmado.ResultType == ResultTypes.Success)
                    {
                        if (resultLogBitacora.ResultType == ResultTypes.Success)
                        {
                            foreach (var logbitacora in resultLogBitacora.Objeto)
                            {
                                if (logbitacora.Trip == 1 && logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 8))
                                {
                                    ActualizaBitacora(logbitacora, 8, 1, false, "Se consulto correctamente el inventario confirmado.");
                                }
                                else if (logbitacora.Trip ==1 &&
                                         logbitacora.LogBitacoraDetail.Find(x => x.Process.Id == 8 && x.Success) == null)
                                {
                                    ActualizaBitacora(logbitacora, 8, 1, false, "Se consulto correctamente el inventario confirmado.");
                                }
                            }
                            if (!JornadaIniciada(cedi, 1, int.Parse(ruta)))
                            {
                                var resultEliminarInvConf = EliminarInventarioConfirmadoWbc(inventoryId,
                                    resultToken.Objeto.AccessToken);
                                if (resultEliminarInvConf.ResultType == ResultTypes.Success)
                                {
                                    foreach (var logbitacora in resultLogBitacora.Objeto)
                                    {
                                        if (logbitacora.Trip == 1 && logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 10))
                                        {
                                            ActualizaBitacora(logbitacora, 10, 1, false, "Se elimino correctamente la confirmación del inventario: " + inventoryId);
                                        }
                                        else if (logbitacora.Trip == 1 &&
                                                 logbitacora.LogBitacoraDetail.Find(x => x.Process.Id == 10 && x.Success) == null)
                                        {
                                            ActualizaBitacora(logbitacora, 10, 1, false, "Se elimino correctamente la confirmación del inventario: " + inventoryId);
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (var logbitacora in resultLogBitacora.Objeto)
                                    {
                                        if (logbitacora.Trip == 1 && logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 10))
                                        {
                                            ActualizaBitacora(logbitacora, 10, 3, true, "No se pudo eliminar la confirmación del inventario: " + inventoryId);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var pedidosCount = 0;
                logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                  ": Procesando pedidos ..." + Environment.NewLine);
                var resultProcesaPedidos = DeliveryCtrl.ProcesaPedidos(AppSettings()["URL_INTEGRACION_API"], AppSettings()["URL_OPE_API"], AppSettings()["URL_WBC_API"],
                    resultToken.Objeto.AccessToken, cedi.ToString(), ruta);
                if (resultProcesaPedidos.ResultType == ResultTypes.Success)
                {
                    logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                  ": Se procesaron los pedidos" + Environment.NewLine);
                    //return (Products ?? new List<Product>()).Sum(product => (int)product.Amount);
                    pedidosCount = resultProcesaPedidos.Objeto.FindAll(x => x.BottlerData.DeliveryDate.Substring(0, 10) == DateTime.Now.ToString("dd/MM/yyyy")).Sum(prod => (int) prod.DeliveriesAmount);
                }
                else
                {
                    logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                 ": " + resultProcesaPedidos.Mensaje + Environment.NewLine);
                }

                if (resultEnvioIntegraWbc.Objeto.InventoryId == 0)
                {
                    return;
                }
                resultLogBitacora = LogBitacoraCtrl.GetLogBitacora(cedi, int.Parse(ruta), 0, DateTime.Now.ToString("yyyy-MM-dd"), 0);
                var confirmarInventario = ConfirmarInventarioWbc(
                resultEnvioIntegraWbc.Objeto.InventoryId,
                new Confirmacion(resultEnvioIntegraWbc.Objeto.ProductsAmount, pedidosCount, 0, 0), resultToken.Objeto.AccessToken);
                if (confirmarInventario.ResultType == ResultTypes.Error)
                {
                    if (resultLogBitacora.ResultType == ResultTypes.Success)
                    {
                        foreach (var logbitacora in resultLogBitacora.Objeto)
                        {
                            if (logbitacora.Trip == resultEnvioIntegraWbc.Objeto.Order && logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 9))
                            {
                                ActualizaBitacora(logbitacora, 9, 3, true, "El inventario " + resultEnvioIntegraWbc.Objeto.InventoryId + " no se confirmo. " + confirmarInventario.Mensaje);
                            }
                        }
                    }

                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Error_InventoryCtrl.ProcesaInventariosIntegraWbc: El envio del inventario para cedis:" +
                        cedi + ", ruta:" +
                        ruta + " no se confirmó."+Environment.NewLine + confirmarInventario.Mensaje, true);
                }
                else
                {
                    if (resultLogBitacora.ResultType != ResultTypes.Success)
                    {
                        return;
                    }
                    else
                    {
                        logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                  ": Envio de inventario confirmado de Integra a WBC para cedi:" + cedi +
                                                                " ruta:" + ruta + Environment.NewLine);
                    }
                    foreach (var logbitacora in resultLogBitacora.Objeto)
                    {
                        if (logbitacora.Trip == resultEnvioIntegraWbc.Objeto.Order && logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 9))
                        {
                            ActualizaBitacora(logbitacora, 9, 1, false, "El inventario " + resultEnvioIntegraWbc.Objeto.InventoryId + " se confirmo correctamente.");
                        }
                        else if (logbitacora.Trip == resultEnvioIntegraWbc.Objeto.Order &&
                                 logbitacora.LogBitacoraDetail.Find(x => x.Process.Id == 9 && x.Success) == null)
                        {
                            ActualizaBitacora(logbitacora, 9, 1, false, "El inventario " + resultEnvioIntegraWbc.Objeto.InventoryId + " se confirmo correctamente.");
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_InventoryCtrl.ProcesaInventariosIntegraWbc para cedi:"+cedi+" ruta:"+ruta+": " + ex.Message + "."+Environment.NewLine + ex.StackTrace +
                    "."+Environment.NewLine, true);
            }
            finally
            {
                CierraLogDictionary(logDictionary, cedi,ruta);
            }
        }

        private static void CierraLogDictionary(string logDictionary,int cedi,string ruta)
        {
            if (string.IsNullOrEmpty(logDictionary)) return;
            logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                               ": Termina proceso de inventarios en SRV_WBC para cedi:" +
                                               cedi + " ruta:" + ruta + Environment.NewLine);
            LogServicioBr.EscribirLog(AppSettings()["InventoryLog"],
                logDictionary +
                Environment.NewLine, true); // *** Monitor Customer Log
        }

  #region Integracion-WBC

        public static Result<Inventory> EnvioInventario_Integra_Wbc(int cediid, string routeCode, DateTime deliveryDate,
            string token, string logDictionary)
        {
            var result = new Result<Inventory> {Objeto = new Inventory()};
            try
            {
                var resultLogBitacora = LogBitacoraCtrl.GetLogBitacora(cediid, int.Parse(routeCode), 0,
                    DateTime.Now.ToString("yyyy-MM-dd"), 0);
                var result1 = GetBranchWbc(cediid, token);
                if (result1.ResultType == ResultTypes.Success)
                {
                    var branch = result1.Objeto[0];
                    var cediidWbc = branch.BranchId;
                    var result2 = GetUserWbc(cediidWbc, routeCode, token);
                    if (result2.ResultType == ResultTypes.Success)
                    {
                        var inventoryHelper = new InventoryHelper();
                        logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                 ": Obteniendo inventario de Integracion." + Environment.NewLine);
                        //LogDictionary[cediid + ":" + routeCode].AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        //            ": Obteniendo inventario de Integracion.");

                        var result4 =
                            inventoryHelper.TripInventoryIntegracion(
                                AppSettings()["URL_INTEGRACION_API"] +
                                ApiIntegracionesCnx.GetTripInventoryIntegra(cediid, routeCode, deliveryDate),
                                int.Parse(AppSettings()["TimeOutCnx"]));
                        if (result4.ResultType == ResultTypes.Success)
                        {
                            foreach (var trip in result4.Objeto)
                            {
                               
                                var bandTerminado = false;
                                if (resultLogBitacora.ResultType == ResultTypes.Success)
                                {
                                    foreach (var logbitacora in resultLogBitacora.Objeto)
                                    {
                                        if (logbitacora.Trip == trip.Id &&
                                            logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 5))
                                        {
                                            ActualizaBitacora(logbitacora, 5, 1, false,
                                                "El inventario se consulto correctamente en Integracion.");
                                            logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                             ": El inventario se consulto correctamente en Integracion." + Environment.NewLine);
                                            //LogDictionary[cediid + ":" + routeCode].AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                            //     ": El inventario se consulto correctamente en Integracion.");
                                        }
                                        else if (logbitacora.Trip == trip.Id &&
                                                 logbitacora.LogBitacoraDetail.Find(x => x.Process.Id == 5 && x.Success) == null)
                                        {
                                            ActualizaBitacora(logbitacora, 5, 1, false,
                                                "El inventario se consulto correctamente en Integracion.");
                                            logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                            ": El inventario se consulto correctamente en Integracion." + Environment.NewLine);
                                            //LogDictionary[cediid + ":" + routeCode].AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                            //     ": El inventario se consulto correctamente en Integracion.");
                                        }
                                    }
                                    if (resultLogBitacora.Objeto.All(x => x.Trip != trip.Id))
                                    {
                                        InsertaBitacora(cediid, int.Parse(routeCode), trip.Id,
                                         trip.DeliveryDate.ToString("yyyy-MM-dd"), 5, 1, false,
                                         "El inventario se consulto correctamente en Integracion.");
                                    }
                                    if (
                                        resultLogBitacora.Objeto.Any(
                                            logbitacora => logbitacora.Trip == trip.Id && logbitacora.ProcessStatus == 2))
                                    {
                                        bandTerminado = true;
                                    }
                                }
                                else
                                {
                                    InsertaBitacora(cediid, int.Parse(routeCode), trip.Id,
                                     trip.DeliveryDate.ToString("yyyy-MM-dd"), 5, 1, false,
                                     "El inventario se consulto correctamente en Integracion.");
                                }
                                if (bandTerminado)
                                {
                                    continue;
                                }

                                if (trip.Id > 1)
                                {
                                    if (!ViajeFinalizado(cediid, trip.Id - 1, trip.Route)) continue;
                                }

                                if (ViajeFinalizado(cediid, trip.Id, trip.Route)) continue;

                                if (trip.ConfirmedItems.Count > 0)
                                {
                                    //  ----------------  inventarios a WBC -------------------------
                                    var invReq = new DeliveryCtrl.InventoryRequest
                                    {
                                        Code =
                                            GeneraCode.Code(trip.DeliveryDate, branch.BranchId.ToString(),
                                                trip.Route.ToString(),
                                                trip.Id.ToString()),
                                        Userid = result2.Objeto[0].UserId,
                                        Order = trip.Id,
                                        State = 0,
                                        Date = DateTime.Now.ToString("s")
                                    };

                                    var bandInventarioCreado = false;
                                    if (resultLogBitacora.ResultType == ResultTypes.Success)
                                    {
                                        if (resultLogBitacora.Objeto.Any(
                                            logbitacora =>
                                                logbitacora.Trip == trip.Id &&
                                                logbitacora.LogBitacoraDetail.Any(x => (x.Process.Id == 6) && x.Success) &&
                                                logbitacora.LogBitacoraDetail.Any(x => (x.Process.Id == 9) && x.Success)))
                                        {
                                            bandInventarioCreado = true;
                                        }
                                    }
                                    if (bandInventarioCreado)
                                    {

                                        break;
                                    }

                                    logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                             ": Publicando inventario en WBC." + Environment.NewLine);
                                    //LogDictionary[cediid + ":" + routeCode].AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                    //             ": Publicando inventario en WBC.");

                                    result =
                                        inventoryHelper.PublicaInventarioWbc(
                                            AppSettings()["URL_WBC_API"] + ApiWbcCnx.PostInventory(),
                                            int.Parse(AppSettings()["TimeOutCnx"]),
                                            invReq, token);

                                    result.Objeto.Products = new List<Product>();
                                    result.Objeto.Replacements = new List<Replacement>();
                                    result.Objeto.Implements = new List<Implement>();
                                    result.Objeto.Deliveries = new List<Product>();
                                    result.Objeto.Code = invReq.Code;
                                    if (result.ResultType == ResultTypes.Success)
                                    {
                                        if (resultLogBitacora.ResultType == ResultTypes.Success)
                                        {
                                            foreach (var logbitacora in resultLogBitacora.Objeto)
                                            {
                                                if (logbitacora.Trip == trip.Id &&
                                                    logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 6))
                                                {
                                                    ActualizaBitacora(logbitacora, 6, 1, false,
                                                        "Se creo correctamente el inventario: " +
                                                        result.Objeto.InventoryId);
                                                    logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                             ": Se creo correctamente el inventario: " + result.Objeto.InventoryId + Environment.NewLine);
                                                    //LogDictionary[cediid + ":" + routeCode].AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                    //    ": Se creo correctamente el inventario: " + result.Objeto.InventoryId);
                                                }
                                                else if (logbitacora.Trip == trip.Id &&
                                                         logbitacora.LogBitacoraDetail.Find(x => x.Process.Id == 6 && x.Success) == null)
                                                {
                                                    ActualizaBitacora(logbitacora, 6, 1, false,
                                                        "Se creo correctamente el inventario: " +
                                                        result.Objeto.InventoryId);
                                                    logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                             ": Se creo correctamente el inventario: " + result.Objeto.InventoryId + Environment.NewLine);
                                                    //LogDictionary[cediid + ":" + routeCode].AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                    //     ": Se creo correctamente el inventario: " + result.Objeto.InventoryId);
                                                }
                                            }
                                        }

                                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " +
                                            result.ResultType +
                                            "_InventoryCtrl.PublicaInventarioWbc: " +
                                            (string.IsNullOrEmpty(result.Mensaje)
                                                ? JsonConvert.SerializeObject(result.Objeto)
                                                : result.Mensaje), true);

                                        var inventoryOk = true;
                                        foreach (var item in trip.ConfirmedItems)
                                        {
                                            if (item.WbcId == 0)
                                            {
                                                var result6 = ProductCtrl.GetProductWbc(item.ItemId, token);
                                                if (result6.ResultType == ResultTypes.Success)
                                                {
                                                    item.ProductId = result6.Objeto[0].ProductId.ToString();
                                                }
                                            }

                                            if (item.WbcId != 0)
                                            {
                                                var result7 =
                                                    EnviaProductosInventario(item, result.Objeto.InventoryId, token);
                                                if (result7.ResultType == ResultTypes.Success)
                                                {
                                                    result.Objeto.Products.Add(result7.Objeto);
                                                }
                                                else
                                                {
                                                    result.ResultType = ResultTypes.Warning;
                                                    result.Mensaje += "No se publico el producto " + item.ProductId +
                                                                      "."+Environment.NewLine;
                                                    inventoryOk = false;
                                                }
                                            }
                                            else
                                            {
                                                result.ResultType = ResultTypes.Warning;
                                                result.Mensaje += "No se obtuvo el productId de WBC del producto: " +
                                                                  item.ItemId + "."+Environment.NewLine;
                                                inventoryOk = false;
                                            }
                                        }

                                        if (inventoryOk) //envio de inventario Exitoso
                                        {
                                            if (resultLogBitacora.ResultType == ResultTypes.Success)
                                            {
                                                foreach (var logbitacora in resultLogBitacora.Objeto)
                                                {
                                                    if (logbitacora.Trip == trip.Id &&
                                                        logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 7))
                                                    {
                                                        ActualizaBitacora(logbitacora, 7, 1, false,
                                                            "Se insertaron correctamente los productos al inventario: " +
                                                            result.Objeto.InventoryId);
                                                        logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                             ": Se insertaron correctamente los productos al inventario: " + result.Objeto.InventoryId + Environment.NewLine);
                                                        //LogDictionary[cediid + ":" + routeCode].AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                        //    ": Se insertaron correctamente los productos al inventario: " + result.Objeto.InventoryId);
                                                    }
                                                    else if (logbitacora.Trip == trip.Id &&
                                                             logbitacora.LogBitacoraDetail.Find(x => x.Process.Id == 7 && x.Success) == null)
                                                    {
                                                        ActualizaBitacora(logbitacora, 7, 1, false,
                                                            "Se insertaron correctamente los productos al inventario: " +
                                                            result.Objeto.InventoryId);
                                                        logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                             ": Se insertaron correctamente los productos al inventario: " + result.Objeto.InventoryId + Environment.NewLine);
                                                        //LogDictionary[cediid + ":" + routeCode].AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                        //    ": Se insertaron correctamente los productos al inventario: " + result.Objeto.InventoryId);
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (resultLogBitacora.ResultType == ResultTypes.Success)
                                            {
                                                foreach (var logbitacora in resultLogBitacora.Objeto)
                                                {
                                                    if (logbitacora.Trip == trip.Id &&
                                                        logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 7))
                                                    {
                                                        ActualizaBitacora(logbitacora, 7, 3, true,
                                                            "Algunos productos no se insertaron en WBC."+Environment.NewLine +
                                                            result.Mensaje);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        result.ResultType = ResultTypes.Warning;
                                        result.Mensaje = "No se envio la cabecera de inventario a WBC";
                                        if (resultLogBitacora.ResultType == ResultTypes.Success)
                                        {
                                            foreach (var logbitacora in resultLogBitacora.Objeto)
                                            {
                                                if (logbitacora.Trip == trip.Id &&
                                                    logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 6))
                                                {
                                                    ActualizaBitacora(logbitacora, 6, 3, true, result.Mensaje);
                                                }
                                            }
                                        }
                                    }

                                    // -------------------- Fin inventarios Wbc ---------------------
                                }
                                else
                                {
                                    result.ResultType = ResultTypes.Warning;
                                    result.Mensaje = "No se obtuvieron inventarios de la Base de Datos de Intermedia";
                                    if (resultLogBitacora.ResultType == ResultTypes.Success)
                                    {
                                        foreach (var logbitacora in resultLogBitacora.Objeto)
                                        {
                                            if (logbitacora.Trip == trip.Id &&
                                                logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 7))
                                            {
                                                ActualizaBitacora(logbitacora, 7, 3, true, result.Mensaje);
                                            }
                                        }
                                    }
                                }

                                break; // dejar que procese 1 inventario a la vez
                            }
                        }
                        else
                        {
                            result.ResultType = ResultTypes.Warning;
                            result.Mensaje = "No se obtuvieron inventarios de la Base de Datos de Intermedia";
                            InsertaBitacora(cediid, int.Parse(routeCode), null, DateTime.Now.ToString("yyyy-MM-dd"), 5,
                                3, true, result.Mensaje);
                            logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                            ": No se obtuvieron inventarios de la Base de Datos de Integracion." + Environment.NewLine);
                            //LogDictionary[cediid + ":" + routeCode].AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                            //             ": No se obtuvieron inventarios de la Base de Datos de Integracion.");
                        }
                    }
                    else
                    {
                        result.ResultType = ResultTypes.Warning;
                        result.Mensaje = "No se obtuvo el UserIdWbc";
                        InsertaBitacora(cediid, int.Parse(routeCode), null, DateTime.Now.ToString("yyyy-MM-dd"), 5, 3,
                            true, result.Mensaje);
                    }
                }
                else
                {
                    result.ResultType = ResultTypes.Warning;
                    result.Mensaje = "No se obtuvo el BranchIdWbc";
                    InsertaBitacora(cediid, int.Parse(routeCode), null, DateTime.Now.ToString("yyyy-MM-dd"), 5, 3, true,
                        result.Mensaje);
                }
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Error_InventoryCtrl.EnvioInventario_Integra_Wbc: " + ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine,
                    true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
                InsertaBitacora(cediid, int.Parse(routeCode), null, DateTime.Now.ToString("yyyy-MM-dd"), 5, 3, true,
                    result.Mensaje);
            }

            return result;
        }

        #endregion

        public class CedisRutaInventario
        {
            public int Cedi { get; set; }
            public List<Ruta> Destinos { get; set; }

            public class Ruta
            {
                public int RouteCodeOpecd { get; set; }
                private int _enable;

                public int? Enable
                {
                    get { return _enable; }
                    set { _enable = value ?? 1; }

                }
            }
        }

        //private static bool ViajesEnCurso(int branchCode, int routeCode)
        //{
        //    try
        //    {
        //        var date = DateTime.Today;
        //        var service = new DownloadService(AppSettings()["URL_WBC_Ventas"]);
        //        var resultViaje = VentasCtrl.GetViajes(service, branchCode, date);
        //        if (resultViaje.ResultType != ResultTypes.Success)
        //        {
        //            return false;
        //        }

        //        if (resultViaje.Objeto.Any(t => t.Ruta == routeCode && !t.ViajeFinalizado))
        //        {
        //            return true;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //    return false;
        //}

        private static bool ViajeFinalizado(int branchCode, int tripId, int routecode)
        {
            try
            {
                var date = DateTime.Today;
                var service = new DownloadService(AppSettings()["URL_WBC_Ventas"]);
                var resultViaje = VentasCtrl.GetViajes(service, branchCode, date);
                if (resultViaje.ResultType != ResultTypes.Success)
                {
                    return false;
                }

                if (resultViaje.Objeto.Any(t => t.NumeroViaje == tripId && t.Ruta == routecode && t.ViajeFinalizado))
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        private static bool JornadaIniciada(int branchCode, int tripId, int routeCode)
        {
            try
            {
                var date = DateTime.Today;
                var service = new DownloadService(AppSettings()["URL_WBC_Ventas"]);
                var resultViaje = VentasCtrl.GetViajes(service, branchCode, date);
                if (resultViaje.ResultType != ResultTypes.Success)
                {
                    return false;
                }

                if (resultViaje.Objeto.Any(t => t.NumeroViaje == tripId && t.Ruta == routeCode && !t.JornadaFinalizada))
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public class Confirmacion
        {
            public Confirmacion(int prod, int deli, int repla, int imple)
            {
                ProductsAmount = prod;
                DeliveriesAmount = deli;
                ReplacementsAmount = repla;
                ImplementsAmount = imple;
            }

            public int ProductsAmount { get; set; }
            public int DeliveriesAmount { get; set; }
            public int ReplacementsAmount { get; set; }
            public int ImplementsAmount { get; set; }
        }

        public static void InsertaBitacora(int cedis, int ruta, int? viaje, string deliveryDate, int proceso,
            int estatus, bool error, string errorDescription)
        {
            try
            {
                var logBitacora = new LogBitacora
                {
                    CedisId = cedis,
                    RouteId = ruta,
                    ProcessStatus = estatus,
                    Trip = viaje,
                    Process = new ProcesosSistema {Id = proceso},
                    DeliveryDate = deliveryDate
                };
                var resultLogBitacora = LogBitacoraCtrl.PostEnvioLogBitacora(logBitacora);
                if (resultLogBitacora.ResultType != ResultTypes.Success) return;
                var logBitacoraDetail = new LogBitacoraDetail
                {
                    LogId = resultLogBitacora.Objeto.Id,
                    Process = new ProcesosSistema {Id = proceso},
                    Success = !error,
                    ErrorDescription = errorDescription
                };
                LogBitacoraCtrl.PostEnvioLogBitacoraDetail(logBitacoraDetail);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void ActualizaBitacora(LogBitacora logBitacora, int proceso, int estatus, bool error,
            string errorDescription)
        {
            try
            {
                logBitacora.ProcessStatus = estatus;
                logBitacora.Process = new ProcesosSistema {Id = proceso};
                var resultLogBitacora = LogBitacoraCtrl.PutEnvioLogBitacora(logBitacora);
                if (resultLogBitacora.ResultType != ResultTypes.Success) return;
                var logBitacoraDetail = new LogBitacoraDetail
                {
                    LogId = logBitacora.Id,
                    Process = new ProcesosSistema {Id = proceso},
                    Success = !error,
                    ErrorDescription = errorDescription
                };
                LogBitacoraCtrl.PostEnvioLogBitacoraDetail(logBitacoraDetail);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static bool EsJornadaCerrada(int cedisId, int route)
        {
            try
            {
                var inventarioHelper = new InventoryHelper();
                return inventarioHelper.GetJornadaCerrada(
                           AppSettings()["URL_INTEGRACION_API"] +
                           ApiIntegracionesCnx.GetEstadoJornada(cedisId, route, DateTime.Today.ToString("yyyy-MM-dd")),
                           int.Parse(AppSettings()["TimeOutCnx"])).Objeto.Count != 0;
            }
            catch 
            {
                return false;
            }
        }

        public static int ReconfirmarInventario(int cedis, int ruta, string token)
        {
            try
            {
                var inventoryId = 0;
                var resultLogBitacora = LogBitacoraCtrl.GetLogBitacora(cedis, ruta, 0,
                    DateTime.Now.ToString("yyyy-MM-dd"), 0);
                if (resultLogBitacora.ResultType == ResultTypes.Success)
                {
                    foreach (var logbitacora in resultLogBitacora.Objeto)
                    {
                        if (logbitacora.ProcessStatus == 2)
                        {
                            return 0;
                        }
                        if (logbitacora.Trip != 1 ||
                            !logbitacora.LogBitacoraDetail.All(x => x.Process.Id == 6 && x.Success)) continue;
                        {
                            var detailLog = logbitacora.LogBitacoraDetail.Find(x => x.Process.Id == 6);
                            var cad = detailLog.ErrorDescription.Split(':');
                            inventoryId = int.Parse(cad[1].Trim());
                        }
                    }
                }
                if (inventoryId <= 0) return inventoryId;
                var inventoryHelper = new InventoryHelper();
                var result4 =
                    inventoryHelper.TripInventoryIntegracion(
                        AppSettings()["URL_INTEGRACION_API"] +
                        ApiIntegracionesCnx.GetTripInventoryIntegra(cedis, ruta.ToString(), DateTime.Today),
                        int.Parse(AppSettings()["TimeOutCnx"]));
                if (result4.ResultType != ResultTypes.Success) return 0;
                var productosInventario = ConsultarProductosInventarioWbc(inventoryId, token);
                if (productosInventario.ResultType != ResultTypes.Success) return 0;
                foreach (var item in result4.Objeto[0].ConfirmedItems)
                {
                    if (item.WbcId != 0) continue;
                    var result6 = ProductCtrl.GetProductWbc(item.ItemId, token);
                    if (result6.ResultType == ResultTypes.Success)
                    {
                        item.ProductId = result6.Objeto[0].ProductId.ToString();
                    }
                }
                return productosInventario.Objeto.Any(itemProdWbc => result4.Objeto[0].ConfirmedItems.Any(itemProdInt => itemProdWbc.ProductId == itemProdInt.WbcId && itemProdWbc.Amount != itemProdInt.Amount)) ? inventoryId : 0;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return 0;
            }
        }

    }
}