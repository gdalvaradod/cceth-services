using CCETH091703.Model.Objects.BO;
using OpeCDLib.Services;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using CCETH091703.Base;
using CCETH091703.Model.BR;
using CCETH091703.Model.DataAccess;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;
using Newtonsoft.Json;
using System.Linq;
using System.Text;


namespace CCETH091703.Model.Services
{
    public static class VentasCtrl
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        //obtiene Viages del DLL WBC
        public static Result<List<ViajeJornada>> GetViajes(DownloadService service, int branchCode, DateTime date)
        {
            var ventasHelper = new VentasHelper();
            return ventasHelper.GetViajes(service, branchCode, date);
        }

        // obtiene ventas del dll WBC
        public static Result<List<Ventas>> GetVentas(DownloadService service, int branchCode, int route, DateTime date,
            int trip)
        {
            var ventasHelper = new VentasHelper();
            return ventasHelper.GetVentas(service, branchCode, route, date, trip);
        }

        //obtiene visitas del dll Wbc
        public static Result<List<Visitas>> GetVisitas(DownloadService service, int branchCode, int userCode,
            DateTime date)
        {
            var ventasHelper = new VentasHelper();
            return ventasHelper.GetVisitas(service, branchCode, userCode, date);
        }

        
        public static Result<bool> PostVentasIntegracion(List<Ventas> ventas, int cedisId)
        {
            var result = new Result<bool>();
            try
            {
                var helper = new VentasHelper();
                result.ResultType = ResultTypes.Success;
                result =
                    helper.PostVentasIntegracion(
                        AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.PostSalesIntegracion(cedisId),
                        int.Parse(AppSettings()["TimeOutCnx"]), JsonConvert.SerializeObject(ventas));
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_VentasCtrl.PostVentasIntegracion: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.Objeto = false;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<object> EnviarVentasAll(int branchCode, int route, string logDictionary)
        {
            var resultGral = new Result<object>();
            try
            {
                var date = DateTime.Today;
                //var date=new DateTime(2019, 01, 30); //Fecha a consultar
                var service = new DownloadService(AppSettings()["URL_WBC_Ventas"]);

                logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                 ": Obteniendo Viajes." + Environment.NewLine);

                var resultViaje = GetViajes(service, branchCode, date);
                if (resultViaje.ResultType != ResultTypes.Success)
                {
                    if (resultViaje.ResultType == ResultTypes.Error)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Error_VentasCtrl.EnviarVentasAll: No se obtuvieron viajes de BranchCode:" + branchCode +
                        " ,Fecha:" + date + "."+Environment.NewLine, true);
                    }
                    resultGral.Mensaje = DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Error_VentasCtrl.EnviarVentasAll: No se obtuvieron viajes de BranchCode:" + branchCode +
                        " ,Fecha:" + date + "."+Environment.NewLine;
                    resultGral.ResultType = resultViaje.ResultType;
                    logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                ": Problema al obtener Viajes." + Environment.NewLine);
         
                    return resultGral;
                }
                logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                ": Viajes obtenidos." + Environment.NewLine);
                
                var jornadaFin = false;
                foreach (var t in resultViaje.Objeto)
                {
                    if (t.Ruta != route) continue;

                    if (!t.ViajeFinalizado) continue;
                    var resultLogBitacora = LogBitacoraCtrl.GetLogBitacora(branchCode, route, t.NumeroViaje, date.ToString("yyyy-MM-dd"), 0);
                    logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                ": Obteniendo Ventas." + Environment.NewLine);
                    
                    var resultVentas = GetVentas(service, branchCode, route, date, t.NumeroViaje);
                    if (resultVentas.ResultType == ResultTypes.Success)
                    {
                        if (resultLogBitacora.ResultType == ResultTypes.Success)
                        {
                            foreach (var logbitacora in resultLogBitacora.Objeto)
                            {
                                if (logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 1))
                                {
                                    InventoryCtrl.ActualizaBitacora(logbitacora, 1, 1, false, "Se obtuvieron correctamente las ventas.");
                                    logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                ": Se obtuvieron correctamente las ventas." + Environment.NewLine);
                                }
                                else if (logbitacora.LogBitacoraDetail.Find(x => x.Process.Id == 1 && x.Success) == null)
                                {
                                    InventoryCtrl.ActualizaBitacora(logbitacora, 1, 1, false, "Se obtuvieron correctamente las ventas.");
                                    logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                ": Se obtuvieron correctamente las ventas." + Environment.NewLine);
                                }
                            }
                        }
                        resultVentas.Objeto.RemoveAll(x => x.Code == null);
                        foreach (var t1 in resultVentas.Objeto)
                        {
                            var resultGetCustomerRel =
                                CustomerCtrl.GetCustomerRelation(branchCode, "", t1.Code, -1);  
                            if (resultGetCustomerRel.ResultType != ResultTypes.Success) continue;
                            if (!resultGetCustomerRel.Objeto[0].CustomerType.Equals("CLIENTE_COCACOLA")) continue;
                            t1.CustomerCCTH = true;
                            t1.Cuc = resultGetCustomerRel.Objeto[0].Cuc;
                        }
                        var result2 = PostVentasIntegracion(resultVentas.Objeto, branchCode);
                        if (result2.ResultType == ResultTypes.Error)
                        {
                            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " +
                                result2.ResultType +
                                "_VentasCtrl.PostVentasIntegracion: " +
                                (string.IsNullOrEmpty(result2.Mensaje)
                                    ? JsonConvert.SerializeObject(result2.Objeto)
                                    : result2.Mensaje), true);
                            resultGral.Mensaje = DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " +
                                result2.ResultType +
                                "_VentasCtrl.PostVentasIntegracion: " +
                                (string.IsNullOrEmpty(result2.Mensaje)
                                    ? JsonConvert.SerializeObject(result2.Objeto)
                                    : result2.Mensaje);
                            resultGral.ResultType = result2.ResultType;
                            if (resultLogBitacora.ResultType != ResultTypes.Success) return resultGral;
                            foreach (var logbitacora in resultLogBitacora.Objeto)
                            {
                                if (logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 2))
                                {
                                    InventoryCtrl.ActualizaBitacora(logbitacora, 2, 3, true, "No se pudieron insertar las ventas en intermedia. "+Environment.NewLine + resultGral.Mensaje);
                                    logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                ": No se pudieron insertar las ventas en intermedia." + Environment.NewLine);
                                }
                            }
                            return resultGral;
                        }
                        if (resultLogBitacora.ResultType != ResultTypes.Success) continue;
                        {
                            foreach (var logbitacora in resultLogBitacora.Objeto)
                            {
                                if (logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 2))
                                {
                                    InventoryCtrl.ActualizaBitacora(logbitacora, 2, 1, false, "Se insertaron correctamente las ventas en intermedia.");
                                    logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                ": Se insertaron correctamente las ventas en intermedia." + Environment.NewLine);
                                }
                                else if (logbitacora.LogBitacoraDetail.Find(x => x.Process.Id == 2 && x.Success) == null)
                                {
                                    InventoryCtrl.ActualizaBitacora(logbitacora, 2, 1, false, "Se insertaron correctamente las ventas en intermedia.");
                                    logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                ": Se insertaron correctamente las ventas en intermedia." + Environment.NewLine);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (resultVentas.ResultType == ResultTypes.Error)
                        {
                            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                ": Error_VentasCtrl.GetVentas: No se obtuvieron ventas para BranchCode : " + branchCode +
                                ", route : " + route + ", fecha : " + date + ",viaje : " + t.NumeroViaje + "."+Environment.NewLine + resultVentas.Mensaje, true);

                            if (resultLogBitacora.ResultType == ResultTypes.Success)
                            {
                                foreach (var logbitacora in resultLogBitacora.Objeto)
                                {
                                    if (logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 1))
                                    {
                                        InventoryCtrl.ActualizaBitacora(logbitacora, 1, 3, true, "No se pudieron obtener las ventas de WBC."+Environment.NewLine + resultVentas.Mensaje);
                                    }
                                }
                            }
                        }
                        resultGral.Mensaje = DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                ": Error_VentasCtrl.GetVentas: No se obtuvieron ventas para BranchCode : " + branchCode +
                                ", route : " + route + ", fecha : " + date + ",viaje : " + t.NumeroViaje + "."+Environment.NewLine + resultVentas.Mensaje;
                        resultGral.ResultType = resultVentas.ResultType;

                        //return resultGral;
                    }
                    resultLogBitacora = LogBitacoraCtrl.GetLogBitacora(branchCode, route, t.NumeroViaje, date.ToString("yyyy-MM-dd"), 0);
                    if (!t.JornadaFinalizada) continue;
                    {
                        var helper = new VentasHelper();
                        var jornada = new Jornada(branchCode, route, date);
                        logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                ": Cerrando Jornada." + Environment.NewLine);

                        Result<Jornada> resultJourney;
                        if (jornadaFin)
                        {
                            resultJourney = new Result<Jornada> { ResultType = ResultTypes.Success };
                        }
                        else
                        {
                            resultJourney = helper.PostJornadaCerrada(
                                AppSettings()["URL_INTEGRACION_API"] + ApiIntegracionesCnx.PostEstadoJornada(),
                                int.Parse(AppSettings()["TimeOutCnx"]), JsonConvert.SerializeObject(jornada));
                        }

                        if (resultJourney.ResultType == ResultTypes.Success)
                        {
                            if (resultLogBitacora.ResultType == ResultTypes.Success)
                            {
                                foreach (var logbitacora in resultLogBitacora.Objeto)
                                {
                                    if (logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 13))
                                    {
                                        InventoryCtrl.ActualizaBitacora(logbitacora, 13, 1, false, "Se inserto correctamente la jornada finalizada.");
                                        logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                ":Se inserto correctamente la jornada finalizada.." + Environment.NewLine);
                                    }
                                    else if (logbitacora.LogBitacoraDetail.Find(x => x.Process.Id == 13 && x.Success) == null)
                                    {
                                        InventoryCtrl.ActualizaBitacora(logbitacora, 13, 1, false, "Se inserto correctamente la jornada finalizada.");
                                        logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                ":Se inserto correctamente la jornada finalizada.." + Environment.NewLine);
                                    }
                                }
                            }
                        }
                        else if (resultJourney.ResultType == ResultTypes.Error)
                        {
                            if (resultLogBitacora.ResultType == ResultTypes.Success)
                            {
                                foreach (var logbitacora in resultLogBitacora.Objeto)
                                {
                                    if (logbitacora.LogBitacoraDetail.All(x => x.Process.Id != 13))
                                    {
                                        InventoryCtrl.ActualizaBitacora(logbitacora, 13, 3, true, "No se pudo indicar jornada finalizada en integración." + Environment.NewLine + resultGral.Mensaje);
                                        logDictionary += new StringBuilder(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                                ": No se pudo indicar jornada finalizada en integración." + Environment.NewLine);
                                    }
                                }
                            }
                        }
                        jornadaFin = true;
                        continue;
                    }
                }
                return resultGral;
            }

            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_VentasCtrl.EnviarVentasAll: " +
                    ex.Message +
                    "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                resultGral.Mensaje = DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_VentasCtrl.EnviarVentasAll: " +
                                     ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                resultGral.ResultType = ResultTypes.Error;
                return resultGral;
            }
        }

    }

}