﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using CCETH091703.Base;
using CCETH091703.Model.BR;
using CCETH091703.Model.DataAccess;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib.Model.Objects.Enum;
using CCETH091703.Model.Objects.BO;

namespace CCETH091703.Model.Services
{
    public static class RouteCtrl
    {
        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        public static Result<List<Route>> GetRouteWbc( int branchId, string code, string token)
        {
            var result = new Result<List<Route>>();
            try
            {
                var helper = new RouteHelper();
                result = helper.GetRouteWbc(AppSettings()["URL_WBC_API"] + ApiWbcCnx.GetRoute(branchId, code),
                    int.Parse(AppSettings()["TimeOutCnx"]), token);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_RouteCtrl.GetEouteWbc: " + ex.Message +
                    "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }

        public static Result<Route> PostRouteWbc( Route route, string token)
        {
            var result = new Result<Route>();
            try
            {
                var helper = new RouteHelper();
                result = helper.PostRouteWbc(AppSettings()["URL_WBC_API"] + ApiWbcCnx.PostRoute(),
                    int.Parse(AppSettings()["TimeOutCnx"]), token, route);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_RouteCtrl.PostRouteWbc: " +
                    ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                result.Objeto = null;
                result.Mensaje = ex.Message + "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine;
                result.ResultType = ResultTypes.Error;
            }
            return result;
        }
    }
}