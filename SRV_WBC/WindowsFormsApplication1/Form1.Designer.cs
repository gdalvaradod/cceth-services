﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnCustomerAll = new System.Windows.Forms.Button();
            this.btnVentasWBC = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Inventorybtn = new System.Windows.Forms.Button();
            this.btnpedidos = new System.Windows.Forms.Button();
            this.btnProductos = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(12, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(341, 261);
            this.textBox1.TabIndex = 1;
            // 
            // btnCustomerAll
            // 
            this.btnCustomerAll.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnCustomerAll.Location = new System.Drawing.Point(19, 21);
            this.btnCustomerAll.Name = "btnCustomerAll";
            this.btnCustomerAll.Size = new System.Drawing.Size(68, 23);
            this.btnCustomerAll.TabIndex = 2;
            this.btnCustomerAll.Text = "Clientes All";
            this.btnCustomerAll.UseVisualStyleBackColor = true;
            this.btnCustomerAll.Click += new System.EventHandler(this.btnCustomerAll_Click);
            // 
            // btnVentasWBC
            // 
            this.btnVentasWBC.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnVentasWBC.Location = new System.Drawing.Point(20, 50);
            this.btnVentasWBC.Name = "btnVentasWBC";
            this.btnVentasWBC.Size = new System.Drawing.Size(68, 23);
            this.btnVentasWBC.TabIndex = 3;
            this.btnVentasWBC.Text = "VentasWBC";
            this.btnVentasWBC.UseVisualStyleBackColor = true;
            this.btnVentasWBC.Click += new System.EventHandler(this.btnVentasWBC_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Inventorybtn);
            this.panel1.Controls.Add(this.btnpedidos);
            this.panel1.Controls.Add(this.btnProductos);
            this.panel1.Controls.Add(this.btnCustomerAll);
            this.panel1.Controls.Add(this.btnVentasWBC);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(359, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(105, 285);
            this.panel1.TabIndex = 4;
            // 
            // Inventorybtn
            // 
            this.Inventorybtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Inventorybtn.Location = new System.Drawing.Point(19, 138);
            this.Inventorybtn.Name = "Inventorybtn";
            this.Inventorybtn.Size = new System.Drawing.Size(68, 23);
            this.Inventorybtn.TabIndex = 8;
            this.Inventorybtn.Text = "Inventarios";
            this.Inventorybtn.UseVisualStyleBackColor = true;
            this.Inventorybtn.Click += new System.EventHandler(this.Inventarios_Click);
            // 
            // btnpedidos
            // 
            this.btnpedidos.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnpedidos.Location = new System.Drawing.Point(19, 79);
            this.btnpedidos.Name = "btnpedidos";
            this.btnpedidos.Size = new System.Drawing.Size(68, 23);
            this.btnpedidos.TabIndex = 4;
            this.btnpedidos.Text = "Pedidos";
            this.btnpedidos.UseVisualStyleBackColor = true;
            this.btnpedidos.Click += new System.EventHandler(this.btnpedidos_Click);
            // 
            // btnProductos
            // 
            this.btnProductos.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnProductos.Location = new System.Drawing.Point(19, 108);
            this.btnProductos.Name = "btnProductos";
            this.btnProductos.Size = new System.Drawing.Size(68, 23);
            this.btnProductos.TabIndex = 5;
            this.btnProductos.Text = "Productos";
            this.btnProductos.UseVisualStyleBackColor = true;
            this.btnProductos.Click += new System.EventHandler(this.btn_ProductosAll_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 285);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Servicio WBC";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnCustomerAll;
        private System.Windows.Forms.Button btnVentasWBC;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnpedidos;
        private System.Windows.Forms.Button btnProductos;
        private System.Windows.Forms.Button Inventorybtn;
    }
}

