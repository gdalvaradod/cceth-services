﻿using CCETH091703.Model.Services;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows.Forms;
using CCETH091703.Model.BR;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.BO;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        private Result<Token> _resultToken;
        //private readonly string _resultToken.Objeto.AccessToken;
        public Form1()
        {
            InitializeComponent();
            //IntegracionCtlr.ActualizaConfig();
            LogServicioBr.EscribirLog(AppSettings()["UsandoApisLog"],
                DateTime.Now.ToString(CultureInfo.InvariantCulture) + Environment.NewLine +
                ": Api_Integracion=" + AppSettings()["URL_INTEGRACION_API"] + Environment.NewLine +
                ": Api_WBC=" + AppSettings()["URL_WBC_API"] + Environment.NewLine);
            
            if (!AppSettings()["grant_type"].StartsWith("1_"))
            {
                AddUpdateAppSettings("grant_type",
                    "1_" + Metodos.Seguridad_Cifrar(AppSettings()["grant_type"]).Objeto);
            }
            if (!AppSettings()["username"].StartsWith("1_"))
            {
                AddUpdateAppSettings("username", "1_" + Metodos.Seguridad_Cifrar(AppSettings()["username"]).Objeto);
            }
            if (!AppSettings()["password"].StartsWith("1_"))
            {
                AddUpdateAppSettings("password", "1_" + Metodos.Seguridad_Cifrar(AppSettings()["password"]).Objeto);
            }
            //_resultToken = new Result<Token>
            //{
            //    Objeto = new Token
            //    {
            //        Expires =
            //            DateTime.Now.AddHours(TimeSpan.FromHours(double.Parse("129349")).Hours)
            //                .ToString(CultureInfo.InvariantCulture),
            //        AccessToken =
            //            "u6udGYoJl586KXZ_anjrO8WmJIgg98jqg4xGqu9qFlSC25NUijwbegqvTw3EIBpHydm6NyuNBmi83r_DRUvsfZkayB5irBgNJe6mISPpXtmWdrbY5DNu43Tjefd2DLA0oHa9p75UoHgqUTK8W7pMFkSynglmAvSljr4GjRuJy6tmW4hbEUZxVa2RIOtbYmAo6y0PFz1F4EwWa4rLCBOEPYB7m2hNtoeyFUeTEN0lbDEit9Fb5Zg2VsZHufRt7bnZ7GKjIS2lrjisylgXad00MIa8oAw7KRqpla9zq9v1FgmQbLg3E7DUl-GBHcVa9brNzwVWihK0921pWPyrKawdUcifYLnVmIaOp5otzMPNVQzgtPOqEINfe5Sr-vZLD35v"
            //    }
            //};
            //_resultToken.Objeto.AccessToken = "gCHELNv-7oYgC6gtbr6l1vG3W9DhPMdeJjDgkwWeNZivmuhMy6bNUiWh5Z9nUBt_EYUYlsV1TZroJmF3mhz2UfFQWhMPEeFz3FGqQCN5rNcfNNfr6MFUatTGgOM9sRi9i3gEWHo_-Vc9kI9kZMl4JwNfcMW-tApnZfwCwnxjPfIaAZeaL6tcm2AQCc5xR_Lwr8pLQHjr1RC_WJvxymU5Kj9zRtLqRcti74_CUL_PAEdPJnN1kevrorndegR8Qd1myMAGsoHrzqyFR_RoL9ArsJv86ikc9_Avqtxcr6YRslk3G5P2GXLAaOqeHHAhgYudFUfPsZiI51JhYXeaMDhLgxo8ASNjCVNZU_l5HfujoCaA2AZJt8YKM9KZs_7d_UdX";

            ObtenerToken();
        }

        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        private static void AddUpdateAppSettings(string key, string value)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {
                    settings[key].Value = value;
                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException e)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_AddUpdateAppSettings",
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + e.Message.Replace(",", ""), true);
                Console.WriteLine(e.Message);
            }
        }

        private void btnCustomerAll_Click(object sender, EventArgs e)
        {
            //CustomerCtrl.CustomerAll(_resultToken);
            CustomerCtrl.Customer(_resultToken,97,"520");
            //EnviarClientesAsync();
        }

        private Task _taskClientes;

        protected async void EnviarClientesAsync()
        {
            if (_taskClientes != null)
            {
                if (_taskClientes.IsCompleted)
                {
                    await (_taskClientes = Task.Run(() =>
                    {
                        try
                        {
                            CustomerCtrl.CustomerAll(_resultToken);
                        }
                        catch (Exception ex)
                        {
                            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_ConsultarVentas",
                                DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""),
                                true);
                            Console.WriteLine(ex);
                        }

                    }));
                }
            }
            else
            {
                await (_taskClientes = Task.Run(() =>
                {
                    try
                    {
                        CustomerCtrl.CustomerAll(_resultToken);
                    }
                    catch (Exception ex)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_ConsultarVentas",
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""),
                            true);
                        Console.WriteLine(ex);
                    }

                }));
            }
        }

        private void ObtenerToken()
        {
            try
            {
                
                _resultToken = TokenCtrl.GetToken(Metodos.Seguridad_Descifrar(AppSettings()["grant_type"]).Objeto,
                    Metodos.Seguridad_Descifrar(AppSettings()["username"]).Objeto,
                    Metodos.Seguridad_Descifrar(AppSettings()["password"]).Objeto);
            }
            catch (Exception e)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_ObtenerToken",
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + e.Message.Replace(",", ""), true);
                Console.WriteLine(e);
            }
        }

        private void btnVentasWBC_Click(object sender, EventArgs e)
        {
            try
            {
                
                VentasCtrl.EnviarVentasAll(97, 453, "");
                //EnviarVentasAsync();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_ConsultarVentas",
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""), true);
                Console.WriteLine(ex);
            }
        }

        private Task _task;

        protected async void EnviarVentasAsync()
        {
            if (_task != null)
            {
                if (_task.IsCompleted)
                {
                    await (_task = Task.Run(() =>
                    {
                        try
                        {
                            VentasCtrl.EnviarVentasAll(97, 470,"");
                        }
                        catch (Exception ex)
                        {
                            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_ConsultarVentas",
                                DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""),
                                true);
                            Console.WriteLine(ex);
                        }

                    }));
                }
            }
            else
            {
                await (_task = Task.Run(() =>
                {
                    try
                    {
                        VentasCtrl.EnviarVentasAll(97, 470,"");
                    }
                    catch (Exception ex)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_ConsultarVentas",
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""),
                            true);
                        Console.WriteLine(ex);
                    }

                }));
            }

        }

        private void btnpedidos_Click(object sender, EventArgs e)
        {
            try
            {
                EnviarPedidos();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_ConsultaPedidos",
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""), true);
            }
        }

        private async void EnviarPedidos()
        {
            //await Task.Run(() =>
            //{
                var result = DeliveryCtrl.ProcesaPedidos(AppSettings()["URL_INTEGRACION_API"], AppSettings()["URL_OPE_API"],
                    AppSettings()["URL_WBC_API"], _resultToken.Objeto.AccessToken,"97","469");

            //});
        }

        private void btn_ProductosAll_Click(object sender, EventArgs e)
        {
            if (DateTime.Compare(DateTime.Now, DateTime.Parse(_resultToken.Objeto.Expires)) > 0)
            {
                ObtenerToken();
            }
            try
            {
                ProductCtrl.ProcesaProductosIntegraWbcAllAsync(_resultToken);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_Form.btn_ProductosAll_Click: " + ex.Message +
                    "."+Environment.NewLine + ex.StackTrace + "."+Environment.NewLine, true);
                Console.WriteLine(ex);
            }
        }

        private void Inventarios_Click(object sender, EventArgs e)
        {
            //InventoryCtrl.ProcesaInventariosOpeIntegraWbcAllAsync(_resultToken);
            InventoryCtrl.ProcesaInventariosIntegraWbc(97,"469", _resultToken);
            //EnviarInventariosAsync();
        }

        private Task _taskInventarios;
        protected async void EnviarInventariosAsync()
        {
            if (_taskInventarios != null)
            {
                if (!_taskInventarios.IsCompleted) return;
                LogServicioBr.EscribirLog(AppSettings()["InventoryLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Termino proceso de inventarios Integra-Wbc", true);  // *** Monitor Customer Log
                await (_taskInventarios = Task.Run(() =>
                {
                    try
                    {
                        InventoryCtrl.ProcesaInventariosIntegraWbcAllAsync(_resultToken);
                    }
                    catch (Exception ex)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_ConsultarVentas",
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""),
                            true);
                        Console.WriteLine(ex);
                    }

                }));
            }
            else
            {
                await (_taskInventarios = Task.Run(() =>
                {
                    try
                    {
                        InventoryCtrl.ProcesaInventariosIntegraWbcAllAsync(_resultToken);
                    }
                    catch (Exception ex)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_ConsultarVentas",
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""),
                            true);
                        Console.WriteLine(ex);
                    }
                }));
            }
        }
    }
}