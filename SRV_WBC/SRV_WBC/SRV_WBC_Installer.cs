﻿using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using System.ServiceProcess;

namespace SRV_WBC
{
    [RunInstaller(true)]
    public partial class SRV_WBC_Installer : Installer
    {
        
        public SRV_WBC_Installer()
        {
            InitializeComponent();
        }

        protected override void OnAfterInstall(IDictionary savedState)
        {
            base.OnAfterInstall(savedState);

            //The following code starts the services after it is installed.
            //using (var serviceController = new ServiceController(CCETHInstaller.ServiceName))
            //{
            //    serviceController.Start();
            //}
        }

        protected override void OnBeforeInstall(IDictionary savedState)
        {
            SetServiceName();
            base.OnBeforeInstall(savedState);
        }

        protected override void OnBeforeUninstall(IDictionary savedState)
        {
            SetServiceName();
            base.OnBeforeUninstall(savedState);
        }

        private void SetServiceName()
        {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            var fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            var version = fvi.FileVersion;
            CCETHInstaller.ServiceName = "CCETH_SRV_WBC";
            CCETHInstaller.Description = "CCETH_Servicio_Comunicación_Intermedia_WBC_Versión: " + version;
            CCETHInstaller.StartType = ServiceStartMode.Manual;
        }
    }
}
