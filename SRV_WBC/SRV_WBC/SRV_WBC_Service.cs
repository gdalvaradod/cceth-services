﻿using CCETH091703.Model.Services;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Text;
using System.Timers;
using Timer = System.Timers.Timer;
using CCETH091703.Model.BR;
using CCETH091703.Model.Objects.BO;
using GrupoGitLib.Model.Objects.BO;
using GrupoGitLib;
using GrupoGitLib.Model.Objects.Enum;

namespace SRV_WBC
{
    public partial class SRV_WBC_Service : ServiceBase
    {
        private Timer _tmrPedidos;
        private Timer _tmrVentas;
        private Timer _tmrClientes;
        private Timer _tmrProductos;
        private Timer _tmrInventario;
        private Task _taskClientes;
        private Task _taskPedidos;
        //private Task _taskProductos;
        private Task _taskInventarios;
        private Result<Token> _resultToken;

        public SRV_WBC_Service()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            //Debugger.Launch();
            IntegracionCtlr.ActualizaConfig();
            LogServicioBr.EscribirLog(AppSettings()["UsandoApisLog"],
                DateTime.Now.ToString(CultureInfo.InvariantCulture) + Environment.NewLine +
                ": Api_Integracion=" + AppSettings()["URL_INTEGRACION_API"] + Environment.NewLine +
                ": Api_WBC=" + AppSettings()["URL_WBC_API"] + Environment.NewLine);

            try
            {
                if (!AppSettings()["grant_type"].StartsWith("1_"))
                {
                    AddUpdateAppSettings("grant_type",
                        "1_" + Metodos.Seguridad_Cifrar(AppSettings()["grant_type"]).Objeto);
                }
                if (!AppSettings()["username"].StartsWith("1_"))
                {
                    AddUpdateAppSettings("username",
                        "1_" + Metodos.Seguridad_Cifrar(AppSettings()["username"]).Objeto);
                }
                if (!AppSettings()["password"].StartsWith("1_"))
                {
                    AddUpdateAppSettings("password",
                        "1_" + Metodos.Seguridad_Cifrar(AppSettings()["password"]).Objeto);
                }
                
                ObtenerToken();
                //Ejecutar inmediatamente
                //_taskInventario=InventoryCtrl.ProcesaInventariosIntegraWbcAllAsync(_resultToken);
                EnviarInventariosAsync();
                EnviarClientesAsync();
                //EnviarPedidosAsync();
                //iniciar timers
                IniciaTimerInventarios();
                IniciaTimerClientes();
                //IniciaTimerPedidos();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                 DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_SRV_WBC_Service.OnStart: " + ex.Message +
                 ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        protected override void OnStop()
        {
            try
            {
                StopTimerInventarios();
                StopTimerClientes();
                //StopTimerPedidos();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                 DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_SRV_WBC_Service.OnStop: " + ex.Message +
                 ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        private static void AddUpdateAppSettings(string key, string value)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {
                    settings[key].Value = value;
                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException ex)
            {
                Console.WriteLine(ex.Message);
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                 DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_SRV_WBC_Service.AddUpdateAppSettings: " + ex.Message +
                 ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        private static NameValueCollection AppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        private void ObtenerToken()
        {
            try
            {
                _resultToken = TokenCtrl.GetToken(Metodos.Seguridad_Descifrar(AppSettings()["grant_type"]).Objeto,
                    Metodos.Seguridad_Descifrar(AppSettings()["username"]).Objeto,
                    Metodos.Seguridad_Descifrar(AppSettings()["password"]).Objeto);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                 DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_SRV_WBC_Service.ObtenerToken: " + ex.Message +
                 ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        #region TimerPedidos

        protected void IniciaTimerPedidos()
        {
            try
            {
                _tmrPedidos = new Timer { Interval = int.Parse(AppSettings()["PedidosTimer"]) };
                _tmrPedidos.Elapsed += OnTimerPedidos;
                _tmrPedidos.Start();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                 DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_SRV_WBC_Service.IniciaTimerPedidos: " + ex.Message +
                 ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        protected void StopTimerPedidos()
        {
            _tmrPedidos.Stop();
            _tmrPedidos = null;
        }

        protected void OnTimerPedidos(object sender, ElapsedEventArgs args)
        {
            try
            {
                if (_resultToken.Objeto == null)
                {
                    ObtenerToken();
                }
                else if (DateTime.Compare(DateTime.Now, DateTime.Parse(_resultToken.Objeto.Expires)) > 0)
                {
                    ObtenerToken();
                }
                EnviarPedidosAsync();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                  DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_SRV_WBC_Service.OnTimerPedidos: " + ex.Message +
                  ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        private async void EnviarPedidosAsync()
        {
            if (_taskPedidos != null)
            {
                if (_taskPedidos.IsCompleted)
                {
                    await (
                        _taskPedidos = Task.Run(() =>
                        {
                            try
                            {
                                //var result = DeliveryCtrl.ProcesaPedidos(AppSettings()["URL_INTEGRACION_API"],
                                //    AppSettings()["URL_OPE_API"],
                                //    AppSettings()["URL_WBC_API"], _resultToken.Objeto.AccessToken);
                                //if (result.ResultType == ResultTypes.Success)
                                //{
                                //    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                //        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                //        ", Los pedidos fueron enviados con exito",
                                //        true);
                                //}
                            }
                            catch (Exception ex)
                            {
                                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " +
                                    ex.Message.Replace(",", ""),
                                    true);
                            }
                        })
                    );
                }
            }
            else
            {
                await (
                    _taskPedidos = Task.Run(() =>
                    {
                        try
                        {
                            //var result = DeliveryCtrl.ProcesaPedidos(AppSettings()["URL_INTEGRACION_API"],
                            //    AppSettings()["URL_OPE_API"],
                            //    AppSettings()["URL_WBC_API"], _resultToken.Objeto.AccessToken);
                            //if (result.ResultType == ResultTypes.Success)
                            //{
                            //    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                            //        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                            //        ", Los pedidos fueron enviados con exito",
                            //        true);
                            //}
                        }
                        catch (Exception ex)
                        {
                            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                                DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + ex.Message.Replace(",", ""),
                                true);
                        }
                    })
                );
            }
        }

        #endregion

        #region TimerClientes

        protected void IniciaTimerClientes()
        {
            try
            {
                _tmrClientes = new Timer {Interval = int.Parse(AppSettings()["ClientesTimer"]) };
                _tmrClientes.Elapsed += OnTimerClientes;
                _tmrClientes.Start();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                 DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_SRV_WBC_Service.IniciaTimerClientes: " + ex.Message +
                 ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        protected void StopTimerClientes()
        {
            _tmrClientes.Stop();
            _tmrClientes = null;
        }

        protected void OnTimerClientes(object sender, ElapsedEventArgs args)
        {
            try
            {
                if (_resultToken.Objeto == null)
                {
                    ObtenerToken();
                }
                else if (DateTime.Compare(DateTime.Now, DateTime.Parse(_resultToken.Objeto.Expires)) > 0)
                {
                    ObtenerToken();
                }
                EnviarClientesAsync();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_SRV_WBC_Service.OnTimerClientes: " + ex.Message +
                    ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        protected async void EnviarClientesAsync()
        {
            if (_taskClientes != null)
            {
                if (_taskClientes.IsCompleted)
                {
                    await (_taskClientes = Task.Run(() =>
                    {
                        try
                        {
                            CustomerCtrl.CustomerAll(_resultToken);
                        }
                        catch (Exception ex)
                        {
                            LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_ConsultarVentas",
                                DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""),
                                true);
                            Console.WriteLine(ex);
                        }

                    }));
                }
            }
            else
            {
                await (_taskClientes = Task.Run(() =>
                {
                    try
                    {
                        CustomerCtrl.CustomerAll(_resultToken);
                    }
                    catch (Exception ex)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_ConsultarVentas",
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""),
                            true);
                        Console.WriteLine(ex);
                    }

                }));
            }
        }
        #endregion

        #region TimerProductos

        protected void IniciaTimerProductos()
        {
            try
            {
                while (DateTime.Now.Second > 0)
                {
                }
                _tmrProductos = new Timer {Interval = 30000};
                _tmrProductos.Elapsed += OnTimerProductos;
                _tmrProductos.Start();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                  DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_SRV_WBC_Service.IniciaTimerProductos: " + ex.Message +
                  ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        protected void StopTimerProductos()
        {
            _tmrProductos.Stop();
            _tmrProductos = null;
        }

        protected void OnTimerProductos(object sender, ElapsedEventArgs args)
        {
            try
            {
                var timeInicio = TimeSpan.Parse(AppSettings()["ProductosHoraInicioTimer"]);
                var timeFin = TimeSpan.Parse(AppSettings()["ProductosHoraFinTimer"]);

                if (TimeSpan.Parse(DateTime.Now.ToString("HH:mm")).Ticks < timeInicio.Ticks ||
                    TimeSpan.Parse(DateTime.Now.ToString("HH:mm")).Ticks > timeFin.Ticks) return;
                if (_resultToken.Objeto == null)
                {
                    ObtenerToken();
                }
                else if (DateTime.Compare(DateTime.Now, DateTime.Parse(_resultToken.Objeto.Expires)) > 0)
                {
                    ObtenerToken();
                }
                ProductCtrl.ProcesaProductosIntegraWbcAllAsync(_resultToken);
                //EnviaProductosAsync();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                  DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_SRV_WBC_Service.OnTimerProductos: " + ex.Message +
                  ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        //protected async void EnviaProductosAsync()
        //{
        //    if (_taskProductos != null)
        //    {
        //        if (_taskProductos.IsCompleted)
        //        {
        //            await (
        //                _taskProductos = Task.Run(() =>
        //                {
        //                    try
        //                    {
                                
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
        //                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " +
        //                            ex.Message.Replace(",", ""),
        //                            true);
        //                    }
        //                })
        //            );
        //        }
        //    }
        //    else
        //    {
        //        await (
        //            _taskProductos = Task.Run(() =>
        //            {
        //                try
        //                {
        //                    string text;
        //                    ProductCtrl.ProductosAll(_resultToken, out text);
        //                }
        //                catch (Exception ex)
        //                {
        //                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
        //                        DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + ex.Message.Replace(",", ""),
        //                        true);
        //                }
        //            })
        //        );
        //    }
        //}

        #endregion

        #region TimerVentas

        protected void IniciaTimerVentas()
        {
            try
            {
                _tmrVentas = new Timer {Interval = int.Parse(AppSettings()["VentasTimer"])};
                _tmrVentas.Elapsed += OnTimerVentas;
                _tmrVentas.Start();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                  DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_SRV_WBC_Service.IniciaTimerVentas: " + ex.Message +
                  ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        protected void StopTimerVentas()
        {
            _tmrVentas.Stop();
            _tmrVentas = null;
        }

        protected void OnTimerVentas(object sender, ElapsedEventArgs args)
        {
            try
            {
                EnviarVentasAsync();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                   DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_SRV_WBC_Service.OnTimerVentas: " + ex.Message +
                   ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        protected async void EnviarVentasAsync()
        {
            await Task.Run(() =>
            {
                try
                {
                    VentasCtrl.EnviarVentasAll(97, 451,"");
                }
                catch (Exception ex)
                {
                    LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_ConsultarVentas",
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""), true);
                    Console.WriteLine(ex);
                }

            });
        }

        #endregion

        #region TimerInventarios

        protected void IniciaTimerInventarios()
        {
            try
            {
                _tmrInventario = new Timer {Interval = int.Parse(AppSettings()["InventariosTimer"])};
                _tmrInventario.Elapsed += OnTimerInventarios;
                _tmrInventario.Start();
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                 DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_SRV_WBC_Service.IniciaTimerInventarios: " + ex.Message +
                 ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        protected void StopTimerInventarios()
        {
            _tmrInventario.Stop();
            _tmrInventario = null;
        }

        protected void OnTimerInventarios(object sender, ElapsedEventArgs args)
        {
            try
            {
                if (_resultToken.Objeto == null)
                {
                    ObtenerToken();
                }
                else if (DateTime.Compare(DateTime.Now, DateTime.Parse(_resultToken.Objeto.Expires)) > 0)
                {
                    ObtenerToken();
                }
                EnviarInventariosAsync();
                //_taskInventario=InventoryCtrl.ProcesaInventariosIntegraWbcAllAsync(_resultToken);
            }
            catch (Exception ex)
            {
                LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"],
                 DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Error_SRV_WBC_Service.OnTimerInventarios: " + ex.Message +
                 ".\n" + ex.StackTrace + ".\n", true);
            }
        }

        protected async void EnviarInventariosAsync()
        {
            if (_taskInventarios == null || _taskInventarios.IsCompleted)
            {

                if (_taskInventarios != null)
                {
                    LogServicioBr.EscribirLog(AppSettings()["InventoryLog"],
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Termino proceso de inventarios Integra-Wbc", true); // *** Monitor Customer Log
                    LogServicioBr.EscribirLog("Hebras",
                        DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                        ": Finaliza Hebra principal de Inventarios.", false);
                }

                LogServicioBr.EscribirLog("Hebras",
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Inicia Hebra principal de Inventarios.", false);

                await (_taskInventarios = Task.Run(() =>
                {
                    try
                    {
                        InventoryCtrl.ProcesaInventariosIntegraWbcAllAsync(_resultToken);
                    }
                    catch (Exception ex)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_ConsultarVentas",
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""),
                            true);
                        Console.WriteLine(ex);
                    }

                }));
            }

            /*
            if (_taskInventarios != null)
            {
                if (!_taskInventarios.IsCompleted) return;
                LogServicioBr.EscribirLog(AppSettings()["InventoryLog"],
                    DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                    ": Termino proceso de inventarios Integra-Wbc", true);  // *** Monitor Customer Log
                await (_taskInventarios = Task.Run(() =>
                {
                    try
                    {
                        InventoryCtrl.ProcesaInventariosIntegraWbcAllAsync(_resultToken);
                    }
                    catch (Exception ex)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_ConsultarVentas",
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""),
                            true);
                        Console.WriteLine(ex);
                    }

                }));
            }
            else
            {
                await (_taskInventarios = Task.Run(() =>
                {
                    try
                    {
                        InventoryCtrl.ProcesaInventariosIntegraWbcAllAsync(_resultToken);
                    }
                    catch (Exception ex)
                    {
                        LogServicioBr.EscribirLog(AppSettings()["ProyectoLog"] + "_ConsultarVentas",
                            DateTime.Now.ToString(CultureInfo.InvariantCulture) + "," + ex.Message.Replace(",", ""),
                            true);
                        Console.WriteLine(ex);
                    }
                }));
            }
            */
        }
        #endregion
    }
}