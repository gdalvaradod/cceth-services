﻿
namespace API_Integraciones.Enums
{
    public enum ProcessStatus
    {
        PROCESO = 1,
        CORRECTO = 2,
        FALLO = 3
    }
}