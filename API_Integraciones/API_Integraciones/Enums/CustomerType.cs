﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Enums
{
    public static class CustomerType
    {
        public const string CLIENTE_COCACOLA = "CLIENTE_COCACOLA";
        public const string CLIENTE_PORTAL = "CLIENTE_PORTAL";
    }
}