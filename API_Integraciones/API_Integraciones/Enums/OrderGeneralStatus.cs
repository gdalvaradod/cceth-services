﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones
{ 
    public static class OrderGeneralStatus
    {
        public const string PENDIENTE = "PENDIENTE";
        public const string PREPARADO = "PREPARADO";
        public const string CONFIRMADO = "CONFIRMADO";
        public const string VALIDADO = "VALIDADO";
        public const string ENVIADO = "ENVIADO";
        public const string COMPLETADO = "COMPLETADO";
        public const string CANCELADO = "CANCELADO";
    }
}