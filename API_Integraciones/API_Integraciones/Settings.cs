﻿using API_Integraciones.Models.WBC;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace API_Integraciones
{
    public static class Settings
    {
        public static string Api_OpeCd()
        {
            return ConfigurationManager.AppSettings["API_OpeCd"];
        }

        public static string Api_Wbc()
        {
            return ConfigurationManager.AppSettings["API_WBC"];
        }

        public static Credentials AuthCredentials()
        {
            var credentials = new Credentials
            {
                Grant_Type = ConfigurationManager.AppSettings["Wbc_AuthGrant_Type"],
                Username = ConfigurationManager.AppSettings["Wbc_AuthUsername"],
                Password = ConfigurationManager.AppSettings["Wbc_AuthPassword"]
            };

            return credentials;
        }

        public static int RazonDevolucionAux()
        {
            return Convert.ToInt32(ConfigurationManager.AppSettings["RazonDevolucionAux"]);
        }

        public static DateTime FechaHoraActual()
        {
            string nzTimeZoneKey = "Central Standard Time";
            TimeZoneInfo nzTimeZone = TimeZoneInfo.FindSystemTimeZoneById(nzTimeZoneKey);
            return TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, nzTimeZone);

        }

        public static int RequestTimeOut()
        {
            return Convert.ToInt32(ConfigurationManager.AppSettings["RequestTimeOut"]);
        }
    }
}