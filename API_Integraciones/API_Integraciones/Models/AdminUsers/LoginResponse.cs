﻿using API_Integraciones.Models.AdminPedidos;
using API_Integraciones.Models.AdminUsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models
{
    public class LoginResponse
    {
        public User User { get; set; }
        public Cedis Cedis{ get; set; }
        public bool Access { get; set; }
        public string Message { get; set; }
    }
}