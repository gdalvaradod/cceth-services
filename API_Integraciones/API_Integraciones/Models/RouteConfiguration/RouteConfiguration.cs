﻿using API_Integraciones.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.RouteConfiguration
{
    public class RouteConfiguration
    {
        public int Id { get; set; }
        public int RouteId { get; set; }
        public int CedisId { get; set; }
        public int ZipCode { get; set; }
        public string Neigborhood { get; set; }
        public List<WeekDay> DeliveryDays { get; set; }

        public RouteConfiguration()
        {
            DeliveryDays = new List<WeekDay>();
        }
    }
}