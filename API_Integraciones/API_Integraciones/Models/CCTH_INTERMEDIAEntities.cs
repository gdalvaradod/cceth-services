﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.Entity
{
    public partial class CCTH_INTERMEDIAEntities
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {       
            modelBuilder.Entity<DET_Promotions>()
                .HasRequired(detProm => detProm.ENC_Sales)
                .WithMany()
                .HasForeignKey(detProm => detProm.DEP_SaleId);
            modelBuilder.Entity<DET_Sales>()
                .HasRequired(detSale => detSale.ENC_Sales)
                .WithMany()
                .HasForeignKey(detSale => detSale.DSS_SaleId);
        }

    }
}