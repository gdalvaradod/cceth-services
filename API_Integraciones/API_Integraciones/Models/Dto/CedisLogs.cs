﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API_Integraciones.Models.Intermedia;

namespace API_Integraciones.Models.Dto
{
    public class CedisLogs
    {
        public int CedisId { get; set; }
        public List<Log> Logs { get; set; }

        public CedisLogs()
        {
            Logs = new List<Log>();
        }
    }
}