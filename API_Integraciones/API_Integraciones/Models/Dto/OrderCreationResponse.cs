﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.Dto
{
    public class OrderCreationResponse
    {
        public int Entity_Id { get; set; }
        public string Message { get; set; }
    }
}