﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.OpeCD
{
    public class OpeCDGET
    {
        public string ItemId { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public bool Active { get; set; }
    }
}