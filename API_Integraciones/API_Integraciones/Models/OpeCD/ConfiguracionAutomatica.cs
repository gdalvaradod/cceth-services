﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.OpeCD
{
    public class ConfiguracionAutomatica
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Sistema { get; set; }
        public string Hora { get; set; }
        public int Cedis { get; set; }
        public int Ruta { get; set; }
        public System.DateTime Modificado { get; set; }
        public Nullable<int> Status { get; set; }
    }
}