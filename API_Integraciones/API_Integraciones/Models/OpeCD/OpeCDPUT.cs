﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.OpeCD
{
    public class OpeCDPUT
    {
        public string Id { get; set; }

        public int Trip { get; set; }
        
        public int DeliveryOrderId { get; set; }

        public List<Product> Products { get; set; }
        
        public ErrorItemList ErrorItemList { get; set; }
    }
}