﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.OpeCD
{
    public class ErrorItemList
    {
        [JsonProperty("ErrorNum")]
        public int ErrorNum { get; set; }

        [JsonProperty("ErrorType")]
        public int ErrorType { get; set; }

        [JsonProperty("ErrorDescription")]
        public string ErrorDescription { get; set; }
    }
}