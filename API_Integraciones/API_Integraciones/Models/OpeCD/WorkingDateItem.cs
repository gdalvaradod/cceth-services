﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.OpeCD
{
    public class WorkingDateItem
    {
        public DateTime Date { get; set; }
        public bool IsWorkingDay { get; set; }
    }
}