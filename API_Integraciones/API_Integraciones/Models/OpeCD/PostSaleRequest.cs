﻿using API_Integraciones.Models.OpeCD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.WBC
{
    public class PostSaleRequest
    {
        public int CustomerOpeId { get; set; }
        public DateTime DeliveryDate { get; set; }
        public long DeliveryOrderId { get; set; }
        public int RouteCode { get; set; }
        public short Trip { get; set; }
        public List<SaleItem> SaleDetail { get; set; }
        public List<SalePromotion> SalePromotions { get; set; }
        public string Code { get; set; }
        public PostSaleRequest()
        {
            SaleDetail = new List<SaleItem>();
            SalePromotions = new List<SalePromotion>();
        }
    }
}