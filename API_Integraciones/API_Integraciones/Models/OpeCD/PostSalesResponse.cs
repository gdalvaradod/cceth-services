﻿using API_Integraciones.Models.WBC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.OpeCD
{
    public class PostSalesResponse
    {
        public PostSaleRequest Sale { get; set; }
        public List<string> ErrorDescriptions { get; set; }
        public bool Success { get; set; }

        public PostSalesResponse()
        {
            ErrorDescriptions = new List<string>();
        }
    }
}