﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.OpeCD
{
    public struct SaleItem
    {
        public string ItemId { get; set; }
        public int Qty { get; set; }
    }
}