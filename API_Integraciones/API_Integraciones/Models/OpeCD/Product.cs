﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.OpeCD
{
    public class Product
    {
        public string Code { get; set; }
        public int Quantity { get; set; }
        public int Type { get; set; }
    }
}