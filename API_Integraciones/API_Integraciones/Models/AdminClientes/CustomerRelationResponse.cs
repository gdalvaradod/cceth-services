﻿namespace API_Integraciones.Models.AdminClientes
{
    public class CustomerRelationResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Tel { get; set; }
        public int? Cuc { get; set; }
        public string CrmId { get; set; }
        public string BarCode { get; set; }
        public string CustomerType { get; set; }

    }
}