﻿namespace API_Integraciones.Models.AdminClientes
{
    public class CustomerRelation
    {
        public string Barcode { get; set; }
        public string CrmId { get; set; }
        public int Cuc { get; set; }
    }
}