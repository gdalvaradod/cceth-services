﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.WBC
{
    public class Route
    {
        public int RouteId { get; set; }
        public int BranchId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string CreatedOn { get; set; }
        public string ModifiedOn { get; set; }
        public bool Active { get; set; }
    }
}