﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.WBC
{
    public class Site
    {
        public int SiteId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
    }
}