﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.WBC
{
    public class CustomerVisits
    {
        public int customerId { get; set; }
        public int routeId { get; set; }
        public int day { get; set; }
        public int visitType { get; set; }
        public int order { get; set; }
        public string createdOn { get; set; }
        public string modifiedOn { get; set; }
        public bool active { get; set; } = true;
    }
}