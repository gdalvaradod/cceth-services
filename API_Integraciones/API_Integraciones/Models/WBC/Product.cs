﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.WBC
{
    public class SaleProduct
    {
        public int Product { get; set; }
        public int Quantity { get; set; }
        public int CreditAmount { get; set; }
        public decimal Price { get; set; }
        public decimal BasePrice { get; set; }
        public decimal Discount { get; set; }
        public decimal DiscountRate { get; set; }
    }
}