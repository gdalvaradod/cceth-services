﻿using API_Integraciones.Models.CRM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.WBC
{
    public class Customer
    {
        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string WBCId { get; set; }
        public string CRMId { get; set; }
        public string Name { get; set; }
        public string Cuc { get; set; }
        public string BarCode { get; set; }
        public long PriceList { get; set; }
        public int Sequence { get; set; }
        public string DistributionCenter { get; set; }
        public string RouteCode { get; set; }
        public string Manager { get; set; }
        public string PhysicalAddress { get; set; }
        public string Street { get; set; }
        public string IndoorNumber { get; set; }
        public string OutdoorNumber { get; set; }
        public string Intersection1 { get; set; }
        public string Intersection2 { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Neighborhood { get; set; }
        public bool ClientType { get; set; }
        public Visit Visit { get; set; }
        public bool CustomerVarious { get; set; }
        public string CustomerType { get; set; }
        public string Email { get; set; }
        public string PostCode { get; set; }
        public string Telephone { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CreatedOn { get; set; }
        public string ModifiedOn { get; set; }
        public string Contact { get; set; }
        public string Address => PhysicalAddress;
        public string Description { get; set; }
        public string Code => BarCode;
        public bool Active { get; set; }
        public string CRMCreateOn { get; set; }
    }
}