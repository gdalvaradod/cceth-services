﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.WBC
{
    public class ClosedJourney
    {
        public int CedisIdOpecd { get; set; }
        public int Route { get; set; }
        public DateTime DeliveryDate { get; set; }
    }
}