﻿namespace API_Integraciones.Models.WBC
{
    public class ItemQty
    {
        public string ItemId { get; set; }
        public int Qty { get; set; }
        public int WbcId { get; set; }
    }
}