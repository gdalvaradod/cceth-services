﻿using API_Integraciones.Models.AdminPedidos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.WBC
{
    public class Cedisinfo
    {
        public int cedi { get; set; }
        public List<Destinos> Destinos { get; set; }
    }
}