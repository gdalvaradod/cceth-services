﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.WBC
{
    public class RouteGetFilter
    {
        public int routeId { get; set; }
        public int branchId { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string createdOn { get; set; }
        public string modifiedOn { get; set; }
        public bool active { get; set; }
    }
}