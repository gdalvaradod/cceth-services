﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.WBC
{
    public class Credentials
    {
        public string Grant_Type { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}