﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.WBC
{
    public class Branch
    {
        [JsonProperty("companyId")]
        public int companyId { get; set; }

        [JsonProperty("branchCode")]
        public string branchCode { get; set; }

        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("SiteId")]
        public int siteId { get; set; }

        [JsonProperty("taxPercent")]
        public int taxPercent { get; set; }
    }
}