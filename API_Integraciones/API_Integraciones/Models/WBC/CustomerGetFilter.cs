﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.WBC
{
    public class CustomerGetFilter
    {
        public int customerId { get; set; }
        public string contact { get; set; }
        public string address { get; set; }
        public string description { get; set; }
        public string email { get; set; }
        public Double latitude { get; set; }
        public Double longitude { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string createdOn { get; set; }
        public string modifiedOn { get; set; }
        public bool active { get; set; }
    }
}