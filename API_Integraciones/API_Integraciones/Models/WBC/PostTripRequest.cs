﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.WBC
{
    public class PostTripRequest
    {
        [Required]
        public int CedisId { get; set; }
        [Required]
        public List<Trip> TripList { get; set; }
    }
}