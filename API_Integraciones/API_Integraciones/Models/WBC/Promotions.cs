﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.WBC
{
    public class SalePromotion
    {
        public int Product { get; set; }
        public int Quantity { get; set; }
        public int Type { get; set; }
    }
}