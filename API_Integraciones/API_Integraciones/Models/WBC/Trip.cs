﻿using System;
using System.Collections.Generic;

namespace API_Integraciones.Models.WBC
{
    public class Trip
    {
        public List<ItemQty> ConfirmedItems { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int Id { get; set; }
        public int Route { get; set; }

        public Trip()
        {
            ConfirmedItems = new List<ItemQty>();
        }
    }
}