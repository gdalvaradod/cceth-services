﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.WBC
{
    public class BranchGetFilter
    {
        public int branchId { get; set; }
        public int companyId { get; set; }
        public int siteId { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public int taxPercent { get; set; }
        public string createdon { get; set; }
        public string modifiedon { get; set; }
        public bool active { get; set; }
    }
}