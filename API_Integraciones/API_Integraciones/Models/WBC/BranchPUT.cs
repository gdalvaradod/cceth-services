﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.WBC
{
    public class BranchPUT
    {
        [JsonProperty("companyId")]
        public int companyId { get; set; }

        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("taxPercent")]
        public int taxPercent { get; set; }
    }
}