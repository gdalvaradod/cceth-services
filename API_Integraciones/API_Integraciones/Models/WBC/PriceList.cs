﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.WBC
{
    public class PriceList
    {
        public int priceListId { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public int branchId { get; set; }
        public DateTime created { get; set; }
        public DateTime modified { get; set; }
        public bool active { get; set; }
        public string validityStart { get; set; }
        public string validityEnd { get; set; }
        public bool master { get; set; }
    }
}