﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.AdminPedidos
{
    public class PedidosDet
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("cliente")]
        public OrderAddresses Cliente { get; set; }

        [JsonProperty("productos")]
        public List<OrderItem> Producto { get; set; }

        public bool CanBeSetToPrepared { get; set; }
    }
}