﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.AdminPedidos
{
    public class CedisPUT
    {
        public int CedisIdOpeCd { get; set; }
        public string CedisNameCCHT { get; set; }
        public string Nombre { get; set; }
        public Nullable<int> Enable { get; set; }
    }
}