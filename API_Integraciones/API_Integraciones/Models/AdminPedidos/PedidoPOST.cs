﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.AdminPedidos
{
    public class PedidoPOST
    {
        [JsonProperty("delivery_date")]
        public string DELIVERY_DATE { get; set; }

        [JsonProperty("route_code")]
        public string ROUTE_CODE { get; set; }

        [JsonProperty("customer_id")]
        public string cliente { get; set; }

        [JsonProperty("cedis_id")]
        public int cedis { get; set; }

        [JsonProperty("payment_method")]
        public string PaymentMethod { get; set; }

        [JsonProperty("WantBill")]
        public short WantBill { get; set; }

        [JsonProperty("items")]
        public List<OrderItem> Productos { get; set; }
    }
}