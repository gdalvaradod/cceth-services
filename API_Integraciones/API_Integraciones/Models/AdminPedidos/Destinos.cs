﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.AdminPedidos
{
    public class Destinos
    {
        public int Id { get; set; }
        public string RouteCodeCCHT { get; set; }
        public int RouteCodeOpecd { get; set; }
        public string Nombre { get; set; }
        public Nullable<int> Manager { get; set; }
        public Nullable<int> Cuc { get; set; }
        public int CucCCTH { get; set; }
        public int Cedis { get; set; }
        public Nullable<int> Enable { get; set; }
    }
}