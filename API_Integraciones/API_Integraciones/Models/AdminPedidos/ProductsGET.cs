﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.AdminPedidos
{
    public class ProductsGET
    {
        public int Id { get; set; }
        [Required]
        public string CodigoProducto { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        public int Ruta { get; set; }
        [Required]
        public int Cedis { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> WBCId { get; set; }
        public Nullable<int> Tipo { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }
}