﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.AdminPedidos
{
    public class InventoryDet
    {
        public int Id { get; set; }
        public int TripId { get; set; }
        public string ItemId { get; set; }
        public string Name { get; set; }
        public int Qty { get; set; }
        public int WbcId { get; set; }
    }
}