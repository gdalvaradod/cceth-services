﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.AdminPedidos
{
    public class Cedis
    {
        public int Id { get; set; }
        public string CedisNameCCHT { get; set; }
        public int CedisIdOpecd { get; set; }
        public string Nombre { get; set; }
        public int SiteId { get; set; }
        public Nullable<int> Enable { get; set; }
    }
}