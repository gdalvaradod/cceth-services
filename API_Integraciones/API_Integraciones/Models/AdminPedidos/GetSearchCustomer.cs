﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.AdminPedidos {
    public class GetSearchCustomer {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Tel { get; set; }
        public int Cuc { get; set; }
        public string CrmId { get; set; }
        public string DeliveryDate{ get; set; }
        public string BarCode { get; set; }
        public bool WantBill { get; set; }
    }
}