﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.AdminPedidos
{
    public class CustomerPUT
    {
        [JsonProperty("CustomerId")]
        public int Id { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("routeCode")]
        public string RutaCode { get; set; }

        [JsonProperty("cuc")]
        public int? Cuc { get; set; }

    }
}