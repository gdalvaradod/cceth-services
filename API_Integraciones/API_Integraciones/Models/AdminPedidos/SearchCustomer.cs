﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.AdminPedidos {
    public class SearchCustomer {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Tel { get; set; }
        public string BarCode { get; set; }
        public int Cuc { get; set; }
        public int Route { get; set; }
    }
}