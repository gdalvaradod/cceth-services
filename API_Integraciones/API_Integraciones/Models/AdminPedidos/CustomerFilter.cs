﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.AdminPedidos
{
    public class CustomerFilter
    {
        public string Name { get; set; }
        public string BarCode { get; set; }
        public string AddressType { get; set; }
        public int Cuc { get;  set; }
    }
}