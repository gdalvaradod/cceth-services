﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.AdminPedidos
{
    public class InventoryEnc
    {
        public int Id { get; set; }
        public int TripId { get; set; }
        public int Route { get; set; }
        public Nullable<System.DateTime> DeliveryDate { get; set; }
        public int CedisIdOpeCd { get; set; }
    }
}