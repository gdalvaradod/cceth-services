﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.AdminPedidos
{
    public class PedidosEnc
    {
        [JsonProperty("OPECId")]
        public Nullable<long> ORD_OPECDId { get; set; }

        [JsonProperty("delivery_date")]
        public string DELIVERY_DATE { get; set; }

        [JsonProperty("route_code")]
        public string ROUTE_CODE { get; set; }

        [JsonProperty("viaje")]
        public int Trip { get; set; }

        [JsonProperty("statusOrden")]
        public string ORD_StatusOrden { get; set; }

        [JsonProperty("createFrom")]
        public string ORD_CreateFrom { get; set; }

        public int CedisId { get; set; }
    }
}