﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.AdminPedidos
{
    public class ProductsPOST
    {
        public int Cedi { get; set; }

        [JsonProperty("Codigo")]
        public List<string> Code { get; set; }
        public List<string> Ruta { get; set; }
    }
}