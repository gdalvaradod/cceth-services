﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.AdminConfig
{
    public class Colonia
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string OpeId { get; set; }
        public string CodeCCTH { get; set; }
        public string CodeCRM { get; set; }
    }
}