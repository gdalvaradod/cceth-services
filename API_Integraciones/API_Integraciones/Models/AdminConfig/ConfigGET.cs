﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.AdminConfig
{
    public class ConfigGET
    {
        public List<Estado> Estados { get; set; }
        public List<Municipio> Municipios { get; set; }
        public List<Pais> Paises { get; set; }
        public List<Colonia> Colonias { get; set; }
    }
}