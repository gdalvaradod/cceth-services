﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.Intermedia
{
    public class VisitDays
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }
        public bool Sunday { get; set; }

        public int NumericFormat
        {
            get
            {
                int _NumericFormat = 0;
                if (Sunday) _NumericFormat |= 1;
                if (Monday) _NumericFormat |= 2;
                if (Tuesday) _NumericFormat |= 4;
                if (Wednesday) _NumericFormat |= 8;
                if (Thursday) _NumericFormat |= 16;
                if (Friday) _NumericFormat |= 32;
                if (Saturday) _NumericFormat |= 64;
                return _NumericFormat;
            }
            set
            {
                Sunday = (value & 1) > 0;
                Monday = (value & 2) > 0;
                Tuesday = (value & 4) > 0;
                Wednesday = (value & 8) > 0;
                Thursday = (value & 16) > 0;
                Friday = (value & 32) > 0;
                Saturday = (value & 64) > 0;
            }
        }

        public int this[int index] => NumericFormat >> index;


        public DateTime ProximaVisita(DateTime fecha)
        {
            int diasem = (int)fecha.DayOfWeek;
            for (int x = diasem + 1; x < diasem + 8; x++)
            {
                if ((this[x % 7] & 1) == 1)
                {
                    return fecha.Add(new TimeSpan(x - diasem, 0, 0, 0));
                }
            }
            return fecha;
        }
    }
}