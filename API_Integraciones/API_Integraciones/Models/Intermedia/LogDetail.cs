﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.Intermedia
{
    public class LogDetail
    {
        public int Id { get; set; }
        public int LogId { get; set; }
        public Process Process { get; set; }
        public string ErrorDescription { get; set; }
        public bool Success { get; set; }
    }
}