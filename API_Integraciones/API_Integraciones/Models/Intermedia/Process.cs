﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.Intermedia
{
    public class Process
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}