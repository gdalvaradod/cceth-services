//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace API_Integraciones.Models.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class CONF_Destinos
    {
        public int Id { get; set; }
        public string COD_RouteCodeCCHT { get; set; }
        public int COD_RouteCodeOpecd { get; set; }
        public string COD_Nombre { get; set; }
        public Nullable<int> COD_Manager { get; set; }
        public Nullable<int> COD_Cuc { get; set; }
        public int COD_CedisRefId { get; set; }
        public Nullable<int> COD_Enable { get; set; }
        public int COD_CucCCTH { get; set; }
    
        public virtual REF_Cedis REF_Cedis { get; set; }
    }
}
