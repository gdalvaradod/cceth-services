//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace API_Integraciones.Models.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class ENC_Log
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ENC_Log()
        {
            this.DET_Log = new HashSet<DET_Log>();
        }
    
        public int ELO_Id { get; set; }
        public Nullable<System.DateTime> ELO_DeliveryDate { get; set; }
        public Nullable<int> ELO_CedisId { get; set; }
        public Nullable<int> ELO_RouteId { get; set; }
        public Nullable<int> ELO_Trip { get; set; }
        public Nullable<int> ELO_ProcessId { get; set; }
        public Nullable<int> ELO_ProcessStatus { get; set; }
        public System.DateTime ELO_CreatedOn { get; set; }
        public System.DateTime ELO_ModifiedOn { get; set; }
    
        public virtual CAT_Process CAT_Process { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DET_Log> DET_Log { get; set; }
    }
}
