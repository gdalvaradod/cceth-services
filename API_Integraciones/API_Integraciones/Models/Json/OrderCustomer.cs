﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Models.Json
{
    public class OrderCustomer
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("firstname")]
        public string Firstname { get; set; }

        [JsonProperty("lastname")]
        public string Lastname { get; set; }

        [JsonProperty("postcode")]
        public string Postcode { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("telephone")]
        public string Telephone { get; set; }

        [JsonProperty("addressType")]
        public string AddressType { get; set; }

        [JsonProperty("countryId")]
        public string CountryId { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("statusConfirmacion")]
        public Nullable<int> StatusConfirmacion { get; set; }

        [JsonProperty("CRMId")]
        public string CRMId { get; set; }

        [JsonProperty("routeCode")]
        public string RouteCode { get; set; }

        [JsonProperty("cuc")]
        public Nullable<int> Cuc { get; set; }
    }
}