﻿using API_Integraciones.Models.AdminPedidos;
using API_Integraciones.Models.OpeCD;
using API_Integraciones.Models.WBC;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace API_Integraciones.Controllers.APIs
{
    public class APIOpeCD
    {
        public List<OpeCDGET> GetProducts(int cedisId)
        {
            var client = new RestClient(Settings.Api_OpeCd());
            var uri = string.Format("/api/products?cedisId={0}", cedisId);
            var request = new RestRequest(uri, Method.GET);
            request.Timeout = Settings.RequestTimeOut();
            var response = client.Execute<List<OpeCDGET>>(request);
            return response.Data;

        }

        public List<PostSalesResponse> PostSales(List<PostSaleRequest> Sales, int cedisId)
        {
            var client = new RestClient(Settings.Api_OpeCd());
            var uri = string.Format("/api/sales?cedisId={0}", cedisId);
            var request = new RestRequest(uri, Method.POST);
            request.Timeout = Settings.RequestTimeOut();
            request.RequestFormat = DataFormat.Json;
            request.AddBody(Sales);
            var response = client.Execute<List<PostSalesResponse>>(request);
            return response.Data;
        }

        public string GetPayDesk(int cedisId)
        {
            var client = new RestClient(Settings.Api_OpeCd());
            var uri = string.Format("/api/PayDesk/Date?cedisId={0}", cedisId);
            var request = new RestRequest(uri, Method.GET);
            request.Timeout = Settings.RequestTimeOut();
            var response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                return response.Content.ToString();
            if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                return null;
            return null;
        }

        public List<WorkingDateItem> GetWorkingDays(int cedisId)
        {
            var client = new RestClient(Settings.Api_OpeCd());
            var uri = string.Format("/api/workingDays?cedisId={0}", cedisId);
            var request = new RestRequest(uri, Method.GET);
            request.Timeout = Settings.RequestTimeOut();
            var response = client.Execute<List<WorkingDateItem>>(request);
            if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                throw new Exception(response.Content);
            return response.Data;
        }

    }
}
