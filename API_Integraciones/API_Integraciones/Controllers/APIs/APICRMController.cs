﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using API_Integraciones.Models.CRM;
using API_Integraciones.Models.Dto;
using RestSharp;


namespace API_Integraciones.Controllers.APIs
{
    public class APICRMController
    {
        public BaseResponse PostCustomer(Customer customer)
        {
            var restClient = new RestClient(ConfigurationManager.AppSettings["API_CRM"]);
            var request = new RestRequest("api/Customer", Method.POST);
            request.Timeout = Settings.RequestTimeOut();
            request.RequestFormat = DataFormat.Json;
            request.AddBody(customer);
            var response = restClient.Execute(request);
            var baseResponse = new BaseResponse();
            baseResponse.Success = response.StatusCode == System.Net.HttpStatusCode.Created;
            baseResponse.Message = response.Content;
            return baseResponse;
        }
        public string GetCustomer(string barCode)
        {
            var client = new RestClient(ConfigurationManager.AppSettings["API_CRM"]);
            var request = new RestRequest("api/Customer?barcode=" + barCode, Method.GET);
            request.Timeout = Settings.RequestTimeOut();
            var response = client.Execute<List<string>>(request);
            if (response.StatusCode != HttpStatusCode.OK)
                return string.Empty;
            return response.Data.Any() ? response.Data.First() : string.Empty;
        }

        public BaseResponse PutCustomer(Customer customer)
        {
            var restClient = new RestClient(ConfigurationManager.AppSettings["API_CRM"]);
            var request = new RestRequest("api/Customer", Method.PUT);
            request.Timeout = Settings.RequestTimeOut();
            request.RequestFormat = DataFormat.Json;
            request.AddBody(customer);
            var response = restClient.Execute(request);
            var baseResponse = new BaseResponse();
            baseResponse.Success = response.StatusCode != System.Net.HttpStatusCode.InternalServerError;
            baseResponse.Message = response.Content;
            return baseResponse;
        }
    }
}
