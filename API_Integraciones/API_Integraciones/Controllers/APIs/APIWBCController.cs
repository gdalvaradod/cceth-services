﻿using API_Integraciones.Models.AdminPedidos;
using API_Integraciones.Models.WBC;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using API_Integraciones.Models.Dto;

namespace API_Integraciones.Controllers.APIs
{
    public class APIWBCController
    {
        public string POST(Cedis cedis, Token authToken)
        {
            string resp = "";
            string json = Json(Map(cedis));
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[1] + "Branches");
            webrequest.Method = "POST";
            webrequest.KeepAlive = false;
            webrequest.Headers.Add("Authorization", string.Format("Bearer {0}", authToken.Access_Token));
            webrequest.Timeout = Settings.RequestTimeOut();
            webrequest.ContentType = "application/json";

            byte[] byteArray = Encoding.UTF8.GetBytes(json);
            webrequest.ContentLength = byteArray.Length;

            using (var streamWriter = new StreamWriter(webrequest.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 201)
                    {
                        resp = responsePr.StatusDescription;
                    }
                    else
                    {
                        resp = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                resp = e.Message;
            }
            return resp;
        }
        public string Post(Route ruta, Token authToken)
        {
            string resp = "";
            string json = JObject.FromObject(ruta).ToString();
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[1] + "Routes");
            webrequest.Method = "POST";
            webrequest.KeepAlive = false;
            webrequest.Headers.Add("Authorization", string.Format("Bearer {0}", authToken.Access_Token));
            webrequest.Timeout = Settings.RequestTimeOut();
            webrequest.ContentType = "application/json";

            byte[] byteArray = Encoding.UTF8.GetBytes(json);
            webrequest.ContentLength = byteArray.Length;

            using (var streamWriter = new StreamWriter(webrequest.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 201)
                    {
                        resp = responsePr.StatusDescription;
                    }
                    else
                    {
                        resp = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                resp = e.Message;
            }
            return resp;
        }
        public string Put(Route ruta, Token authToken)
        {
            string resp = "";
            string json = JObject.FromObject(ruta).ToString();
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[1] + "Routes/"+ruta.RouteId);
            webrequest.Method = "PUT";
            webrequest.KeepAlive = false;
            webrequest.Headers.Add("Authorization", string.Format("Bearer {0}", authToken.Access_Token));
            webrequest.Timeout = Settings.RequestTimeOut();
            webrequest.ContentType = "application/json";

            byte[] byteArray = Encoding.UTF8.GetBytes(json);
            webrequest.ContentLength = byteArray.Length;

            using (var streamWriter = new StreamWriter(webrequest.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 204)
                    {
                        resp = responsePr.StatusDescription;
                    }
                    else
                    {
                        resp = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                resp = e.Message;
            }
            return resp;
        }
        public string Put(int branchId, CedisPUT cedis, Token authToken)
        {
            string resp = "";
            try
            {
                
                string json = Json(Map(cedis));
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[1] + "Branches/" + branchId);
                webrequest.Method = "PUT";
                webrequest.Headers.Add("Authorization", string.Format("Bearer {0}", authToken.Access_Token));
                webrequest.KeepAlive = false;
                webrequest.Timeout = Settings.RequestTimeOut();
                webrequest.ContentType = "application/json";

                byte[] byteArray = Encoding.UTF8.GetBytes(json);
                webrequest.ContentLength = byteArray.Length;

                using (var streamWriter = new StreamWriter(webrequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                using (HttpWebResponse responsePr = (HttpWebResponse)webrequest.GetResponse())
                    if ((int)responsePr.StatusCode == 204)
                    {
                        resp = responsePr.ContentType;
                    }
                    else
                    {
                        resp = responsePr.ContentType;
                    }
            }
            catch (WebException e)
            {
                resp = e.Message;
            }
            return resp;
        }
        public PostResponse PostCustomer(Models.WBC.Customer entity, Models.WBC.Token token)
        {
            var client = new RestClient(Settings.Api_Wbc());
            var request = new RestRequest("/customers", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(entity);
            request.AddHeader("Authorization", string.Format("Bearer {0}", token.Access_Token));
            request.Timeout = Settings.RequestTimeOut();
            var response = client.Execute<Customer>(request);
            return new PostResponse
            {
                Success = response.StatusCode == HttpStatusCode.Created,
                Message = response.StatusCode == HttpStatusCode.Created ? "" : response.Content,
                Id = response.StatusCode == HttpStatusCode.Created ? Convert.ToInt32(response.Data.CustomerId) : 0
            };
        }
        public BaseResponse PutCustomer(Models.WBC.Customer entity, Models.WBC.Token token)
        {
            var client = new RestClient(Settings.Api_Wbc());
            var request = new RestRequest("/customers/" + entity.CustomerId, Method.PUT);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(entity);
            request.AddHeader("Authorization", string.Format("Bearer {0}", token.Access_Token));
            request.Timeout = Settings.RequestTimeOut();
            var response = client.Execute(request);
            return new BaseResponse {
                Success = response.StatusCode == HttpStatusCode.InternalServerError ? false : true,
                Message = response.StatusCode == HttpStatusCode.InternalServerError ? response.Content : ""
            };
        }
        public void AddCustomerVisits(Models.CRM.Visit visits,int routeId,string CustomerId, Models.WBC.Token token)
        {
            if (visits.Monday) {
                Models.WBC.CustomerVisits visit = new CustomerVisits() { routeId = routeId ,day = 2,visitType = 1 , order = 1};
                PostCustomerVisits(visit, CustomerId,token);
            }
            if (visits.Tuesday)
            {
                Models.WBC.CustomerVisits visit = new CustomerVisits() { routeId = routeId, day = 3, visitType = 1, order = 1 };
                PostCustomerVisits(visit, CustomerId, token);
            }
            if (visits.Wednesday)
            {
                Models.WBC.CustomerVisits visit = new CustomerVisits() { routeId = routeId, day = 4, visitType = 1, order = 1 };
                PostCustomerVisits(visit, CustomerId, token);
            }
            if (visits.Thursday)
            {
                Models.WBC.CustomerVisits visit = new CustomerVisits() { routeId = routeId, day = 5, visitType = 1, order = 1 };
                PostCustomerVisits(visit, CustomerId, token);
            }
            if (visits.Friday)
            {
                Models.WBC.CustomerVisits visit = new CustomerVisits() { routeId = routeId, day = 6, visitType = 1, order = 1 };
                PostCustomerVisits(visit, CustomerId, token);
            }
            if (visits.Saturday)
            {
                Models.WBC.CustomerVisits visit = new CustomerVisits() { routeId = routeId, day = 7, visitType = 1, order = 1 };
                PostCustomerVisits(visit, CustomerId, token);
            }
            if (visits.Sunday)
            {
                Models.WBC.CustomerVisits visit = new CustomerVisits() { routeId = routeId, day = 1, visitType = 1, order = 1 };
                PostCustomerVisits(visit, CustomerId, token);
            }
        }
        public void UpdateCustomerVisits(Models.CRM.Visit visits, int routeId, string customerId, Models.WBC.Token token)
        {
            for(int i = 1; i < 8; i++)
            {
                DeleteCustomerVisit(Convert.ToInt32(customerId), routeId, i, 1, token);
            }
            AddCustomerVisits(visits, routeId, customerId, token);
            EditCustomerVisits(visits, routeId, customerId, token);
        }
        public void EditCustomerVisits(Models.CRM.Visit visits, int routeId, string CustomerId, Models.WBC.Token token)
        {
            if (visits.Monday)
            {
                Models.WBC.CustomerVisits visit = new CustomerVisits() { order = 1 };
                PutCustomerVisit(Convert.ToInt32(CustomerId), routeId, 2, 1, token, visit);
            }
            if (visits.Tuesday)
            {
                Models.WBC.CustomerVisits visit = new CustomerVisits() { order = 1 };
                PutCustomerVisit(Convert.ToInt32(CustomerId), routeId, 3, 1, token, visit);
            }
            if (visits.Wednesday)
            {
                Models.WBC.CustomerVisits visit = new CustomerVisits() { order = 1 };
                PutCustomerVisit(Convert.ToInt32(CustomerId), routeId, 4, 1, token, visit);
            }
            if (visits.Thursday)
            {
                Models.WBC.CustomerVisits visit = new CustomerVisits() { order = 1 };
                PutCustomerVisit(Convert.ToInt32(CustomerId), routeId, 5, 1, token, visit);
            }
            if (visits.Friday)
            {
                Models.WBC.CustomerVisits visit = new CustomerVisits() { order = 1 };
                PutCustomerVisit(Convert.ToInt32(CustomerId), routeId, 6, 1, token, visit);
            }
            if (visits.Saturday)
            {
                Models.WBC.CustomerVisits visit = new CustomerVisits() { order = 1 };
                PutCustomerVisit(Convert.ToInt32(CustomerId), routeId, 7, 1, token, visit);
            }
            if (visits.Sunday)
            {
                Models.WBC.CustomerVisits visit = new CustomerVisits() { order = 1 };
                PutCustomerVisit(Convert.ToInt32(CustomerId), routeId, 1, 1, token, visit);
            }
        }
        public void DeleteCustomerVisit(int customerId, int routeId, int day, int visitType, Token token)
        {
            var client = new RestClient(Settings.Api_Wbc());
            var request = new RestRequest($"Customers/{customerId}/Visits/{routeId}/{day}/{visitType}" , Method.DELETE);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", string.Format("Bearer {0}", token.Access_Token));
            request.Timeout = Settings.RequestTimeOut();
            var response = client.Execute(request);
        }
        public void PutCustomerVisit(int customerId, int routeId, int day, int visitType, Token token, CustomerVisits visit)
        {
            var client = new RestClient(Settings.Api_Wbc());
            var request = new RestRequest($"Customers/{customerId}/Visits/{routeId}/{day}/{visitType}", Method.PUT);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(visit);
            request.AddHeader("Authorization", string.Format("Bearer {0}", token.Access_Token));
            request.Timeout = Settings.RequestTimeOut();
            var response = client.Execute(request);
        }
        public Models.WBC.CustomerVisits PostCustomerVisits(Models.WBC.CustomerVisits visit, string CustomerId, Models.WBC.Token token)
        {
            var client = new RestClient(Settings.Api_Wbc());
            var request = new RestRequest("Customers/"+ CustomerId + "/Visits", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(visit);
            request.AddHeader("Authorization", string.Format("Bearer {0}", token.Access_Token));
            request.Timeout = Settings.RequestTimeOut();
            var response = client.Execute<CustomerVisits>(request);
            if (response.StatusCode != HttpStatusCode.Created)
                return null;
            return response.Data;
        }
        public CustomerGetFilter getCustomerCode(string WBCcustomerCode, Models.WBC.Token token)
        {
            if (token == null)
                throw new InvalidOperationException("No fue posible autenticarse con el servicio wbc");
            var client = new RestClient(Settings.Api_Wbc());
            var request = new RestRequest("/Customers?code=" + WBCcustomerCode, Method.GET);
            request.AddHeader("Authorization", string.Format("Bearer {0}", token.Access_Token));
            request.Timeout = Settings.RequestTimeOut();
            var response = client.Execute<CustomerGetFilter>(request);
            if (response.StatusCode != HttpStatusCode.NoContent)
                return null;
            return response.Data;
        }
        
        public List<RouteGetFilter> getRouteCode(string WBCRouteId, int WBCBranchId, Models.WBC.Token token)
        {
            if (token == null)
                throw new InvalidOperationException("No fue posible autenticarse con el servicio wbc");
            var client = new RestClient(Settings.Api_Wbc());
            var request = new RestRequest("Routes?branchId="+ WBCBranchId + "&code=" + WBCRouteId, Method.GET);
            request.AddHeader("Authorization", string.Format("Bearer {0}", token.Access_Token));
            request.Timeout = Settings.RequestTimeOut();
            var response = client.Execute<List<RouteGetFilter>>(request);
            if (response.StatusCode != HttpStatusCode.OK)
                return new List<RouteGetFilter>();
            return response.Data;
        }
        public List<BranchGetFilter> getBranchCode(string WBCBranchId, Models.WBC.Token token)
        {
            //var token = Authenticate(Settings.AuthCredentials());
            if (token == null)
                throw new InvalidOperationException("No fue posible autenticarse con el servicio wbc");
            var client = new RestClient(Settings.Api_Wbc());
            var request = new RestRequest("/Branches?code=" + WBCBranchId, Method.GET);
            request.AddHeader("Authorization", string.Format("Bearer {0}", token.Access_Token));
            request.Timeout = Settings.RequestTimeOut();
            var response = client.Execute<List<BranchGetFilter>>(request);
            if (response.StatusCode != HttpStatusCode.OK)
                return new List<BranchGetFilter>();
            return response.Data;
        }
        public customerPriceList getCustomerPricelist(string WBCcustomerId, Models.WBC.Token token)
        {
            if (token == null)
                throw new InvalidOperationException("No fue posible autenticarse con el servicio wbc");
            var client = new RestClient(Settings.Api_Wbc());
            var request = new RestRequest("/Customers/" + WBCcustomerId + "/PriceList", Method.GET);
            request.AddHeader("Authorization", string.Format("Bearer {0}", token.Access_Token));
            request.Timeout = Settings.RequestTimeOut();
            var response = client.Execute<customerPriceList>(request);
            if (response.StatusCode != HttpStatusCode.NoContent)
                return null;
            return response.Data;
        }
        public List<PriceList> getPricelistByCode(string WBCcustomerId, Models.WBC.Token token)
        {
            if (token == null)
                throw new InvalidOperationException("No fue posible autenticarse con el servicio wbc");
            var client = new RestClient(Settings.Api_Wbc());
            var request = new RestRequest("/PriceLists?Code=" + WBCcustomerId, Method.GET);
            request.AddHeader("Authorization", string.Format("Bearer {0}", token.Access_Token));
            request.Timeout = Settings.RequestTimeOut();
            var response = client.Execute<List<PriceList>>(request);
            if (response.StatusCode != HttpStatusCode.OK)
                return null;
            return response.Data;
        }
        public object putCustomerPricelist(string WBCcustomerId, int WBCcustomerPriceListId, Models.WBC.Token token)
        {
            var client = new RestClient(Settings.Api_Wbc());
            var request = new RestRequest("/Customers/"+ WBCcustomerId + "/PriceList/" + WBCcustomerPriceListId, Method.PUT);
            request.AddHeader("Authorization", string.Format("Bearer {0}", token.Access_Token));
            request.Timeout = Settings.RequestTimeOut();
            var response = client.Execute<Customer>(request);
            if (response.StatusCode != HttpStatusCode.NoContent)
                return null;
            return response.Data;
        }
        public List<Site> GetSites(Models.WBC.Token token)
        {
            var url = ConfigurationManager.AppSettings["API_WBC"];
            var client = new RestClient(url);
            var request = new RestRequest("sites",Method.GET);
            request.Timeout = Settings.RequestTimeOut();
            request.AddHeader("Authorization", string.Format("Bearer {0}", token.Access_Token));
            var response = client.Execute<List<Site>>(request);
            return response.Data;
        }
        public Customer FindCustomerByBarcode(string barcode, Token token)
        {
            var restClient = new RestClient(Settings.Api_Wbc());
            var request = new RestRequest($"Customers?code={barcode}", Method.GET);
            request.Timeout = Settings.RequestTimeOut();
            request.AddHeader("Authorization", $"Bearer {token.Access_Token}");
            var response = restClient.Execute<List<Customer>>(request);
            if (response.Data.Any())
                return response.Data.First();
            return null;
        }
        private Branch Map(Cedis Cedis)
        {
            Branch branch = new Branch()
            {
                companyId = 2,
                siteId = Cedis.SiteId,
                branchCode = Convert.ToString(Cedis.CedisIdOpecd),
                name = Cedis.Nombre,
                taxPercent = 0
            };
            return branch;
        }
        private BranchPUT Map(CedisPUT Cedis)
        {
            BranchPUT branch = new BranchPUT()
            {
                companyId = 1,
                name = Cedis.Nombre,
                taxPercent = 0
            };
            return branch;
        }
        private string Json(Branch branch)
        {
            JObject Row = null;
            Row = JObject.FromObject(branch);
            return Row.ToString();
        }
        private string Json(BranchPUT branch)
        {
            JObject Row = null;
            Row = JObject.FromObject(branch);
            return Row.ToString();
        }

        public Token Authenticate(Credentials credentials)
        {
            var client = new RestClient(ConfigurationManager.AppSettings["API_WBC"]);
            var request = new RestRequest("/token", Method.POST);
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddParameter("application/x-www-form-urlencoded", string.Format("grant_type={0}&Username={1}&Password={2}",credentials.Grant_Type, credentials.Username, credentials.Password), ParameterType.RequestBody);
            request.Timeout = 180000;
            var response = client.Execute<Token>(request);
            if (response.StatusCode != HttpStatusCode.OK)
                return null;
            return response.Data;
        }
    }
}
