﻿using API_Integraciones.Controllers.DataAccess;
using API_Integraciones.Models.AdminConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_Integraciones.Models.Entity;

namespace API_Integraciones.Controllers.Services
{
    public class AdminConfigController : ApiController
    {

        private readonly CONF_ConfigController configRepository = new CONF_ConfigController();

        //[HttpGet]
        //[Route("api/AdminConfig/GetConfig")]
        //[ResponseType(typeof(ConfigGET))]
        //public IHttpActionResult GetConfig()
        //{
        //    try
        //    {
        //        ConfigGET Cnfig = new CONF_ConfigController().FindConfig();
        //        if (Cnfig == null)
        //            return NotFound();
        //        return Ok(Cnfig);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        [HttpGet]
        [Route("api/AdminConfig/Paises")]
        public IHttpActionResult GetPaises()
        {
            try
            {
                var paises = configRepository.FindPaises();
                return Ok(paises);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("api/AdminConfig/Estados")]
        public IHttpActionResult GetEstados(float paisOpeId)
        {
            try
            {
                var estados = configRepository.FindEstados(paisOpeId);
                return Ok(estados);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("api/AdminConfig/Colonias")]
        public IHttpActionResult GetColonias(float estadoOpeId, float municipioOpeId)
        {
            try
            {
                var colonias = configRepository.FindColonias(estadoOpeId, municipioOpeId);
                return Ok(colonias);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("api/AdminConfig/Municipios")]
        public IHttpActionResult GetMunicipios(float estadoOpeId)
        {
            try
            {
                var municipios = configRepository.FindMunicipios(estadoOpeId);
                return Ok(municipios);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
