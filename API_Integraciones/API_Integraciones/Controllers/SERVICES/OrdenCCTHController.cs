﻿using API_Integraciones.Controllers.DAO;
using API_Integraciones.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.Dto;
using System.Web.Http.Description;
using API_Integraciones.Enums;
using System.Globalization;
using API_Integraciones.Models.AdminPedidos;

namespace API_Integraciones.Controllers
{
    public class OrdenCCTHController : ApiController
    {
        private CCTH_INTERMEDIAEntities _db;
        private CAT_OrderGeneralController _orderGeneral;
        private CAT_BottlerDataController _bottlerData;
        private CAT_OrderAddressesController _orderAddresses;
        private CAT_OrderItemsController _orderItems;
        private CAT_OrderCommentsController _orderComments;
        private CAT_CustomersController _customer;

        public void InitializeRepositories(CCTH_INTERMEDIAEntities db)
        {
            _db = db;
            _orderGeneral = new CAT_OrderGeneralController(_db);
            _bottlerData = new CAT_BottlerDataController(_db);
            _orderAddresses = new CAT_OrderAddressesController(_db);
            _orderItems = new CAT_OrderItemsController(_db);
            _orderComments = new CAT_OrderCommentsController(_db);
            _customer = new CAT_CustomersController(_db);
        }

        [NonAction]
        public List<OrderGeneral> Get()
        {
            try
            {
                CAT_OrderGeneralController ordenGeneral = new CAT_OrderGeneralController();
                List<OrderGeneral> json = ordenGeneral.find(2);
                return json;
            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpPost]
        [Route("api/OrdenCCTH/Productos")]
        [ResponseType(typeof(List<OrderCreationResponse>))]
        public IHttpActionResult BulkOrderCreation([FromBody]List<OrderGeneral> orders)
        {
            var failedCreations = new List<OrderCreationResponse>();
            foreach (var order in orders)
            {
                try
                {
                    CreateOrder(order);
                }
                catch (Exception ex)
                {
                    failedCreations.Add(new OrderCreationResponse { Entity_Id = order.ENTITY_ID, Message = ex.Message });
                }
            }
            return Ok(failedCreations);
        }

        [NonAction]
        public int CreateOrder(OrderGeneral orderGeneral)
        {
            int orderId = 0;
            try
            {
                InitializeRepositories(new CCTH_INTERMEDIAEntities());
                var orderExists = _orderGeneral.FindByIncrementId(orderGeneral.INCREMENT_ID) != null;
                if (!orderExists)
                {
                    orderGeneral.STATUS_ORDEN = OrderGeneralStatus.PENDIENTE;
                    orderGeneral.CREATE_FROM = OrderCreatedAt.COCACOLA;
                    orderId = _orderGeneral.Create(orderGeneral);
                    var customer = _customer.FindCustomerByCcthId((int)orderGeneral.CUSTOMER_ID);

                    var isValidCustomer = customer != null && !string.IsNullOrEmpty(customer.CRMId) && !string.IsNullOrEmpty(customer.BarCode);
                    var canSetOrderToPrepared = _orderGeneral.CanSetOrderStatusToPrepared(orderId, orderGeneral.BOTTLER_DATA.ROUTE_CODE, DateTime.Parse(orderGeneral.BOTTLER_DATA.DELIVERY_DATE, CultureInfo.CreateSpecificCulture("es-MX")), orderGeneral.BOTTLER_DATA.CEDI_NAME);
                    var wantBill = orderGeneral.ORDER_ADDRESSES.Where(oAddress => oAddress.WANT_BILL == 1 && oAddress.ADDRESS_TYPE == "billing").Count() == 1;

                    if (isValidCustomer && canSetOrderToPrepared)
                    {
                        if (wantBill && string.IsNullOrEmpty(customer.Cuc) || wantBill && Convert.ToInt32(customer.Cuc) <= 0)
                        {
                            _orderGeneral.ChangeOrderStatus(orderId, OrderGeneralStatus.PENDIENTE);
                        }
                        else
                        {
                            _orderGeneral.ChangeOrderStatus(orderId, OrderGeneralStatus.PREPARADO);
                        }  
                    }

                    _bottlerData.Create(orderGeneral.BOTTLER_DATA, orderId);

                    if (customer != null)
                    {
                        foreach (var orderAddress in orderGeneral.ORDER_ADDRESSES)
                        {
                            orderAddress.CUSTOMER_ID = customer.Cuc;
                            orderAddress.CRM_Id = customer.CRMId;
                            orderAddress.BarCode = customer.BarCode;
                        }
                    }
                    orderGeneral.ORDER_ADDRESSES.ForEach(orderAddress => orderAddress.CustomerType = CustomerType.CLIENTE_COCACOLA);
                    _orderAddresses.BulkCreation(orderGeneral.ORDER_ADDRESSES, orderId);

                    if (customer == null)
                    {
                        orderGeneral.ORDER_ADDRESSES.ForEach(orderAddress => orderAddress.CUSTOMER_ID = orderGeneral.CUSTOMER_ID.ToString());
                        _customer.BulkCreation(orderGeneral.ORDER_ADDRESSES, orderGeneral.BOTTLER_DATA.ROUTE_CODE, orderGeneral.BOTTLER_DATA.CEDI_NAME);
                    }

                    _orderItems.BulkCreation(orderGeneral.ORDER_ITEMS, orderId); 

                    if (orderGeneral.ORDER_COMMENTS != null)
                        _orderComments.BulkCreation(orderGeneral.ORDER_COMMENTS, orderId);
                    _db.SaveChanges();
                }
                return orderId;
            }
            catch (Exception ex)
            {
                if (orderId != 0)
                    new CAT_OrderGeneralController().Delete(orderId);
                throw ex;
            }
            finally
            {
                _db.Dispose();
            }
        }


    }
}
