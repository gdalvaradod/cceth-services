using API_Integraciones.Controllers.APIs;
using API_Integraciones.Controllers.DAO;
using API_Integraciones.Controllers.DataAcces;
using API_Integraciones.Controllers.DataAccess;
using API_Integraciones.Enums;
using API_Integraciones.Models.AdminClientes;
using API_Integraciones.Models.AdminPedidos;
using API_Integraciones.Models.CRM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;

using System.Web.Http;
using System.Web.Http.Description;
using API_Integraciones.Models;

namespace API_Integraciones.Controllers
{
    public class AdminClientesController : ApiController
    {

        [HttpGet]
        [ResponseType(typeof(List<CustomerFilter>))]
        [Route("api/Customer/GetFilter")]
        public List<CustomerFilter> GetFilter(int cedi, string BarCode)
        {
            try
            {
                return new CAT_CustomersController().FilterLIke(cedi, BarCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpGet]
        [ResponseType(typeof(List<CustomerRelationResponse>))]
        [Route("api/Customer/GetSearchFilterRelation")]
        public List<CustomerRelationResponse> GetSearchFilterRelation(int cedi, string barcode = "", int cuc = -1, string crmid = "")
        {
            try
            {
                CustomerRelation search = new CustomerRelation()
                {
                    Barcode = barcode,
                    CrmId = crmid,
                    Cuc = cuc == 0 ? 1 : cuc
                };
                return new CAT_CustomersController().SearchAdvanceCustomerRelation(cedi, search);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpGet]
        [ResponseType(typeof(List<GetSearchCustomer>))]
        [Route("api/Customer/GetSearchFilter")]
        public List<GetSearchCustomer> GetSearchFilter(int cedi, string Name = null, string Email = null, string Tel = null,string Barcode = null, int Cuc = 0,int Route = 0)
        {
            try
            {
                SearchCustomer search = new SearchCustomer()
                {
                    Name = Name,
                    Email = Email,
                    Tel = Tel,
                    BarCode = Barcode,
                    Cuc = Cuc,
                    Route = Route
                };
                return new CAT_CustomersController().SearchAdvanceCustomer(cedi, search);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("api/Customer/GetCustomer")]
        public List<Customers> GetCustomer(int cedis, string name = null, string email = null, string addressType = null, string route = null)
        {
            try
            {
                CAT_CustomersController customerRepository = new CAT_CustomersController();
                return customerRepository.FindCustomersBy(name, cedis, route, email);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("api/Clientes")]
        public IHttpActionResult Get(string name = null, string route = null, int? status = null, string cedisId = null, string customerType = null, int? start = 0, int? length = int.MaxValue)
        {
            try
            {
                CAT_CustomersController customer = new CAT_CustomersController();
                int dbTotalRecords = 0;
                int filteredRecords = 0;
                List<Customers> customers = customer.Find(name, route, status, cedisId, customerType, out dbTotalRecords, out filteredRecords, start, length);
                customers.OrderBy(cuztomer => cuztomer.Id);
                return Ok(new { data = customers, totalRecords = dbTotalRecords, filteredRecords = filteredRecords });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpGet]
        [Route("api/Clientes/{id}")]
        public Customers Get(int id)
        {
            try
            {
                CAT_CustomersController customer = new CAT_CustomersController();
                Customers json = customer.FindCustomerbyId(id);
                return json;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("api/Clientes/VisitDays")]
        [ResponseType(typeof(List<WeekDay>))]
        public List<WeekDay> GetVisitDays(int cedisId, int routeId, int zipCode, string neighborhood)
        {
            try
            {
                var routeConfigurations = new CONF_RoutesController().Find(cedisId, routeId, zipCode, neighborhood);
                if (routeConfigurations.Any())
                    return routeConfigurations.First().DeliveryDays;
                return new List<WeekDay>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [Route("api/Clientes")]
        public IHttpActionResult Post([FromBody]Customers customerEntity)
        {
            try
            {
                #region adding customer into CRM
                var customerCrmId = new APICRMController().GetCustomer(customerEntity.BarCode);
                if (string.IsNullOrEmpty(customerCrmId))
                {
                    var crmCustomer = IntermediaCustomerToCrmCustomer(customerEntity);
                    ValidateCRMCustomerForCreation(crmCustomer);
                    var postCrmCustomerResponse = new APICRMController().PostCustomer(crmCustomer);
                    if (postCrmCustomerResponse.Success)
                        customerCrmId = postCrmCustomerResponse.Message;
                    else
                        return Content(HttpStatusCode.InternalServerError, "Error al insertar cliente en Crm : " + postCrmCustomerResponse.Message);
                }
                #endregion

                #region adding customer into wbc
                #region customer
                var wbcRepository = new APIWBCController();
                Models.WBC.Token token = wbcRepository.Authenticate(Settings.AuthCredentials());
                if (token == null)
                    throw new InvalidOperationException("No fue posible autenticarse con el servicio wbc");
                var wbcCustomer = wbcRepository.FindCustomerByBarcode(customerEntity.BarCode, token);
                if (wbcCustomer == null)
                {
                    wbcCustomer = IntermediaCustomerToWbcCustomer(customerEntity);
                    ValidateWBCCustomerForCreation(wbcCustomer);
                    var postWbcCustomerResponse = wbcRepository.PostCustomer(wbcCustomer, token);
                    if (postWbcCustomerResponse.Success)
                        wbcCustomer.CustomerId = postWbcCustomerResponse.Id.ToString();
                    else
                        return Content(HttpStatusCode.InternalServerError, "Error al insertar cliente en Wbc : " + postWbcCustomerResponse.Message);
                }
                #endregion
                #region add visits
                List<Models.WBC.BranchGetFilter> branch = new APIWBCController().getBranchCode(customerEntity.DistributionCenter, token);
                List<Models.WBC.RouteGetFilter> route = new APIWBCController().getRouteCode(customerEntity.RouteCode, branch[0].branchId, token);
                new APIWBCController().AddCustomerVisits(customerEntity.Visit, route[0].routeId, wbcCustomer.CustomerId, token);
                #endregion
                #region add pricelist
                Models.WBC.customerPriceList priceList = new APIWBCController().getCustomerPricelist(wbcCustomer.CustomerId, token);
                if (priceList == null)
                {
                    var cuc = new REF_CedisController().FindCucGeneric(Convert.ToInt32(customerEntity.DistributionCenter), Convert.ToInt32(customerEntity.RouteCode));
                    int CucGeneric = cuc.Cuc ?? 0;
                    List<Models.WBC.PriceList> priceListcode = new APIWBCController().getPricelistByCode(Convert.ToString(CucGeneric), token);
                    if (priceListcode != null && priceListcode.Count > 0)
                    {
                        object putpricelist = new APIWBCController().putCustomerPricelist(wbcCustomer.CustomerId, priceListcode[0].priceListId, token);
                    }
                }
                #endregion
                #endregion

                #region adding customer to intermedia
                var customersRepository = new CAT_CustomersController();
                var customerExists = customersRepository.FindByBarCode(customerEntity.BarCode);
                if (!customerExists)
                {
                    customerEntity.CRMId = string.IsNullOrEmpty(customerCrmId) ? customerCrmId : customerCrmId.Trim('"', '\\');
                    customerEntity.WBCId = wbcCustomer.CustomerId;
                    customerEntity.CRMCreateOn = DateTime.Now.ToShortDateString();
                    SetConfirmationStatus(customerEntity);
                    ValidateIntermediaCustomerForCreation(customerEntity);
                    customerEntity.AddressType = "shipping";
                    customersRepository.Create(customerEntity);
                    customerEntity.AddressType = "billing";
                    customersRepository.Create(customerEntity);
                }
                #endregion

                return Ok(new { CrmId = customerCrmId, WbcId = wbcCustomer.CustomerId });
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPut]
        [Route("api/Clientes/{customerId}")]
        public IHttpActionResult Put(Customers customer, string customerId)
        {
            try
            {
                
                #region Update customer in crm
                if (customer.CustomerType == CustomerType.CLIENTE_COCACOLA || !string.IsNullOrEmpty(customer.CCTHId))
                {
                    if (string.IsNullOrEmpty(customer.CRMId) || customer.CRMId.Equals("0"))
                    {
                        #region adding customer into CRM
                        customer.CRMId = new APICRMController().GetCustomer(customer.BarCode);
                        if (string.IsNullOrEmpty(customer.CRMId))
                        {
                            var crmCustomer = IntermediaCustomerToCrmCustomer(customer);
                            ValidateCRMCustomerForCreation(crmCustomer);
                            if (string.IsNullOrEmpty(customer.Cuc))
                                crmCustomer.Cuc = "0";
                            var postCrmCustomerResponse = new APICRMController().PostCustomer(crmCustomer);
                            if (postCrmCustomerResponse.Success)
                                customer.CRMId = postCrmCustomerResponse.Message;
                            else
                                return Content(HttpStatusCode.InternalServerError, "Error al insertar cliente en Crm : " + postCrmCustomerResponse.Message);
                        }
                        #endregion
                    }
                    else
                    {
                        var crmCustomer = IntermediaCustomerToCrmCustomer(customer);
                        ValidateCRMCustomerForEdition(crmCustomer);
                        var putCrmCustomerResponse = new APICRMController().PutCustomer(crmCustomer);
                        if (!putCrmCustomerResponse.Success)
                            return Content(HttpStatusCode.InternalServerError, putCrmCustomerResponse.Message);
                    }
                }
                else
                {
                    var crmCustomer = IntermediaCustomerToCrmCustomer(customer);
                    ValidateCRMCustomerForEdition(crmCustomer);
                    var putCrmCustomerResponse = new APICRMController().PutCustomer(crmCustomer);
                    if (!putCrmCustomerResponse.Success)
                        return Content(HttpStatusCode.InternalServerError, putCrmCustomerResponse.Message);
                }

                #endregion
                #region Update customer in wbc
                var wbcApi = new APIWBCController();
                if (customer.CustomerType == CustomerType.CLIENTE_COCACOLA || !string.IsNullOrEmpty(customer.CCTHId))
                {
                    if (string.IsNullOrEmpty(customer.CustomerId) || customer.CustomerId.Equals("0"))
                    {
                        #region customer
                        var wbcRepository = new APIWBCController();
                        Models.WBC.Token token = wbcRepository.Authenticate(Settings.AuthCredentials());
                        if (token == null)
                            throw new InvalidOperationException("No fue posible autenticarse con el servicio wbc");
                        var wbcCustomer = wbcRepository.FindCustomerByBarcode(customer.BarCode, token);
                        if (wbcCustomer == null)
                        {
                            wbcCustomer = IntermediaCustomerToWbcCustomer(customer);
                            ValidateWBCCustomerForCreation(wbcCustomer);
                            var postWbcCustomerResponse = wbcRepository.PostCustomer(wbcCustomer, token);
                            if (postWbcCustomerResponse.Success)
                                customer.CustomerId = postWbcCustomerResponse.Id.ToString();
                            else
                                return Content(HttpStatusCode.InternalServerError, "Error al insertar cliente en Wbc : " + postWbcCustomerResponse.Message);
                            
                            #region add visits
                            List<Models.WBC.BranchGetFilter> branch = new APIWBCController().getBranchCode(customer.DistributionCenter, token);
                            List<Models.WBC.RouteGetFilter> route = new APIWBCController().getRouteCode(customer.RouteCode, branch[0].branchId, token);
                            new APIWBCController().AddCustomerVisits(customer.Visit, route[0].routeId, wbcCustomer.CustomerId, token);
                            #endregion
                            //#region add pricelist
                            //Models.WBC.customerPriceList priceList = new APIWBCController().getCustomerPricelist(wbcCustomer.CustomerId, token);
                            //if (priceList == null)
                            //{
                            //    var cuc = new REF_CedisController().FindCucGeneric(Convert.ToInt32(customer.DistributionCenter), Convert.ToInt32(customer.RouteCode));
                            //    int CucGeneric = cuc.Cuc ?? 0;
                            //    List<Models.WBC.PriceList> priceListcode = new APIWBCController().getPricelistByCode(Convert.ToString(CucGeneric), token);
                            //    if (priceListcode != null && priceListcode.Count > 0)
                            //    {
                            //        object putpricelist = new APIWBCController().putCustomerPricelist(wbcCustomer.CustomerId, priceListcode[0].priceListId, token);
                            //    }
                            //}
                            //#endregion
                        }else
                        {
                            customer.CustomerId = wbcCustomer.CustomerId;
                        }
                        #endregion

                    }
                    else
                    {
                        var wbcCustomer = IntermediaCustomerToWbcCustomer(customer);
                        ValidateWBCCustomerForEdition(wbcCustomer);
                        Models.WBC.Token token = wbcApi.Authenticate(Settings.AuthCredentials());
                        if (token == null)
                            throw new InvalidOperationException("No fue posible autenticarse con el servicio wbc");
                        var putWbcCustomerResponse = wbcApi.PutCustomer(wbcCustomer, token);
                        if (!putWbcCustomerResponse.Success)
                            return Content(HttpStatusCode.InternalServerError, "Error al editar el cliente en Wbc : " + putWbcCustomerResponse.Message);
                        List<Models.WBC.BranchGetFilter> branch = wbcApi.getBranchCode(customer.DistributionCenter, token);
                        List<Models.WBC.RouteGetFilter> route = wbcApi.getRouteCode(customer.RouteCode, branch[0].branchId, token);
                        wbcApi.UpdateCustomerVisits(customer.Visit, route[0].routeId, wbcCustomer.CustomerId, token);
                        //Models.WBC.customerPriceList priceList = new APIWBCController().getCustomerPricelist(customer.CustomerId, token);
                        //if (priceList == null)
                        //{
                        //    var cuc = new REF_CedisController().FindCucGeneric(Convert.ToInt32(customer.DistributionCenter), Convert.ToInt32(customer.RouteCode));
                        //    int CucGeneric = cuc.Cuc ?? 0;
                        //    List<Models.WBC.PriceList> priceListcode = new APIWBCController().getPricelistByCode(Convert.ToString(CucGeneric), token);
                        //    if (priceListcode != null && priceListcode.Count > 0)
                        //    {
                        //        object putpricelist = new APIWBCController().putCustomerPricelist(wbcCustomer.CustomerId, priceListcode[0].priceListId, token);
                        //    }
                        //}
                    }
                }
                else
                {
                    var wbcCustomer = IntermediaCustomerToWbcCustomer(customer);
                    ValidateWBCCustomerForEdition(wbcCustomer);
                    Models.WBC.Token token = wbcApi.Authenticate(Settings.AuthCredentials());
                    if (token == null)
                        throw new InvalidOperationException("No fue posible autenticarse con el servicio wbc");
                    var putWbcCustomerResponse = wbcApi.PutCustomer(wbcCustomer, token);
                    if (!putWbcCustomerResponse.Success)
                        return Content(HttpStatusCode.InternalServerError, "Error al editar el cliente en Wbc : " + putWbcCustomerResponse.Message);
                    List<Models.WBC.BranchGetFilter> branch = wbcApi.getBranchCode(customer.DistributionCenter, token);
                    List<Models.WBC.RouteGetFilter> route = wbcApi.getRouteCode(customer.RouteCode, branch[0].branchId, token);
                    wbcApi.UpdateCustomerVisits(customer.Visit, route[0].routeId, wbcCustomer.CustomerId, token);
                    Models.WBC.customerPriceList priceList = new APIWBCController().getCustomerPricelist(wbcCustomer.CustomerId, token);
                    if (priceList == null)
                    {
                        var cuc = new REF_CedisController().FindCucGeneric(Convert.ToInt32(customer.DistributionCenter), Convert.ToInt32(customer.RouteCode));
                        int CucGeneric = cuc.Cuc ?? 0;
                        List<Models.WBC.PriceList> priceListcode = new APIWBCController().getPricelistByCode(Convert.ToString(CucGeneric), token);
                        if (priceListcode != null && priceListcode.Count > 0)
                        {
                            object putpricelist = new APIWBCController().putCustomerPricelist(wbcCustomer.CustomerId, priceListcode[0].priceListId, token);
                        }
                    }
                }
                #endregion
                #region Update customer in intermedia
                customer.ModifiedOn = DateTime.Now.ToShortDateString();
                SetConfirmationStatus(customer);
                ValidateIntermediaCustomerForEdition(customer);
                new CAT_OrderAddressesController().Edit(customer.CRMId, customer.BarCode, customer.CCTHId, customer.Cuc);
                new CAT_CustomersController().Edit(customer);
                var customerCopy = new CAT_CustomersController().SearchAdvanceCustomerRelation(Convert.ToInt32(customer.DistributionCenter), new CustomerRelation { CrmId = customer.CRMId })
                    .SingleOrDefault(c => c.Id.ToString() != customer.Id);
                if(customerCopy != null)
                {
                    var customerForEdition = customer;
                    customerForEdition.Id = customerCopy.Id.ToString();
                    customerForEdition.AddressType = customer.AddressType == "shipping" ? "billing" : "shipping";
                    new CAT_CustomersController().Edit(customerForEdition);
                }
                
                #endregion

                return Content(HttpStatusCode.NoContent, string.Empty);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }

        }

        [HttpPut]
        [Route("api/OrderAddress/{orderAddressId}")]
        public IHttpActionResult Put(OrderAddresses orderAddress, int orderAddressId, int orderGeneralId)
        {
            try
            {
                if (orderAddress.ID != orderAddressId)
                    return BadRequest("orderAddressId en uri no coincide con Id en el modelo");
                new CAT_OrderAddressesController().Edit(orderAddress, orderGeneralId);
                return Content(HttpStatusCode.NoContent,"");
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpGet]
        [Route("api/OrderAddress")]
        public IHttpActionResult Get(int orderGeneralId, string addressType)
        {
            try
            {
                var orderAddress = new CAT_OrderAddressesController().Find(orderGeneralId, addressType);
                return Ok(orderAddress);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private HttpResponseMessage validateOK(bool Customers, string accion)
        {
            HttpResponseMessage response;
            string value = "";
            if (accion == "PUT")
            {
                if (Customers)
                {
                    value = "completado!";
                }
                else
                {
                    value = "Error de actualización en: Customers ";
                }
            }
            if (accion == "POST")
            {
                if (Customers)
                {
                    value = "completado!";
                }
                else
                {
                    value = "Error de inserción en: Customers ";
                }
            }
            response = Request.CreateResponse(HttpStatusCode.OK, value);
            return response;
        }
        private Customer IntermediaCustomerToCrmCustomer(Customers customer)
        {
            var crmCustomer = new Customer();
            crmCustomer.Id = customer.CRMId;
            crmCustomer.Name = customer.Name;
            if (!string.IsNullOrEmpty(customer.Cuc) && Convert.ToInt32(customer.Cuc) > 0)
            {
                crmCustomer.Cuc = customer.Cuc;
            }
            crmCustomer.BarCode = customer.BarCode;
            crmCustomer.PriceList = customer.PriceList;
            crmCustomer.Sequence = customer.Sequence;
            crmCustomer.DistributionCenter = customer.DistributionCenter;
            crmCustomer.RouteCode = customer.RouteCode;
            crmCustomer.Manager = customer.Manager;
            crmCustomer.PhysicalAddress = customer.PhysicalAddress;
            crmCustomer.Street = customer.Street;
            crmCustomer.IndoorNumber = customer.IndoorNumber;
            crmCustomer.OutdoorNumber = customer.OutdoorNumber;
            crmCustomer.Intersection1 = customer.Intersection1;
            crmCustomer.Intersection2 = customer.Intersection2;
            crmCustomer.ClientType = customer.ClientType;
            crmCustomer.Visit = customer.Visit;
            crmCustomer.CustomerVarious = customer.CustomerVarious;
            crmCustomer.CustomerType = customer.CustomerType;
            crmCustomer.PostCode = customer.PostCode;
            crmCustomer.Email = customer.Email;
            crmCustomer.Telephone = customer.Telephone;
            crmCustomer.Country = customer.Country;
            crmCustomer.City = customer.City;
            crmCustomer.State = customer.State.ToString();
            crmCustomer.Neighborhood = customer.Neighborhood;
            crmCustomer.Latitude = customer.Latitude;
            crmCustomer.Longitude = customer.Longitude;
            crmCustomer.CreatedOn = customer.CreatedOn;
            crmCustomer.ModifiedOn = customer.ModifiedOn;
            return crmCustomer;

        }
        private Models.WBC.Customer IntermediaCustomerToWbcCustomer(Customers customer)
        {
            
            var wbcCustomer = new Models.WBC.Customer()
            {
                CustomerId = customer.CustomerId,
                Contact = customer.Contact,
                PhysicalAddress = customer.PhysicalAddress,
                Description = customer.Description,
                Email = customer.Email,
                Latitude = customer.Latitude,
                Longitude = customer.Longitude,
                Name = customer.Name,
                Visit = customer.Visit,
                PostCode = customer.PostCode,
                Telephone = customer.Telephone,
                Country = customer.Country,
                RouteCode = customer.RouteCode,
                BarCode = customer.BarCode,
                Intersection1 = customer.Intersection1,
                Intersection2 = customer.Intersection2,
                IndoorNumber = customer.IndoorNumber,
                OutdoorNumber = customer.OutdoorNumber,
                Neighborhood = customer.Neighborhood,
                CustomerVarious = customer.CustomerVarious,
                City = customer.City,
                Manager = customer.Manager,
                Street = customer.Street,
                CustomerType = customer.CustomerType
            };
            if (!string.IsNullOrEmpty(customer.Cuc) && Convert.ToInt32(customer.Cuc) > 0)
                wbcCustomer.Cuc = customer.Cuc;

            return wbcCustomer;
        }
        private void SetConfirmationStatus(Customers customer)
        {
            if (!string.IsNullOrEmpty(customer.CRMId) && !string.IsNullOrEmpty(customer.BarCode))
                customer.StatusConfirmation = 1;
        }
        private void ValidateCRMCustomerForEdition(Customer customer)
        {
            var messageHeader = "Faltan datos para edicion de cliente en CRM: ";
            var errorMessage = "";
            if (string.IsNullOrEmpty(customer.Name))
                errorMessage += "Nombre vacio, ";
            if (customer.PriceList < 0)
                errorMessage += "Lista de precios menor a 0, ";
            if (string.IsNullOrEmpty(customer.DistributionCenter))
                errorMessage += "Cedis vacio, ";
            if (string.IsNullOrEmpty(customer.RouteCode))
                errorMessage += "Ruta vacia, ";
            if (string.IsNullOrEmpty(customer.Manager))
                errorMessage += "Supervisor vacio, ";
            if (string.IsNullOrEmpty(customer.Country))
                errorMessage += "Pais vacio, ";
            if (string.IsNullOrEmpty(customer.State))
                errorMessage += "Estado vacio, ";
            if (string.IsNullOrEmpty(customer.City))
                errorMessage += "Ciudad vacio, ";
            if (string.IsNullOrEmpty(customer.Neighborhood))
                errorMessage += "Colonia vacio, ";
            if (customer.Visit == null)
                errorMessage += "Visitas nulo, ";
            if (string.IsNullOrEmpty(customer.PostCode))
                errorMessage += "CodigoPostal vacio, ";
            if (string.IsNullOrEmpty(customer.Id))
                errorMessage += "Id vacio,";
            if (!customer.ClientType)
                errorMessage += "ClientType es falso";
            if (!string.IsNullOrEmpty(errorMessage))
                throw new ArgumentException(messageHeader + errorMessage);
        }
        private void ValidateCRMCustomerForCreation(Customer customer)
        {
            var messageHeader = "Faltan datos para creacion de cliente en CRM: ";
            var errorMessage = "";
            if (string.IsNullOrEmpty(customer.Name))
                errorMessage += "Nombre vacio, ";
            if (customer.PriceList < 0)
                errorMessage += "Lista de precios menor a 0, ";
            if (string.IsNullOrEmpty(customer.DistributionCenter))
                errorMessage += "Cedis vacio, ";
            if (string.IsNullOrEmpty(customer.RouteCode))
                errorMessage += "Ruta vacia, ";
            if (string.IsNullOrEmpty(customer.Manager))
                errorMessage += "Supervisor vacio, ";
            if (string.IsNullOrEmpty(customer.Country))
                errorMessage += "Pais vacio, ";
            if (string.IsNullOrEmpty(customer.State))
                errorMessage += "Estado vacio, ";
            if (string.IsNullOrEmpty(customer.City))
                errorMessage += "Ciudad vacio, ";
            if (string.IsNullOrEmpty(customer.Neighborhood))
                errorMessage += "Colonia vacio, ";
            if (customer.Visit == null)
                errorMessage += "Visitas nulo, ";
            if (string.IsNullOrEmpty(customer.PostCode))
                errorMessage += "CodigoPostal vacio, ";
            if (!customer.ClientType)
                errorMessage += "ClientType es falso";
            if (!string.IsNullOrEmpty(errorMessage))
                throw new ArgumentException(messageHeader + errorMessage);
        }
        private void ValidateWBCCustomerForCreation(Models.WBC.Customer customer)
        {
            var headerMessage = "Faltan datos para insertar cliente en WBC. ";
            var errorMessage = "";

            if (string.IsNullOrEmpty(customer.Contact))
                errorMessage += "Contacto vacio, ";
            if (string.IsNullOrEmpty(customer.Address))
                errorMessage += "Direccion vacia, ";
            if (string.IsNullOrEmpty(customer.Email))
                errorMessage += "Email vacio, ";
            if (string.IsNullOrEmpty(customer.Code))
                errorMessage += "Codigo vacio, ";
            if (string.IsNullOrEmpty(customer.Name))
                errorMessage += "Nombre vacio";
            if (!string.IsNullOrEmpty(errorMessage))
                throw new ArgumentException(headerMessage + errorMessage);
        }
        private void ValidateWBCCustomerForEdition(Models.WBC.Customer customer)
        {
            var headerMessage = "Faltan datos para editar cliente en WBC. ";
            var errorMessage = "";

            if (string.IsNullOrEmpty(customer.CustomerId) || customer.CustomerId.Equals("0"))
                errorMessage += "Id vacio, ";
            if (string.IsNullOrEmpty(customer.Contact))
                errorMessage += "Contacto vacio, ";
            if (string.IsNullOrEmpty(customer.Address))
                errorMessage += "Direccion vacia, ";
            if (string.IsNullOrEmpty(customer.Email))
                errorMessage += "Email vacio, ";
            if (string.IsNullOrEmpty(customer.Code))
                errorMessage += "Codigo vacio, ";
            if (string.IsNullOrEmpty(customer.Name))
                errorMessage += "Nombre vacio";
            if (!string.IsNullOrEmpty(errorMessage))
                throw new ArgumentException(headerMessage + errorMessage);
        }
        private void ValidateIntermediaCustomerForEdition(Customers customer)
        {
            int uselessAux;

            var headerMessage = "Faltan datos para insertar cliente en Intermedia. ";
            var errorMessage = "";
            if (string.IsNullOrEmpty(customer.Id))
                errorMessage += "Id de intermedia vacio, ";
            if (string.IsNullOrEmpty(customer.CRMId))
                errorMessage += "CRMId vacio, ";
            if (string.IsNullOrEmpty(customer.Name))
                errorMessage += "Nombre vacio, ";
            if (!string.IsNullOrEmpty(customer.Cuc) && !int.TryParse(customer.Cuc, out uselessAux))
                errorMessage += "Cuc no es numerico, ";
            if (string.IsNullOrEmpty(customer.BarCode))
                errorMessage += "Codigo de barras vacio, ";
            if (customer.Sequence < 1)
                errorMessage += "Secuencia no puede ser menor a 1, ";
            if (string.IsNullOrEmpty(customer.DistributionCenter) || !int.TryParse(customer.DistributionCenter, out uselessAux))
                errorMessage += "Cedis vacio o no numerico, ";
            if (string.IsNullOrEmpty(customer.RouteCode) || !int.TryParse(customer.RouteCode, out uselessAux))
                errorMessage += "Ruta vacia o no numerica, ";
            if (string.IsNullOrEmpty(customer.Manager) || !int.TryParse(customer.Manager, out uselessAux))
                errorMessage += "Supervisor vacio o no numerico";
            if (string.IsNullOrEmpty(customer.PhysicalAddress))
                errorMessage += "Direccion fisica vacia, ";
            if (string.IsNullOrEmpty(customer.Street))
                errorMessage += "Calle vacia";
            if (string.IsNullOrEmpty(customer.Country))
                errorMessage += "Pais vacio, ";
            if (string.IsNullOrEmpty(customer.State))
                errorMessage += "Estado vacio, ";
            if (string.IsNullOrEmpty(customer.City))
                errorMessage += "Ciudad vacia, ";
            if (string.IsNullOrEmpty(customer.Neighborhood))
                errorMessage += "Colonia vacia, ";
            if (string.IsNullOrEmpty(customer.Email))
                errorMessage += "Email vacio, ";
            if (string.IsNullOrEmpty(customer.PostCode))
                errorMessage += "Codigo postal vacio, ";
            if (string.IsNullOrEmpty(customer.Contact))
                errorMessage += "Contacto vacio, ";
            if (string.IsNullOrEmpty(customer.AddressType))
                errorMessage += "Tipo de direccion vacio, ";
            if (string.IsNullOrEmpty(customer.CustomerId))
                errorMessage += "WbcId vacio, ";
            if (string.IsNullOrEmpty(customer.CustomerType))
                errorMessage += "CustomerType vacio, ";
            if (customer.Visit == null)
                errorMessage += "Visitas vacio";
            if (errorMessage.Length > 1)
                throw new ArgumentException(headerMessage + errorMessage);
        }
        private void ValidateIntermediaCustomerForCreation(Customers customer)
        {
            int uselessAux;

            var headerMessage = "Faltan datos para insertar cliente en Intermedia. ";
            var errorMessage = "";

            if (string.IsNullOrEmpty(customer.WBCId))
                errorMessage += "WbcId vacio, ";
            if (string.IsNullOrEmpty(customer.CRMId))
                errorMessage += "CRMId vacio, ";
            if (string.IsNullOrEmpty(customer.Name))
                errorMessage += "Nombre vacio, ";
            if (!string.IsNullOrEmpty(customer.Cuc) && !int.TryParse(customer.Cuc, out uselessAux))
                errorMessage += "Cuc no es numerico, ";
            if (string.IsNullOrEmpty(customer.BarCode))
                errorMessage += "Codigo de barras vacio, ";
            if (customer.Sequence < 1)
                errorMessage += "Secuencia no puede ser menor a 1, ";
            if (string.IsNullOrEmpty(customer.DistributionCenter) || !int.TryParse(customer.DistributionCenter, out uselessAux))
                errorMessage += "Cedis vacio o no numerico, ";
            if (string.IsNullOrEmpty(customer.RouteCode) || !int.TryParse(customer.RouteCode, out uselessAux))
                errorMessage += "Ruta vacia o no numerica, ";
            if (string.IsNullOrEmpty(customer.Manager) || !int.TryParse(customer.Manager, out uselessAux))
                errorMessage += "Supervisor vacio o no numerico";
            if (string.IsNullOrEmpty(customer.PhysicalAddress))
                errorMessage += "Direccion fisica vacia, ";
            if (string.IsNullOrEmpty(customer.Street))
                errorMessage += "Calle vacia";
            if (string.IsNullOrEmpty(customer.Country))
                errorMessage += "Pais vacio, ";
            if (string.IsNullOrEmpty(customer.State))
                errorMessage += "Estado vacio, ";
            if (string.IsNullOrEmpty(customer.City))
                errorMessage += "Ciudad vacia, ";
            if (string.IsNullOrEmpty(customer.Neighborhood))
                errorMessage += "Colonia vacia, ";
            if (string.IsNullOrEmpty(customer.Email))
                errorMessage += "Email vacio, ";
            if (string.IsNullOrEmpty(customer.PostCode))
                errorMessage += "Codigo postal vacio, ";
            if (string.IsNullOrEmpty(customer.Contact))
                errorMessage += "Contacto vacio, ";
            if (string.IsNullOrEmpty(customer.AddressType))
                errorMessage += "Tipo de direccion vacio, ";
            if (string.IsNullOrEmpty(customer.CustomerType))
                errorMessage += "CustomerType vacio, ";
            if (customer.Visit == null)
                errorMessage += "Visitas vacio";
            if (errorMessage.Length > 1)
                throw new ArgumentException(headerMessage + errorMessage);
        }
        private bool IsGenericCuc(string customerCuc, string cedisId, string routeId)
        {
            var cuc = new CONF_DestinosController().findCucGeneric(cedisId, routeId);
            if (customerCuc != cuc.ToString())
                return false;
            return true;
        }
    }
}
