﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using API_Integraciones.Controllers.DataAccess;
using API_Integraciones.Models.Entity;

namespace API_Integraciones.Controllers.Services
{
    public class NeighborhoodsController : ApiController
    {

        private readonly CONF_RoutesController _routesRepository = new CONF_RoutesController();

        [HttpGet]
        [Route("api/neighborhoods")]
        [ResponseType(typeof(List<string>))]
        public List<string> Get()
        {
            try
            {
                var neighborhoods = _routesRepository.FindAll()
                .Select(route => route.Neigborhood.ToUpper())
                .Distinct()
                .ToList();
                return neighborhoods;
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}