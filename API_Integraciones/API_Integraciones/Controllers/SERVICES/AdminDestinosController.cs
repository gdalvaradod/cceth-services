﻿using API_Integraciones.Controllers.DataAcces;
using API_Integraciones.Models.AdminPedidos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API_Integraciones.Controllers.APIs;
using API_Integraciones.Models.WBC;
using System.Web.Http.Description;

namespace API_Integraciones.Controllers.Services
{
    public class AdminDestinosController : ApiController
    {
        [HttpGet]
        [Route("api/Destinos")]
        public List<Destinos> Get()
        {
            try
            {
                CONF_DestinosController destinoController = new CONF_DestinosController();
                return destinoController.FindAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpGet]
        [Route("api/Destinos/{destinoId}")]
        [ResponseType(typeof(Destinos))]
        public IHttpActionResult Get(int destinoId)
        {
            try
            {
                CONF_DestinosController destinoController = new CONF_DestinosController();
                var destino = destinoController.FindBy(new Destinos { Id = destinoId }).SingleOrDefault();
                return Ok(destino);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        [Route("api/Destinos")]
        public Object Post([FromBody]Destinos destinoEntity)
        {
            try
            {
                CONF_DestinosController destinoController = new CONF_DestinosController();
                var destinos = destinoController.FindBy(new Destinos
                {
                    Cedis = destinoEntity.Cedis,
                    RouteCodeOpecd = destinoEntity.RouteCodeOpecd
                }).Where(destinno => destinno.Enable == 1);
                if (destinos.Count() > 0)
                    return false;
                else
                {
                    destinoController.Create(destinoEntity);
                    APIWBCController APIWBC = new APIWBCController();
                    var token = APIWBC.Authenticate(Settings.AuthCredentials());
                    var branch = APIWBC.getBranchCode(destinoEntity.Cedis.ToString(), token).SingleOrDefault();
                    var ruta = new Route()
                    {
                        Active = true,
                        BranchId = branch.branchId,
                        Code = destinoEntity.RouteCodeOpecd.ToString(),
                        Name = destinoEntity.Nombre
                    };
                    APIWBC.Post(ruta, token);
                }
                return Ok(true);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPut]
        [Route("api/Destinos/{id}")]
        public Object Put(int id, [FromBody]DestinosPUT destino, string branchCode)
        {
            try
            {
                CONF_DestinosController destinoController = new CONF_DestinosController();
                string msj, APIs = null;
                destinoController.Edit(id, destino);
                APIWBCController APIWBC = new APIWBCController();
                var token = APIWBC.Authenticate(Settings.AuthCredentials());
                var branch = APIWBC.getBranchCode(branchCode, token).SingleOrDefault();
                var route = APIWBC.getRouteCode(destino.RouteCodeOpecd.ToString(), branch.branchId, token).SingleOrDefault();
                Route ruta = new Route
                {
                    Active = destino.Enable == 1 ? true : false,
                    Code = destino.RouteCodeOpecd.ToString(),
                    ModifiedOn = DateTime.Now.ToString(),
                    Name = destino.Nombre,
                    BranchId = branch.branchId,
                    RouteId = route.routeId
                };
                APIs = APIWBC.Put(ruta, APIWBC.Authenticate(Settings.AuthCredentials()));
                return Ok(true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
