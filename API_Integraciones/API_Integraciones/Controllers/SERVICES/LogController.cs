﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using API_Integraciones.Models.Dto;
using API_Integraciones.Models.Intermedia;
using API_Integraciones.Repositories;

namespace API_Integraciones.Controllers.Services
{
    public class LogController : ApiController
    {
        private readonly LogRepository _logRepository = new LogRepository();

        [HttpGet]
        [Route("api/Log")]
        public IHttpActionResult GetLogs(int? cedisId = null, int? routeId = null, int? trip = null, DateTime? deliveryDate = null, int? processSatus = null, bool? groupedByCedis = null)
        {
            try
            {
                var logs = _logRepository.Find(cedisId, routeId, trip, deliveryDate, processSatus);
                if (groupedByCedis != null && groupedByCedis == true)
                    return Ok(GroupByCedis(logs));
                return Ok(logs);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("api/Log/DataTable")]
        public IHttpActionResult GetLogs(int? cedisId = null, int? routeId = null, int? trip = null, DateTime? deliveryDate = null, int? processStatus = null, int? start = 0, int? length = 100)
        {
            try
            {
                var dbTotalRecords = 0;
                var filteredRecords = 0;
                var logs = _logRepository.Find(out dbTotalRecords, out filteredRecords, cedisId, routeId, trip, deliveryDate, processStatus, start, length);
                return Ok(new { data = logs, filteredRecords = filteredRecords, totalRecords = dbTotalRecords });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("api/Log/{id}")]
        public IHttpActionResult GetLog(int id)
        {
            try
            {
                var log = _logRepository.Find(id);
                return Ok(log);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost]
        [Route("api/Log")]
        public IHttpActionResult CreateLog(Log log)
        {
            try
            {
                log.Id = _logRepository.Insert(log);
                return Content(HttpStatusCode.Created, new { Id = log.Id });
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        [HttpPut]
        [Route("api/Log/{logId}")]
        public IHttpActionResult EditLog(Log log, int logId)
        {
            try
            {
                if (logId != log.Id)
                    return BadRequest("Id en uri no coincide con id de la entidad");
                _logRepository.Edit(log);
                return Content(HttpStatusCode.NoContent, "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private List<CedisLogs> GroupByCedis(List<Log> logs)
        {
            var cedisLogsList = new List<CedisLogs>();
            var logGroups = logs.GroupBy(log => log.CedisId);
            foreach (var logGroup in logGroups)
            {
                var cedisLog = new CedisLogs();
                cedisLog.CedisId = logGroup.First().CedisId;
                logGroup.ToList().ForEach(log => cedisLog.Logs.Add(log));
                cedisLogsList.Add(cedisLog);
            }
            return cedisLogsList;
        }
    }
}