﻿
using API_Integraciones.Controllers.DataAccess;
using API_Integraciones.Models.OpeCD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_Integraciones.Models.Dto;

namespace API_Integraciones.Controllers.Services
{
    public class AdminConfigAutoController : ApiController
    {
        private readonly CONF_ProcesosAutomaticosController _procesosAutomRepository = new CONF_ProcesosAutomaticosController();

        [HttpGet]
        [ResponseType(typeof(List<ConfiguracionAutomatica>))]
        [Route("api/AdminConfigAuto")]
        public IHttpActionResult Get(int? id = null, string nombre = null, string sistema = null, int? cedisId = null, int? ruta = null, string hora = null, bool? activo = null)
        {
            try
            {
                var configuracionesAutomaticas = _procesosAutomRepository.Find(id, nombre, sistema, cedisId, ruta, hora, activo);
                return Ok(configuracionesAutomaticas);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [ResponseType(typeof(ConfiguracionAutomatica))]
        [Route("api/AdminConfigAuto/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var configuracionAutomatica = _procesosAutomRepository.Find(id);
                return Ok(configuracionAutomatica);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [Route("api/AdminConfigAuto")]
        [ResponseType(typeof(PostResponse))]
        public IHttpActionResult Post(ConfiguracionAutomatica configuration)
        {
            try
            {
                ValidateAutomaticConfiguration(configuration);
                if (_procesosAutomRepository.ConfigurationExists(configuration.Ruta, configuration.Cedis))
                    return Content(HttpStatusCode.OK, new PostResponse
                    {
                        Success = false,
                        Message = string.Format("Ya existe una configuracion para el cedis {0} y ruta {1}", configuration.Cedis, configuration.Ruta),
                        Id = 0
                    });
                var configurationId = _procesosAutomRepository.Create(configuration);
                return Content(HttpStatusCode.Created, new PostResponse { Success = true, Message = "Creado", Id = configurationId });
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, new PostResponse { Success = false, Message = ex.Message, Id = 0 });
            }
        }

        [HttpPut]
        [Route("api/AdminConfigAuto/{id}")]
        [ResponseType(typeof(BaseResponse))]
        public IHttpActionResult Put(int id, ConfiguracionAutomatica configuracion)
        {
            try
            {
                if (id != configuracion.Id)
                    return Content(HttpStatusCode.BadRequest, new BaseResponse { Success = false, Message = "Id en uri no coincide con Id de la entidad" });
                ValidateAutomaticConfiguration(configuracion);
                if (SimilarConfigurationExists(configuracion))
                    return Content(HttpStatusCode.OK, new BaseResponse { Success = false, Message = "No editado. Existe una configuracion con el mismo cedis y ruta" });
                _procesosAutomRepository.Edit(configuracion);
                return Content(HttpStatusCode.OK, new BaseResponse { Success = true, Message = "Editado" });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpDelete]
        [Route("api/AdminConfigAuto/{id}")]
        [ResponseType(typeof(BaseResponse))]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _procesosAutomRepository.Delete(id);
                return Ok(new BaseResponse { Success = true, Message = "Eliminado" });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse { Success = false, Message = ex.Message });
            }

        }

        private void ValidateAutomaticConfiguration(ConfiguracionAutomatica automaticConfiguration)
        {
            var errorMessage = string.Empty;
            if (string.IsNullOrEmpty(automaticConfiguration.Nombre))
                errorMessage += "Nombre es requerido, ";
            if (string.IsNullOrEmpty(automaticConfiguration.Sistema))
                errorMessage += "Sistema es requerido, ";
            if (string.IsNullOrEmpty(automaticConfiguration.Hora))
                errorMessage += "Hora es requerido, ";
            if (automaticConfiguration.Cedis <= 0)
                errorMessage += "Cedis es requerido ";
            if (automaticConfiguration.Ruta <= 0)
                errorMessage += "Ruta es requerido";
            if (!string.IsNullOrEmpty(errorMessage))
                throw new ArgumentException(errorMessage);
        }

        private bool SimilarConfigurationExists(ConfiguracionAutomatica automaticConfiguration)
        {
            var similarConfigurationsResult = _procesosAutomRepository.Find(null, null, null, automaticConfiguration.Cedis, automaticConfiguration.Ruta, null)
                                        .Where(configuration => configuration.Id != automaticConfiguration.Id)
                                        .Any();
            return similarConfigurationsResult;
        }
    }
}
