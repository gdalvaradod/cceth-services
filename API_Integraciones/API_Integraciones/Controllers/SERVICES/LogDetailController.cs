﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using API_Integraciones.Models.Intermedia;
using API_Integraciones.Repositories;

namespace API_Integraciones.Controllers.Services
{
    public class LogDetailController : ApiController
    {

        private readonly LogDetailRepository _logDetailRepo = new LogDetailRepository();

        [HttpPost]
        [Route("api/LogDetail")]
        public IHttpActionResult Post(LogDetail logDetail)
        {
            try
            {
                LogDetailValidation(logDetail);
                logDetail.Id = _logDetailRepo.Insert(logDetail);
                return Content(HttpStatusCode.Created, new { Id = logDetail.Id });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void LogDetailValidation(LogDetail logDetail)
        {
            var errorMessage = "";
            if (logDetail.LogId <= 0)
                errorMessage += "logId debe ser mayor a 0; ";
        }
    }
}