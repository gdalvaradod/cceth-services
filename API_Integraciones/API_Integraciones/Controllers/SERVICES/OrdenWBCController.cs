﻿using API_Integraciones.Controllers.DAO;
using API_Integraciones.Controllers.DataAcces;
using API_Integraciones.Models;
using API_Integraciones.Models.AdminPedidos;
using API_Integraciones.Models.WBC;
using API_Integraciones.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using API_Integraciones.Enums;
using API_Integraciones.Controllers.APIs;

namespace API_Integraciones.Controllers.Services
{
    public class OrdenWBCController : ApiController
    {
        private readonly SalesRepository _salesRepository = new SalesRepository();
        private readonly CAT_OrderGeneralController _ordersRepository = new CAT_OrderGeneralController();
        private readonly VisitDaysRepository _visitDaysRepository = new VisitDaysRepository();
        private readonly REF_CedisController _cedisRepository = new REF_CedisController();

        /// <returns></returns>
        [HttpGet]
        [Route("api/OrdenWBC/GetCucGeneric")]
        [ResponseType(typeof(int))]
        public IHttpActionResult GetCucGeneric(int cedis, int route)
        {
            try
            {
                return Ok(new REF_CedisController().FindCucGeneric(cedis, route));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <returns></returns>
        [HttpGet]
        [Route("api/OrdenWBC/GetOrders")]
        [ResponseType(typeof(List<OrderGeneral>))]
        public IHttpActionResult Get()
        {
            try
            {

                CAT_OrderGeneralController ordenGeneral = new CAT_OrderGeneralController();
                List<OrderGeneral> json = ordenGeneral.findValidateOK();
                return Ok(json);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <param name="cedis"/>
        /// <param name="ruta"/>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OrdenWBC/GetOrdersWbc")]
        [ResponseType(typeof(List<OrderGeneral>))]
        public IHttpActionResult GetOrdersWbc(int cedis, int ruta)
        {
            try
            {
                CAT_OrderGeneralController ordenGeneral = new CAT_OrderGeneralController();
                List<OrderGeneral> json = ordenGeneral.findValidateOK(cedis, ruta);
                return Ok(json);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPut]
        [Route("api/OrdenWBC/PutCustomers")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult PutCustomers(List<Customer> customers)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                var customerController = new CAT_CustomersController();
                foreach (var customer in customers)
                {
                    var customerRecord = customerController.FindByIdCustomer(customer.CRMId);
                    customer.WBCId = customer.CustomerId;
                    customer.CustomerId = customerRecord.Id;
                    customer.CustomerType = CustomerType.CLIENTE_PORTAL;
                    if (customerRecord.CRMId == null)
                    {
                        var integrationCustomer = new CAT_CustomersController().Map(customer);
                        integrationCustomer.CreatedOn = DateTime.Now.ToString();
                        integrationCustomer.ModifiedOn = DateTime.Now.ToString();
                        integrationCustomer.AddressType = "shipping";
                        customerController.Create(integrationCustomer);
                    }
                    else
                    {
                        var integrationCustomer = new CAT_CustomersController().Map(customer);
                        integrationCustomer.ModifiedOn = DateTime.Now.ToString();
                        customerController.Edit(integrationCustomer);
                    }
                }

                return Content(HttpStatusCode.NoContent, string.Empty);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Cedisinfo>))]
        [Route("api/OrdenWBC/CedisInfo")]
        public IHttpActionResult GetCedisInfo()
        {
            try
            {
                var cedisInfo = new REF_CedisController().FindCedisInfo();
                return Ok(cedisInfo);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [NonAction]
        public void UpdatePendingOrdersToCompleted(int cedisId, int routeCode, DateTime deliveryDate)
        {
            var sales = _salesRepository.Find(new Sale { CedisId = cedisId, RouteCode = routeCode, DeliveryDate = deliveryDate });
            //var cedisName = _cedisRepository.FindByOpeCdId(cedisId);
            var ordersGroupedByClient = _ordersRepository.FindBy(cedisId.ToString(), routeCode, deliveryDate)
                .Where(order => order.STATUS.ToUpper().Trim().Equals(OrderGeneralStatus.ENVIADO) || order.STATUS.ToUpper().Trim().Equals(OrderGeneralStatus.COMPLETADO))
                .GroupBy(order => order.ORDER_ADDRESSES[0].CUSTOMER_ID);

            foreach (var ordersGroup in ordersGroupedByClient)
            {
                var salesFromGroup = sales.Where(sale => sale.Cuc == Convert.ToInt32(ordersGroup.First().ORDER_ADDRESSES[0].CUSTOMER_ID)).ToList();

                var saleItems = new List<SaleProduct>();
                salesFromGroup.ForEach(sale => saleItems.AddRange(sale.SaleProducts));
                var distinctSaleItems = new List<SaleProduct>();
                foreach (var saleItemGroup in saleItems.GroupBy(item => item.Product))
                {
                    var saleItem = new SaleProduct();
                    saleItem.Product = saleItemGroup.First().Product;
                    saleItemGroup.ToList().ForEach(item => saleItem.Quantity += item.Quantity);
                    distinctSaleItems.Add(saleItem);
                }

                var salePromotions = new List<SalePromotion>();
                salesFromGroup.ForEach(sale => salePromotions.AddRange(sale.SalePromotions));

                foreach (var salePromotionGroup in salePromotions.GroupBy(promotion => promotion.Product))
                {
                    var saleProduct = new SaleProduct();
                    saleProduct.Product = salePromotionGroup.First().Product;
                    salePromotionGroup.ToList().ForEach(promotion => saleProduct.Quantity += promotion.Quantity);

                    var saleItem = distinctSaleItems.SingleOrDefault(item => item.Product == saleProduct.Product);
                    if (saleItem != null)
                    {
                        saleItem.Quantity += saleProduct.Quantity;
                    }
                    else
                    {
                        distinctSaleItems.Add(saleProduct);
                    }
                }

                foreach (var order in ordersGroup.OrderBy(order => order.InterID))
                {
                    var completedOrder = false;
                    foreach (var item in order.ORDER_ITEMS)
                    {
                        var catItem = distinctSaleItems.SingleOrDefault(saleItem => saleItem.Product == int.Parse(item.SKU));
                        if (catItem == null)
                            continue;
                        if (catItem.Quantity > 0)
                            completedOrder = true;

                        var difference = catItem.Quantity - item.QTY_ORDERED;
                        catItem.Quantity = difference < 0 ? 0 : (int)difference;
                    }
                    if (completedOrder && order.STATUS.Equals(OrderGeneralStatus.ENVIADO))
                        _ordersRepository.EditOrderStatus(order.InterID, OrderGeneralStatus.COMPLETADO);
                }

            }
        }

        [NonAction]
        public void RescheduleOrdersToNextTrip(List<Sale> sales)
        {

            var emptySales = sales.Where(sale => !sale.SaleProducts.Any() && !sale.SalePromotions.Any() && sale.RazonDev == Settings.RazonDevolucionAux()).ToList();
            var groupedEmptySales = from saless in emptySales
                                    group saless by new { saless.CedisId, saless.RouteCode, saless.DeliveryDate.Date, saless.Cuc, saless.Trip } into groupedSaless
                                    select groupedSaless;
            foreach (var salesGroup in groupedEmptySales)
            {
                //var cedisName = _cedisRepository.FindByOpeCdId(salesGroup.First().CedisId);
                var ordersToReschedule = _ordersRepository.FindBy(salesGroup.First().CedisId.ToString(), salesGroup.First().RouteCode, salesGroup.First().DeliveryDate)
                                    .Where(order => Convert.ToInt32(order.ORDER_ADDRESSES.First().CUSTOMER_ID) == salesGroup.First().Cuc
                                    && order.BOTTLER_DATA.TRIP_ID == salesGroup.First().Trip).ToList();
                if (ordersToReschedule.Any())
                {
                    foreach (var order in ordersToReschedule)
                    {
                        _ordersRepository.ChangeDeliveryDate(order.InterID, GetNextWorkingDate(DateTime.Today, salesGroup.First().CedisId));
                        _ordersRepository.ChangeOrderStatus(order.InterID, OrderGeneralStatus.PREPARADO);
                    }
                }
            }
        }
        private DateTime GetNextWorkingDate(DateTime date, int cedisId)
        {
            var apiOpeCd = new APIOpeCD();
            var workingDays = apiOpeCd.GetWorkingDays(cedisId);
            var nextDay = date.AddDays(1);
            var workingDay = workingDays.SingleOrDefault(wDay => wDay.Date == nextDay && wDay.IsWorkingDay);
            if (workingDays != null)
                return workingDay.Date;
            return GetNextWorkingDate(nextDay.AddDays(1), cedisId);
        }

        [NonAction]
        public void CancelOrdersWithEmptySales(List<Sale> sales)
        {

            var emptySales = sales.Where(sale => !sale.SaleProducts.Any() && !sale.SalePromotions.Any() && sale.RazonDev != Settings.RazonDevolucionAux()).ToList();
            var groupedEmptySales = from saless in emptySales
                                    group saless by new { saless.CedisId, saless.RouteCode, saless.DeliveryDate.Date, saless.Cuc, saless.Trip } into groupedSaless
                                    select groupedSaless;
            foreach (var salesGroup in groupedEmptySales)
            {
                //var cedisName = _cedisRepository.FindByOpeCdId(salesGroup.First().CedisId);
                var ordersToCancel = _ordersRepository.FindBy(salesGroup.First().CedisId.ToString(), salesGroup.First().RouteCode, salesGroup.First().DeliveryDate)
                                    .Where(order => Convert.ToInt32(order.ORDER_ADDRESSES.First().CUSTOMER_ID) == salesGroup.First().Cuc
                                    && order.BOTTLER_DATA.TRIP_ID == salesGroup.First().Trip).ToList();
                if (ordersToCancel.Any())
                    ordersToCancel.ForEach(order => _ordersRepository.EditOrderStatus(order.InterID, OrderGeneralStatus.CANCELADO));
            }
        }
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Customers>))]
        [Route("api/OrdenWBC/GetListCustomers")]
        public IHttpActionResult GetListCustomers(int cedi, int route)
        {
            try
            {
                List<Customers> customers = new CAT_CustomersController().FindByCustomerList(cedi, route);
                if (customers == null)
                    return Content(HttpStatusCode.OK, customers);
                return Ok(customers);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
