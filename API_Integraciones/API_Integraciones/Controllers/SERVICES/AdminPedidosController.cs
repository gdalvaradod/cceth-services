﻿using API_Integraciones.Controllers.APIs;
using API_Integraciones.Controllers.DAO;
using API_Integraciones.Controllers.DataAccess;
using API_Integraciones.Models;
using API_Integraciones.Models.AdminPedidos;
using API_Integraciones.Repositories;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace API_Integraciones.Controllers
{
    public class AdminPedidosController : ApiController
    {
        /// <summary>
        /// Devuelve un listado de todas las ordenes.
        /// </summary>
        /// <returns></returns>
        /// [HttpGet]
        [Route("api/Pedidos/GetALL")]
        public List<OrderGeneral> GetALL()
        {
            try
            {
                CAT_OrderGeneralController ordenGeneral = new CAT_OrderGeneralController();
                List<OrderGeneral> json = ordenGeneral.find();
                return json;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        /// Devuelve un listado agrupado de las ordenes.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/Pedidos")]
        public IHttpActionResult Get(int cedisId, DateTime? deliveryDate = null, string route = null, string status = null, int? start = 0, int? length = int.MaxValue)
        {
            try
            {
                CAT_OrderGeneralController ordenGeneral = new CAT_OrderGeneralController();
                var totalRecords = 0;
                List<PedidosEnc> orders = ordenGeneral.findAllPedidosEnc(cedisId, out totalRecords, deliveryDate, route, status);
                return Ok(new { data = orders.OrderByDescending(ordr => ordr.ORD_OPECDId).Skip((int)start).Take((int)length), filteredRecords = orders.Count, totalRecords = totalRecords });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        /// Devuelve un listado de las ordenes a nivel cliente/producto.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("API/AdminPedidos/{id}")]
        public List<PedidosDet> Get(int id, string date, string status, string route)
        {
            try
            {
                DateTime tempDate = Convert.ToDateTime(date);
                CAT_OrderGeneralController ordenGeneral = new CAT_OrderGeneralController();
                List<PedidosDet> json = ordenGeneral.findAllPedidosDet(id, tempDate, status, route);
                return json;
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Devuelve un pedido por ID 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("API/AdminPedidos/GetById/{id}")]
        public PedidosGET GetById(int id)
        {
            try
            {
                PedidosGET pedido = new PedidosGET();
                pedido = new CAT_OrderGeneralController().findOrderById(id);
                return pedido;
            }
            catch (Exception)
            {

                throw;
            }

        }
        [HttpPost]
        [Route("api/Pedidos")]
        public object Post([FromBody]PedidoPOST value)
        {
            try
            {
                CAT_OrderGeneralController orderGeneral = new CAT_OrderGeneralController();
                CAT_BottlerDataController bottlerData = new CAT_BottlerDataController();
                CAT_OrderAddressesController orderAddresses = new CAT_OrderAddressesController();
                CAT_OrderItemsController orderItems = new CAT_OrderItemsController();
                CAT_CustomersController customer = new CAT_CustomersController();

                if (!new CAT_CustomersController().FindByBarCode(value.cliente))
                    return Content(HttpStatusCode.OK, "El Barcode no existe.");
                var orderCustomer = customer.FindByBarCodeCustomer(value.cliente);
                var orderStatus = string.IsNullOrEmpty(orderCustomer.CRMId) ? OrderGeneralStatus.PENDIENTE : OrderGeneralStatus.PREPARADO;
                orderGeneral.CreateAP(Convert.ToInt32(orderCustomer.Cuc), value.PaymentMethod, orderStatus);
                var orderGeneralId = orderGeneral.ViewLast();
                var OGeneral = orderGeneral.FindByIdcreator(orderGeneralId);
                value.DELIVERY_DATE = CalculateDeliveryDate(value.cedis, Convert.ToInt32(value.ROUTE_CODE), Convert.ToInt32(orderCustomer.Id), Settings.FechaHoraActual(), DateTime.Parse(value.DELIVERY_DATE)).ToString("yyyy/MM/dd");
                bottlerData.CreateAP(value, orderGeneralId, OGeneral.ORD_CreateFrom);
                orderItems.Create(value.Productos, orderGeneralId);
                orderAddresses.Create(value.cliente, orderGeneralId, value.PaymentMethod, value.WantBill, orderCustomer.Cuc);
                return true;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Actualiza la orden a CONFIRMADO para OPECD
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("API/AdminPedidos/{id}")]
        public IHttpActionResult Put(int Id, [FromBody]List<string> value)
        {
            try
            {
                List<int> notEditedOrderIds = new List<int>();
                foreach (var key in value)
                {
                    if (new CAT_OrderGeneralController().CanSetOrderStatusToPrepared(Convert.ToInt32(key)))
                    {
                        CAT_OrderGeneralController orderGeneral = new CAT_OrderGeneralController();
                        var edited = orderGeneral.Edit(Convert.ToInt32(key));
                        if (!edited)
                            notEditedOrderIds.Add(int.Parse(key));
                    }
                }
                return Ok(notEditedOrderIds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPut]
        [Route("API/AdminPedidos/PutOrder/{id}")]
        public object PutOrder(int Id, [FromBody]PedidoPUT value)
        {
            try
            {
                if (new CAT_OrderGeneralController().validateOrdenById(Id))
                {
                    var orderCustomer = new CAT_CustomersController().FindByBarCodeCustomer(value.cliente);
                    value.DELIVERY_DATE = value.DELIVERY_DATE = CalculateDeliveryDate(value.cedis, Convert.ToInt32(value.ROUTE_CODE), Convert.ToInt32(orderCustomer.Id), Settings.FechaHoraActual(), DateTime.Parse(value.DELIVERY_DATE)).ToString("yyyy/MM/dd");
                    new CAT_BottlerDataController().Edit(Id, value);
                    if (new CAT_CustomersController().validateCustomerById(value.cliente))
                    {
                        new CAT_OrderAddressesController().Edit(value.cliente, Id, value.PaymentMethod);
                    }
                    new CAT_OrderItemsController().Edit(value.Productos, value.Removes, Id);
                }
                else
                {
                    throw new InvalidOperationException("No se puede editar el pedido esta ya fue Confirmado");
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Inventario/carga base por cedis
        /// </summary>
        /// <param name="cedisId"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<InventoryEnc>))]
        [Route("api/Inventory/GetInventoryEnc/{cedisId}")]
        public IHttpActionResult GetInventoryEnc(int cedisId)
        {
            try
            {
                List<InventoryEnc> listInventory = new ENC_TripsController().FindListInventoryEnc(cedisId);
                if (listInventory == null)
                    return Content(HttpStatusCode.OK, listInventory);
                return Ok(listInventory);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Detalle del inventario por inventoryId
        /// </summary>
        /// <param name="InventoryId"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(InventoryDet))]
        [Route("api/Inventory/GetInventoryDet/{InventoryId}")]
        public IHttpActionResult GetInventoryDet(int InventoryId)
        {
            try
            {
                List<InventoryDet> listInventory = new REF_TripItemsController().FindInventoryDetById(InventoryId);
                if (listInventory == null)
                    return Content(HttpStatusCode.OK, listInventory);
                return Ok(listInventory);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private HttpResponseMessage validateOK(bool OrdenGeneral, bool BottlerData, bool Items, bool Addresses, string accion, string Msj)
        {
            HttpResponseMessage response = null;
            string value = "";
            if (accion == "POST")
            {

                if (OrdenGeneral && BottlerData && Items)
                {
                    value = "completado!";
                    response = Request.CreateResponse(HttpStatusCode.OK, value);
                }
                else
                {
                    value = "Error de insercion en: ";
                    if (!OrdenGeneral)
                    {
                        value += "OrdenGeneral ";
                    }
                    if (!BottlerData)
                    {
                        value += "BottlerData ";
                    }
                    if (!Items)
                    {
                        value += "Items ";
                    }
                    if (!Addresses)
                    {
                        value += "Items ";
                    }
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, value);
                }
            }
            if (accion == "PUT")
            {

                if (OrdenGeneral)
                {
                    value = "completado!";
                    response = Request.CreateResponse(HttpStatusCode.OK, value);
                }
                else
                {
                    value = "Error de insercion en: ";
                    if (!BottlerData)
                    {
                        value += "BottlerData ";
                    }
                    if (!Items)
                    {
                        value += "Items ";
                    }
                    value += Msj;

                    response = Request.CreateResponse(HttpStatusCode.OK, Msj);
                }
            }
            return response;
        }
        private DateTime CalculateDeliveryDate(int cedisId, int routeCode, int customerId, DateTime orderCreationDate, DateTime orderDeliveryDate)
        {
            
            var automaticConfiguration = new CONF_ProcesosAutomaticosController().Find(null, null, null, cedisId, routeCode, null, true).SingleOrDefault();
            if (automaticConfiguration == null)
                throw new Exception("No se encontro la configuracion automatica para el cedis " + cedisId + " y la ruta " + routeCode);

            var currentDateTime = Settings.FechaHoraActual();
            var timeArray = automaticConfiguration.Hora.Split(':');
            var payDeskDate = new DateTime(currentDateTime.Year, currentDateTime.Month, currentDateTime.Day, Convert.ToInt32(timeArray[0]), Convert.ToInt32(timeArray[1]), 0);

            if (orderCreationDate > payDeskDate && orderDeliveryDate.Date == currentDateTime.Date.AddDays(1))
                return GetNextWorkingDate(orderDeliveryDate, cedisId);
            return orderDeliveryDate;
        }
        private DateTime GetNextWorkingDate(DateTime currentDeliveryDate, int cedisId)
        {
            var apiOpeCd = new APIOpeCD();
            var workingDays = apiOpeCd.GetWorkingDays(cedisId);
            var nextDay = currentDeliveryDate.AddDays(1);
            var workingDay = workingDays.SingleOrDefault(wDay => wDay.Date == nextDay && wDay.IsWorkingDay);
            if (workingDays != null)
                return workingDay.Date;
            return GetNextWorkingDate(nextDay.AddDays(1), cedisId);
        }
    }
}
