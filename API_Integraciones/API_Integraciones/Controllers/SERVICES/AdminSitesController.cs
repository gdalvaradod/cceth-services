﻿using API_Integraciones.Controllers.APIs;
using API_Integraciones.Models.WBC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API_Integraciones.Controllers.Services
{
    public class AdminSitesController : ApiController
    {
        private APIWBCController wbcApi = new APIWBCController();

        [HttpGet]
        [Route("api/Sites")]
        public List<Site> GetSites()
        {
            try
            {
                var token = wbcApi.Authenticate(Settings.AuthCredentials());
                return wbcApi.GetSites(token);
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
