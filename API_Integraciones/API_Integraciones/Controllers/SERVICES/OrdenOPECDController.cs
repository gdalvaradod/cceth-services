﻿using API_Integraciones.Controllers.DataAccess;
using API_Integraciones.Models;
using API_Integraciones.Models.OpeCD;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_Integraciones.Repositories;
using API_Integraciones.Controllers.APIs;

namespace API_Integraciones.Controllers
{
    public class OrdenOPECDController : ApiController
    {
        private readonly VisitDaysRepository _visitDaysRepository = new VisitDaysRepository();
        private readonly CONF_ProcesosAutomaticosController _automaticConfigRepository = new CONF_ProcesosAutomaticosController();
        private readonly CAT_OrderGeneralController _orderGeneralRepository = new CAT_OrderGeneralController();

        /// <param name="Status"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<OrderGeneral>))]
        [Route("api/OrdenOpeCd/GetOrders/{Status}")]
        public IHttpActionResult Get(int Status)
        {
            try
            {

                CAT_OrderGeneralController ordenGeneral = new CAT_OrderGeneralController();
                List<OrderGeneral> json = ordenGeneral.find(Status);
                return Ok(json);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <param name="status"/>
        /// <param name="cedis"/>
        /// <param name="ruta"/>
        /// <param name="deliveryDate"/>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<OrderGeneral>))]
        [Route("api/OrdenOpeCd/GetOrders/")]
        public IHttpActionResult Get(int status, int cedis, int ruta, string deliveryDate = "")
        {
            try
            {

                CAT_OrderGeneralController ordenGeneral = new CAT_OrderGeneralController();
                List<OrderGeneral> json = ordenGeneral.find(status, cedis, ruta, deliveryDate);
                return Ok(json);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <param name="barcode"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<OrderGeneral>))]
        [Route("api/OrdenOpeCd/GetOrderCliente/{barcode}")]
        public IHttpActionResult GetOrderCliente(string barcode)
        {
            try
            {
                CAT_OrderGeneralController ordenGeneral = new CAT_OrderGeneralController();
                List<OrderGeneral> json = ordenGeneral.findOrderCliente(barcode);
                return Ok(json);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Accion = 0 → Cuando se actualize solo el viaje.
        /// Accion = 1 → Cuando se actualize solo el OpeCDId.
        /// </summary>
        /// <param name="Accion"></param>
        /// <param name="value"></param>
        [HttpPut]
        [Route("api/OrdenOpeCd/PutOrder/{Accion}")]
        public IHttpActionResult Put(int Accion, [FromBody]List<OpeCDPUT> orders)
        {
            try
            {
                var invalidOrders = SubstractInvalidOrders(orders);
                if (!orders.Any())
                    return Content(HttpStatusCode.Conflict, invalidOrders);
                CAT_OrderGeneralController ordenGeneral = new CAT_OrderGeneralController();
                ordenGeneral.Edit(orders, Accion);
                return Content(HttpStatusCode.OK, invalidOrders);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpGet]
        [ResponseType(typeof(ConfiguracionAutomatica))]
        [Route("api/OrdenOpeCd/GetConfigAuto")]
        public IHttpActionResult GetConfigAuto(int cedis, int route)
        {
            try
            {
                CONF_ProcesosAutomaticosController auto = new CONF_ProcesosAutomaticosController();
                if (!auto.validateCedis(cedis))
                    return Content(HttpStatusCode.NoContent, "El Cedis no es valido.");
                ConfiguracionAutomatica configAuto = auto.FindConfigAuto(cedis, route);
                if (configAuto == null)
                    return Content(HttpStatusCode.NoContent, "No hay configuracion para el cedis: ." + cedis);
                return Ok(configAuto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Status = 2 - PREPARADO 
        /// </summary>
        /// <param name="Status"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/OrdenOpeCd/PutStatusAuto/")]
        public IHttpActionResult PutStatusAuto(int Status, int cedi, int route)
        {
            try
            {
                if (Status == 2)
                {
                    var currentDateTime = Settings.FechaHoraActual();
                    var pendigOrders = new CAT_OrderGeneralController().GetPendingOrders(cedi, route);

                    foreach (var order in pendigOrders)
                    {

                        var cedisName = Convert.ToInt32(order.BOTTLER_DATA.CEDI_ID);
                        var customerId = Convert.ToInt32(order.ORDER_ADDRESSES[0].CUSTOMER_ID);
                        var deliveryDate = DateTime.Parse(order.BOTTLER_DATA.DELIVERY_DATE);

                        var visitDays = _visitDaysRepository.Find(customerId);
                        var automaticConfiguration = _automaticConfigRepository.Find(null, null, null, cedisName, route, null, true).SingleOrDefault();

                        if (automaticConfiguration != null && visitDays != null)
                        {
                            var timeArray = automaticConfiguration.Hora.Split(':');
                            var customHour = Convert.ToInt32(timeArray[0]);
                            var customMinutes = Convert.ToInt32(timeArray[1]);
                            var customDateTime = new DateTime(currentDateTime.Year, currentDateTime.Month, currentDateTime.Day, customHour, customMinutes, 0);

                            var beforeConfigurationHour = currentDateTime < customDateTime;
                            var isCCTHCustomer = order.CUSTOMER_ID != null && order.CUSTOMER_ID > 0;
                            
                            if (isCCTHCustomer)
                                deliveryDate = GetNextWorkingDate(deliveryDate, cedi);

                            if (!isCCTHCustomer && beforeConfigurationHour)
                                deliveryDate = visitDays.ProximaVisita(currentDateTime);

                            if (!isCCTHCustomer && !beforeConfigurationHour)
                                deliveryDate = visitDays.ProximaVisita(visitDays.ProximaVisita(currentDateTime));

                            _orderGeneralRepository.ChangeDeliveryDate(order.InterID, deliveryDate);
                        }
                        _orderGeneralRepository.ChangeOrderStatus(order.InterID, OrderGeneralStatus.PREPARADO);
                    }
                }
                return Content(HttpStatusCode.OK, "Completado.");

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private DateTime GetNextWorkingDate(DateTime currentDeliveryDate, int cedisId)
        {
            var apiOpeCd = new APIOpeCD();
            var workingDays = apiOpeCd.GetWorkingDays(cedisId);
            var nextDay = currentDeliveryDate.AddDays(1);
            var workingDay = workingDays.SingleOrDefault(wDay => wDay.Date == nextDay && wDay.IsWorkingDay);
            if (workingDays != null)
                return workingDay.Date;
            return GetNextWorkingDate(nextDay.AddDays(1), cedisId);
        }
        private bool OrderHasCCTHCustomer(int orderGeneralId)
        {
            var orderGeneral = _orderGeneralRepository.FindById(orderGeneralId);
            if (orderGeneral.CUSTOMER_ID == null || orderGeneral.CUSTOMER_ID <= 0)
                return false;
            return true;
        }
        private HttpResponseMessage validateOK(bool ordenGeneral, string Accion, string Msj)
        {
            HttpResponseMessage response = null;
            string value = "";
            if (Accion == "PUT")
            {
                if (ordenGeneral)
                {
                    value = "completado!";
                    response = Request.CreateResponse(HttpStatusCode.OK, value);
                }
                else
                {
                    value = "Error de actualización en: cedis " + Msj;
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, value);
                }
            }
            return response;
        }
        private List<OpeCDPUT> SubstractInvalidOrders(List<OpeCDPUT> orders)
        {
            var invalidOrders = orders.Where(order => order.DeliveryOrderId < 1);
            orders.RemoveAll(order => order.DeliveryOrderId < 1);
            return invalidOrders.ToList();
        }

    }
}
