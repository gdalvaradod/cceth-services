﻿using API_Integraciones.Controllers.APIs;
using API_Integraciones.Mappers;
using API_Integraciones.Models.OpeCD;
using API_Integraciones.Models.WBC;
using API_Integraciones.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace API_Integraciones.Controllers.Services
{
    public class VentaWBCController : ApiController
    {
        private readonly SalesRepository _salesRepository = new SalesRepository();
        private readonly PrecioClienteCocaRepository _precioClienteRepository = new PrecioClienteCocaRepository();

        [HttpPost]
        [Route("api/VentasWBC/IntegrationSales")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult PostIntegrationSales(List<Sale> sales, int cedisId)
        {
               
                
            try
            {
                var ordenWbc = new OrdenWBCController();
                ordenWbc.RescheduleOrdersToNextTrip(sales);
                ordenWbc.CancelOrdersWithEmptySales(sales);
                _salesRepository.BulkInsert(sales);

                var groupedSales = from saless in sales
                                   group saless by new { saless.CedisId, saless.RouteCode, saless.DeliveryDate.Date } into groupedSaless
                                   select groupedSaless;

                foreach (var salesGroup in groupedSales)
                {
                    ordenWbc.UpdatePendingOrdersToCompleted(salesGroup.First().CedisId, salesGroup.First().RouteCode, salesGroup.First().DeliveryDate.Date);
                }
                return Content(HttpStatusCode.Created, true);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpGet]
        [Route("api/VentasWBC/IntegrationSales")]
        [ResponseType(typeof(List<Sale>))]
        public IHttpActionResult GetIntegrationSales(int? cedisId = null, int? routeId = null, DateTime? deliveryDate = null)
        {
            try
            {
                var integrationSales = _salesRepository.Find(new Sale {
                    CedisId = (int)cedisId,
                    RouteCode = (int)routeId,
                    DeliveryDate = (DateTime)deliveryDate
                });
                return Ok(integrationSales);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
    }
}
