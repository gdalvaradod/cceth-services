﻿using API_Integraciones.Repositories;
using System;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using API_Integraciones.Models.PrecioClienteCoca;

namespace API_Integraciones.Controllers.Services
{
    public class PrecioClienteCocaController : ApiController
    {
        private readonly PrecioClienteCocaRepository _precioRepository = new PrecioClienteCocaRepository();

        // GET: PrecioClienteCoca
        [HttpGet]
        [ResponseType(typeof(PrecioClienteCoca))]
        [Route("api/PrecioClienteCoca")]
        public IHttpActionResult GetPrecioClienteCoca(int? cedisId = null, string barcode = "", DateTime? deliveryDate = null)
        {
            try
            {
                var precios = _precioRepository.Find(cedisId, barcode, deliveryDate);
                return Ok(precios);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

    }
}