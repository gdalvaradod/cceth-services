﻿using API_Integraciones.Controllers.DataAcces;
using API_Integraciones.Models;
using API_Integraciones.Models.AdminPedidos;
using API_Integraciones.Models.AdminUsers;
using API_Integraciones.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace API_Integraciones.Controllers.Services
{
    public class AdminUsersController : ApiController
    {
        private CAT_UsersController userDao;
        private REF_CedisController cedisDao;

        public AdminUsersController()
        {
            userDao = new CAT_UsersController();
            cedisDao = new REF_CedisController();
        }

        [HttpPost]
        [Route("api/AdminUsers/Login")]
        [ResponseType(typeof(LoginResponse))]
        public IHttpActionResult Login(LoginRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                var user = userDao.Find(new User { Username = request.Username, Password = request.Password, Enabled = 1 }).SingleOrDefault();
                if (user == null)
                    return Ok(new LoginResponse { Access = false, Message = "No se encontro el usuario" });
                var cedis = new REF_CedisController().FindCedisByUserId(user.Id).SingleOrDefault(c => c.CedisIdOpecd == request.CedisId);
                if (cedis == null)
                    return Ok(new LoginResponse { Access = false, Message = "El usuario no tiene acceso al cedis solicitado" });
                return Ok(new LoginResponse { User = user, Cedis = cedis, Access = true });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("api/AdminUsers/GetUserCedis/")]
        [ResponseType(typeof(List<Cedis>))]
        public IHttpActionResult GetUserCedis([FromUri]string username)
        {
            try
            {
                var user = userDao.Find(new User { Username = username, Enabled = 1 }).SingleOrDefault();
                if (user == null)
                    return Ok(new List<Cedis>());
                var cedisList = cedisDao.FindCedisByUserId(user.Id);
                return Ok(cedisList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}