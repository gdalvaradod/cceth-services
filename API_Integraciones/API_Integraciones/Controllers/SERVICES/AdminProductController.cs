﻿using API_Integraciones.Controllers.APIs;
using API_Integraciones.Controllers.DataAcces;
using API_Integraciones.Models.AdminPedidos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace API_Integraciones.Controllers.Services
{
    public class AdminProductController : ApiController
    {
        /// <summary>
        /// Obtiene los productos que existen en Ope y/o en intermedia
        /// </summary>
        /// <param name="cedis">Identificador del cedis</param>
        /// <param name="ruta">Identificador de la ruta</param>
        /// <returns></returns>
        [HttpGet]
        [Route("API/AdminProduct/{cedis}")]
        [ResponseType(typeof(List<ProductsGET>))]
        public IHttpActionResult Get(int cedis)
        {
            try
            {
                var products = new CAT_ProductsController().FindAll(cedis);
                var opeProducts = new APIOpeCD().GetProducts(cedis).Where(opeProd => opeProd.Active);
                var filteredProducts = new List<ProductsGET>();
                foreach (var opeProduct in opeProducts)
                {
                    var productGet = new ProductsGET();
                    productGet.Nombre = opeProduct.Name.TrimEnd();
                    productGet.CodigoProducto = opeProduct.ItemId;
                    productGet.Tipo = opeProduct.Type;
                    productGet.Ruta = 0;
                    productGet.Cedis = cedis;
                    filteredProducts.Add(productGet);
                }
                return Ok(filteredProducts);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Obtiene los productos que existen en intermedia
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("API/AdminProduct/GetProducts")]
        [ResponseType(typeof(List<ProductsGET>))]
        public IHttpActionResult GetProducts(int cedisId)
        {
            try
            {
                var productsList = new CAT_ProductsController().FindBy(cedisId);
                return Ok(productsList);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpGet]
        [ResponseType(typeof(List<SearchProduct>))]
        [Route("api/Product/GetSearchFilter")]
        public List<SearchProduct> GetSearchFilter(int cedi, int route, string Name = null, string Code = null)
        {
            try
            {
                SearchProduct search = new SearchProduct()
                {
                    Name = Name,
                    Code = Code
                };
                return new CAT_ProductsController().SearchAdvanceProducts(cedi, route, search);
            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpPost]
        [Route("API/AdminProduct/Productos")]
        public Object Post(int cedi, [FromBody]ProductsPOST value)
        {
            try
            {
                if (value.Code.Count() == 0 || value.Ruta.Count == 0)
                    return Content(HttpStatusCode.OK, "hay valores vacios.");
                var products = new CAT_ProductsController().FindAll(cedi);
                var opeProducts = new APIOpeCD().GetProducts(cedi).Where(opeProd => opeProd.Active);
                var filteredProducts = new List<ProductsGET>();
                if (!opeProducts.Any())
                    return Content(HttpStatusCode.InternalServerError, "No hay conexión a OPECD.");
                foreach (var opeProduct in opeProducts)
                {
                    var productGet = new ProductsGET();
                    productGet.Nombre = opeProduct.Name.TrimEnd();
                    productGet.CodigoProducto = opeProduct.ItemId;
                    productGet.Tipo = opeProduct.Type;
                    productGet.Ruta = 0;
                    productGet.Cedis = cedi;
                    productGet.WBCId = 0;
                    filteredProducts.Add(productGet);
                }
                string msj;
                foreach (var OpeProduct in filteredProducts)
                {
                    foreach (var itemCode in value.Code)
                    {
                        foreach (var itemRoute in value.Ruta)
                        {
                            if (itemCode == OpeProduct.CodigoProducto)
                            {
                                if (!interExist(products, itemCode, Convert.ToInt32(itemRoute)))
                                {
                                    OpeProduct.Ruta = Convert.ToInt32(itemRoute);
                                    OpeProduct.CreateDate = DateTime.Now;
                                    new CAT_ProductsController().Create(OpeProduct);
                                }
                            }
                        }
                    }
                }
                return Content(HttpStatusCode.Created, "Creado");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPut]
        [Route("API/AdminProduct/PutProducts")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult PutProducts([FromBody]List<ProductsGET> products)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                string msj = "";
                new CAT_ProductsController().Update(products);
                return Ok(true);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private HttpResponseMessage validateOK(bool Product, string Accion, string Msj)
        {

            HttpResponseMessage response;
            string value = "";
            if (Accion == "PUT")
            {
                if (Product)
                {
                    value = "completado!";
                }
                else
                {
                    value = "Error de actualización en: Producto - " + Msj;
                }
            }
            if (Accion == "POST")
            {
                if (Product)
                {
                    value = "completado!";
                }
                else
                {
                    value = "Error de inserción en: Producto - " + Msj;
                }
            }
            response = Request.CreateResponse(HttpStatusCode.OK, value);
            return response;
        }

        private bool interExist(List<ProductsGET> products, string code, int route)
        {
            bool resp = false;
            List<ProductsGET> Rows = products.Where(p => p.CodigoProducto == code && p.Ruta == route).ToList();
            if (Rows.Count() > 0)
            {
                resp = true;
            }
            return resp;
        }

    }
}
