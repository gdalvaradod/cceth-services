﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_Integraciones.Controllers.DataAccess;
using API_Integraciones.Models.WBC;

namespace API_Integraciones.Controllers.Services
{
    public class JornadasFinalizadasController : ApiController
    {
        private readonly ClosedJourneyDAO _closedJourneyDao = new ClosedJourneyDAO();

        [HttpGet]
        [Route("api/jornadasFinalizadas")]
        [ResponseType(typeof(List<ClosedJourney>))]
        public IHttpActionResult Get(int? cedisId = null, int? routeId = null, DateTime? deliveryDate = null)
        {
            try
            {
                var closedJourneys = _closedJourneyDao.Find(cedisId, routeId, deliveryDate);
                return Ok(closedJourneys);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPost]
        [Route("api/jornadasFinalizadas")]
        [ResponseType(typeof(ClosedJourney))]
        public IHttpActionResult Post(ClosedJourney closedJourney)
        {
            try
            {
                ClosedJourneyValidation(closedJourney);
                if (ClosedJourneyExists(closedJourney.CedisIdOpecd, closedJourney.Route, closedJourney.DeliveryDate))
                    return Content(HttpStatusCode.Conflict, "No insertado. Ya existe un registro con los mismos datos");
                _closedJourneyDao.Add(closedJourney);
                return Created(Request.RequestUri, closedJourney);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpDelete]
        [Route("api/jornadasFinalizadas")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult Delete(int? cedisId, int? routeId, DateTime? deliveryDate)
        {
            try
            {
                if (!IsDeleteUriValid(cedisId, routeId, deliveryDate))
                    return BadRequest("cedis, ruta y fecha de entrega no pueden ser nulos");
                var deleted = _closedJourneyDao.Delete((int)cedisId, (int)routeId, (DateTime)deliveryDate);
                return Ok(deleted);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }

        }

        private bool ClosedJourneyExists(int cedisId, int routeId, DateTime deliveryDate)
        {
            return _closedJourneyDao.Find((int?)cedisId, (int?)routeId, (DateTime?)deliveryDate).Count == 1;
        }

        private bool IsPutUriValid(int? cedisId, int? routeId, DateTime? deliveryDate)
        {
            if (cedisId == null || routeId == null || deliveryDate == null)
                return false;
            return true;
        }

        private bool IsDeleteUriValid(int? cedisId, int? routeId, DateTime? deliveryDate)
        {
            if (cedisId == null || routeId == null || deliveryDate == null)
                return false;
            return true;
        }

        private void ClosedJourneyValidation(ClosedJourney closedJourney)
        {
            var errorMessage = "";
            if (closedJourney.CedisIdOpecd <= 0)
                errorMessage += "cedisId debe ser mayor a 0.";
            if(closedJourney.Route <= 0)
                errorMessage += " rutaId debe ser mayor a 0.";
            if (closedJourney.DeliveryDate == null)
                errorMessage += "fecha de entrega no puede ser null.";
            if (!string.IsNullOrEmpty(errorMessage))
                throw new ArgumentException(errorMessage);
        }
    }
}
