﻿using API_Integraciones.Controllers.APIs;
using API_Integraciones.Controllers.DataAcces;
using API_Integraciones.Models.AdminPedidos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API_Integraciones.Controllers.Services
{

    public class AdminCedisController : ApiController
    {
        [HttpGet]
        [Route("api/Cedis")]
        public List<Cedis> Get()
        {
            try
            {
                REF_CedisController CedisController = new REF_CedisController();
                return CedisController.FindAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        [Route("api/Cedis")]
        public Object Post([FromBody]Cedis Cedis)
        {
            try
            {
                REF_CedisController CedisController = new REF_CedisController();
                string msj, APIs = null;
                var cedisRecord = CedisController.FindAll().Where(cedis => cedis.CedisNameCCHT == Cedis.CedisNameCCHT && cedis.Nombre == Cedis.Nombre && cedis.CedisIdOpecd == Cedis.CedisIdOpecd);
                if (cedisRecord != null && cedisRecord.Any())
                    return validateOK(false, "POST", "El cedis ya esta dado de alta. ", APIs);
                CedisController.Create(Cedis);
                var wbcApi = new APIWBCController();
                var wbcToken = wbcApi.Authenticate(Settings.AuthCredentials());
                var wbcBranch = wbcApi.getBranchCode(Cedis.CedisIdOpecd.ToString(), wbcToken);
                if (wbcBranch == null)
                    APIs = wbcApi.POST(Cedis, wbcToken);
                return Ok(true);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPut]
        [Route("api/Cedis/{id}")]
        public Object Put(int id, int branchCode, [FromBody]CedisPUT Cedis)
        {
            try
            {
                REF_CedisController CedisController = new REF_CedisController();
                CedisController.Edit(id, Cedis);
                APIWBCController APIWBC = new APIWBCController();
                var token = APIWBC.Authenticate(Settings.AuthCredentials());
                var branch = APIWBC.getBranchCode(branchCode.ToString(), token).SingleOrDefault();
                string APIs = APIWBC.Put(branch.branchId, Cedis, APIWBC.Authenticate(Settings.AuthCredentials()));
                return Ok(true);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpDelete]
        [Route("api/Cedis/{id}")]
        public Object Delete(int id)
        {
            try
            {
                REF_CedisController CedisController = new REF_CedisController();
                CedisController.Delete(id);
                return Ok(true);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private HttpResponseMessage validateOK(bool Cedis, string Accion, string Msj, string APIs = null)
        {
            HttpResponseMessage response;
            string value = "";
            if (Accion == "PUT")
            {
                if (Cedis)
                {
                    value = "completado!";
                }
                else
                {
                    value = "Error de actualización en: cedis " + Msj;
                }
            }
            if (Accion == "POST")
            {
                if (Cedis)
                {
                    value = "completado!";
                }
                else
                {
                    value = "Error de inserción en: cedis " + Msj;
                }
            }
            response = Request.CreateResponse(HttpStatusCode.OK, value);
            return response;
        }

    }
}
