﻿using API_Integraciones.Models.WBC;
using API_Integraciones.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using API_Integraciones.Controllers.APIs;
using API_Integraciones.Controllers.DataAcces;

namespace API_Integraciones.Controllers.Services
{
    public class InventarioWBCController : ApiController
    {

        private readonly TripRepository _tripRepository = new TripRepository();
        private readonly CAT_OrderGeneralController _orderRespository = new CAT_OrderGeneralController();
        private readonly OrderItemRepository _orderItemRepository = new OrderItemRepository();
        private readonly REF_CedisController _cedisRepository = new REF_CedisController();

        [HttpPost]
        [Route("api/Inventario")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult Post(PostTripRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                _tripRepository.BulkUpsert(request.TripList, request.CedisId);
                RescheduleOrdersToNextTrip(request.CedisId, request.TripList);
                //CancelOrdersWithNoInventory(request.CedisId, request.TripList);
                return Content(HttpStatusCode.Created, true);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void CancelOrdersWithNoInventory(int cedisId, List<Trip> trips)
        {
            //var cedisName = _cedisRepository.FindByOpeCdId(cedisId);
            var groupedTrips = from tripss in trips
                               group tripss by new { tripss.Route, tripss.DeliveryDate.Date } into groupedTripss
                               select groupedTripss;

            foreach (var tripsGroup in groupedTrips)
            {
                var orders = _orderRespository.FindBy(cedisId.ToString(), tripsGroup.First().Route, tripsGroup.First().DeliveryDate.Date)
                    .Where(order => order.STATUS.ToUpper().Trim().Equals(OrderGeneralStatus.VALIDADO));

                var inventoryList = _tripRepository.GetInventory(cedisId, tripsGroup.First().Route, tripsGroup.First().DeliveryDate.Date).OrderBy(trip => trip.Id);
                Trip inventory = new Trip();
                if (inventoryList.Any())
                    inventory = inventoryList.Last();
                else
                    break;
                foreach (var order in orders.OrderBy(order => order.InterID))
                {
                    var orderItemsNotFound = 0;
                    foreach (var orderItem in order.ORDER_ITEMS)
                    {
                        var inventoryItem = inventory.ConfirmedItems.SingleOrDefault(invItem => invItem.ItemId == orderItem.SKU);
                        if (inventoryItem == null)
                        {
                            orderItemsNotFound++;
                            _orderItemRepository.EditOrderItemQty(orderItem.InterId, 0);
                            continue;
                        }
                        else
                        {
                            if (inventoryItem.Qty <= 0)
                            {
                                orderItemsNotFound++;
                                _orderItemRepository.EditOrderItemQty(orderItem.InterId, 0);
                                continue;
                            }
                            var difference = inventoryItem.Qty - (int)orderItem.QTY_ORDERED;
                            if (difference < 0)
                            {
                                _orderItemRepository.EditOrderItemQty(orderItem.InterId, inventoryItem.Qty);
                            }
                            inventoryItem.Qty =
                                difference < 0 ? 0 : difference;
                        }
                    }
                    if (orderItemsNotFound == order.ORDER_ITEMS.Count())
                        _orderRespository.EditOrderStatus(order.InterID, OrderGeneralStatus.CANCELADO);
                    else
                        _orderRespository.EditOrderStatus(order.InterID, OrderGeneralStatus.ENVIADO);
                }
            }

        }

        [NonAction]
        public void RescheduleOrdersToNextTrip(int cedisId, List<Trip> trips)
        {
            //var cedisName = _cedisRepository.FindByOpeCdId(cedisId);
            var groupedTrips = from tripss in trips
                               group tripss by new { tripss.Route, tripss.DeliveryDate.Date } into groupedTripss
                               select groupedTripss;

            foreach (var tripsGroup in groupedTrips)
            {
                var orders = _orderRespository.FindBy(cedisId.ToString(), tripsGroup.First().Route, tripsGroup.First().DeliveryDate.Date)
                    .Where(order => order.STATUS.ToUpper().Trim().Equals(OrderGeneralStatus.VALIDADO));

                var inventoryList = _tripRepository.GetInventory(cedisId, tripsGroup.First().Route, tripsGroup.First().DeliveryDate.Date).OrderBy(trip => trip.Id);
                Trip inventory = new Trip();
                if (inventoryList.Any())
                    inventory = inventoryList.Last();
                else
                    break;
                foreach (var order in orders.OrderBy(order => order.InterID))
                {
                    var orderItemsNotFound = 0;
                    foreach (var orderItem in order.ORDER_ITEMS)
                    {
                        var inventoryItem = inventory.ConfirmedItems.SingleOrDefault(invItem => invItem.ItemId == orderItem.SKU);
                        if (inventoryItem == null)
                        {
                            orderItemsNotFound++;
                            _orderItemRepository.EditOrderItemQty(orderItem.InterId, 0);
                            continue;
                        }
                        else
                        {
                            if (inventoryItem.Qty <= 0)
                            {
                                orderItemsNotFound++;
                                _orderItemRepository.EditOrderItemQty(orderItem.InterId, 0);
                                continue;
                            }
                            var difference = inventoryItem.Qty - (int)orderItem.QTY_ORDERED;
                            if (difference < 0)
                            {
                                _orderItemRepository.EditOrderItemQty(orderItem.InterId, inventoryItem.Qty);
                            }
                            inventoryItem.Qty =
                                difference < 0 ? 0 : difference;
                        }
                    }
                    if (orderItemsNotFound == order.ORDER_ITEMS.Count())
                    {
                        _orderRespository.ChangeDeliveryDate(order.InterID, GetNextWorkingDate(DateTime.Today, cedisId));
                        _orderRespository.ChangeOrderStatus(order.InterID, OrderGeneralStatus.PREPARADO);
                        //_orderRespository.EditOrderStatus(order.InterID, OrderGeneralStatus.CANCELADO);
                    }
                    else
                        _orderRespository.EditOrderStatus(order.InterID, OrderGeneralStatus.ENVIADO);
                }
            }
        }

        private DateTime GetNextWorkingDate(DateTime date, int cedisId)
        {
            var apiOpeCd = new APIOpeCD();
            var workingDays = apiOpeCd.GetWorkingDays(cedisId);
            var nextDay = date.AddDays(1);
            var workingDay = workingDays.SingleOrDefault(wDay => wDay.Date == nextDay && wDay.IsWorkingDay);
            if (workingDays != null)
                return workingDay.Date;
            return GetNextWorkingDate(nextDay.AddDays(1), cedisId);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        [HttpPost]
        [Route("api/Inventario/CancelUpdateOrderInventory")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult CancelUpdateOrderInventory(PostTripRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                //var cedisName = _cedisRepository.FindByOpeCdId(request.CedisId);
                var trips = request.TripList;
                var groupedTrips = from tripss in trips
                                   group tripss by new { tripss.Route, tripss.DeliveryDate.Date } into groupedTripss
                                   select groupedTripss;

                foreach (var tripsGroup in groupedTrips)
                {
                    var orders = _orderRespository.FindBy(request.CedisId.ToString(), tripsGroup.First().Route, tripsGroup.First().DeliveryDate.Date)
                        .Where(order => order.STATUS.ToUpper().Trim().Equals(OrderGeneralStatus.VALIDADO));

                    var inventoryList = _tripRepository.GetInventory(request.CedisId, tripsGroup.First().Route, tripsGroup.First().DeliveryDate.Date).OrderBy(trip => trip.Id);
                    Trip inventory;
                    if (inventoryList.Any())
                        inventory = inventoryList.Last();
                    else
                        break;
                    foreach (var order in orders.OrderBy(order => order.InterID))
                    {
                        var orderItemsNotFound = 0;
                        foreach (var orderItem in order.ORDER_ITEMS)
                        {
                            var inventoryItem = inventory.ConfirmedItems.SingleOrDefault(invItem => invItem.ItemId == orderItem.SKU);
                            if (inventoryItem == null)
                            {
                                orderItemsNotFound++;
                                _orderItemRepository.EditOrderItemQty(orderItem.InterId, 0);
                                continue;
                            }
                            else
                            {
                                if (inventoryItem.Qty <= 0)
                                {
                                    orderItemsNotFound++;
                                    _orderItemRepository.EditOrderItemQty(orderItem.InterId, 0);
                                    continue;
                                }
                                var difference = inventoryItem.Qty - (int)orderItem.QTY_ORDERED;
                                if (difference < 0)
                                {
                                    _orderItemRepository.EditOrderItemQty(orderItem.InterId, inventoryItem.Qty);
                                }
                                inventoryItem.Qty =
                                    difference < 0 ? 0 : difference;
                            }
                        }
                        _orderRespository.EditOrderStatus(order.InterID,
                            orderItemsNotFound == order.ORDER_ITEMS.Count()
                                ? OrderGeneralStatus.CANCELADO
                                : OrderGeneralStatus.ENVIADO);
                    }
                }
                return Content(HttpStatusCode.Created, true);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpGet]
        [Route("api/Inventario")]
        [ResponseType(typeof(Trip))]
        public IHttpActionResult Get(int cedisId, int routeId, DateTime deliveryDate, bool withSales = true)
        {
            try
            {
                var inventory = new List<Trip>();
                if (withSales)
                    inventory = _tripRepository.GetInventory(cedisId, routeId, deliveryDate);
                else
                    inventory = _tripRepository.GetInventoryWithoutSales(cedisId, routeId, deliveryDate);
                return Ok(inventory);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

    }
}
