﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models;
using API_Integraciones.Models.AdminPedidos;
using API_Integraciones.Models.OpeCD;
using Newtonsoft.Json.Linq;
using API_Integraciones.Controllers.DataAcces;
using API_Integraciones.Controllers.DAO;
using API_Integraciones.Enums;

namespace API_Integraciones.Controllers
{
    public class CAT_OrderGeneralController : Controller
    {
        private CCTH_INTERMEDIAEntities db = new CCTH_INTERMEDIAEntities();

        public CAT_OrderGeneralController()
        {

        }

        public CAT_OrderGeneralController(CCTH_INTERMEDIAEntities db)
        {
            this.db = db;
        }
        public List<OrderGeneral> GetPendingOrders(int cedisId, int route)
        {
            var foundOrders = (from ordenGenral in db.CAT_OrderGeneral
                               join bottler in db.CAT_BottlerData on ordenGenral.Id equals bottler.BOT_OrderGeneralId
                               where ordenGenral.ORD_StatusOrden == OrderGeneralStatus.PENDIENTE &&
                               ordenGenral.ORD_OPECDId == 0 &&
                               bottler.BOT_CediName == cedisId.ToString() &&
                               bottler.BOT_RouteCode == route.ToString() 
                               select ordenGenral).ToList();

            List<OrderGeneral> orders = MapJson(foundOrders);
            return orders;
        }
        public void ChangeOrderStatus(int orderId, string orderStatus)
        {
            var orderGeneral = db.CAT_OrderGeneral.Find(orderId);
            if (orderGeneral != null)
            {
                orderGeneral.ORD_StatusOrden = orderStatus;
                db.SaveChanges();
            }
        }
        public void ChangeDeliveryDate(int orderId, DateTime deliveryDate)
        {
            var orderGeneral = db.CAT_OrderGeneral.Find(orderId);
            if (orderGeneral != null)
            {
                var bottlerData = orderGeneral.CAT_BottlerData.ToList().FirstOrDefault();
                if (bottlerData == null)
                    return;
                bottlerData.BOT_DeliveryDate = deliveryDate;
                db.SaveChanges();
            }
        }

        public bool CanSetOrderStatusToPrepared(int orderId, string routeCode = null, DateTime? deliveryDate = null, string cedisName = null)
        {
            var orderGeneral = db.CAT_OrderGeneral.Find(orderId);
            if (!orderGeneral.ORD_StatusOrden.Equals(OrderGeneralStatus.PENDIENTE))
                return false;

            var customerId = orderGeneral.ORD_CustomerId;
            
            if(routeCode == null)
            {
                routeCode = orderGeneral.CAT_BottlerData.First().BOT_RouteCode;
            }
            if(deliveryDate == null)
            {
                deliveryDate = orderGeneral.CAT_BottlerData.First().BOT_DeliveryDate;
            }
            if(cedisName == null)
            {
                cedisName = orderGeneral.CAT_BottlerData.First().BOT_CediName;
            }
            
            var similarOrders = from orders in db.CAT_OrderGeneral
                                join bottlerData in db.CAT_BottlerData on orders.Id equals bottlerData.BOT_OrderGeneralId
                                where orders.ORD_CustomerId == customerId &&
                                bottlerData.BOT_RouteCode == routeCode &&
                                bottlerData.BOT_DeliveryDate == deliveryDate &&
                                bottlerData.BOT_CediName == cedisName
                                select orders;

            var pendingAndConfirmedOrdersCount = similarOrders.Count(ordr => 
                ordr.ORD_StatusOrden.ToUpper().Equals(OrderGeneralStatus.PREPARADO) ||
                ordr.ORD_StatusOrden.ToUpper().Equals(OrderGeneralStatus.CONFIRMADO)
            );

            if (pendingAndConfirmedOrdersCount >= 1)
                return false;
            return true;
        }

        public List<PedidosDet> findAllPedidosDet(int OpeCDId, DateTime date, string status, string route)
        {
            CAT_OrderAddressesController AddressesController = new CAT_OrderAddressesController();
            CAT_OrderItemsController ItemsController = new CAT_OrderItemsController();
            List<PedidosDet> listPEdidosDet = new List<PedidosDet>();

            var listOrden = from orderss in db.CAT_OrderGeneral
                            join bottlerData in db.CAT_BottlerData on orderss.Id equals bottlerData.BOT_OrderGeneralId
                            where bottlerData.BOT_RouteCode == route && orderss.ORD_OPECDId == OpeCDId && orderss.ORD_StatusOrden == status
                            select orderss;

            foreach (var key in listOrden)
            {
                if (validaExtisBottler(key.CAT_BottlerData, date, key.Id))
                {
                    OrderAddresses Addresses = new OrderAddresses();
                    if (key.CAT_OrderAddresses != null && key.CAT_OrderAddresses.Any())
                    {
                        var orderAddress = key.CAT_OrderAddresses.SingleOrDefault(oAddress => oAddress.ORE_AddressType == "billing" && oAddress.ORE_WantBill == 1);

                        if (orderAddress == null)
                            orderAddress = key.CAT_OrderAddresses.SingleOrDefault(oAddress => oAddress.ORE_AddressType == "shipping");

                        Addresses = AddressesController.MapAddressesJson(orderAddress);
                    }
                        
                    List<OrderItem> ListProducto = ItemsController.MapItemsJson(key.CAT_OrderItems);

                    PedidosDet pedidoDet = new PedidosDet()
                    {
                        Id = key.Id,
                        Cliente = Addresses,
                        Producto = ListProducto,
                        CanBeSetToPrepared = CanSetOrderStatusToPrepared(key.Id)
                    };
                    listPEdidosDet.Add(pedidoDet);
                }
            }

            return listPEdidosDet;
        }
        /// <summary>
        /// busca todas las ordenes confirmadas por adminpedidos
        /// </summary>
        /// <returns></returns>
        public List<OrderGeneral> find()
        {
            List<CAT_OrderGeneral> orden = db.CAT_OrderGeneral.ToList();
            List<OrderGeneral> ordenJson = MapJson(orden);
            return ordenJson;
        }
        public CAT_OrderGeneral FindByIdcreator(int id)
        {
            return db.CAT_OrderGeneral.Find(id);
        }
        public OrderGeneral FindById(int id)
        {
            var catOrderGeneral = db.CAT_OrderGeneral.Find(id);
            if (catOrderGeneral == null)
                return null;
            var orderGeneralList = new List<CAT_OrderGeneral>() { catOrderGeneral };
            return MapJson(orderGeneralList).First();
        }
        public List<OrderGeneral> findOrderCliente(string barcode)
        {
            //var orden = db.CAT_OrderGeneral.Where(o => o.ORD_StatusOrden == "ENVIADO").ToList();
            var orden = db.CAT_OrderGeneral.Where(o => o.ORD_StatusOrden != "PENDIENTE").ToList();
            var ordenJson = MapJson(orden);

            return ordenJson.FindAll(x => x.ORDER_ADDRESSES.Any(y => y.BarCode == barcode) && DateTime.Parse(x.BOTTLER_DATA.DELIVERY_DATE).ToString("yyyy-MM-dd") == DateTime.Today.ToString("yyyy-MM-dd"));
        }
        public List<OrderGeneral> find(int opeIdOK)
        {
            List<OrderGeneral> ordenJson = null;

            List<CAT_OrderGeneral> orden = null;
            if (opeIdOK == 0)
                orden = db.CAT_OrderGeneral.Where(o => o.ORD_StatusOrden == "PENDIENTE").ToList();
            //opeIdOK == 1 → no tienen OPECDID
            if (opeIdOK == 1)
                orden = db.CAT_OrderGeneral.Where(o => o.ORD_StatusOrden == "PREPARADO" && o.ORD_OPECDId == 0).ToList();
            //opeIdOK == 2 → si tienen OPECDID
            if (opeIdOK == 2)
                orden = db.CAT_OrderGeneral.Where(o => o.ORD_StatusOrden == "CONFIRMADO").ToList();
            if (opeIdOK == 3)
                orden = db.CAT_OrderGeneral.Where(o => o.ORD_StatusOrden == "VALIDADO").ToList();
            if (opeIdOK == 4)
                orden = db.CAT_OrderGeneral.Where(o => o.ORD_StatusOrden == "ENVIADO").ToList();
            if (opeIdOK == 5)
                orden = db.CAT_OrderGeneral.Where(o => o.ORD_StatusOrden == "REPROGRAMADO").ToList();
            if (opeIdOK == 6)
                orden = db.CAT_OrderGeneral.Where(o => o.ORD_StatusOrden == "COMPLETADO").ToList();
            if (opeIdOK == 7)
                orden = db.CAT_OrderGeneral.Where(o => o.ORD_StatusOrden == "CANCELADO").ToList();
            ordenJson = MapJson(orden);

            return ordenJson;
        }
        public List<OrderGeneral> find(int status, int cedis, int ruta, string deliveryDate)
        {
            List<OrderGeneral> ordenJson = null;
            var cedisName = db.REF_Cedis.Single(ced => ced.REC_CedisIdOpecd == cedis && ced.REC_Enable == 1).REC_CedisIdOpecd.ToString();
            List<CAT_OrderGeneral> orden = null;
            if (status == 0)
            {
                if (string.IsNullOrEmpty(deliveryDate))
                {
                    orden =
                        db.CAT_OrderGeneral.Where(
                            o =>
                                o.ORD_StatusOrden == "PENDIENTE" &&
                                o.CAT_BottlerData.FirstOrDefault().BOT_CediName == cedisName &&
                                o.CAT_BottlerData.FirstOrDefault().BOT_RouteCode == ruta.ToString()).ToList();
                }
                else
                {
                    var dateTime = DateTime.Parse(deliveryDate);
                    orden =
                        db.CAT_OrderGeneral.Where(
                                o =>
                                    o.ORD_StatusOrden == "PENDIENTE" &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_CediName == cedisName &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_RouteCode == ruta.ToString() &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_DeliveryDate == dateTime)
                            .ToList();
                }
            }
            //opeIdOK == 1 → no tienen OPECDID
            if (status == 1)
            {
                if (string.IsNullOrEmpty(deliveryDate))
                {
                    orden =
                        db.CAT_OrderGeneral.Where(
                            o =>
                                o.ORD_StatusOrden == "PREPARADO" && o.ORD_OPECDId == 0 &&
                                o.CAT_BottlerData.FirstOrDefault().BOT_CediName == cedisName &&
                                o.CAT_BottlerData.FirstOrDefault().BOT_RouteCode == ruta.ToString()).ToList();
                }
                else
                {
                    var dateTime = DateTime.Parse(deliveryDate);
                    orden =
                        db.CAT_OrderGeneral.Where(
                                o =>
                                    o.ORD_StatusOrden == "PREPARADO" && o.ORD_OPECDId == 0 &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_CediName == cedisName &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_RouteCode == ruta.ToString() &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_DeliveryDate == dateTime)
                            .ToList();
                }
            }
            //opeIdOK == 2 → si tienen OPECDID
            if (status == 2)
            {
                if (string.IsNullOrEmpty(deliveryDate))
                {
                    orden =
                        db.CAT_OrderGeneral.Where(
                            o =>
                                o.ORD_StatusOrden == "CONFIRMADO" &&
                                o.CAT_BottlerData.FirstOrDefault().BOT_CediName == cedisName &&
                                o.CAT_BottlerData.FirstOrDefault().BOT_RouteCode == ruta.ToString()).ToList();
                }
                else
                {
                    var dateTime = DateTime.Parse(deliveryDate);
                    orden =
                        db.CAT_OrderGeneral.Where(
                            o =>
                                o.ORD_StatusOrden == "CONFIRMADO" &&
                                o.CAT_BottlerData.FirstOrDefault().BOT_CediName == cedisName &&
                                o.CAT_BottlerData.FirstOrDefault().BOT_RouteCode == ruta.ToString() &&
                                o.CAT_BottlerData.FirstOrDefault().BOT_DeliveryDate == dateTime).ToList();
                }
            }
            if (status == 3)
            {
                if (string.IsNullOrEmpty(deliveryDate))
                {
                    orden =
                        db.CAT_OrderGeneral.Where(
                            o =>
                                o.ORD_StatusOrden == "VALIDADO" &&
                                o.CAT_BottlerData.FirstOrDefault().BOT_CediName == cedisName &&
                                o.CAT_BottlerData.FirstOrDefault().BOT_RouteCode == ruta.ToString()).ToList();
                }
                else
                {
                    var dateTime = DateTime.Parse(deliveryDate);
                    orden =
                        db.CAT_OrderGeneral.Where(
                                o =>
                                    o.ORD_StatusOrden == "VALIDADO" &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_CediName == cedisName &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_RouteCode == ruta.ToString() &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_DeliveryDate == dateTime)
                            .ToList();
                }
            }
            if (status == 4)
            {
                if (string.IsNullOrEmpty(deliveryDate))
                {
                    orden =
                        db.CAT_OrderGeneral.Where(
                            o =>
                                o.ORD_StatusOrden == "ENVIADO" &&
                                o.CAT_BottlerData.FirstOrDefault().BOT_CediName == cedisName &&
                                o.CAT_BottlerData.FirstOrDefault().BOT_RouteCode == ruta.ToString()).ToList();
                }
                else
                {
                    var dateTime = DateTime.Parse(deliveryDate);
                    orden =
                        db.CAT_OrderGeneral.Where(
                                o =>
                                    o.ORD_StatusOrden == "ENVIADO" &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_CediName == cedisName &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_RouteCode == ruta.ToString() &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_DeliveryDate == dateTime)
                            .ToList();
                }
            }
            if (status == 5)
            {
                if (string.IsNullOrEmpty(deliveryDate))
                {
                    orden =
                        db.CAT_OrderGeneral.Where(
                            o =>
                                o.ORD_StatusOrden == "REPROGRAMADO" &&
                                o.CAT_BottlerData.FirstOrDefault().BOT_CediName == cedisName &&
                                o.CAT_BottlerData.FirstOrDefault().BOT_RouteCode == ruta.ToString()).ToList();
                }
                else
                {
                    var dateTime = DateTime.Parse(deliveryDate);
                    orden =
                        db.CAT_OrderGeneral.Where(
                                o =>
                                    o.ORD_StatusOrden == "REPROGRAMADO" &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_CediName == cedisName &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_RouteCode == ruta.ToString() &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_DeliveryDate == dateTime)
                            .ToList();
                }
            }
            if (status == 6)
            {
                if (string.IsNullOrEmpty(deliveryDate))
                {
                    orden =
                        db.CAT_OrderGeneral.Where(
                            o =>
                                o.ORD_StatusOrden == "COMPLETADO" &&
                                o.CAT_BottlerData.FirstOrDefault().BOT_CediName == cedisName &&
                                o.CAT_BottlerData.FirstOrDefault().BOT_RouteCode == ruta.ToString()).ToList();
                }
                else
                {
                    var dateTime = DateTime.Parse(deliveryDate);
                    orden =
                        db.CAT_OrderGeneral.Where(
                                o =>
                                    o.ORD_StatusOrden == "COMPLETADO" &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_CediName == cedisName &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_RouteCode == ruta.ToString() &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_DeliveryDate == dateTime)
                            .ToList();
                }
            }
            if (status == 7)
            {
                if (string.IsNullOrEmpty(deliveryDate))
                {
                    orden =
                        db.CAT_OrderGeneral.Where(
                            o =>
                                o.ORD_StatusOrden == "CANCELADO" &&
                                o.CAT_BottlerData.FirstOrDefault().BOT_CediName == cedisName &&
                                o.CAT_BottlerData.FirstOrDefault().BOT_RouteCode == ruta.ToString()).ToList();
                }
                else
                {
                    var dateTime = DateTime.Parse(deliveryDate);
                    orden =
                        db.CAT_OrderGeneral.Where(
                                o =>
                                    o.ORD_StatusOrden == "CANCELADO" &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_CediName == cedisName &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_RouteCode == ruta.ToString() &&
                                    o.CAT_BottlerData.FirstOrDefault().BOT_DeliveryDate == dateTime)
                            .ToList();
                }
            }
            ordenJson = MapJson(orden);

            return ordenJson;
        }
        public OrderGeneral FindByIncrementId(string incrementId)
        {
            var orderGeneral = db.CAT_OrderGeneral.SingleOrDefault(order => order.ORD_IncrementId == incrementId);
            if (orderGeneral != null)
            {
                var catOrderGeneralList = new List<CAT_OrderGeneral>();
                catOrderGeneralList.Add(orderGeneral);
                return MapJson(catOrderGeneralList).First();
            }
            return null;
        }
        /// <summary>
        /// busca todas las ordenes validadas por ope
        /// </summary>
        /// <returns></returns>
        public List<OrderGeneral> findValidateOK()
        {
            List<OrderGeneral> ordenJson = new List<OrderGeneral>();
            List<CAT_OrderGeneral> orden = db.CAT_OrderGeneral.Where(o => o.ORD_StatusOrden == "ENVIADO").ToList();
            ordenJson = MapJson(orden);

            return ordenJson;
        }
        /// <summary>
        /// busca todas las ordenes validadas por ope
        /// </summary>
        /// <returns></returns>
        public List<OrderGeneral> findValidateOK(int cedis, int ruta)
        {
            List<OrderGeneral> ordenJson = new List<OrderGeneral>();
            var cedisIdOpeCd = db.REF_Cedis.Single(ced => ced.REC_CedisIdOpecd == cedis).REC_CedisIdOpecd;
            List<CAT_OrderGeneral> orden = db.CAT_OrderGeneral.Where(o => o.ORD_StatusOrden == "ENVIADO" && o.CAT_BottlerData.FirstOrDefault().BOT_CediName == cedisIdOpeCd.ToString() && o.CAT_BottlerData.FirstOrDefault().BOT_RouteCode == ruta.ToString()).ToList();
            ordenJson = MapJson(orden);

            return ordenJson;
        }
        public List<OrderGeneral> FindBy(string cedisName, int routeCode, DateTime deliveryDate)
        {
            List<CAT_OrderGeneral> foundOrders = db.CAT_OrderGeneral.Include(order => order.CAT_BottlerData).ToList();
            foundOrders.RemoveAll(x => x.CAT_BottlerData == null || x.CAT_BottlerData.Count == 0);
            foundOrders = foundOrders.Where(order =>
            order.CAT_BottlerData.ToList()[0].BOT_RouteCode == routeCode.ToString() &&
            order.CAT_BottlerData.ToList()[0].BOT_DeliveryDate == deliveryDate.Date &&
            order.CAT_BottlerData.ToList()[0].BOT_CediName == cedisName
        ).ToList();
            List<OrderGeneral> orders = MapJson(foundOrders);
            return orders;
        }
        public List<PedidosEnc> findAllPedidosEnc(int cedisId, out int dbTotalRecords, DateTime? deliveryDate, string route, string status)
        {
            List<PedidosEnc> ordenJson = null;

            var orden = db.CAT_OrderGeneral.ToList();
            if (deliveryDate != null)
                orden = orden.Where(ordr => ordr.CAT_BottlerData.First().BOT_DeliveryDate == deliveryDate).ToList();
            if (!string.IsNullOrEmpty(route))
                orden = orden.Where(ordr => ordr.CAT_BottlerData.First().BOT_RouteCode == route).ToList();
            if (status != null)
                orden = orden.Where(ordr => ordr.ORD_StatusOrden.ToUpper() == status.ToUpper()).ToList();
            dbTotalRecords = orden.Count();
            ordenJson = MapOrdenGeneral(orden.ToList());

            var Agrupar = (from c in ordenJson
                           group c by new { c.ORD_OPECDId, c.DELIVERY_DATE, c.ROUTE_CODE, c.Trip, c.ORD_StatusOrden, c.ORD_CreateFrom, c.CedisId }
                into g
                           where g.Count() >= 1 && g.Key.CedisId == cedisId
                           orderby g.Key.DELIVERY_DATE /*g.Key.SucursalCode, g.Key.Clave*/
                           select g).ToList();
            ordenJson.RemoveRange(0, ordenJson.Count);
            foreach (var key in Agrupar)
            {
                //string tempDate = "";
                //if (key.Key.DELIVERY_DATE != null)
                //{
                //    string Datetime = Convert.ToString(key.Key.DELIVERY_DATE);
                //    tempDate = Datetime.Substring(0, 10).Replace("/", "-");
                //}

                PedidosEnc Pedido = new PedidosEnc()
                {
                    ORD_OPECDId = key.Key.ORD_OPECDId,
                    DELIVERY_DATE = key.Key.DELIVERY_DATE,
                    ROUTE_CODE = key.Key.ROUTE_CODE,
                    Trip = key.Key.Trip,
                    ORD_StatusOrden = key.Key.ORD_StatusOrden,
                    ORD_CreateFrom = key.Key.ORD_CreateFrom,
                };
                ordenJson.Add(Pedido);
            }

            return ordenJson;
        }
        public PedidosGET findOrderById(int InterId)
        {
            PedidosGET pedido = new PedidosGET();
            if (validateOrdenById(InterId))
            {
                CAT_BottlerData bottler = new CAT_BottlerDataController().findByOrdenId(InterId);
                List<OrderItem> itmes = new CAT_OrderItemsController().findListById(InterId);
                CAT_OrderAddresses customer = new CAT_OrderAddressesController().findAddressesByInterId(InterId);
                pedido.InterId = InterId;
                //pedido.cedis = db.REF_Cedis.Single(cedis => cedis.REC_CedisNameCCHT.ToUpper() == bottler.BOT_CediName.ToUpper()).REC_CedisIdOpecd;
                pedido.cedis = Convert.ToInt32(bottler.BOT_CediName);
                pedido.DELIVERY_DATE = Convert.ToString(bottler.BOT_DeliveryDate);
                pedido.ROUTE_CODE = bottler.BOT_RouteCode;
                pedido.cliente = customer.ORE_BarCode;
                pedido.PaymentMethod = customer.ORE_PaymentMethod;
                pedido.WantBill = (short)customer.ORE_WantBill;
                pedido.name = customer.ORE_Firstname;
                pedido.Productos = itmes;
            }
            return pedido;
        }

        public bool validateOrdenById(int OrdenId)
        {
            bool resp = false;

            CAT_OrderGeneral OrG = db.CAT_OrderGeneral.First(o => (o.Id == OrdenId && o.ORD_StatusOrden == "PREPARADO") || (o.Id == OrdenId && o.ORD_StatusOrden == "PENDIENTE") || (o.Id == OrdenId && o.ORD_StatusOrden == "CANCELADO"));
            if (OrG != null)
            {
                resp = true;
            }
            return resp;
        }
        //public bool Create(OrderGeneral OrderGeneral)
        //{
        //    bool resp = false;
        //    try{
        //        if (ModelState.IsValid)
        //        {
        //            db.CAT_OrderGeneral.Add(Map(OrderGeneral));
        //            db.SaveChanges();
        //            resp = true;
        //        }
        //    }catch(Exception e){
        //        var error = e.Message;
        //        resp = false;
        //    }
        //    return resp;
        //}

        public int Create(OrderGeneral orderGeneral)
        {
            var catOrderGeneral = Map(orderGeneral);
            db.CAT_OrderGeneral.Add(catOrderGeneral);
            db.SaveChanges();
            return catOrderGeneral.Id;
        }

        public void Delete(int orderId)
        {
            var order = db.CAT_OrderGeneral.Find(orderId);
            if (order != null)
                db.CAT_OrderGeneral.Remove(order);
            db.SaveChanges();
        }

        public void CreateAP(int customerId, string paymentMethod, string orderStatus)
        {
            CAT_OrderGeneral ORDER = new CAT_OrderGeneral()
            {
                Id = 0,
                ORD_EntityId = 0,
                ORD_Status = "pending",
                ORD_CouponCode = null,
                ORD_CustomerId = customerId,
                ORD_ShippingDescription = null,
                ORD_BaseDiscountAmount = 0M,
                ORD_BaseGrandTotal = 0M,
                ORD_BaseShippingAmount = 0M,
                ORD_BaseShippingTaxAmount = 0M,
                ORD_BaseSubtotal = 0M,
                ORD_BaseTaxAmount = 0M,
                ORD_BaseTotalPaid = 0M,
                ORD_BaseTotalRefunded = 0M,
                ORD_DiscountAmount = 0M,
                ORD_GrandTotal = 0M,
                ORD_ShippingAmount = 0M,
                ORD_ShippingTaxAmount = 0M,
                ORD_StoreToOrderRate = 0M,
                ORD_Subtotal = 0M,
                ORD_TaxAmount = 0M,
                ORD_TotalPaid = 0M,
                ORD_TotalQtyOrdered = 0M,
                ORD_TotalRefunded = 0M,
                ORD_BaseShippingDiscountAmount = 0M,
                ORD_BaseSubtotalInclTax = 0M,
                ORD_BaseTotalDue = 0M,
                ORD_ShippingDiscountAmount = 0M,
                ORD_SubtotalInclTax = 0M,
                ORD_TotalDue = 0M,
                ORD_IncrementId = "0",
                ORD_AppliedRuleIds = "0",
                ORD_BaseCurrencyCode = "0",
                ORD_CustomerEmail = "0",
                ORD_DiscountDescription = null,
                ORD_RemoteIp = null,
                ORD_StoreCurrencyCode = null,
                ORD_StoreName = null,
                ORD_CreatedAt = Convert.ToString(DateTime.Now),
                ORD_TotalItemCount = 0,
                ORD_ShippingInclTax = 0M,
                ORD_BaseCustomerBalanceAmount = 0M,
                ORD_CustomerBalanceAmount = 0M,
                ORD_BaseGiftCardsAmount = 0M,
                ORD_GiftCardsAmount = 0M,
                ORD_RewardPointsBalance = 0,
                ORD_BaseRewardCurrencyAmount = 0M,
                ORD_RewardCurrencyAmount = 0M,
                ORD_PaymentMethod = paymentMethod,
                ORD_OPECDId = 0,
                ORD_Comments = null,
                ORD_StatusOrden = orderStatus.ToUpper(),
                ORD_CreateFrom = OrderCreatedAt.PORTAL,
                ORD_CreateDateInter = DateTime.Now,
                ORD_Method_title = null,
                ORD_Cc_las_4 = null,
                ORD_Cc_name = null
            };
            db.CAT_OrderGeneral.Add(ORDER);
            db.SaveChanges();
        }

        public int ViewLast()
        {
            List<CAT_OrderGeneral> lista = db.CAT_OrderGeneral.ToList();
            CAT_OrderGeneral order = lista.Last();
            return order.Id;
        }
        private CAT_OrderGeneral Map(OrderGeneral OrG)
        {
            CAT_OrderGeneral ORDER = new CAT_OrderGeneral()
            {
                Id = 0,
                ORD_EntityId = OrG.ENTITY_ID,
                ORD_Status = OrG.STATUS,
                ORD_CouponCode = OrG.COUPON_CODE,
                ORD_CustomerId = OrG.CUSTOMER_ID,
                ORD_ShippingDescription = OrG.SHIPPING_DESCRIPTION,
                ORD_BaseDiscountAmount = OrG.BASE_DISCOUNT_AMOUNT,
                ORD_BaseGrandTotal = OrG.BASE_GRAND_TOTAL,
                ORD_BaseShippingAmount = OrG.BASE_SHIPPING_AMOUNT,
                ORD_BaseShippingTaxAmount = OrG.BASE_SHIPPING_TAX_AMOUNT,
                ORD_BaseSubtotal = OrG.BASE_SUBTOTAL,
                ORD_BaseTaxAmount = OrG.BASE_TAX_AMOUNT,
                ORD_BaseTotalPaid = OrG.BASE_TOTAL_PAID,
                ORD_BaseTotalRefunded = OrG.BASE_TOTAL_REFUNDED,
                ORD_DiscountAmount = OrG.DISCOUNT_AMOUNT,
                ORD_GrandTotal = OrG.GRAND_TOTAL,
                ORD_ShippingAmount = OrG.SHIPPING_AMOUNT,
                ORD_ShippingTaxAmount = OrG.SHIPPING_TAX_AMOUNT,
                ORD_StoreToOrderRate = OrG.STORE_TO_ORDER_RATE,
                ORD_Subtotal = OrG.SUBTOTAL,
                ORD_TaxAmount = OrG.TAX_AMOUNT,
                ORD_TotalPaid = OrG.TOTAL_PAID,
                ORD_TotalQtyOrdered = OrG.TOTAL_QTY_ORDERED,
                ORD_TotalRefunded = OrG.TOTAL_REFUNDED,
                ORD_BaseShippingDiscountAmount = OrG.BASE_SHIPPING_DISCOUNT_AMOUNT,
                ORD_BaseSubtotalInclTax = OrG.BASE_SUBTOTAL_INCL_TAX,
                ORD_BaseTotalDue = OrG.BASE_TOTAL_DUE,
                ORD_ShippingDiscountAmount = OrG.SHIPPING_DISCOUNT_AMOUNT,
                ORD_SubtotalInclTax = OrG.SUBTOTAL_INCL_TAX,
                ORD_TotalDue = OrG.TOTAL_DUE,
                ORD_IncrementId = OrG.INCREMENT_ID,
                ORD_AppliedRuleIds = OrG.APPLIED_RULE_IDS,
                ORD_BaseCurrencyCode = OrG.BASE_CURRENCY_CODE,
                ORD_CustomerEmail = OrG.CUSTOMER_EMAIL,
                ORD_DiscountDescription = OrG.DISCOUNT_DESCRIPTION,
                ORD_RemoteIp = OrG.REMOTE_IP,
                ORD_StoreCurrencyCode = OrG.STORE_CURRENCY_CODE,
                ORD_StoreName = OrG.STORE_NAME,
                ORD_CreatedAt = OrG.CREATED_AT,
                ORD_TotalItemCount = OrG.TOTAL_ITEM_COUNT,
                ORD_ShippingInclTax = OrG.SHIPPING_INCL_TAX,
                ORD_BaseCustomerBalanceAmount = OrG.BASE_CUSTOMER_BALANCE_AMOUNT,
                ORD_CustomerBalanceAmount = OrG.CUSTOMER_BALANCE_AMOUNT,
                ORD_BaseGiftCardsAmount = OrG.BASE_GIFT_CARDS_AMOUNT,
                ORD_GiftCardsAmount = OrG.GIFT_CARDS_AMOUNT,
                ORD_RewardPointsBalance = OrG.REWARD_POINT_BALANCE,
                ORD_BaseRewardCurrencyAmount = OrG.BASE_REWARD_CURRENCY_AMOUNT,
                ORD_RewardCurrencyAmount = OrG.REWARD_CURRENCY_AMOUNT,
                ORD_PaymentMethod = OrG.PAYMENT_METHOD,
                ORD_OPECDId = 0,
                ORD_Comments = null,
                ORD_StatusOrden = OrG.STATUS_ORDEN,
                ORD_CreateFrom = OrG.CREATE_FROM,
                ORD_CreateDateInter = DateTime.Now,
                ORD_Method_title = OrG.METHOD_TITLE,
                ORD_Cc_name = OrG.CC_NAME,
                ORD_Cc_las_4 = OrG.CC_LAS_4
            };

            return ORDER;
        }
        private List<PedidosEnc> MapOrdenGeneral(List<CAT_OrderGeneral> OrG)
        {
            List<PedidosEnc> listOrden = new List<PedidosEnc>();
            foreach (var key in OrG)
            {
                List<CAT_BottlerData> bottler = db.CAT_BottlerData.Where(b => b.BOT_OrderGeneralId == key.Id).ToList();
                if (!bottler.Any())
                    continue;
                var cedisName = bottler.First().BOT_CediName;
                var cedis = new REF_Cedis();
                int cedisOpecd = Convert.ToInt32(cedisName);
                if (key.ORD_CreateFrom == OrderCreatedAt.COCACOLA)
                {
                    cedis = db.REF_Cedis.SingleOrDefault(ced => ced.REC_CedisNameCCHT == cedisName);
                }
                else {
                    cedis = db.REF_Cedis.SingleOrDefault(ced => ced.REC_CedisIdOpecd == cedisOpecd);
                }
                 
                if (bottler.Count > 0 && cedis != null)
                {
                    PedidosEnc orden = new PedidosEnc();
                    orden.ORD_OPECDId = key.ORD_OPECDId;
                    orden.DELIVERY_DATE = bottler[0].BOT_DeliveryDate.Value.ToString("dd-MM-yyyy");
                    orden.ROUTE_CODE = bottler[0].BOT_RouteCode;
                    orden.Trip = bottler[0].BOT_Trip;
                    orden.ORD_StatusOrden = key.ORD_StatusOrden;
                    orden.ORD_CreateFrom = key.ORD_CreateFrom;
                    orden.CedisId = cedis.REC_CedisIdOpecd;
                    listOrden.Add(orden);
                }

            }
            return listOrden;
        }
        private List<OrderGeneral> MapJson(List<CAT_OrderGeneral> OrG)
        {
            List<OrderGeneral> listOrden = new List<OrderGeneral>();
            REF_CedisController refCedis = new REF_CedisController();

            foreach (var key in OrG)
            {
                List<BottlerData> listBottler = new List<BottlerData>();
                List<OrderAddresses> listAddresses = new List<OrderAddresses>();
                List<OrderItem> listItem = new List<OrderItem>();
                List<OrderComments> listComments = new List<OrderComments>();

                foreach (var BtD in key.CAT_BottlerData)
                {
                    BottlerData Bottler = new BottlerData()
                    {
                        ROUTE_ID = BtD.BOT_RouteId,
                        WEBSITE = BtD.BOT_Website,
                        STORE = BtD.BOT_Store,
                        STOREVIEW = BtD.BOT_Storeview,
                        POSTALCODE = BtD.BOT_Postalcode,
                        DELIVERYCO = BtD.BOT_Deliveryco,
                        STATUS = BtD.BOT_Status,
                        SETTLEMENT = BtD.BOT_Settlement,
                        DELIVERY_DATE = Convert.ToString(BtD.BOT_DeliveryDate),
                        ROUTE_CODE = BtD.BOT_RouteCode,
                        CEDI_ID = key.ORD_CreateFrom == OrderCreatedAt.PORTAL ? Convert.ToInt32(BtD.BOT_CediName) : refCedis.FindByCedisCCTH(BtD.BOT_CediName),
                        TRIP_ID = (short)BtD.BOT_Trip
                    };
                    listBottler.Add(Bottler);
                }
                foreach (var Adrs in key.CAT_OrderAddresses)
                {
                    OrderAddresses Addresses = new OrderAddresses()
                    {
                        ENTITY_ID = Adrs.ORE_EntityId,
                        PARENT_ID = Adrs.ORE_ParentId,
                        CUSTOMER_ADDRESS_ID = Adrs.ORE_CustomerAddressId,
                        QUOTE_ADDRESS_ID = Adrs.ORE_QuoteAddressId,
                        REGION_ID = Adrs.ORE_RegionId,
                        FAX = Adrs.ORE_Fax,
                        REGION = Adrs.ORE_Region,
                        POSTCODE = Adrs.ORE_Postcode,
                        LASTNAME = Adrs.ORE_Lastname,
                        STREET = Adrs.ORE_Street,
                        CITY = Adrs.ORE_City,
                        EMAIL = Adrs.ORE_Email,
                        TELEPHONE = Adrs.ORE_Telephone,
                        COUNTRY_ID = Adrs.ORE_CountryId,
                        FIRSTNAME = Adrs.ORE_Firstname,
                        ADDRESS_TYPE = Adrs.ORE_AddressType,
                        PREFIX = Adrs.ORE_Prefix,
                        MIDDLENAME = Adrs.ORE_Middlename,
                        SUFFIX = Adrs.ORE_Suffix,
                        COMPANY = Adrs.ORE_Company,
                        VAT_ID = Adrs.ORE_VatId,
                        VAT_IS_VALID = Adrs.ORE_VatIsValid,
                        VAT_REQUEST_ID = Adrs.ORE_VatRequestId,
                        VAT_REQUEST_DATE = Adrs.ORE_VatRequestDate,
                        VAT_REQUEST_SUCCESS = Adrs.ORE_VatRequestSuccess,
                        GIFTREGISTRY_ITEM_ID = Adrs.ORE_GiftregistryItemId,
                        WANT_BILL = Adrs.ORE_WantBill,
                        RFC = Adrs.ORE_Rfc,
                        BILLING_NAME = Adrs.ORE_BillingName,
                        NUMBER = Adrs.ORE_Number,
                        NUMBER_INT = Adrs.ORE_NumberInt,
                        NEIGHBORHOOD = Adrs.ORE_Neighborhood,
                        MUNICIPALITY = Adrs.ORE_Municipality,
                        REFERENCES = Adrs.ORE_References,
                        ALIAS = Adrs.ORE_Alias,
                        PAYMENT_METHOD = Adrs.ORE_PaymentMethod,
                        CUSTOMER_ID = Convert.ToString(Adrs.ORE_CustomerId),
                        ADDRESS_FLAG = Adrs.ORE_address_flag,
                        BILLING_DISTRICT = Adrs.ORE_Billing_district,
                        BILLING_NEIGHBORHOOD = Adrs.ORE_Billing_neighborhood,
                        CRM_Id = Adrs.ORE_CRMId,
                        BarCode = Adrs.ORE_BarCode

                    };
                    listAddresses.Add(Addresses);
                }
                foreach (var OrI in key.CAT_OrderItems)
                {
                    CAT_Products pro = new CAT_ProductsController().Product(OrI.ORI_InterId);
                    OrderItem item = new OrderItem()
                    {
                        WBCID = pro.Id > 0 ? Convert.ToString(pro.CPD_WBCId) : "0",
                        PRODUCTTYPE = pro.Id > 0 ? Convert.ToInt32(pro.CPD_Tipo) : 0,
                        ITEM_ID = OrI.ORI_ItemId,
                        PARENT_ITEM_ID = OrI.ORI_ParentItemId,
                        SKU = OrI.ORI_Sku,
                        NAME = OrI.ORI_Name,
                        QTY_CANCELED = OrI.ORI_QtyCanceled,
                        QTY_INVOICED = OrI.ORI_QtyInvoiced,
                        QTY_ORDERED = OrI.ORI_QtyOrdered,
                        QTY_REFUNDED = OrI.ORI_QtyRefunded,
                        QTY_SHIPPED = OrI.ORI_QtyShipped,
                        PRICE = OrI.ORI_Price,
                        ORIGINAL_PRICE = OrI.ORI_OriginalPrice,
                        DISCOUNT_AMOUNT = OrI.ORI_DiscountAmount,
                        ROW_TOTAL = OrI.ORI_RowTotal,
                        PRICE_INCL_TAX = OrI.ORI_PriceInclTax,
                        ROW_TOTAL_INCL_TAX = OrI.ORI_RowTotalInclTax,
                        APPLIED_RULE_IDS = OrI.ORI_Applied_rule_ids
                    };

                    listItem.Add(item);
                }
                foreach (var OrC in key.CAT_OrderComments)
                {
                    OrderComments Comments = new OrderComments()
                    {
                        ENTITY_ID = OrC.OCM_EntityId,
                        PARENT_ID = OrC.OCM_ParentId,
                        IS_CUSTOMER_NOTIFIED = OrC.OCM_IsCustomerNotified,
                        IS_VISIBLE_ON_FRONT = OrC.OCM_IsVisibleOnFront,
                        COMMENT = OrC.OCM_Comment,
                        STATUS = OrC.OCM_Status,
                        CREATED_AT = OrC.OCM_CreatedAt,
                        ENTITY_NAME = OrC.OCM_EntityName,
                        STORE_ID = OrC.OCM_StoreId

                    };
                    listComments.Add(Comments);
                }
                OrderGeneral orden = new OrderGeneral()
                {
                    InterID = key.Id,
                    OpeID = (long)key.ORD_OPECDId,
                    ENTITY_ID = key.ORD_EntityId,
                    STATUS = key.ORD_StatusOrden,
                    COUPON_CODE = key.ORD_CouponCode,
                    CUSTOMER_ID = key.ORD_CustomerId,
                    SHIPPING_DESCRIPTION = key.ORD_ShippingDescription,
                    BASE_DISCOUNT_AMOUNT = key.ORD_BaseDiscountAmount,
                    BASE_GRAND_TOTAL = key.ORD_BaseGrandTotal,
                    BASE_SHIPPING_AMOUNT = key.ORD_BaseShippingAmount,
                    BASE_SHIPPING_TAX_AMOUNT = key.ORD_BaseShippingTaxAmount,
                    BASE_SUBTOTAL = key.ORD_BaseSubtotal,
                    BASE_TAX_AMOUNT = key.ORD_BaseTaxAmount,
                    BASE_TOTAL_PAID = key.ORD_BaseTotalPaid,
                    BASE_TOTAL_REFUNDED = key.ORD_BaseTotalRefunded,
                    DISCOUNT_AMOUNT = key.ORD_DiscountAmount,
                    GRAND_TOTAL = key.ORD_GrandTotal,
                    SHIPPING_AMOUNT = Convert.ToDecimal(key.ORD_ShippingAmount),
                    SHIPPING_TAX_AMOUNT = Convert.ToDecimal(key.ORD_ShippingTaxAmount),
                    STORE_TO_ORDER_RATE = Convert.ToDecimal(key.ORD_StoreToOrderRate),
                    SUBTOTAL = key.ORD_Subtotal,
                    TAX_AMOUNT = key.ORD_TaxAmount,
                    TOTAL_PAID = key.ORD_TotalPaid,
                    TOTAL_QTY_ORDERED = key.ORD_TotalQtyOrdered,
                    TOTAL_REFUNDED = key.ORD_TotalRefunded,
                    BASE_SHIPPING_DISCOUNT_AMOUNT = key.ORD_BaseShippingDiscountAmount,
                    BASE_SUBTOTAL_INCL_TAX = key.ORD_BaseSubtotalInclTax,
                    BASE_TOTAL_DUE = key.ORD_BaseTotalDue,
                    SHIPPING_DISCOUNT_AMOUNT = key.ORD_ShippingDiscountAmount,
                    SUBTOTAL_INCL_TAX = key.ORD_SubtotalInclTax,
                    TOTAL_DUE = key.ORD_TotalDue,
                    INCREMENT_ID = key.ORD_IncrementId,
                    APPLIED_RULE_IDS = key.ORD_AppliedRuleIds,
                    BASE_CURRENCY_CODE = key.ORD_BaseCurrencyCode,
                    CUSTOMER_EMAIL = key.ORD_CustomerEmail,
                    DISCOUNT_DESCRIPTION = key.ORD_DiscountDescription,
                    REMOTE_IP = key.ORD_RemoteIp,
                    STORE_CURRENCY_CODE = key.ORD_StoreCurrencyCode,
                    STORE_NAME = key.ORD_StoreName,
                    CREATED_AT = key.ORD_CreatedAt,
                    TOTAL_ITEM_COUNT = key.ORD_TotalItemCount,
                    SHIPPING_INCL_TAX = key.ORD_ShippingInclTax,
                    BASE_CUSTOMER_BALANCE_AMOUNT = key.ORD_BaseCustomerBalanceAmount,
                    CUSTOMER_BALANCE_AMOUNT = key.ORD_CustomerBalanceAmount,
                    BASE_GIFT_CARDS_AMOUNT = key.ORD_BaseGiftCardsAmount,
                    GIFT_CARDS_AMOUNT = key.ORD_GiftCardsAmount,
                    REWARD_POINT_BALANCE = key.ORD_RewardPointsBalance,
                    BASE_REWARD_CURRENCY_AMOUNT = key.ORD_BaseRewardCurrencyAmount,
                    REWARD_CURRENCY_AMOUNT = key.ORD_RewardCurrencyAmount,
                    PAYMENT_METHOD = key.ORD_PaymentMethod,
                    BOTTLER_DATA = listBottler[0],
                    ORDER_ADDRESSES = listAddresses,
                    ORDER_ITEMS = listItem,
                    ORDER_COMMENTS = listComments,
                    METHOD_TITLE = key.ORD_Method_title,
                    CC_LAS_4 = key.ORD_Cc_las_4,
                    CC_NAME = key.ORD_Cc_name,
                    CREATE_FROM = key.ORD_CreateFrom
                };
                listOrden.Add(orden);
            }
            return listOrden;
        }
        private bool validaExtisBottler(ICollection<CAT_BottlerData> BOTTLER, DateTime date, int ordenId)
        {
            bool resp = false;
            List<CAT_BottlerData> bo = BOTTLER.Where(b => b.BOT_OrderGeneralId == ordenId && b.BOT_DeliveryDate == date).ToList();
            if (bo.Count > 0) { resp = true; }
            else { resp = false; }
            return resp;
        }
        public string CreateJson(List<PedidosEnc> ListOrderEnc)
        {
            JArray json = new JArray();
            JObject Row = null;

            foreach (var key in ListOrderEnc)
            {
                Row = JObject.FromObject(key);
                json.Add(Row);
            }
            string arry = json.ToString();
            return arry.Replace(@"\r", "").Replace(@"\n", "").Replace(@"    \", "").Replace(@"\", "").Replace("\"[", "[").Replace("]\"", "]").Replace(@"\", "");
        }
        public string CreateJson(List<PedidosDet> ListOrderDet)
        {
            JArray json = new JArray();
            JObject Row = null;

            foreach (var key in ListOrderDet)
            {
                Row = JObject.FromObject(key);
                json.Add(Row);
            }
            string arry = json.ToString();
            return arry.Replace(@"\r", "").Replace(@"\n", "").Replace(@"    \", "").Replace(@"\", "").Replace("\"[", "[").Replace("]\"", "]").Replace(@"\", "");
        }
        public void Edit(List<OpeCDPUT> json, int accion)
        {

            foreach (var key in json)
            {

                String[] ListId = key.Id.Trim().Split(',');
                foreach (var item in ListId)
                {
                    if (item != null)
                    {
                        if (key.Trip != 0)
                        {
                            new CAT_BottlerDataController().Edit(Convert.ToInt32(item), key.Trip);
                        }
                        CAT_OrderGeneral ORDEN = findByOrdenId(Convert.ToInt32(item));
                        ORDEN.ORD_OPECDId = key.DeliveryOrderId;
                        ORDEN.ORD_StatusOrden = accion == 1 ? "CONFIRMADO" : "VALIDADO";
                        db.Entry(ORDEN).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
            }

        }
        public bool Edit(int OrdenId)
        {
            bool resp = false;
            if (OrdenId != 0)
            {
                CAT_OrderGeneral ORDEN = findByOrdenId(OrdenId);
                if (ORDEN != null && HasValidatedCustomer((int)ORDEN.Id))
                {
                    ORDEN.ORD_StatusOrden = "PREPARADO";
                    db.Entry(ORDEN).State = EntityState.Modified;
                    db.SaveChanges();
                    resp = true;
                }
            }
            return resp;
        }

        private bool HasValidatedCustomer(int orderId) {
            var orderAddress = db.CAT_OrderAddresses.First(oAddress => oAddress.ORE_OrderGeneralId == orderId);
            var customer = db.CAT_Customers.Where(custmer => custmer.CCL_CRMId == orderAddress.ORE_CRMId && custmer.CCL_StatusConfirmation == 1);
            return customer.Any() ? true : false;
        }

        //private bool HasValidatedCustomer(string customerCCTHId)
        //{
        //    var customer = db.CAT_Customers.SingleOrDefault(custmer => custmer.CCL_CCTHId == customerCCTHId && !string.IsNullOrEmpty(custmer.CCL_BarCode) && !string.IsNullOrEmpty(custmer.CCL_CRMId));
        //    return customer != null ? true : false;
        //}
        public void EditOrderStatus(int orderId, string status)
        {
            if (orderId != 0)
            {
                CAT_OrderGeneral order = findByOrdenId(orderId);
                if (order != null)
                {
                    order.ORD_StatusOrden = status;
                    db.Entry(order).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }
        private CAT_OrderGeneral findByOrdenId(int Id)
        {
            List<CAT_OrderGeneral> ordenList = db.CAT_OrderGeneral.Where(o => o.Id == Id).ToList();
            db.Entry(ordenList[0]).State = EntityState.Detached;
            return ordenList[0];
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        private ActionResult DeleteConfirmed(int id)
        {
            CAT_OrderGeneral cAT_OrderGeneral = db.CAT_OrderGeneral.Find(id);
            db.CAT_OrderGeneral.Remove(cAT_OrderGeneral);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
