﻿using API_Integraciones.Controllers.APIs;
using API_Integraciones.Models.AdminConfig;
using API_Integraciones.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace API_Integraciones.Controllers.DataAccess
{
    public class CONF_ConfigController : Controller
    {
        private CCTH_INTERMEDIAEntities db = new CCTH_INTERMEDIAEntities();

        public string FindPayDesk(int cedisId)
        {
            return new APIOpeCD().GetPayDesk(cedisId);
        }
        //public ConfigGET FindConfig()
        //{
        //    ConfigGET confi = new ConfigGET();
        //    confi.Estados = FindEstado();
        //    confi.Municipios = FindMunicipio();
        //    confi.Paises = FindPaises();
        //    confi.Colonias = FindColonias();

        //    if (!confi.Estados.Any() && !confi.Municipios.Any() && !confi.Paises.Any())
        //        return confi;
        //    return confi;
        //}
        public List<Estado> FindEstados(float paisOpeId)
        {
            List<CONF_Estado> estados = db.CONF_Estado.Where(estado => estado.COD_PaisOpeId == paisOpeId).ToList();
            return MapEstados(estados);
        }
        public List<Municipio> FindMunicipios(float estadoOpeId)
        {
            List<CONF_Municipio> municipio = db.CONF_Municipio.Where(municipiio => municipiio.COD_EstadoOpeId == estadoOpeId).ToList();
            return MapMunicipio(municipio);
        }
        public List<Pais> FindPaises()
        {
            List<CONF_Pais> Pais = db.CONF_Pais.ToList();
            return MapPais(Pais);
        }
        private List<Estado> MapEstados(List<CONF_Estado> Estados)
        {
            List<Estado> Edos = new List<Estado>();
            if (Estados == null || !Estados.Any())
                return Edos;
            foreach (var key in Estados)
            {
                Estado row = new Estado() { Id = key.id, Name = key.COD_Name, OpeId = key.COD_OpeId.ToString(), CodeCCTH = key.COD_CodeCCTH, CodeCRM = key.COD_CodeCRM};
                Edos.Add(row);
            }
            return Edos;
        }
        private List<Colonia> MapColonias(List<CONF_Colonia> colonias)
        {
            List<Colonia> listaColonias = new List<Colonia>();
            if (colonias == null || !colonias.Any())
                return listaColonias;
            foreach (var key in colonias)
            {
                Colonia row = new Colonia() { Id = key.id, Name = key.COD_Name, OpeId = key.COD_OpeId.ToString(), CodeCCTH = key.COD_CodeCCTH, CodeCRM = key.COD_CodeCRM };
                listaColonias.Add(row);
            }
            return listaColonias;
        }
        private List<Municipio> MapMunicipio(List<CONF_Municipio> Municipio)
        {
            List<Municipio> Muni = new List<Municipio>();
            if (Municipio == null || !Municipio.Any())
                return Muni;
            foreach (var key in Municipio)
            {
                Municipio row = new Municipio() { Id = key.id, Name = key.COD_Name, OpeId = key.COD_OpeId.ToString(), CodeCCTH = key.COD_CodeCCTH, CodeCRM = key.COD_CodeCRM };
                Muni.Add(row);
            }
            return Muni;
        }
        private List<Pais> MapPais(List<CONF_Pais> PaisList)
        {
            List<Pais> pais = new List<Pais>();
            if (PaisList == null || !PaisList.Any())
                return pais;
            foreach (var key in PaisList)
            {
                Pais row = new Pais() { Id = key.Id, Name = key.COD_Name, OpeId = key.COD_OpeId.ToString(), CodeCCTH = key.COD_CodeCCTH, CodeCRM = key.COD_CodeCRM };
                pais.Add(row);
            }
            return pais;
        }
        public List<Colonia> FindColonias(float estadoOpeId, float municipioOpeId)
        {
            var confColonias = db.CONF_Colonia.Where(colonia => colonia.COD_MunicipioOpeId == municipioOpeId && colonia.COD_EstadoOpeId == estadoOpeId);
            return MapColonias(confColonias.ToList());
        }
    }
}
