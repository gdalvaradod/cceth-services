﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.AdminPedidos;

namespace API_Integraciones.Controllers.DataAcces
{
    public class CAT_ProductsController : Controller
    {
        private CCTH_INTERMEDIAEntities db = new CCTH_INTERMEDIAEntities();

        public List<ProductsGET> FindAll(int cedis)
        {
            return Map(db.CAT_Products.Where(p => p.CPD_Cedis == cedis).ToList());
        }

        public List<ProductsGET> FindAll()
        {
            return Map(db.CAT_Products.ToList());
        }

        public List<ProductsGET> FindBy(int cedisId)
        {
            return Map(db.CAT_Products.Where(product => product.CPD_Cedis == cedisId).ToList());
        }

        public List<SearchProduct> SearchAdvanceProducts(int cedi, int route, SearchProduct search)
        {
            List<SearchProduct> listSearch = new List<SearchProduct>();
            var SearchFilter = (from product in db.CAT_Products
                                where product.CPD_Cedis == cedi && product.CPD_Ruta == route && (product.CPD_Nombre.ToUpper().ToString().Contains(search.Name.ToUpper()) || product.CPD_CodigoProducto == search.Code)
                                select new { product }).ToList();
            if (SearchFilter.Any())
            {
                foreach (var key in SearchFilter)
                {
                    SearchProduct row = new SearchProduct()
                    {
                        Id = key.product.Id,
                        Name = key.product.CPD_Nombre,
                        Code = key.product.CPD_CodigoProducto,
                        Type = key.product.CPD_Tipo
                    };
                    listSearch.Add(row);
                }
            }
            return listSearch;
        }

        public void Create(ProductsGET product)
        {
            db.CAT_Products.Add(Map(product));
            db.SaveChanges();
        }

        public void Update(List<ProductsGET> products)
        {
            foreach (var product in products)
            {
                if (!ModelState.IsValid || !ValidateProduct(product))
                    continue;
                db.Entry(Map(product)).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public CAT_Products Product(int InterId)
        {
            var prod = db.CAT_Products.Any(p => p.CPD_CodigoProducto == InterId.ToString());
            var pro = new CAT_Products();
            if (prod)
            {
                pro = db.CAT_Products.First(p => p.CPD_CodigoProducto == InterId.ToString());
            }
            return pro;
        }

        private CAT_Products Map(ProductsGET productGet)
        {
            CAT_Products Prod = new CAT_Products()
            {
                Id = productGet.Id != 0 ? productGet.Id : 0,
                CPD_CodigoProducto = productGet.CodigoProducto,
                CPD_Nombre = productGet.Nombre,
                CPD_Cedis = productGet.Cedis,
                CPD_Ruta = productGet.Ruta,
                CPD_Tipo = productGet.Tipo,
                CPD_Status = 1,
                CPD_WBCId = productGet.WBCId,
                CPD_CreateDate = productGet.CreateDate != null ? DateTime.Now : productGet.CreateDate
            };

            return Prod;
        }
        private List<ProductsGET> Map(List<CAT_Products> pro)
        {
            List<ProductsGET> list = new List<ProductsGET>();

            foreach (var key in pro)
            {
                ProductsGET row = new ProductsGET()
                {
                    Id = key.Id,
                    CodigoProducto = key.CPD_CodigoProducto,
                    Nombre = key.CPD_Nombre,
                    Ruta = key.CPD_Ruta,
                    Cedis = key.CPD_Cedis,
                    WBCId = key.CPD_WBCId,
                    Tipo = key.CPD_Tipo,
                    Status = key.CPD_Status,
                    CreateDate = key.CPD_CreateDate
                };
                list.Add(row);
            }

            return list;
        }
        private bool ValidateProduct(ProductsGET product)
        {
            if (String.IsNullOrWhiteSpace(Convert.ToString(product.Cedis)) || String.IsNullOrWhiteSpace(Convert.ToString(product.Ruta)) || String.IsNullOrWhiteSpace(product.CodigoProducto) || String.IsNullOrWhiteSpace(product.Nombre) || product.Cedis == 0 || product.Ruta == 0 || product.WBCId == null)
                return false;
            return true;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
