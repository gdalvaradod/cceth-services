using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models;
using API_Integraciones.Models.Json;
using API_Integraciones.Models.AdminPedidos;
using API_Integraciones.Controllers.DAO;
using API_Integraciones.Controllers.DataAcces;

namespace API_Integraciones.Controllers
{
    public class CAT_OrderAddressesController : Controller
    {
        private CCTH_INTERMEDIAEntities db = new CCTH_INTERMEDIAEntities();
        public CAT_OrderAddressesController()
        {

        }
        public CAT_OrderAddressesController(CCTH_INTERMEDIAEntities db)
        {
            this.db = db;
        }
        public OrderAddresses Find(int orderGeneralId, string addressType)
        {
            var catOrderAddress = db.CAT_OrderAddresses.SingleOrDefault(orderAdr => orderAdr.ORE_OrderGeneralId == orderGeneralId && orderAdr.ORE_AddressType.ToUpper() == addressType.ToUpper());
            return MapAddressesJson(catOrderAddress);
        }
        public int Create(OrderAddresses orderAddress, int ordenId)
        {
            var catOrderAddress = Map(orderAddress, ordenId, new CAT_OrderAddresses());
            db.CAT_OrderAddresses.Add(catOrderAddress);
            return catOrderAddress.Id;
        }

        public void BulkCreation(List<OrderAddresses> orderAddresses, int ordenId)
        {
            orderAddresses.ForEach(orderAddress => Create(orderAddress, ordenId));
        }

        public void Create(string customerId, int ordenId, string payment, short wantbill, string cuc)
        {

            CAT_CustomersController customerController = new CAT_CustomersController();
            Customers customer = customerController.FindByBarCodeCustomer(customerId);
            var cucGenerico = new CONF_DestinosController().findCucGeneric(customer.DistributionCenter, customer.RouteCode);
            if (customer != null)
            {
                if (string.IsNullOrEmpty(customer.Cuc) || customer.Cuc == "0" || customer.Cuc == cucGenerico.ToString())
                {
                    customer.AddressType = "shipping";
                    db.CAT_OrderAddresses.Add(Map(customer, ordenId, payment, 0, cucGenerico.ToString()));
                }
                else {
                    customer.AddressType = "billing";
                    db.CAT_OrderAddresses.Add(Map(customer, ordenId, payment, 1, customer.Cuc));
                }
                db.SaveChanges();
            }
        }
        public void Edit(OrderAddresses orderAddress, int orderId)
        {
            var catOrderAddress = db.CAT_OrderAddresses.Find(orderAddress.ID);
            if (catOrderAddress == null)
                return;
            MapBillingOrderAddress(orderAddress, orderId, catOrderAddress);
            db.SaveChanges();
        }
        public void Edit(string customerId, int ordenId, string payment)
        {
            Customers customer = new CAT_CustomersController().FindByBarCodeCustomer(customerId);
            CAT_OrderAddresses addres = db.CAT_OrderAddresses.First(ad => ad.ORE_OrderGeneralId == ordenId && ad.ORE_BarCode == customerId);
            if (customer != null)
            {
                var wantBill = (short)(customer.AddressType == "billing" ? 1 : 0);
                db.Entry(MapUpdate(addres, customer, ordenId, payment, wantBill)).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
        //public void Edit(string crmId, string barcode, int id)
        //{
        //    var orderAddress = db.CAT_OrderAddresses.Find(id);
        //    orderAddress.ORE_CRMId = crmId;
        //    orderAddress.ORE_BarCode = barcode;
        //    db.SaveChanges();
        //}
        public void Edit(string crmId, string barcode, string customerCCTHId, string cuc)
        {
            var generalOrders = db.CAT_OrderGeneral.Where(generalOrder => generalOrder.ORD_CustomerId.ToString() == customerCCTHId);
            var orderAddresses = new List<OrderAddresses>();
            foreach (var generalOrder in generalOrders)
            {
                foreach (var orderAddress in generalOrder.CAT_OrderAddresses)
                {
                    orderAddress.ORE_CRMId = crmId;
                    orderAddress.ORE_BarCode = barcode;
                    orderAddress.ORE_CustomerId = cuc;
                    //orderAddress.ORE_WantBill = wantBill;
                }
            }
            db.SaveChanges();
        }
        public CAT_OrderAddresses findAddressesByInterId(int InterId)
        {
            CAT_OrderAddresses address = db.CAT_OrderAddresses.First(ad => ad.ORE_OrderGeneralId == InterId);
            return address;
        }
        private CAT_OrderAddresses MapUpdate(CAT_OrderAddresses ADDRESSES, Customers CUS, int ordenId, string payment, short wantbill)
        {
            ADDRESSES.ORE_Firstname = CUS.Name;
            ADDRESSES.ORE_Postcode = CUS.PostCode;
            ADDRESSES.ORE_Street = CUS.Street;
            ADDRESSES.ORE_City = CUS.City;
            ADDRESSES.ORE_Email = CUS.Email;
            ADDRESSES.ORE_Telephone = CUS.Telephone;
            ADDRESSES.ORE_CountryId = CUS.Country;
            ADDRESSES.ORE_WantBill = wantbill;
            ADDRESSES.ORE_Neighborhood = CUS.Neighborhood;
            ADDRESSES.ORE_PaymentMethod = payment;
            ADDRESSES.ORE_OrderGeneralId = ordenId;
            ADDRESSES.ORE_CRMId = CUS.CRMId;
            ADDRESSES.ORE_AddressType = CUS.AddressType;
            ADDRESSES.ORE_CustomerId = string.IsNullOrEmpty(CUS.Cuc) == false && CUS.Cuc != "0" ? CUS.Cuc : CUS.BarCode;
            ADDRESSES.ORE_BarCode = CUS.BarCode;

            return ADDRESSES;
        }
        private CAT_OrderAddresses Map(Customers CUS, int OrdenId, string payment, short wantbill = 0, string cuc = null)
        {
            CAT_OrderAddresses ADDRESSES = new CAT_OrderAddresses();
            ADDRESSES.Id = 0;
            ADDRESSES.ORE_Firstname = CUS.Name;
            ADDRESSES.ORE_Postcode = CUS.PostCode;
            ADDRESSES.ORE_Street = CUS.Street;
            ADDRESSES.ORE_City = CUS.City;
            ADDRESSES.ORE_Email = CUS.Email;
            ADDRESSES.ORE_Telephone = CUS.Telephone;
            ADDRESSES.ORE_CountryId = CUS.Country;
            ADDRESSES.ORE_WantBill = wantbill;
            ADDRESSES.ORE_Neighborhood = CUS.Neighborhood;
            ADDRESSES.ORE_PaymentMethod = payment;
            ADDRESSES.ORE_OrderGeneralId = OrdenId;
            ADDRESSES.ORE_CRMId = CUS.CRMId;
            ADDRESSES.ORE_AddressType = CUS.AddressType;
            ADDRESSES.ORE_CustomerId = cuc;
            ADDRESSES.ORE_BarCode = CUS.BarCode;
            return ADDRESSES;
        }
        private CAT_OrderAddresses Map(OrderAddresses Adrs, int ordenId, CAT_OrderAddresses orderAddress)
        {
            orderAddress.Id = Adrs.ID;
            orderAddress.ORE_EntityId = Adrs.ENTITY_ID;
            orderAddress.ORE_ParentId = Adrs.PARENT_ID;
            orderAddress.ORE_CustomerAddressId = Adrs.CUSTOMER_ADDRESS_ID;
            orderAddress.ORE_QuoteAddressId = Adrs.QUOTE_ADDRESS_ID;
            orderAddress.ORE_RegionId = Adrs.REGION_ID;
            orderAddress.ORE_Fax = Adrs.FAX;
            orderAddress.ORE_Region = Adrs.REGION;
            orderAddress.ORE_Postcode = Adrs.POSTCODE;
            orderAddress.ORE_Lastname = Adrs.LASTNAME;
            orderAddress.ORE_Street = Adrs.STREET;
            orderAddress.ORE_City = Adrs.CITY;
            orderAddress.ORE_Email = Adrs.EMAIL;
            orderAddress.ORE_Telephone = Adrs.TELEPHONE;
            orderAddress.ORE_CountryId = Adrs.COUNTRY_ID;
            orderAddress.ORE_Firstname = Adrs.FIRSTNAME;
            orderAddress.ORE_AddressType = Adrs.ADDRESS_TYPE;
            orderAddress.ORE_Prefix = Adrs.PREFIX;
            orderAddress.ORE_Middlename = Adrs.MIDDLENAME;
            orderAddress.ORE_Suffix = Adrs.SUFFIX;
            orderAddress.ORE_Company = Adrs.COMPANY;
            orderAddress.ORE_VatId = Adrs.VAT_ID;
            orderAddress.ORE_VatIsValid = Adrs.VAT_IS_VALID;
            orderAddress.ORE_VatRequestId = Adrs.VAT_REQUEST_ID;
            orderAddress.ORE_VatRequestDate = Adrs.VAT_REQUEST_DATE;
            orderAddress.ORE_VatRequestSuccess = Adrs.VAT_REQUEST_SUCCESS;
            orderAddress.ORE_GiftregistryItemId = Adrs.GIFTREGISTRY_ITEM_ID;
            orderAddress.ORE_WantBill = Adrs.WANT_BILL == null ? 0 : Adrs.WANT_BILL;
            orderAddress.ORE_Rfc = Adrs.RFC;
            orderAddress.ORE_BillingName = Adrs.BILLING_NAME;
            orderAddress.ORE_Number = Adrs.NUMBER;
            orderAddress.ORE_NumberInt = Adrs.NUMBER_INT;
            orderAddress.ORE_Neighborhood = Adrs.NEIGHBORHOOD;
            orderAddress.ORE_Municipality = Adrs.MUNICIPALITY;
            orderAddress.ORE_References = Adrs.REFERENCES;
            orderAddress.ORE_Alias = Adrs.ALIAS;
            orderAddress.ORE_PaymentMethod = Adrs.PAYMENT_METHOD;
            orderAddress.ORE_OrderGeneralId = ordenId;
            orderAddress.ORE_address_flag = Adrs.ADDRESS_FLAG;
            orderAddress.ORE_Billing_district = Adrs.BILLING_DISTRICT;
            orderAddress.ORE_Billing_neighborhood = Adrs.BILLING_NEIGHBORHOOD;
            orderAddress.ORE_CRMId = Adrs.CRM_Id;
            orderAddress.ORE_BarCode = Adrs.BarCode;
            orderAddress.ORE_CustomerId = Adrs.CUSTOMER_ID;
            return orderAddress;
        }
        private CAT_OrderAddresses MapBillingOrderAddress(OrderAddresses Adrs, int ordenId, CAT_OrderAddresses orderAddress)
        {
            orderAddress.Id = Adrs.ID;
            orderAddress.ORE_Postcode = Adrs.POSTCODE;
            orderAddress.ORE_Lastname = Adrs.LASTNAME;
            orderAddress.ORE_Street = Adrs.STREET;
            orderAddress.ORE_City = Adrs.CITY;
            orderAddress.ORE_Email = Adrs.EMAIL;
            orderAddress.ORE_Telephone = Adrs.TELEPHONE;
            orderAddress.ORE_Firstname = Adrs.FIRSTNAME;
            orderAddress.ORE_Company = Adrs.COMPANY;
            orderAddress.ORE_WantBill = Adrs.WANT_BILL;
            orderAddress.ORE_Rfc = Adrs.RFC;
            orderAddress.ORE_Number = Adrs.NUMBER;
            orderAddress.ORE_Neighborhood = Adrs.NEIGHBORHOOD;
            orderAddress.ORE_Municipality = Adrs.MUNICIPALITY;
            orderAddress.ORE_OrderGeneralId = ordenId;
           
            return orderAddress;
        }
        public OrderAddresses MapAddressesJson(CAT_OrderAddresses Adrs)
        {
            OrderAddresses orderAddresses = new OrderAddresses()
            {
                ID = Adrs.Id,
                ENTITY_ID = Adrs.ORE_EntityId,
                PARENT_ID = Adrs.ORE_ParentId,
                CUSTOMER_ADDRESS_ID = Adrs.ORE_CustomerAddressId,
                QUOTE_ADDRESS_ID = Adrs.ORE_QuoteAddressId,
                REGION_ID = Adrs.ORE_RegionId,
                FAX = Adrs.ORE_Fax,
                REGION = Adrs.ORE_Region,
                POSTCODE = Adrs.ORE_Postcode,
                LASTNAME = Adrs.ORE_Lastname,
                STREET = Adrs.ORE_Street,
                CITY = Adrs.ORE_City,
                EMAIL = Adrs.ORE_Email,
                TELEPHONE = Adrs.ORE_Telephone,
                COUNTRY_ID = Adrs.ORE_CountryId,
                FIRSTNAME = Adrs.ORE_Firstname,
                ADDRESS_TYPE = Adrs.ORE_AddressType,
                PREFIX = Adrs.ORE_Prefix,
                MIDDLENAME = Adrs.ORE_Middlename,
                SUFFIX = Adrs.ORE_Suffix,
                COMPANY = Adrs.ORE_Company,
                VAT_ID = Adrs.ORE_VatId,
                VAT_IS_VALID = Adrs.ORE_VatIsValid,
                VAT_REQUEST_ID = Adrs.ORE_VatRequestId,
                VAT_REQUEST_DATE = Adrs.ORE_VatRequestDate,
                VAT_REQUEST_SUCCESS = Adrs.ORE_VatRequestSuccess,
                GIFTREGISTRY_ITEM_ID = Adrs.ORE_GiftregistryItemId,
                WANT_BILL = Adrs.ORE_WantBill,
                RFC = Adrs.ORE_Rfc,
                BILLING_NAME = Adrs.ORE_BillingName,
                NUMBER = Adrs.ORE_Number,
                NUMBER_INT = Adrs.ORE_NumberInt,
                NEIGHBORHOOD = Adrs.ORE_Neighborhood,
                MUNICIPALITY = Adrs.ORE_Municipality,
                REFERENCES = Adrs.ORE_References,
                ALIAS = Adrs.ORE_Alias,
                PAYMENT_METHOD = Adrs.ORE_PaymentMethod,
                ADDRESS_FLAG = Adrs.ORE_address_flag,
                BILLING_DISTRICT = Adrs.ORE_Billing_district,
                BILLING_NEIGHBORHOOD = Adrs.ORE_Billing_neighborhood,
                CRM_Id = Adrs.ORE_CRMId,
                BarCode = Adrs.ORE_BarCode,
                OrderGeneralId = Adrs.ORE_OrderGeneralId
            };
            return orderAddresses;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
