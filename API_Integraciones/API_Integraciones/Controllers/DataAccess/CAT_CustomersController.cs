﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models;
using API_Integraciones.Models.AdminPedidos;
using Newtonsoft.Json.Linq;
using API_Integraciones.Mappers;
using static System.String;
using API_Integraciones.Models.AdminClientes;
using API_Integraciones.Enums;

namespace API_Integraciones.Controllers.DAO
{
    public class CAT_CustomersController : Controller
    {

        private readonly CCTH_INTERMEDIAEntities _db = new CCTH_INTERMEDIAEntities();
        private readonly VisitMapper _visitMapper = new VisitMapper();

        public CAT_CustomersController()
        {

        }
        public CAT_CustomersController(CCTH_INTERMEDIAEntities db)
        {
            _db = db;
        }

        public List<CustomerFilter> FilterLIke(int cedi, string barcode)
        {
            var res = (from xx in _db.CAT_Customers
                       where xx.CCL_BarCode.Contains(barcode) && xx.CCL_DistributionCenter == cedi && xx.CCL_StatusConfirmation == 1
                       select xx).ToList();
            var filter = new List<CustomerFilter>();
            if (!res.Any()) return filter;
            filter.AddRange(res.Select(key => new CustomerFilter()
            {
                BarCode = key.CCL_BarCode,
                Name = key.CCL_Name,
                AddressType = key.CCL_AddressType,
                Cuc = (int)key.CCL_Cuc
            }));
            return filter;
        }
        public List<CustomerRelationResponse> SearchAdvanceCustomerRelation(int cedi, CustomerRelation search)
        {
            var listSearch = new List<CustomerRelationResponse>();
            var customerQuery = from customers in _db.CAT_Customers
                                select new { customers };
            if(cedi > 0)
                customerQuery = customerQuery.Where(pre => pre.customers.CCL_DistributionCenter == cedi);
            if (!IsNullOrEmpty(search.Barcode))
                customerQuery = customerQuery.Where(pre => pre.customers.CCL_BarCode == search.Barcode);
            if (!IsNullOrEmpty(search.CrmId))
                customerQuery = customerQuery.Where(pre => pre.customers.CCL_CRMId == search.CrmId);
            if (search.Cuc > 0)
                customerQuery = customerQuery.Where(pre => pre.customers.CCL_Cuc == search.Cuc);

            if (!customerQuery.Any()) return listSearch;
            listSearch.AddRange(customerQuery.Select(key => new CustomerRelationResponse()
            {
                Id = key.customers.Id,
                Name = key.customers.CCL_Name,
                Email = key.customers.CCL_Email,
                Tel = key.customers.CCL_Telephone,
                Cuc = key.customers.CCL_Cuc ?? 0,
                CrmId = key.customers.CCL_CRMId,
                BarCode = key.customers.CCL_BarCode,
                CustomerType = key.customers.CCL_CustomerType,
            }));
            return listSearch;
        }
        public List<GetSearchCustomer> SearchAdvanceCustomer(int cedi, SearchCustomer search)
        {
            var listSearch = new List<GetSearchCustomer>();
            var searchFilter = (from customers in _db.CAT_Customers
                                where customers.CCL_DistributionCenter == cedi
                                select customers
                                );

            //var searchFilter = (from customers in _db.CAT_Customers
            //                    where customers.CCL_DistributionCenter == cedi &&
            //                    (customers.CCL_Name.Contains(search.Name) ||
            //                    customers.CCL_Email == search.Email ||
            //                    customers.CCL_Telephone == search.Tel ||
            //                    customers.CCL_Cuc == search.Cuc 
            //                    )
            //                    select new { customers });
            if (search.Route != 0)
            {
                searchFilter = searchFilter.Where(e => e.CCL_RouteCode == search.Route);
            }
            if (search.Cuc != 0)
            {
                searchFilter = searchFilter.Where(e => e.CCL_Cuc == search.Cuc);
            }
            if (!String.IsNullOrEmpty(search.Name))
            {
                searchFilter = searchFilter.Where(e => e.CCL_Name.ToUpper().Contains(search.Name.ToUpper()));
            }
            if (!String.IsNullOrEmpty(search.Email))
            {
                searchFilter = searchFilter.Where(e => e.CCL_Email == search.Email);
            }
            if (!String.IsNullOrEmpty(search.Tel))
            {
                searchFilter = searchFilter.Where(e => e.CCL_Telephone == search.Tel);
            }
            if (!String.IsNullOrEmpty(search.BarCode))
            {
                searchFilter = searchFilter.Where(e => e.CCL_BarCode.ToUpper().Contains(search.BarCode.ToUpper()));
            }

            if (!searchFilter.ToList().Any()) return listSearch;
            foreach (var key in searchFilter.ToList())
            {
                GetSearchCustomer row = new GetSearchCustomer()
                {
                    Id = key.Id,
                    Name = key.CCL_Name,
                    Email = key.CCL_Email,
                    Tel = key.CCL_Telephone,
                    Cuc = key.CCL_Cuc == null ? 0 : Convert.ToInt32(key.CCL_Cuc),
                    CrmId = key.CCL_CRMId,
                    DeliveryDate = DateTime.Now.ToString(),
                    BarCode = key.CCL_BarCode,
                    WantBill = false
                };
                listSearch.Add(row);
            }
            //listSearch.ToList().AddRange(searchFilter.Select(key => new GetSearchCustomer()
            //{
            //    Id = key.customers.Id,
            //    Name = key.customers.CCL_Name,
            //    Email = key.customers.CCL_Email,
            //    Tel = key.customers.CCL_Telephone,
            //    Cuc = key.customers.CCL_Cuc == null ? 0 : Convert.ToInt32(key.customers.CCL_Cuc),
            //    CrmId = key.customers.CCL_CRMId,
            //    DeliveryDate = DateTime.Now.ToString(),
            //    BarCode = key.customers.CCL_BarCode,
            //    WantBill = false,
            //}));
            return listSearch;
        }
        public List<Customers> FindAll(int? start = null, int? length = null)
        {
            var catCustomers = _db.CAT_Customers.OrderBy(customer => customer.Id);
            start = start != null ? start : 0;
            length = length != null ? length : int.MaxValue;
            var pagedCatCustomers = catCustomers.Skip((int)start).Take((int)length).ToList();
            return Map(pagedCatCustomers);
        }
        public List<Customers>Find(string name, string route, int? status, string cedisId, string customerType, out int dbTotalRecords, out int filteredRecords, int? start = 0, int? length = int.MaxValue)
        {
            dbTotalRecords = (from customers in _db.CAT_Customers
                              select customers.Id).Count();
            var catCustomers = from customers in _db.CAT_Customers
                               select customers;
            if (!string.IsNullOrEmpty(cedisId))
                catCustomers = catCustomers.Where(cattCustomer => cattCustomer.CCL_DistributionCenter.ToString() == cedisId);
            if (!string.IsNullOrEmpty(name))
                catCustomers = catCustomers.Where(cattCustomer => cattCustomer.CCL_Name.ToUpper().Contains(name.ToUpper()));
            if (!string.IsNullOrEmpty(route))
                catCustomers = catCustomers.Where(cattCustomer => cattCustomer.CCL_RouteCode.ToString() == route);
            if (status != null)
                catCustomers = catCustomers.Where(cattCustomer => cattCustomer.CCL_StatusConfirmation == status);
            if (!string.IsNullOrEmpty(customerType))
                catCustomers = catCustomers.Where(catCustomer => catCustomer.CCL_CustomerType.ToUpper() == customerType.ToUpper());
            catCustomers = catCustomers.Where(catCustomer => catCustomer.CCL_AddressType != "billing");
            filteredRecords = catCustomers.Count();

            return Map(catCustomers.OrderBy(cattCustomer => cattCustomer.CCL_Cuc).Skip((int)start).Take((int)length).ToList());
        }
        public Customers FindByIdCustomer(string crmId)
        {
            var customer = _db.CAT_Customers.SingleOrDefault(c => c.CCL_CRMId == crmId);
            return customer == null ? new Customers() : Map(customer);
        }
        public Customers FindCustomerbyId(int id)
        {
            var customer = _db.CAT_Customers.First(c => c.Id == id);
            return Map(customer);
        }
        public Customers FindCustomerByCcthId(int ccthId)
        {
            var customer = _db.CAT_Customers.SingleOrDefault(customr => customr.CCL_CCTHId == ccthId.ToString() && customr.CCL_AddressType == "shipping");
            if (customer == null)
                return null;
            return Map(customer);
        }
        public List<Customers> FindByCustomerList(int cedis, int ruta)
        {
            var customer = _db.CAT_Customers.Where(c => c.CCL_DistributionCenter == cedis && c.CCL_RouteCode == ruta).ToList();
            return MapLitsCediRoute(customer);
        }
        public Customers FindByBarCodeCustomer(string id)
        {
            var customer = _db.CAT_Customers.First(c => c.CCL_BarCode == id);
            return Map(customer);
        }
        public bool FindByBarCode(string barcode)
        {
            var resp = false;
            var customer = _db.CAT_Customers.Where(c => c.CCL_BarCode == barcode).ToList();
            if (customer.Any())
                resp = true;
            return resp;
        }
        public List<Customers> FindCustomersBy(string name, int cedis, string route, string email)
        {
            email = email ?? string.Empty;
            var customers = _db.CAT_Customers.Where(cstomer =>
                cstomer.CCL_Name.ToUpper().Contains(name.Trim().ToUpper())
                && cstomer.CCL_Email == email
                && cstomer.CCL_DistributionCenter == cedis
                && cstomer.CCL_RouteCode.ToString() == route);
            var customerList = new List<Customers>();
            customers.ToList().ForEach(customer => customerList.Add(Map(customer)));
            return customerList;
        }
        public string CreateJson(List<Customers> listCustomer)
        {
            var json = new JArray();

            foreach (var key in listCustomer)
            {
                var row = JObject.FromObject(key);
                json.Add(row);
            }
            var arry = json.ToString();
            return arry.Replace(@"\r", "").Replace(@"\n", "").Replace(@"    \", "").Replace(@"\", "").Replace("\"[", "[").Replace("]\"", "]").Replace(@"\", "");
        }
        public string CreateJson(Customers customer)
        {
            var json = new JArray();

            var row = JObject.FromObject(customer);
            json.Add(row);

            var arry = json.ToString();
            return arry.Replace(@"\r", "").Replace(@"\n", "").Replace(@"    \", "").Replace(@"\", "").Replace("\"[", "[").Replace("]\"", "]").Replace(@"\", "");
        }
        public int Create(OrderAddresses orderAddress, string routeCode, string cedis)
        {
            List<int> info;
            if (!ValidateExist(cedis, routeCode, out info))
                throw new InvalidOperationException("Cedis o Ruta invalidos");
            if (ValidateExist(orderAddress))
                return 0;
            SetCorrectCityAndCountry(orderAddress);
            var catCustomer = Map(orderAddress, info);
            _db.CAT_Customers.Add(catCustomer);
            return catCustomer.Id;
        }
        public void BulkCreation(List<OrderAddresses> orderAddresses, string routeCode, string cedis)
        {
            orderAddresses.ForEach(orderAddress => Create(orderAddress, routeCode, cedis));
        }
        public void Create(Customers customer)
        {
            customer.CreatedOn = DateTime.Now.ToShortDateString();
            customer.ModifiedOn = DateTime.Now.ToShortDateString();
            _db.CAT_Customers.Add(Map(customer));
            _db.SaveChanges();
        }
        public void Edit(CustomerPUT customer)
        {
            var row = _db.CAT_Customers.First(c => c.Id == customer.Id);
            row.CCL_RouteCode = Convert.ToInt32(customer.RutaCode);
            row.CCL_StatusConfirmation = customer.Status;
            row.CCL_Cuc = customer.Cuc;

            _db.Entry(row).State = EntityState.Modified;
            _db.SaveChanges();

        }
        public void Edit(Customers customer)
        {
            var dbCustomer = _db.CAT_Customers.Find(Convert.ToInt32(customer.Id));
            Map(customer, dbCustomer);
            dbCustomer.CCL_ModifiedOn = DateTime.UtcNow;
            _db.SaveChanges();
        }
        public bool Edit(Customers customer, string barcode)
        {
            if (!customer.BarCode.ToUpper().Equals(barcode.ToUpper()))
                throw new ArgumentException("El barcode en la entidad Customer debe ser la misma que el parametro barcode");
            var dbCustomer = _db.CAT_Customers.SingleOrDefault(customr => customr.CCL_BarCode == barcode);
            if (dbCustomer == null)
                return false;
            Map(customer, dbCustomer);
            _db.Entry(dbCustomer).State = EntityState.Modified;
            var affectedRecords = _db.SaveChanges();
            return true;
        }
        private bool ValidateExist(string cedi, string route, out List<int> info)
        {
            bool resp;
            var temp = new List<int>();
            var row = (from cedis in _db.REF_Cedis
                       join routes in _db.CONF_Destinos on cedis.Id equals routes.COD_CedisRefId
                       where cedis.REC_CedisNameCCHT == cedi && routes.COD_RouteCodeCCHT == route
                       select new
                       {
                           cedis.REC_CedisIdOpecd,
                           routes.COD_RouteCodeOpecd
                       }).ToList();

            if (row.Any())
            {
                temp.Add(row[0].REC_CedisIdOpecd);
                temp.Add(row[0].COD_RouteCodeOpecd);
                resp = true;
            }
            else
            {
                resp = false;
            }
            info = temp;
            return resp;
        }
        private bool ValidateExist(OrderAddresses addrs)
        {
            var cus = _db.CAT_Customers.Where(c => c.CCL_PostCode == addrs.POSTCODE && c.CCL_Email == addrs.EMAIL && c.CCL_Telephone == addrs.TELEPHONE && c.CCL_AddressType == addrs.ADDRESS_TYPE).ToList();
            var resp = cus.Count > 0;
            return resp;
        }
        private bool ValidateExist(Customers cust)
        {
            var cus = _db.CAT_Customers.Where(c => c.CCL_BarCode == cust.BarCode && c.CCL_CRMId == cust.CRMId).ToList();
            var resp = cus.Count > 0;
            return resp;
        }
        public bool validateCustomerById(string customerId)
        {
            var resp = false;
            var cus = _db.CAT_Customers.First(c => c.CCL_BarCode == customerId);
            if (cus != null)
            {
                resp = true;
            }
            return resp;
        }
        private CAT_Customers Map(OrderAddresses adrs, List<int> info)
        {
            var customer = new CAT_Customers
            {
                Id = 0,
                CCL_Name = $"{adrs.FIRSTNAME} {adrs.LASTNAME}",
                CCL_PostCode = adrs.POSTCODE,
                CCL_AddressType = adrs.ADDRESS_TYPE,
                CCL_Telephone = adrs.TELEPHONE,
                CCL_Email = adrs.EMAIL,
                CCL_Country = Convert.ToInt32(adrs.COUNTRY_ID),
                CCL_City = Convert.ToInt32(adrs.MUNICIPALITY),
                CCL_CRMId = "",
                CCL_CCTHId = adrs.CUSTOMER_ID,
                CCL_StatusConfirmation = 0,
                CCL_RouteCode = info[1],
                CCL_DistributionCenter = info[0],
                CCL_CustomerType = adrs.CustomerType,
                CCL_BarCode = adrs.BarCode,
                CCL_State = Convert.ToInt32(adrs.REGION),
                CCL_Street = adrs.STREET,
                CCL_PhysicalAddress = adrs.STREET,
                CCL_Neighborhood = Convert.ToInt32(adrs.NEIGHBORHOOD)
            };

            return customer;
        }
        private CAT_Customers Map(Customers customer)
        {
            var catCustomer = new CAT_Customers
            {
                Id = customer.Id != null ? Convert.ToInt32(customer.Id) : 0,
                CCL_CRMId = customer.CRMId,
                CCL_StatusConfirmation = customer.StatusConfirmation,
                CCL_Name = customer.Name,
                CCL_Cuc = IsNullOrWhiteSpace(customer.Cuc) ? 0 : Convert.ToInt32(customer.Cuc),
                CCL_BarCode = customer.BarCode,
                CCL_PriceList = customer.PriceList,
                CCL_Sequence = customer.Sequence,
                CCL_DistributionCenter = Convert.ToInt32(customer.DistributionCenter),
                CCL_RouteCode = Convert.ToInt32(customer.RouteCode),
                CCL_Manager = Convert.ToInt32(customer.Manager),
                CCL_PhysicalAddress = customer.PhysicalAddress,
                CCL_Street = customer.Street,
                CCL_IndoorNumber = customer.IndoorNumber,
                CCL_OutdoorNumber = customer.OutdoorNumber,
                CCL_Intersection1 = customer.Intersection1,
                CCL_Intersection2 = customer.Intersection2,
                CCL_Country = Convert.ToInt32(customer.Country),
                CCL_State = Convert.ToInt32(customer.State),
                CCL_City = Convert.ToInt32(customer.City),
                CCL_Neighborhood = Convert.ToInt32(customer.Neighborhood),
                CCL_ClientType = customer.ClientType,
                CCL_CustomerVarious = customer.CustomerVarious,
                CCL_Email = customer.Email,
                CCL_PostCode = customer.PostCode,
                CCL_Telephone = customer.Telephone,
                CCL_Latitude = customer.Latitude,
                CCL_Longitude = customer.Longitude,
                CCL_Contact = customer.Contact,
                CCL_Description = customer.Description,
                CCL_AddressType = customer.AddressType,
                CCL_WBCId = customer.WBCId,
                CCL_CustomerType = string.IsNullOrEmpty(customer.CustomerType) ? CustomerType.CLIENTE_COCACOLA : customer.CustomerType
            };
            if (!IsNullOrWhiteSpace(customer.CreatedOn))
            {
                DateTime createdOn;
                DateTime.TryParse(customer.CreatedOn, out createdOn);
                catCustomer.CCL_CreatedOn = createdOn;
            }
            else
            {
                catCustomer.CCL_CreatedOn = DateTime.Now;
            }
            if (!IsNullOrWhiteSpace(customer.ModifiedOn))
            {
                DateTime modifiedOn;
                DateTime.TryParse(customer.ModifiedOn, out modifiedOn);
                catCustomer.CCL_ModifiedOn = modifiedOn;
            }
            else
            {
                catCustomer.CCL_ModifiedOn = DateTime.Now;
            }
            catCustomer.CCL_CRMCreateOn = customer.CRMCreateOn;
            catCustomer.CAT_Visits.Add(_visitMapper.Map(customer.Visit, catCustomer.Id));
            return catCustomer;
        }
        private void Map(Customers customer, CAT_Customers catCustomer)
        {
            catCustomer.Id = string.IsNullOrEmpty(customer.Id) ? 0 : Convert.ToInt32(customer.Id);
            catCustomer.CCL_CRMId = customer.CRMId;
            catCustomer.CCL_CCTHId = customer.CCTHId;
            catCustomer.CCL_StatusConfirmation = customer.StatusConfirmation;
            catCustomer.CCL_Name = customer.Name;
            if(!string.IsNullOrEmpty(customer.Cuc) && Convert.ToInt32(customer.Cuc) > 0)
            {
                catCustomer.CCL_Cuc = Convert.ToInt32(customer.Cuc);
            }
            catCustomer.CCL_BarCode = customer.BarCode;
            catCustomer.CCL_PriceList = customer.PriceList;
            catCustomer.CCL_Sequence = customer.Sequence;
            catCustomer.CCL_DistributionCenter = Convert.ToInt32(customer.DistributionCenter);
            catCustomer.CCL_RouteCode = Convert.ToInt32(customer.RouteCode);
            catCustomer.CCL_Manager = Convert.ToInt32(customer.Manager);
            catCustomer.CCL_PhysicalAddress = customer.PhysicalAddress;
            catCustomer.CCL_Street = customer.Street;
            catCustomer.CCL_IndoorNumber = customer.IndoorNumber;
            catCustomer.CCL_OutdoorNumber = customer.OutdoorNumber;
            catCustomer.CCL_Intersection1 = customer.Intersection1;
            catCustomer.CCL_Intersection2 = customer.Intersection2;
            catCustomer.CCL_Country = Convert.ToInt32(customer.Country);
            catCustomer.CCL_State = Convert.ToInt32(customer.State);
            catCustomer.CCL_City = Convert.ToInt32(customer.City);
            catCustomer.CCL_Neighborhood = Convert.ToInt32(customer.Neighborhood);
            catCustomer.CCL_ClientType = customer.ClientType;
            catCustomer.CCL_CustomerVarious = customer.CustomerVarious;
            catCustomer.CCL_Email = customer.Email;
            catCustomer.CCL_PostCode = customer.PostCode;
            catCustomer.CCL_Telephone = customer.Telephone;
            catCustomer.CCL_Latitude = customer.Latitude;
            catCustomer.CCL_Longitude = customer.Longitude;
            catCustomer.CCL_Contact = customer.Contact;
            catCustomer.CCL_Description = customer.Description;
            catCustomer.CCL_AddressType = customer.AddressType;
            catCustomer.CCL_WBCId = customer.CustomerId;
            catCustomer.CCL_CustomerType = string.IsNullOrEmpty(customer.CustomerType) ? CustomerType.CLIENTE_COCACOLA : customer.CustomerType;

            if (!catCustomer.CAT_Visits.Any())
                catCustomer.CAT_Visits.Add(new CAT_Visits());
            _visitMapper.Map(customer.Visit, catCustomer.Id, catCustomer.CAT_Visits.First());
        }
        private List<Customers> Map(List<CAT_Customers> customers)
        {
            return customers.Select(customer => Map(customer)).ToList();
        }
        private Customers Map(CAT_Customers catCustomer)
        {
            try
            {
                var customer = new Customers();
                customer.Id = catCustomer.Id.ToString();
                customer.CustomerId = catCustomer.CCL_WBCId;
                customer.CRMId = catCustomer.CCL_CRMId;
                customer.StatusConfirmation = (int)catCustomer.CCL_StatusConfirmation;
                customer.Name = catCustomer.CCL_Name;
                customer.Cuc = catCustomer.CCL_Cuc.ToString();
                customer.BarCode = catCustomer.CCL_BarCode;
                customer.PriceList = catCustomer.CCL_PriceList != null ? (long)catCustomer.CCL_PriceList : 0;
                customer.Sequence = catCustomer.CCL_Sequence != null ? (int)catCustomer.CCL_Sequence : 0;
                customer.DistributionCenter = catCustomer.CCL_DistributionCenter.ToString();
                customer.RouteCode = catCustomer.CCL_RouteCode.ToString();
                customer.Manager = catCustomer.CCL_Manager.ToString();
                customer.PhysicalAddress = catCustomer.CCL_PhysicalAddress;
                customer.Street = catCustomer.CCL_Street;
                customer.IndoorNumber = catCustomer.CCL_IndoorNumber;
                customer.OutdoorNumber = catCustomer.CCL_OutdoorNumber;
                customer.Intersection1 = catCustomer.CCL_Intersection1;
                customer.Intersection2 = catCustomer.CCL_Intersection2;
                customer.Country = catCustomer.CCL_Country.ToString();
                customer.State = catCustomer.CCL_State.ToString();
                customer.City = catCustomer.CCL_City.ToString();
                customer.Neighborhood = catCustomer.CCL_Neighborhood.ToString();
                customer.ClientType = catCustomer.CCL_ClientType != null ? (bool)catCustomer.CCL_ClientType : false;
                customer.CustomerVarious = catCustomer.CCL_CustomerVarious != null ? (bool)catCustomer.CCL_CustomerVarious : false;
                customer.CustomerType = catCustomer.CCL_CustomerType;
                customer.Email = catCustomer.CCL_Email;
                customer.PostCode = catCustomer.CCL_PostCode;
                customer.Telephone = catCustomer.CCL_Telephone;
                customer.Latitude = catCustomer.CCL_Latitude;
                customer.Longitude = catCustomer.CCL_Longitude;
                customer.CreatedOn = catCustomer.CCL_CreatedOn.ToString();
                customer.ModifiedOn = catCustomer.CCL_ModifiedOn.ToString();
                customer.CRMCreateOn = catCustomer.CCL_CRMCreateOn;
                customer.Contact = catCustomer.CCL_Contact;
                customer.Description = catCustomer.CCL_Description;
                customer.AddressType = catCustomer.CCL_AddressType;
                customer.Visit = _visitMapper.Map(catCustomer.CAT_Visits.Any() ? catCustomer.CAT_Visits.First() : new CAT_Visits());
                customer.CCTHId = catCustomer.CCL_CCTHId;

                return customer;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public Customers Map(Models.WBC.Customer customer)
        {
            var customers = new Customers
            {
                Id = customer.Id,
                CustomerId = customer.CustomerId,
                CRMId = customer.CRMId,
                WBCId = customer.WBCId,
                Name = customer.Name,
                Cuc = customer.Cuc,
                BarCode = customer.BarCode,
                PriceList = customer.PriceList,
                Sequence = customer.Sequence,
                DistributionCenter = customer.DistributionCenter,
                RouteCode = customer.RouteCode,
                Manager = customer.Manager,
                PhysicalAddress = customer.PhysicalAddress,
                Street = customer.Street,
                IndoorNumber = customer.IndoorNumber,
                OutdoorNumber = customer.OutdoorNumber,
                Intersection1 = customer.Intersection1,
                Intersection2 = customer.Intersection2,
                Country = customer.Country,
                State = customer.State,
                City = customer.City,
                Neighborhood = customer.Neighborhood,
                ClientType = customer.ClientType,
                CustomerVarious = customer.CustomerVarious,
                CustomerType = customer.CustomerType,
                Email = customer.Email,
                PostCode = customer.PostCode,
                Telephone = customer.Telephone,
                Latitude = customer.Latitude,
                Longitude = customer.Longitude,
                CreatedOn = customer.CreatedOn,
                ModifiedOn = customer.ModifiedOn,
                CRMCreateOn = customer.CRMCreateOn,
                Visit = customer.Visit
            };
            return customers;
        }
        private List<Customers> MapLitsCediRoute(List<CAT_Customers> cus)
        {

            var listCustomer = new List<Customers>();
            if (cus == null || !cus.Any())
                return listCustomer;

            listCustomer.AddRange(cus.Select(key => Map(key)));
            return listCustomer;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
        private void SetCorrectCityAndCountry(OrderAddresses orderAddress)
        {
            int uselessAux = 0;
            if (!int.TryParse(orderAddress.COUNTRY_ID, out uselessAux))
                orderAddress.COUNTRY_ID = _db.CONF_Pais.SingleOrDefault(pais => pais.COD_CodeCCTH.ToUpper() == orderAddress.COUNTRY_ID.ToUpper()).COD_OpeId.ToString();
            if (!int.TryParse(orderAddress.REGION, out uselessAux))
                orderAddress.REGION = _db.CONF_Estado.SingleOrDefault(estado => estado.COD_CodeCCTH.ToUpper() == orderAddress.REGION.ToUpper()).COD_OpeId.ToString();
            if (!int.TryParse(orderAddress.MUNICIPALITY, out uselessAux))
                orderAddress.MUNICIPALITY = _db.CONF_Municipio.SingleOrDefault(municipio => municipio.COD_CodeCCTH.ToUpper() == orderAddress.MUNICIPALITY.ToUpper()).COD_OpeId.ToString();
            if (!int.TryParse(orderAddress.NEIGHBORHOOD, out uselessAux))
            {
                orderAddress.NEIGHBORHOOD = _db.CONF_Colonia.SingleOrDefault(colonia => colonia.COD_CodeCCTH.ToUpper() == orderAddress.NEIGHBORHOOD.ToUpper()
                && colonia.COD_EstadoOpeId.ToString().ToUpper() == orderAddress.REGION.ToUpper()
                && colonia.COD_MunicipioOpeId.ToString() == orderAddress.MUNICIPALITY.ToUpper()).COD_OpeId.ToString();
            }
        }

    }
}
