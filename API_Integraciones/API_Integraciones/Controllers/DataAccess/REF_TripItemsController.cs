﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.AdminPedidos;
using API_Integraciones.Controllers.DataAcces;

namespace API_Integraciones.Controllers.DataAccess
{
    public class REF_TripItemsController : Controller
    {
        private CCTH_INTERMEDIAEntities db = new CCTH_INTERMEDIAEntities();

        public List<InventoryDet> FindInventoryDetById(int tripId)
        {
            List<REF_TripItems> tripItems = db.REF_TripItems.Where(tr => tr.TIT_TripId == tripId).ToList();
            return Map(tripItems);
        }
        private List<InventoryDet> Map(List<REF_TripItems> tripItems)
        {

            List<InventoryDet> listInventoryDet = new List<InventoryDet>();
            if (tripItems == null || !tripItems.Any())
                return listInventoryDet;

            foreach (var key in tripItems)
            {
                CAT_Products prod = new CAT_ProductsController().Product(Convert.ToInt32(key.TIT_ItemId));
                InventoryDet row = new InventoryDet()
                {
                    Id = key.Id,
                    ItemId = key.TIT_ItemId,
                    Name = prod.CPD_Nombre,
                    Qty = key.TIT_Qty,
                    TripId = key.TIT_TripId,
                    WbcId = key.TIT_WbcId
                };
                listInventoryDet.Add(row);
            }
            return listInventoryDet;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
