﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.AdminUsers;

namespace API_Integraciones.Controllers.DataAcces
{
    public class CAT_UsersController
    {
        private CCTH_INTERMEDIAEntities db = new CCTH_INTERMEDIAEntities();

        public bool Validate(CAT_Users su)
        {
            bool resp = false;
            CAT_Users user = db.CAT_Users.First(u => u.CUS_User == su.CUS_User && u.CUS_Pass == su.CUS_Pass && u.CUS_Enable == 1);
            if (user != null)
            {
                resp = true;
            }
            return resp;
        }

        public List<User> Find(User userFilter)
        {

            var query = db.CAT_Users.AsEnumerable();
            if (userFilter.Id > 0)
                query = query.Where(q => q.Id == userFilter.Id);
            if (!string.IsNullOrEmpty(userFilter.Username))
                query = query.Where(q => q.CUS_User == userFilter.Username);
            if (!string.IsNullOrEmpty(userFilter.Password))
                query = query.Where(q => q.CUS_Pass == userFilter.Password);
            query = query.Where(q => q.CUS_Enable == userFilter.Enabled);

            var userList = new List<User>();
            foreach (var user in query)
            {
                userList.Add(Map(user));
            }
            db.Dispose();
            return userList;
        }

        private User Map(CAT_Users catUser)
        {
            var user = new User();
            user.Id = catUser.Id;
            user.Username = catUser.CUS_User;
            user.Password = catUser.CUS_Pass;
            user.Enabled = catUser.CUS_Enable;
            return user;
        }
    }
}
