﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models;

namespace API_Integraciones.Controllers
{
    public class CAT_OrderItemsController : Controller
    {
        private CCTH_INTERMEDIAEntities db = new CCTH_INTERMEDIAEntities();

        // POST: CAT_OrderItems/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        // [HttpPost]
        // [ValidateAntiForgeryToken]
        public CAT_OrderItemsController()
        {

        }
        public CAT_OrderItemsController(CCTH_INTERMEDIAEntities db)
        {
            this.db = db;
        }

        public int Create(OrderItem orderItem, int orderId)
        {
            var catOrderItem = Map(orderItem, orderId);
            db.CAT_OrderItems.Add(catOrderItem);
            return catOrderItem.Id;
        }

        public void BulkCreation(List<OrderItem> orderItems, int orderId)
        {
            orderItems.ForEach(orderItem => Create(orderItem, orderId));
        }

        public void Create(List<OrderItem> OrderItems, int ordenId)
        {
            foreach (var key in OrderItems)
            {
                db.CAT_OrderItems.Add(Map(key, ordenId));
                db.SaveChanges();
            }
        }
        public void CreateAP(List<OrderItem> OrderItems, int ordenId)
        {
            foreach (var key in OrderItems)
            {
                db.CAT_OrderItems.Add(Map(key, ordenId));
                db.SaveChanges();
            }
        }
        public void Edit(List<OrderItem> items, List<int> removes, int InterId)
        {

            foreach (var key in removes)
            {
                CAT_OrderItems cAT_OrderItems = db.CAT_OrderItems.First(o => o.ORI_ItemId == key && o.ORI_OrderGeneralId == InterId);
                db.CAT_OrderItems.Remove(cAT_OrderItems);
                db.SaveChanges();
            }
            foreach (var key in items)
            {
                if (key.InterId != 0 || key.InterId > 0)
                {
                    CAT_OrderItems item = findById(key.InterId, InterId);
                    if (item != null)
                    {
                        item.ORI_QtyOrdered = key.QTY_ORDERED;
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                else
                {
                    db.CAT_OrderItems.Add(Map(key, InterId));
                    db.SaveChanges();
                }
            }

        }
        private CAT_OrderItems findById(int itemId, int InterId)
        {
            CAT_OrderItems item = db.CAT_OrderItems.First(i => i.Id == itemId && i.ORI_OrderGeneralId == InterId);
            db.Entry(item).State = EntityState.Detached;
            return item;
        }
        public List<OrderItem> findListById(int interId)
        {
            List<CAT_OrderItems> items = db.CAT_OrderItems.Where(i => i.ORI_OrderGeneralId == interId).ToList();
            List<OrderItem> listitems = new List<OrderItem>();
            foreach (var key in items)
            {
                OrderItem row = new OrderItem();
                row.InterId = key.Id;
                row.ITEM_ID = key.ORI_ItemId;
                row.NAME = key.ORI_Name;
                row.QTY_ORDERED = key.ORI_QtyOrdered;
                listitems.Add(row);
            }
            return listitems;
        }
        private CAT_OrderItems Map(OrderItem OrI, int ordenId)
        {
            CAT_OrderItems ITEMS = new CAT_OrderItems()
            {
                Id = 0,
                ORI_ItemId = OrI.ITEM_ID,
                ORI_ParentItemId = OrI.PARENT_ITEM_ID,
                ORI_Sku = OrI.SKU,
                ORI_Name = OrI.NAME,
                ORI_QtyCanceled = OrI.QTY_CANCELED,
                ORI_QtyInvoiced = OrI.QTY_INVOICED,
                ORI_QtyOrdered = OrI.QTY_ORDERED,
                ORI_QtyRefunded = OrI.QTY_REFUNDED,
                ORI_QtyShipped = OrI.QTY_SHIPPED,
                ORI_Price = OrI.PRICE,
                ORI_OriginalPrice = OrI.ORIGINAL_PRICE,
                ORI_DiscountAmount = OrI.DISCOUNT_AMOUNT,
                ORI_RowTotal = OrI.ROW_TOTAL,
                ORI_PriceInclTax = OrI.PRICE_INCL_TAX,
                ORI_RowTotalInclTax = OrI.ROW_TOTAL_INCL_TAX,
                ORI_InterId = OrI.ITEM_ID,
                ORI_OrderGeneralId = ordenId,
                ORI_Applied_rule_ids = OrI.APPLIED_RULE_IDS
            };

            return ITEMS;
        }
        public List<OrderItem> MapItemsJson(ICollection<CAT_OrderItems> ITEMS)
        {
            List<OrderItem> listItems = new List<OrderItem>();
            foreach (var OrI in ITEMS)
            {
                OrderItem Item = new OrderItem()
                {
                    InterId = OrI.ORI_OrderGeneralId,
                    ITEM_ID = OrI.ORI_ItemId,
                    PARENT_ITEM_ID = OrI.ORI_ParentItemId,
                    SKU = OrI.ORI_Sku,
                    NAME = OrI.ORI_Name,
                    QTY_CANCELED = OrI.ORI_QtyCanceled,
                    QTY_INVOICED = OrI.ORI_QtyInvoiced,
                    QTY_ORDERED = OrI.ORI_QtyOrdered,
                    QTY_REFUNDED = OrI.ORI_QtyRefunded,
                    QTY_SHIPPED = OrI.ORI_QtyShipped,
                    PRICE = OrI.ORI_Price,
                    ORIGINAL_PRICE = OrI.ORI_OriginalPrice,
                    DISCOUNT_AMOUNT = OrI.ORI_DiscountAmount,
                    ROW_TOTAL = OrI.ORI_RowTotal,
                    PRICE_INCL_TAX = OrI.ORI_PriceInclTax,
                    ROW_TOTAL_INCL_TAX = OrI.ORI_RowTotalInclTax,
                    APPLIED_RULE_IDS = OrI.ORI_Applied_rule_ids
                };
                listItems.Add(Item);
            }
            return listItems;
        }

        // POST: CAT_OrderItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        private ActionResult DeleteConfirmed(int id)
        {
            CAT_OrderItems cAT_OrderItems = db.CAT_OrderItems.Find(id);
            db.CAT_OrderItems.Remove(cAT_OrderItems);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
