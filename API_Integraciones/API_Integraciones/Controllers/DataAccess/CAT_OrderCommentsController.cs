﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models;

namespace API_Integraciones.Controllers
{
    public class CAT_OrderCommentsController : Controller
    {
        private CCTH_INTERMEDIAEntities db = new CCTH_INTERMEDIAEntities();

        public CAT_OrderCommentsController()
        {

        }
        public CAT_OrderCommentsController(CCTH_INTERMEDIAEntities db)
        {
            this.db = db;
        }

        public int Create(OrderComments orderComments, int orderId)
        {
            var catOrderComments = Map(orderComments, orderId);
            db.CAT_OrderComments.Add(catOrderComments);
            return catOrderComments.Id;
        }

        public void BulkCreation(List<OrderComments> orderComments, int orderId)
        {
            orderComments.ForEach(orderComment => Create(orderComment, orderId));
        }

        private CAT_OrderComments Map(OrderComments OrC, int ordenId)
        {
            CAT_OrderComments COMMENTS = new CAT_OrderComments()
            {
                Id = 0,
                OCM_EntityId = OrC.ENTITY_ID,
                OCM_ParentId = OrC.PARENT_ID,
                OCM_IsCustomerNotified = OrC.IS_CUSTOMER_NOTIFIED,
                OCM_IsVisibleOnFront = OrC.IS_VISIBLE_ON_FRONT,
                OCM_Comment = OrC.COMMENT,
                OCM_Status = OrC.STATUS,
                OCM_CreatedAt = OrC.CREATED_AT,
                OCM_EntityName = OrC.ENTITY_NAME,
                OCM_StoreId = OrC.STORE_ID,
                OCM_OrderGeneralId = ordenId
            };

            return COMMENTS;
        }
        // GET: CAT_OrderComments/Edit/5
        private ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CAT_OrderComments cAT_OrderComments = db.CAT_OrderComments.Find(id);
            if (cAT_OrderComments == null)
            {
                return HttpNotFound();
            }
            ViewBag.OCM_OrderGeneralId = new SelectList(db.CAT_OrderGeneral, "Id", "ORD_Status", cAT_OrderComments.OCM_OrderGeneralId);
            return View(cAT_OrderComments);
        }

        // GET: CAT_OrderComments
        private ActionResult Index()
        {
            var cAT_OrderComments = db.CAT_OrderComments.Include(c => c.CAT_OrderGeneral);
            return View(cAT_OrderComments.ToList());
        }

        // GET: CAT_OrderComments/Details/5
        private ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CAT_OrderComments cAT_OrderComments = db.CAT_OrderComments.Find(id);
            if (cAT_OrderComments == null)
            {
                return HttpNotFound();
            }
            return View(cAT_OrderComments);
        }

        // GET: CAT_OrderComments/Create
        private ActionResult Create()
        {
            ViewBag.OCM_OrderGeneralId = new SelectList(db.CAT_OrderGeneral, "Id", "ORD_Status");
            return View();
        }

        // POST: CAT_OrderComments/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        private ActionResult Edit([Bind(Include = "Id,OCM_EntityId,OCM_ParentId,OCM_IsCustomerNotified,OCM_IsVisibleOnFront,OCM_Comment,OCM_Status,OCM_CreatedAt,OCM_EntityName,OCM_StoreId,OCM_OrderGeneralId")] CAT_OrderComments cAT_OrderComments)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cAT_OrderComments).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OCM_OrderGeneralId = new SelectList(db.CAT_OrderGeneral, "Id", "ORD_Status", cAT_OrderComments.OCM_OrderGeneralId);
            return View(cAT_OrderComments);
        }

        // GET: CAT_OrderComments/Delete/5
        private ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CAT_OrderComments cAT_OrderComments = db.CAT_OrderComments.Find(id);
            if (cAT_OrderComments == null)
            {
                return HttpNotFound();
            }
            return View(cAT_OrderComments);
        }

        // POST: CAT_OrderComments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        private ActionResult DeleteConfirmed(int id)
        {
            CAT_OrderComments cAT_OrderComments = db.CAT_OrderComments.Find(id);
            db.CAT_OrderComments.Remove(cAT_OrderComments);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
