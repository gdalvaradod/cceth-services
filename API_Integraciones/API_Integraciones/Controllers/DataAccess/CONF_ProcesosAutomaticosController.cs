﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.OpeCD;

namespace API_Integraciones.Controllers.DataAccess
{
    public class CONF_ProcesosAutomaticosController : Controller
    {
        private CCTH_INTERMEDIAEntities db = new CCTH_INTERMEDIAEntities();

        public ConfiguracionAutomatica FindConfigAuto(int cedis, int route)
        {
            CONF_ProcesosAutomaticos procAuto = db.CONF_ProcesosAutomaticos.
                SingleOrDefault( a=> a.CPA_Cedis == cedis && a.CPA_Ruta == route);
            if (procAuto == null)
                return null;
            return Map(procAuto);
        }
        public List<ConfiguracionAutomatica> FindConfigAutoList()
        {
            List<CONF_ProcesosAutomaticos> config = db.CONF_ProcesosAutomaticos.ToList();
            return Map(config);
        }
        public ConfiguracionAutomatica FindConfigAutoById(int id)
        {
            List<CONF_ProcesosAutomaticos> config = db.CONF_ProcesosAutomaticos.Where(a => a.Id == id).ToList();
            ConfiguracionAutomatica ConfigAuto = null;
            if (config.Count > 0)
            {
                ConfigAuto = Map(config[0]);
            }
            return ConfigAuto;
        }
        public List<ConfiguracionAutomatica> Find(int? id, string nombre, string sistema, int? cedisId, int? ruta, string hora, bool? status = null)
        {
            var queryConfiguracionAutom = from procesosAuto in db.CONF_ProcesosAutomaticos
                                          select procesosAuto;
            if (id != null)
                queryConfiguracionAutom = queryConfiguracionAutom.Where(configAutom => configAutom.Id == id);
            if (!string.IsNullOrEmpty(nombre))
                queryConfiguracionAutom = queryConfiguracionAutom.Where(configAutom => configAutom.CPA_Nombre.Contains(nombre));
            if (!string.IsNullOrEmpty(sistema))
                queryConfiguracionAutom = queryConfiguracionAutom.Where(configAutom => configAutom.CPA_Sistema.Contains(sistema));
            if (cedisId != null)
                queryConfiguracionAutom = queryConfiguracionAutom.Where(configAutom => configAutom.CPA_Cedis == cedisId);
            if (ruta != null)
                queryConfiguracionAutom = queryConfiguracionAutom.Where(configAutom => configAutom.CPA_Ruta == ruta);
            if (!string.IsNullOrEmpty(hora))
                queryConfiguracionAutom = queryConfiguracionAutom.Where(configAutom => configAutom.CPA_Hora.Equals(hora));
            if(status != null)
            {
                var activo = status == true ? 1 : 0;
                queryConfiguracionAutom = queryConfiguracionAutom.Where(configAutom => configAutom.CPA_Status == activo);
            }
                
            var configuracionesAutom = new List<ConfiguracionAutomatica>();
            queryConfiguracionAutom.ToList().ForEach(configAutom => configuracionesAutom.Add(Map(configAutom)));
            return configuracionesAutom;
        }
        public ConfiguracionAutomatica Find(int id)
        {
            var configuracionAutom = db.CONF_ProcesosAutomaticos.SingleOrDefault(proceso => proceso.Id == id);
            if (configuracionAutom == null)
                return null;
            return Map(configuracionAutom);
        }
        public void Edit(ConfiguracionAutomatica configuracion)
        {
            var conf_procesoAutomatico = db.CONF_ProcesosAutomaticos.SingleOrDefault(proceso => proceso.Id == configuracion.Id);
            if (conf_procesoAutomatico == null)
                return;
            Map(configuracion, conf_procesoAutomatico);
            conf_procesoAutomatico.CPA_Modificado = DateTime.Now;
            db.SaveChanges();
        }
        public int Create(ConfiguracionAutomatica configuracion)
        {
            var conf_procesoAutomatico = Map(configuracion);
            conf_procesoAutomatico.CPA_Modificado = DateTime.Now;
            db.CONF_ProcesosAutomaticos.Add(conf_procesoAutomatico);
            db.SaveChanges();
            return conf_procesoAutomatico.Id;
        }
        public void Delete(int configuracionId)
        {
            var configuracionAutomatica = db.CONF_ProcesosAutomaticos.Find(configuracionId);
            if (configuracionAutomatica != null)
            {
                configuracionAutomatica.CPA_Status = 0;
                db.SaveChanges();
            }
        }
        public bool ConfigurationExists(int route, int cedis)
        {
            return db.CONF_ProcesosAutomaticos
                .Where(conf => conf.CPA_Cedis == cedis && conf.CPA_Ruta == route)
                .Count() == 1;
        }
        private bool ValidateExist(int AutoId)
        {
            bool resp = false;
            List<CONF_ProcesosAutomaticos> ListConfigAuto = db.CONF_ProcesosAutomaticos.Where(c => c.CPA_Cedis == AutoId).ToList();
            if (ListConfigAuto.Count > 0) { resp = true; } else { resp = false; }
            return resp;
        }
        public bool validateCedis(int cedis)
        {
            bool resp = false;
            List<REF_Cedis> cedi = db.REF_Cedis.Where(c => c.REC_CedisIdOpecd == cedis).ToList();
            if (cedi.Count() > 0) {
                resp = true;
            }
            return resp;
        }
       
        public bool CedisExists(int cedisId)
        {
            return db.REF_Cedis.Find(cedisId) != null;
        }
        public CONF_ProcesosAutomaticos Map(ConfiguracionAutomatica configuracion)
        {
            CONF_ProcesosAutomaticos auto = new CONF_ProcesosAutomaticos()
            {
                Id = configuracion.Id,
                CPA_Nombre = configuracion.Nombre,
                CPA_Sistema = configuracion.Sistema,
                CPA_Cedis = configuracion.Cedis,
                CPA_Ruta = configuracion.Ruta,
                CPA_Hora = configuracion.Hora,
                CPA_Status = configuracion.Status
            };
            return auto;
        }
        public void Map(ConfiguracionAutomatica configuracion, CONF_ProcesosAutomaticos conf_procesoAutomatico)
        {
            conf_procesoAutomatico.Id = configuracion.Id;
            conf_procesoAutomatico.CPA_Nombre = configuracion.Nombre;
            conf_procesoAutomatico.CPA_Sistema = configuracion.Sistema;
            conf_procesoAutomatico.CPA_Cedis = configuracion.Cedis;
            conf_procesoAutomatico.CPA_Ruta = configuracion.Ruta;
            conf_procesoAutomatico.CPA_Hora = configuracion.Hora;
            conf_procesoAutomatico.CPA_Status = configuracion.Status;
        }
        public ConfiguracionAutomatica Map(CONF_ProcesosAutomaticos conf_procesoAutomatico)
        {
            ConfiguracionAutomatica configAutomatica = new ConfiguracionAutomatica()
            {
                Id = conf_procesoAutomatico.Id,
                Nombre = conf_procesoAutomatico.CPA_Nombre,
                Sistema = conf_procesoAutomatico.CPA_Sistema,
                Cedis = conf_procesoAutomatico.CPA_Cedis,
                Hora = conf_procesoAutomatico.CPA_Hora,
                Ruta = conf_procesoAutomatico.CPA_Ruta,
                Status = conf_procesoAutomatico.CPA_Status
            };
            return configAutomatica;
        }
        public List<ConfiguracionAutomatica> Map(List<CONF_ProcesosAutomaticos> Listconfig)
        {
            List<ConfiguracionAutomatica> list = new List<ConfiguracionAutomatica>();
            foreach (var key in Listconfig)
            {
                ConfiguracionAutomatica auto = new ConfiguracionAutomatica()
                {
                    Id = key.Id,
                    Nombre = key.CPA_Nombre,
                    Sistema = key.CPA_Sistema,
                    Cedis = key.CPA_Cedis,
                    Hora = key.CPA_Hora,
                    Status = key.CPA_Status
                };
                list.Add(auto);
            }
            return list;
        }
    }
}
