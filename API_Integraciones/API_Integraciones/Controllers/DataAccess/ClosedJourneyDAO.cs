﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API_Integraciones.Mappers;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.WBC;

namespace API_Integraciones.Controllers.DataAccess
{
    public class ClosedJourneyDAO
    {
        private readonly CCTH_INTERMEDIAEntities _context = new CCTH_INTERMEDIAEntities();
        private readonly ClosedJourneyMapper mapper = new ClosedJourneyMapper();

        public List<ClosedJourney> FindAll()
        {
            var closedJourneysQuery = from closedJourneys in _context.REF_ClosedJourneys
                                      select closedJourneys;
            return mapper.Map(closedJourneysQuery.ToList());
        }

        public List<ClosedJourney> Find(int? cedisId, int? routeId, DateTime? deliveryDate)
        {
            var closedJourneysQuery = from closedJourneys in _context.REF_ClosedJourneys
                                        select closedJourneys;
            if (cedisId != null)
                closedJourneysQuery = closedJourneysQuery.Where(cJourney => cJourney.CJ_CedisIdOpeCd == cedisId);
            if (routeId != null)
                closedJourneysQuery = closedJourneysQuery.Where(cJourney => cJourney.CJ_Route == routeId);
            if (deliveryDate != null)
                closedJourneysQuery = closedJourneysQuery.Where(cJourney => cJourney.CJ_DeliveryDate == deliveryDate);

            return mapper.Map(closedJourneysQuery.ToList());
        }

        public void Add(ClosedJourney closedJourney)
        {
            REF_ClosedJourneys ref_closedJourney = new REF_ClosedJourneys();
            mapper.Map(closedJourney, ref_closedJourney);
            _context.REF_ClosedJourneys.Add(ref_closedJourney);
            _context.SaveChanges();
        }

        public bool Delete(int cedisId, int routeId, DateTime deliveryDate)
        {
            var closedJourney = _context.REF_ClosedJourneys.SingleOrDefault( cJourney => 
                cJourney.CJ_CedisIdOpeCd == cedisId && cJourney.CJ_Route == routeId && cJourney.CJ_DeliveryDate == deliveryDate);
            if (closedJourney == null)
                return false;
            _context.REF_ClosedJourneys.Remove(closedJourney);
            _context.SaveChanges();
            return true;
        }
        
    }
}