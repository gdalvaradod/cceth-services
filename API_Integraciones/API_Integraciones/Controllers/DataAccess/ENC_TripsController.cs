﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.AdminPedidos;

namespace API_Integraciones.Controllers.DataAccess
{
    public class ENC_TripsController : Controller
    {
        private CCTH_INTERMEDIAEntities db = new CCTH_INTERMEDIAEntities();

        public List<InventoryEnc> FindListInventoryEnc(int cedisId)
        {
            try
            {
                List<ENC_Trips> listInventory = db.ENC_Trips.Where(tr => tr.TRI_CedisIdOpeCd == cedisId).ToList();
                return Map(listInventory);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private List<InventoryEnc> Map(List<ENC_Trips> listInventory)
        {
            List<InventoryEnc> listInventoryEnc = new List<InventoryEnc>();
            if (listInventory == null || !listInventory.Any())
                return listInventoryEnc;

            foreach (var key in listInventory)
            {
                InventoryEnc row = new InventoryEnc()
                {
                    Id = key.Id,
                    TripId = key.TRI_TripId,
                    Route = key.TRI_Route,
                    DeliveryDate = key.TRI_DeliveryDate,
                    CedisIdOpeCd = key.TRI_CedisIdOpeCd
                };
                listInventoryEnc.Add(row);
            }
            return listInventoryEnc;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
