﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.AdminPedidos;
using API_Integraciones.Models.WBC;

namespace API_Integraciones.Controllers.DataAcces
{
    public class REF_CedisController : Controller
    {
        private CCTH_INTERMEDIAEntities db = new CCTH_INTERMEDIAEntities();

        public void Create(Cedis rEF_Cedis)
        {
            if (ModelState.IsValid && ValidateCedis(rEF_Cedis))
            {
                rEF_Cedis.Id = 0;
                db.REF_Cedis.Add(Map(rEF_Cedis));
                db.SaveChanges();

            }
        }

        public Destinos FindCucGeneric(int cedi, int route)
        {
            var destino = new Destinos();
            //int cucgeneric = 0;
            var cuc = (from cedis in db.REF_Cedis
                       join routes in db.CONF_Destinos on cedis.Id equals routes.COD_CedisRefId
                       where cedis.REC_CedisIdOpecd == cedi && routes.COD_RouteCodeOpecd == route
                       select new { routes }).ToList();
            if (!cuc.Any()) return destino;
            destino.Id = cuc[0].routes.Id;
            destino.Cedis = cuc[0].routes.REF_Cedis.REC_CedisIdOpecd;
            destino.Cuc = cuc[0].routes.COD_Cuc;
            var codCucCcth = cuc[0].routes.COD_CucCCTH;
            if (codCucCcth != null) destino.CucCCTH = codCucCcth;
            //cucgeneric = cuc[0].COD_Cuc == null ? 0 : (int)cuc[0].COD_Cuc;
            return destino;
        }

        public List<Cedisinfo> FindCedisInfo()
        {
            List<Cedisinfo> listCedisInfo = new List<Cedisinfo>();
            List<REF_Cedis> listCedis = db.REF_Cedis.ToList().Where(cedis => cedis.REC_Enable == 1).ToList();

            foreach (var key in listCedis)
            {
                List<CONF_Destinos> listDestinos = db.CONF_Destinos.Where(d => d.COD_CedisRefId == key.Id && d.COD_Enable == 1).ToList();
                listCedisInfo.Add(Map(key.REC_CedisIdOpecd, listDestinos));
            }

            return listCedisInfo;
        }
        public List<Cedis> FindAll()
        {
            List<REF_Cedis> RCedis = db.REF_Cedis.ToList();
            return Map(RCedis);
        }
        public List<Cedis> FindCedisByUserId(int userId)
        {
            var ref_userCedis = db.REF_UserCedis.Where(uc => uc.UserId == userId);
            var cedisList = db.REF_Cedis.Where(c => ref_userCedis.Select(uc => uc.CedisId).Contains(c.Id)).ToList();
            var mappedCedis = Map(cedisList);
            return mappedCedis;
        }

        public void Edit(int cedisId, CedisPUT content)
        {

            if (ModelState.IsValid && ValidateExist(cedisId))
            {
                if (ValidateCedis(cedisId, content))
                {
                    db.Entry(Map(cedisId, content)).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }
        public void Delete(int cedisId)
        {
            if (ModelState.IsValid && ValidateExist(cedisId))
            {
                REF_Cedis Cedis = FindById(cedisId);
                Cedis.REC_Enable = 0;
                db.Entry(Cedis).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
        public int FindByCedisCCTH(string CedisCCTH)
        {
            int cediB = 0;
            REF_Cedis cedis = db.REF_Cedis.First(c => c.REC_CedisNameCCHT == CedisCCTH);
            if (cedis != null)
            {
                cediB = cedis.REC_CedisIdOpecd;
            }
            return cediB;
        }
        public string FindByOpeCdId(int cedisOpeCdId)
        {
            var name = db.REF_Cedis.SingleOrDefault(cedis => cedis.REC_CedisIdOpecd == cedisOpeCdId).REC_CedisNameCCHT;
            return name;
        }
        private List<Cedis> Map(List<REF_Cedis> RCedis)
        {
            List<Cedis> listCedis = new List<Cedis>();
            foreach (var key in RCedis)
            {
                Cedis cedi = new Cedis()
                {
                    Id = key.Id,
                    CedisNameCCHT = key.REC_CedisNameCCHT,
                    CedisIdOpecd = key.REC_CedisIdOpecd,
                    Nombre = key.REC_Nombre,
                    Enable = key.REC_Enable
                };
                listCedis.Add(cedi);
            }
            return listCedis;
        }
        private REF_Cedis Map(Cedis cedi)
        {
            REF_Cedis RCedis = new REF_Cedis()
            {
                Id = 0,
                REC_CedisNameCCHT = cedi.CedisNameCCHT,
                REC_CedisIdOpecd = cedi.CedisIdOpecd,
                REC_Nombre = cedi.Nombre,
                REC_Enable = cedi.Enable
            };
            return RCedis;

        }
        private REF_Cedis Map(int CId, CedisPUT cedi)
        {
            REF_Cedis Cedis = FindById(CId);
            REF_Cedis RCedis = new REF_Cedis()
            {
                Id = CId,
                REC_CedisNameCCHT = cedi.CedisNameCCHT,
                REC_CedisIdOpecd = Cedis.REC_CedisIdOpecd,
                REC_Nombre = cedi.Nombre,
                REC_Enable = cedi.Enable
            };
            return RCedis;
        }
        private Cedisinfo Map(int cedi, List<CONF_Destinos> rutas)
        {
            Cedisinfo info = new Cedisinfo();
            List<Destinos> destinos = new List<Destinos>();
            info.cedi = cedi;
            foreach (var key in rutas)
            {
                Destinos row = new Destinos();
                row.RouteCodeOpecd = key.COD_RouteCodeOpecd;
                destinos.Add(row);
            }
            info.Destinos = destinos;
            return info;
        }
        private REF_Cedis FindById(int RCediId)
        {
            REF_Cedis cedis = db.REF_Cedis.First(c => c.Id == RCediId);
            db.Entry(cedis).State = EntityState.Detached;
            return cedis;
        }
        private bool ValidateCedis(Cedis cedi)
        {
            bool resp = true;

            if (String.IsNullOrWhiteSpace(Convert.ToString(cedi.CedisIdOpecd)) || cedi.CedisIdOpecd == 0)
            {
                resp = false;
            }
            return resp;
        }
        private bool ValidateCedis(int CId, CedisPUT cedi)
        {
            bool resp = true;

            if (String.IsNullOrWhiteSpace(Convert.ToString(cedi.Nombre)) || String.IsNullOrWhiteSpace(cedi.CedisNameCCHT) || CId == 0)
            {
                resp = false;
            }
            return resp;
        }
        private bool ValidateExist(int cedisId)
        {
            var resp = false;
            List<REF_Cedis> Cedis = db.REF_Cedis.Where(c => c.Id == cedisId).ToList();
            if (Cedis.Count > 0)
            {
                db.Entry(Cedis[0]).State = EntityState.Detached;
                resp = true;
            }
            else
            {
                resp = false;
            }
            return resp;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
