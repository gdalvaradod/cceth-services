﻿using API_Integraciones;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.RouteConfiguration;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web;
using API_Integraciones.Enums;

namespace API_Integraciones.Controllers.DataAccess
{
    public class CONF_RoutesController
    {
        private CCTH_INTERMEDIAEntities db = new CCTH_INTERMEDIAEntities();

        public List<RouteConfiguration> FindAll()
        {
            var routeConfigurations = new List<RouteConfiguration>();

            var routeConfQuery = from configurations in db.CONF_Routes
                                 select configurations;

            if (routeConfQuery.Any())
            {
                foreach (var routeConf in routeConfQuery)
                {
                    var routeConfiguration = new RouteConfiguration();
                    Map(routeConf, routeConfiguration);
                    routeConfigurations.Add(routeConfiguration);
                }
            }
            return routeConfigurations;
        }

        public List<RouteConfiguration> Find(int? cedisId, int? routeId, int? zipCode, string neighborhood)
        {
            var routeConfQuery = from configurations in db.CONF_Routes
                                 select configurations;
            if (cedisId != null)
                routeConfQuery = routeConfQuery.Where(conf => conf.CedisId == cedisId);
            if (routeId != null)
                routeConfQuery = routeConfQuery.Where(conf => conf.RouteId == routeId);
            if (zipCode != null)
                routeConfQuery = routeConfQuery.Where(conf => conf.ZipCode == zipCode);
            if (!string.IsNullOrEmpty(neighborhood))
                routeConfQuery = routeConfQuery.Where(conf => conf.Neighborhood.ToUpper().Contains(neighborhood.ToUpper()));

            var routeConfigurations = new List<RouteConfiguration>();
            if (routeConfQuery.Any())
            {
                foreach (var routeConf in routeConfQuery)
                {
                    var routeConfiguration = new RouteConfiguration();
                    Map(routeConf, routeConfiguration);
                    routeConfigurations.Add(routeConfiguration);
                }
            }
            return routeConfigurations;
        }

        private void Map(CONF_Routes conf_Route, RouteConfiguration routeConfiguration)
        {
            routeConfiguration.Id = conf_Route.Id;
            routeConfiguration.CedisId = (int)conf_Route.CedisId;
            routeConfiguration.ZipCode = (int)conf_Route.ZipCode;
            routeConfiguration.Neigborhood = conf_Route.Neighborhood;
            routeConfiguration.RouteId = (int)conf_Route.RouteId;
            routeConfiguration.DeliveryDays = GetDeliveryDays(conf_Route.DeliveryDays);
        }

        private List<WeekDay> GetDeliveryDays(string concatenatedDeliveryDays)
        {
            var deliveryDays = new List<WeekDay>();
            var deliveryDaysArray = concatenatedDeliveryDays.Split(',');
            foreach (var deliveryDay in deliveryDaysArray)
            {
                deliveryDays.Add((WeekDay)Convert.ToInt32(deliveryDay));
            }
            return deliveryDays;
        }
    }
}