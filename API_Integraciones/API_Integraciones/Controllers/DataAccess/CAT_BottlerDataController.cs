﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models;
using API_Integraciones.Models.AdminPedidos;
using API_Integraciones.Controllers.DataAcces;
using API_Integraciones.Enums;

namespace API_Integraciones.Controllers
{
    public class CAT_BottlerDataController : Controller
    {
        private CCTH_INTERMEDIAEntities db = new CCTH_INTERMEDIAEntities();

        public CAT_BottlerDataController()
        {

        }

        public CAT_BottlerDataController(CCTH_INTERMEDIAEntities db)
        {
            this.db = db;
        }

        public int Create(BottlerData BottlerData, int ordenId)
        {
            var catBottlerData = Map(BottlerData, ordenId);
            db.CAT_BottlerData.Add(catBottlerData);
            return catBottlerData.Id;          
        }
        /// <summary>
        /// create to AdminPedidos
        /// </summary>
        /// <param name="BottlerData datos del embotallador"></param>
        /// <param name="ordenId id de la orden"></param>
        /// <param name="creadoPor si el portal o la api de coca cola"></param>
        /// <returns></returns>
        public void CreateAP(PedidoPOST BottlerData, int ordenId, string creadoPor)
        {
            db.CAT_BottlerData.Add(MapDefault(BottlerData, ordenId, creadoPor));
            db.SaveChanges();
        }
        public void Edit(int ordenId, int trip)
        {

            CAT_BottlerData Bottler = findByOrdenId(ordenId);
            if (Bottler != null)
            {
                Bottler.BOT_Trip = trip;
                db.Entry(Bottler).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
        public void Edit(int ordenId, PedidoPUT pedido)
        {
            CAT_BottlerData Bottler = findByOrdenId(ordenId);
            if (Bottler != null)
            {
                Bottler.BOT_DeliveryDate = Convert.ToDateTime(pedido.DELIVERY_DATE);
                Bottler.BOT_RouteCode = pedido.ROUTE_CODE;
                //Bottler.BOT_CediName = GetCedisName(pedido.cedis);
                Bottler.BOT_CediName = pedido.cedis.ToString();
                db.Entry(Bottler).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
        public CAT_BottlerData findByOrdenId(int Id)
        {
            List<CAT_BottlerData> ordenList = db.CAT_BottlerData.Where(o => o.BOT_OrderGeneralId == Id).ToList();
            return ordenList[0];
        }
        private CAT_BottlerData Map(BottlerData BtD,int ordenId)
        {
            DateTime Date = DateTime.Parse(BtD.DELIVERY_DATE);
            CAT_BottlerData BOTTLER = new CAT_BottlerData()
            {
                Id = 0,
                BOT_RouteId = BtD.ROUTE_ID,
                BOT_Website = BtD.WEBSITE,
                BOT_Store = BtD.STORE,
                BOT_Storeview = BtD.STOREVIEW,
                BOT_Postalcode = BtD.POSTALCODE,
                BOT_Deliveryco = BtD.DELIVERYCO,
                BOT_Status = BtD.STATUS,
                BOT_Settlement = BtD.SETTLEMENT,
                BOT_DeliveryDate = Date,
                BOT_RouteCode = BtD.ROUTE_CODE,
                BOT_CediName = BtD.CEDI_NAME,
                BOT_OrderGeneralId = ordenId,
                BOT_Trip = BtD.TRIP_ID
            };

            return BOTTLER;
        }
        private CAT_BottlerData MapDefault(PedidoPOST BtD, int ordenId,string creadoPor)
        {
            DateTime Date = DateTime.Parse(BtD.DELIVERY_DATE);
            CAT_BottlerData BOTTLER = new CAT_BottlerData()
            {
                Id = 0,
                BOT_RouteId = 0,
                BOT_Website = null,
                BOT_Store = null,
                BOT_Storeview = null,
                BOT_Postalcode = "null",
                BOT_Deliveryco = null,
                BOT_Status = 0,
                BOT_Settlement = null,
                BOT_DeliveryDate = Date,
                BOT_RouteCode = BtD.ROUTE_CODE,
                BOT_CediName = creadoPor == OrderCreatedAt.PORTAL ? Convert.ToString(BtD.cedis) : GetCedisName(BtD.cedis),
                BOT_Trip = 0,
                BOT_OrderGeneralId = ordenId
            };

            return BOTTLER;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private string GetCedisName(int cedisIdOpeCd) {
            var cedisName = db.REF_Cedis.Single(cedis => cedis.REC_CedisIdOpecd == cedisIdOpeCd).REC_CedisNameCCHT;
            return cedisName;
        }
    }
}
