﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.AdminPedidos;

namespace API_Integraciones.Controllers.DataAcces
{
    public class CONF_DestinosController : Controller
    {
        private CCTH_INTERMEDIAEntities db = new CCTH_INTERMEDIAEntities();


        public List<Destinos> FindAll()
        {
            List<CONF_Destinos> RCedis = db.CONF_Destinos.ToList();
            return Map(RCedis);
        }
        public List<Destinos> FindBy(Destinos destino)
        {
            var destinationsQuery = from destinations in db.CONF_Destinos
                                    select destinations;
            if (destino.Id > 0)
                destinationsQuery = destinationsQuery.Where(destination => destination.Id == destino.Id);
            if (!string.IsNullOrEmpty(destino.Nombre))
                destinationsQuery = destinationsQuery.Where(destination => destination.COD_Nombre.Contains(destino.Nombre));
            if (!string.IsNullOrEmpty(destino.RouteCodeCCHT))
                destinationsQuery = destinationsQuery.Where(destination => destination.COD_RouteCodeCCHT.Contains(destino.RouteCodeCCHT));
            if (destino.RouteCodeOpecd > 0)
                destinationsQuery = destinationsQuery.Where(destination => destination.COD_RouteCodeOpecd == destino.RouteCodeOpecd);
            if (destino.Cuc != null && destino.Cuc > 0)
                destinationsQuery = destinationsQuery.Where(destination => destination.COD_Cuc == destino.Cuc);
            if (destino.Manager != null && destino.Manager > 0)
                destinationsQuery = destinationsQuery.Where(destination => destination.COD_Manager == destino.Manager);
            if (destino.Cedis > 0)
                destinationsQuery = destinationsQuery.Where(destination => destination.REF_Cedis.REC_CedisIdOpecd == destino.Cedis);
            return Map(destinationsQuery.ToList());


        }
        public void Create(Destinos rEF_Destino)
        {
            if (ModelState.IsValid && ValidateDestinos(rEF_Destino))
            {
                rEF_Destino.Id = 0;
                db.CONF_Destinos.Add(Map(rEF_Destino));
                db.SaveChanges();
            }
        }

        public void Edit(int rutaId, DestinosPUT content)
        {
            if (ModelState.IsValid && ValidateExist(rutaId))
            {
                if (ValidateDestinos(rutaId, content))
                {
                    db.Entry(Map(rutaId, content)).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }

        private List<Destinos> Map(List<CONF_Destinos> Rruta)
        {
            List<Destinos> listDestinos = new List<Destinos>();
            foreach (var key in Rruta)
            {
                REF_Cedis cedis = db.REF_Cedis.First(c => c.Id == key.COD_CedisRefId);
                Destinos ruta = new Destinos()
                {
                    Id = key.Id,
                    RouteCodeCCHT = key.COD_RouteCodeCCHT,
                    RouteCodeOpecd = key.COD_RouteCodeOpecd,
                    Nombre = key.COD_Nombre,
                    Cuc = key.COD_Cuc,
                    Manager = key.COD_Manager,
                    Enable = key.COD_Enable,
                    Cedis = cedis.REC_CedisIdOpecd,
                    CucCCTH = key.COD_CucCCTH != null ? (int)key.COD_CucCCTH : 0
                };
                listDestinos.Add(ruta);
            }
            return listDestinos;
        }
        private CONF_Destinos Map(Destinos ruta)
        {
            REF_Cedis cedis = db.REF_Cedis.First(c => c.REC_CedisIdOpecd == ruta.Cedis);
            CONF_Destinos RDestino = new CONF_Destinos()
            {
                Id = 0,
                COD_RouteCodeCCHT = ruta.RouteCodeCCHT,
                COD_RouteCodeOpecd = ruta.RouteCodeOpecd,
                COD_Nombre = ruta.Nombre,
                COD_Enable = ruta.Enable,
                COD_Manager = ruta.Manager,
                COD_CucCCTH = ruta.CucCCTH,
                COD_Cuc = ruta.Cuc,
                COD_CedisRefId = cedis.Id
            };
            return RDestino;

        }
        private CONF_Destinos Map(int CId, DestinosPUT ruta)
        {
            CONF_Destinos Rutas = FindById(CId);
            Rutas.Id = CId;
            Rutas.COD_RouteCodeCCHT = ruta.RouteCodeCCHT;
            Rutas.COD_RouteCodeOpecd = ruta.RouteCodeOpecd;
            Rutas.COD_Nombre = ruta.Nombre;
            Rutas.COD_Enable = ruta.Enable;
            Rutas.COD_CedisRefId = Rutas.COD_CedisRefId;
            Rutas.COD_Cuc = ruta.Cuc;
            Rutas.COD_CucCCTH = ruta.CucCCTH;
            Rutas.COD_Manager = ruta.Manager;
            return Rutas;
        }
        private CONF_Destinos FindById(int RDestinoId)
        {
            CONF_Destinos ruta = db.CONF_Destinos.First(c => c.Id == RDestinoId);
            db.Entry(ruta).State = EntityState.Detached;
            return ruta;
        }
        public int findCucGeneric(string cedisId, string routesId)
        {
            int cediId = Convert.ToInt32(cedisId);
            int routeId = Convert.ToInt32(routesId);
            int resp = 0;
            var cucVario = (from cedis in db.REF_Cedis
                            join rutas in db.CONF_Destinos on cedis.Id equals rutas.COD_CedisRefId
                            where rutas.COD_Enable == 1 && cedis.REC_Enable == 1 && cedis.REC_CedisIdOpecd == cediId && rutas.COD_RouteCodeOpecd == routeId
                            select new { rutas.COD_Cuc }).ToList();
            if (cucVario.Count() > 0)
            {
                resp = (Int32)cucVario.First().COD_Cuc;
            }
            return resp;
        }
        private bool ValidateExist(int rutaId)
        {
            var resp = false;
            List<CONF_Destinos> Rutas = db.CONF_Destinos.Where(c => c.Id == rutaId).ToList();
            if (Rutas.Count > 0)
            {
                db.Entry(Rutas[0]).State = EntityState.Detached;
                resp = true;
            }
            else
            {
                resp = false;
            }
            return resp;
        }
        private bool ValidateDestinos(int DId, DestinosPUT destino)
        {
            bool resp = true;

            if (String.IsNullOrWhiteSpace(Convert.ToString(destino.Nombre)) || String.IsNullOrWhiteSpace(destino.RouteCodeCCHT) || DId == 0)
            {
                resp = false;
            }
            return resp;
        }
        private bool ValidateDestinos(Destinos Destinos)
        {
            bool resp = true;

            if (String.IsNullOrWhiteSpace(Convert.ToString(Destinos.RouteCodeOpecd)) || Destinos.RouteCodeOpecd == 0)
            {
                resp = false;
            }
            return resp;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
