﻿using API_Integraciones.Models.Entity;
using API_Integraciones.Models.WBC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Mappers
{
    public class SalePromotionMapper
    {
        public SalePromotion Map(DET_Promotions promotion)
        {
            var salePromotion = new SalePromotion();
            salePromotion.Quantity = promotion.DEP_Quantity;
            salePromotion.Product = promotion.DEP_Product;
            salePromotion.Type = promotion.DEP_Type;
            return salePromotion;
        }

        public List<SalePromotion> Map(List<DET_Promotions> promotions)
        {
            var salePromotions = new List<SalePromotion>();
            promotions.ForEach(p => salePromotions.Add(Map(p)));
            return salePromotions;
        }

        public DET_Promotions Map(SalePromotion promotion, int saleId)
        {
            var detPromotion = new DET_Promotions();
            detPromotion.DEP_Quantity = promotion.Quantity;
            detPromotion.DEP_Product = promotion.Product;
            detPromotion.DEP_Type = promotion.Type;
            detPromotion.DEP_SaleId = saleId;
            return detPromotion;
        }

        public List<DET_Promotions> Map(List<SalePromotion> promotions, int saleId)
        {
            var detPromotions = new List<DET_Promotions>();
            promotions.ForEach(promotion => detPromotions.Add(Map(promotion, saleId)));
            return detPromotions;
        }
    }
}