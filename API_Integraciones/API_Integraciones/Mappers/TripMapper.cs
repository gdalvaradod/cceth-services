﻿using API_Integraciones.Models.Entity;
using API_Integraciones.Models.WBC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Mappers
{
    public class TripMapper
    {
        public ENC_Trips Map(Trip entity, int cedisId)
        {
            var encTrip = new ENC_Trips();
            encTrip.TRI_TripId = entity.Id;
            encTrip.TRI_CedisIdOpeCd = cedisId;
            encTrip.TRI_DeliveryDate = entity.DeliveryDate;
            encTrip.TRI_Route = entity.Route;
            return encTrip;
        }
        public List<REF_TripItems> Map(Trip entity)
        {
            var tripItems = new List<REF_TripItems>();
            foreach (var item in entity.ConfirmedItems)
            {
                var tripItem = new REF_TripItems();
                tripItem.TIT_TripId = entity.Id;
                tripItem.TIT_ItemId = item.ItemId;
                tripItem.TIT_Qty = item.Qty;
                tripItem.TIT_WbcId = item.WbcId;
                tripItems.Add(tripItem);

            }
            return tripItems;
        }

        public Trip Map(ENC_Trips entity)
        {
            var trip = new Trip();
            trip.Id = entity.Id;
            trip.DeliveryDate = (DateTime)entity.TRI_DeliveryDate;
            trip.Route = entity.TRI_Route;
            return trip;
        }

        public List<Trip> Map(List<ENC_Trips> entities)
        {
            var tripList = new List<Trip>();
            entities.ForEach(entity => tripList.Add(Map(entity)));
            return tripList;
        }

    }
}