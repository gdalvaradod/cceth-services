﻿using API_Integraciones.Models.Entity;
using API_Integraciones.Models.Intermedia;

namespace API_Integraciones.Mappers
{
    public class LogDetailMapper
    {
        public void Map(LogDetail logDetail, DET_Log detLog)
        {
            detLog.DLO_Id = logDetail.Id;
            detLog.DLO_LogId = logDetail.LogId;
            detLog.DLO_ProcessId = logDetail.Process != null ? logDetail.Process.Id : 0;
            detLog.DLO_ErrorDescription = logDetail.ErrorDescription;
            detLog.DLO_Success = logDetail.Success;
        }

        public void Map(DET_Log logDetail, LogDetail detLog)
        {
            detLog.Id = logDetail.DLO_Id;
            detLog.LogId = logDetail.DLO_LogId;
            if (detLog.Process == null)
                detLog.Process = new Process();

            if (logDetail.DLO_ProcessId != null) detLog.Process.Id = (int)logDetail.DLO_ProcessId;
            detLog.ErrorDescription = logDetail.DLO_ErrorDescription;
            detLog.Success = logDetail.DLO_Success;
        }
    }
}