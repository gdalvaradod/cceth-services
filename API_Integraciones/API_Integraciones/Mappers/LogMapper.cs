﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.Intermedia;

namespace API_Integraciones.Mappers
{
    public class LogMapper
    {
        private readonly CCTH_INTERMEDIAEntities _context = new CCTH_INTERMEDIAEntities();
        public void Map(Log log, ENC_Log encLog)
        {
            encLog.ELO_Id = log.Id;
            encLog.ELO_CedisId = log.CedisId;
            encLog.ELO_RouteId = log.RouteId;
            encLog.ELO_DeliveryDate = log.DeliveryDate;
            encLog.ELO_Trip = log.Trip;
            encLog.ELO_ProcessStatus = (int)log.ProcessStatus;
            encLog.ELO_ProcessId = log.Process?.Id ?? 0;
        }

        public void Map(ENC_Log encLog, Log log)
        {
            log.Id = encLog.ELO_Id;
            log.CedisId = (int)encLog.ELO_CedisId;
            log.RouteId = (int)encLog.ELO_RouteId;
            log.DeliveryDate = (DateTime)encLog.ELO_DeliveryDate;
            log.Trip = (int)encLog.ELO_Trip;
            log.ProcessStatus = (ProcessStatus)encLog.ELO_ProcessStatus;
            if (log.Process == null)
                log.Process = new Process();
            if (log.LogDetail == null)
            {
                log.LogDetail = new List<LogDetail>();
            }
            foreach (var item in encLog.DET_Log)
            {
                var logDetail = new LogDetail();
                var mapp = new LogDetailMapper();
                mapp.Map(item, logDetail);
                log.LogDetail.Add(logDetail);
            }
            log.Process.Id = (int)encLog.ELO_ProcessId;
            log.Process.Description = encLog.CAT_Process.CPR_Description;
        }

        public List<Log> Map(List<ENC_Log> encLogs)
        {
            var logs = new List<Log>();
            foreach (var encLog in encLogs)
            {
                var logsDetailsQuery = from logsDetail in _context.DET_Log
                                       select logsDetail;
                logsDetailsQuery = logsDetailsQuery.Where(logD => logD.DLO_LogId == encLog.ELO_Id);

                encLog.DET_Log = logsDetailsQuery.ToList();
                var log = new Log();
                Map(encLog, log);
                logs.Add(log);
            }
            return logs;
        }
    }
}