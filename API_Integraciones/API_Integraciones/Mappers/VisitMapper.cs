﻿using API_Integraciones.Models.CRM;
using API_Integraciones.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Mappers
{
    public class VisitMapper
    {
        public void Map(Visit visit, int customerId, CAT_Visits catVisit)
        {
            catVisit.CTV_CustomerId = customerId;
            catVisit.CTV_Monday = visit.Monday;
            catVisit.CTV_Tuesday = visit.Tuesday;
            catVisit.CTV_Wednesday = visit.Wednesday;
            catVisit.CTV_Thursday = visit.Thursday;
            catVisit.CTV_Friday = visit.Friday;
            catVisit.CTV_Saturday = visit.Saturday;
            catVisit.CTV_Sunday = visit.Sunday;
        }

        public CAT_Visits Map(Visit visit, int customerId)
        {
            var catVisit = new CAT_Visits();
            catVisit.CTV_CustomerId = customerId;
            catVisit.CTV_Monday = visit.Monday;
            catVisit.CTV_Tuesday = visit.Tuesday;
            catVisit.CTV_Wednesday = visit.Wednesday;
            catVisit.CTV_Thursday = visit.Thursday;
            catVisit.CTV_Friday = visit.Friday;
            catVisit.CTV_Saturday = visit.Saturday;
            catVisit.CTV_Sunday = visit.Sunday;
            return catVisit;
        }

        public Visit Map(CAT_Visits catVisit)
        {
            var visit = new Visit();
            visit.Monday = catVisit.CTV_Monday;
            visit.Tuesday = catVisit.CTV_Tuesday;
            visit.Wednesday = catVisit.CTV_Wednesday;
            visit.Thursday = catVisit.CTV_Thursday;
            visit.Friday = catVisit.CTV_Friday;
            visit.Saturday = catVisit.CTV_Saturday;
            visit.Sunday = catVisit.CTV_Sunday;
            return visit;
        }
    }
}