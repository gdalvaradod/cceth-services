﻿using API_Integraciones.Models.Entity;
using API_Integraciones.Models.WBC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Mappers
{
    public class SaleProductMapper
    {
        public SaleProduct Map(DET_Sales detSale)
        {
            var saleProduct = new SaleProduct();
            saleProduct.Product = (int)detSale.DSS_Producto;
            saleProduct.Quantity = (int)detSale.DSS_Cantidad;
            saleProduct.CreditAmount = (int)detSale.DSS_CantidadCredito;
            saleProduct.Price = (decimal)detSale.DSS_Precio;
            saleProduct.BasePrice = (decimal)detSale.DSS_PrecioBase;
            saleProduct.Discount = (decimal)detSale.DSS_Descuento;
            saleProduct.DiscountRate = (decimal)detSale.DSS_PorcentajeDescuento;
            return saleProduct;
        }

        public List<SaleProduct> Map(List<DET_Sales> detSaleList)
        {
            var saleProductList = new List<SaleProduct>();
            detSaleList.ForEach(detSale => saleProductList.Add(Map(detSale)));
            return saleProductList;
        }

        public DET_Sales Map(SaleProduct product, int saleId)
        {
            var detSale = new DET_Sales();
            detSale.DSS_Producto = product.Product;
            detSale.DSS_Cantidad = product.Quantity;
            detSale.DSS_CantidadCredito = product.CreditAmount;
            detSale.DSS_SaleId = saleId;
            detSale.DSS_Precio = product.Price;
            detSale.DSS_PrecioBase = product.BasePrice;
            detSale.DSS_Descuento = product.Discount;
            detSale.DSS_PorcentajeDescuento = product.DiscountRate;
            return detSale;
        }

        public List<DET_Sales> Map(List<SaleProduct> products, int saleId)
        {
            var detSales = new List<DET_Sales>();
            products.ForEach(product => detSales.Add(Map(product, saleId)));
            return detSales;
        }
    }
}