﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.Intermedia;

namespace API_Integraciones.Mappers
{
    public class VisitDaysMapper
    {
        public void Map(CAT_Visits cat_visits, VisitDays visitDays)
        {
            visitDays.Id = cat_visits.Id;
            visitDays.CustomerId = cat_visits.CTV_CustomerId;
            visitDays.Monday = cat_visits.CTV_Monday;
            visitDays.Tuesday = cat_visits.CTV_Tuesday;
            visitDays.Wednesday = cat_visits.CTV_Wednesday;
            visitDays.Thursday = cat_visits.CTV_Thursday;
            visitDays.Friday = cat_visits.CTV_Friday;
        }
    }
}