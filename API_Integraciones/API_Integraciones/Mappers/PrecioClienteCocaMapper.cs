﻿using API_Integraciones.Models.Entity;
using API_Integraciones.Models.PrecioClienteCoca;
using System.Collections.Generic;

namespace API_Integraciones.Mappers
{
    public class PrecioClienteCocaMapper
    {
        private void Map(CustItemPricesWbc custPrice, PrecioClienteCoca precioCliente)
        {
            precioCliente.CedisId = custPrice.IdCedis;
            precioCliente.CustBarCode = custPrice.CustBarCode;
            if (custPrice.DeliveryDate != null)
                precioCliente.DeliveryDate = custPrice.DeliveryDate.Value.ToString("yyyy-MM-dd");
            precioCliente.ItemId = custPrice.ItemId;
            if (custPrice.Price != null) precioCliente.Price = custPrice.Price.Value;
            if (custPrice.BasePrice != null) precioCliente.BasePrice = custPrice.BasePrice.Value;
            if (custPrice.Discount != null) precioCliente.Discount = custPrice.Discount.Value;
            if (custPrice.DiscountRate != null) precioCliente.DiscountRate = custPrice.DiscountRate.Value;
        }

        public List<PrecioClienteCoca> Map(List<CustItemPricesWbc> precioCliente)
        {
            var preciosList = new List<PrecioClienteCoca>();
            foreach (var item in precioCliente)
            {
                var preClient = new PrecioClienteCoca();
                Map(item, preClient);
                preciosList.Add(preClient);
            }
            
            return preciosList;
        }
    }
}