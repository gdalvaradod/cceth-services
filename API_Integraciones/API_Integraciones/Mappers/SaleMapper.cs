﻿using API_Integraciones.Models.Entity;
using API_Integraciones.Models.OpeCD;
using API_Integraciones.Models.WBC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Mappers
{
    public class SaleMapper
    {
        public Sale Map(ENC_Sales encSale)
        {
            var sale = new Sale();
            sale.Id = encSale.Id;
            sale.Codope = (int)encSale.ESS_Codope;
            sale.Cuc = (int)encSale.ESS_CUC;
            sale.Facturado = encSale.ESS_Facturado == 1 ? true : false;
            sale.RazonDev = Convert.ToInt32(encSale.ESS_RazonDev);
            sale.WbcSaleId = (int)encSale.ESS_WBCSaleId;
            sale.RouteCode = (int)encSale.ESS_RouteCode;
            sale.DeliveryDate = (DateTime)encSale.ESS_DeliveryDate;
            sale.Trip = (short)encSale.ESS_Trip;
            sale.CedisId = (int)encSale.ESS_CedisId;
            sale.CustomerCCTH = (bool)encSale.ESS_CustomerCCTH;
            sale.BarCode = encSale.ESS_Barcode;
            return sale;
        }

        public PostSaleRequest Map(Sale sale)
        {
            var postSale = new PostSaleRequest();
            postSale.DeliveryOrderId = 0;
            postSale.CustomerOpeId = sale.Cuc;
            postSale.DeliveryDate = new DateTime(sale.DeliveryDate.Year, sale.DeliveryDate.Month, sale.DeliveryDate.Day, 0, 0, 0);
            postSale.RouteCode = sale.RouteCode; 
            postSale.Trip = sale.Trip;
            postSale.Code = sale.Code;

            foreach (var product in sale.SaleProducts)
            {
                postSale.SaleDetail.Add(
                new SaleItem {
                    ItemId = product.Product.ToString(),
                    Qty = product.Quantity
                });
            }
            postSale.SalePromotions = sale.SalePromotions;
            return postSale;
        }

        public List<PostSaleRequest> Map(List<Sale> sales)
        {
            var postSaleRequestList = new List<PostSaleRequest>();
            sales.ForEach(sale => postSaleRequestList.Add(Map(sale)));
            return postSaleRequestList;
        }

        public ENC_Sales MapToEnc_Sale(Sale sale)
        {
            var encSale = new ENC_Sales();
            encSale.ESS_Codope = sale.Codope;
            encSale.ESS_CUC = sale.Cuc;
            encSale.ESS_Facturado = sale.Facturado == true ? 1 : 0;
            encSale.ESS_RazonDev = sale.RazonDev.ToString();
            encSale.ESS_WBCSaleId = sale.WbcSaleId;
            encSale.ESS_DeliveryDate = sale.DeliveryDate;
            encSale.ESS_RouteCode = sale.RouteCode;
            encSale.ESS_Trip = sale.Trip;
            encSale.ESS_CedisId = sale.CedisId;
            encSale.ESS_FechaCreacion = DateTime.Now;
            encSale.ESS_Barcode = sale.Code;
            encSale.ESS_CustomerCCTH = sale.CustomerCCTH;
            return encSale;
        }

        public List<ENC_Sales> MapToEnc_Sale(List<Sale> sales)
        {
            List<ENC_Sales> encSales = new List<ENC_Sales>();
            sales.ForEach(sale => encSales.Add(MapToEnc_Sale(sale)));
            return encSales;
        }
    }
}