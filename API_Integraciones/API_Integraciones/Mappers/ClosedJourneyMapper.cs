﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.WBC;

namespace API_Integraciones.Mappers
{
    public class ClosedJourneyMapper
    {
        public void Map(ClosedJourney closedJourney, REF_ClosedJourneys ref_closedJourney)
        {
            ref_closedJourney.CJ_CedisIdOpeCd = closedJourney.CedisIdOpecd;
            ref_closedJourney.CJ_Route = closedJourney.Route;
            ref_closedJourney.CJ_DeliveryDate = closedJourney.DeliveryDate;
        }

        public void Map(REF_ClosedJourneys ref_closedJourney, ClosedJourney closedJourney)
        {
            closedJourney.CedisIdOpecd = ref_closedJourney.CJ_CedisIdOpeCd;
            closedJourney.Route = ref_closedJourney.CJ_Route;
            closedJourney.DeliveryDate = ref_closedJourney.CJ_DeliveryDate;
        }

        public List<ClosedJourney> Map(List<REF_ClosedJourneys> ref_closedJourneys)
        {
            List<ClosedJourney> closedJourneys = new List<ClosedJourney>();
            foreach (var ref_cJourney in ref_closedJourneys)
            {
                ClosedJourney closedJourney = new ClosedJourney();
                Map(ref_cJourney, closedJourney);
                closedJourneys.Add(closedJourney);
            }
            return closedJourneys;
        }
    }
}