﻿using API_Integraciones.Models;
using API_Integraciones.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Repositories
{
    public class OrderItemRepository
    {
        private readonly CCTH_INTERMEDIAEntities _db = new CCTH_INTERMEDIAEntities();

        public void EditOrderItemQty(int orderItemId, int qty)
        {
            var orderItem =_db.CAT_OrderItems.Find(orderItemId);
            if(orderItem != null)
            {
                orderItem.ORI_QtyOrdered = qty;
                _db.Entry(orderItem).State = System.Data.Entity.EntityState.Modified;
                _db.SaveChangesAsync();
            }
        } 
    }
}