﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API_Integraciones.Mappers;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.Intermedia;

namespace API_Integraciones.Repositories
{
    public class VisitDaysRepository
    {
        private readonly CCTH_INTERMEDIAEntities _db = new CCTH_INTERMEDIAEntities();
        private readonly VisitDaysMapper _mapper = new VisitDaysMapper();

        public VisitDays Find(int customerId)
        {
            var cat_visits = _db.CAT_Visits.SingleOrDefault(visit => visit.CTV_CustomerId == customerId);
            var visitDays = new VisitDays();
            _mapper.Map(cat_visits, visitDays);
            return visitDays;
        }
    }
}