﻿using API_Integraciones.Mappers;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.WBC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Repositories
{
    public class SalesRepository
    {
        private readonly CCTH_INTERMEDIAEntities _db = new CCTH_INTERMEDIAEntities();
        private readonly SaleMapper _saleMapper = new SaleMapper();
        private readonly SaleProductMapper _saleProductMapper = new SaleProductMapper();
        private readonly SalePromotionMapper _promotionMapper = new SalePromotionMapper();
        private readonly PromotionsRepository _promotionsRepo = new PromotionsRepository();
        private readonly SaleProductRepository _saleProductRepository = new SaleProductRepository();

        public List<Sale> Find(Sale salefilter)
        {
            var query = from sales in _db.ENC_Sales
                        select sales;
            if (salefilter.Id > 0)
                query = query.Where(sale => sale.Id == salefilter.Id);
            if (salefilter.WbcSaleId > 0)
                query = query.Where(sale => sale.ESS_WBCSaleId == salefilter.WbcSaleId);
            if (salefilter.CedisId > 0)
                query = query.Where(sale => sale.ESS_CedisId == salefilter.CedisId);
            if (salefilter.Codope > 0)
                query = query.Where(sale => sale.ESS_Codope == salefilter.Codope);
            if (salefilter.Cuc > 0)
                query = query.Where(sale => sale.ESS_CUC == salefilter.Cuc);
            if (salefilter.DeliveryDate != null)
                query = query.Where(sale => sale.ESS_DeliveryDate == salefilter.DeliveryDate);
            if (salefilter.RouteCode > 0)
                query = query.Where(sale => sale.ESS_RouteCode == salefilter.RouteCode);

            List<Sale> saleList = new List<Sale>();

            foreach (var encSale in query)
            {
                var sale = _saleMapper.Map(encSale);
                sale.SalePromotions = _promotionsRepo.FindPromotions(sale.Id);
                sale.SaleProducts = _saleProductRepository.FindSaleProducts(sale.Id);
                saleList.Add(sale);
            }

            return saleList;
        }


        private void Insert(Sale sale)
        {
            var encSale = _saleMapper.MapToEnc_Sale(sale);
            encSale.DET_Sales = _saleProductMapper.Map(sale.SaleProducts, encSale.Id);
            encSale.DET_Promotions = _promotionMapper.Map(sale.SalePromotions, encSale.Id);
            _db.ENC_Sales.Add(encSale);
        }

        public void BulkInsert(List<Sale> sales)
        {
            try
            {
                foreach(var sale in sales)
                {
                    var found = Find(new Sale
                    {
                        DeliveryDate = sale.DeliveryDate.Date,
                        RouteCode = sale.RouteCode,
                        CedisId = sale.CedisId,
                        WbcSaleId = sale.WbcSaleId
                    }).Count() > 0;

                    if (!found)
                    {
                        sale.DeliveryDate = sale.DeliveryDate.Date;
                        Insert(sale);
                    }
                }
                _db.SaveChanges();
            }
            catch(Exception ex) {
                throw ex;
            }
        }



    }
}