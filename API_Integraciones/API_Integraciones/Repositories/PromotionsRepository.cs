﻿using API_Integraciones.Mappers;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.WBC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Repositories
{
    public class PromotionsRepository
    {
        private readonly CCTH_INTERMEDIAEntities _db = new CCTH_INTERMEDIAEntities();
        private readonly SalePromotionMapper _mapper = new SalePromotionMapper();

        public List<SalePromotion> FindPromotions(int saleId)
        {
            var promotions = _db.DET_Promotions.Where(p => p.DEP_SaleId == saleId).ToList();
            return _mapper.Map(promotions);
        }
    }
}