﻿using API_Integraciones.Mappers;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.PrecioClienteCoca;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Repositories
{
    public class PrecioClienteCocaRepository
    {
        private readonly CCTH_INTERMEDIAEntities _context = new CCTH_INTERMEDIAEntities();
        private readonly PrecioClienteCocaMapper _mapper = new PrecioClienteCocaMapper();

        public List<PrecioClienteCoca> Find(int? cedisId = null, string barcode = "", DateTime? deliveryDate = null)
        {
            var preciosQuery = from precios in _context.CustItemPricesWbc
                            select precios;

            if (cedisId != null)
                preciosQuery = preciosQuery.Where(pre => pre.IdCedis == cedisId);
            if (!string.IsNullOrEmpty(barcode))
                preciosQuery = preciosQuery.Where(pre => pre.CustBarCode == barcode);
            if (deliveryDate != null)
                preciosQuery = preciosQuery.Where(pre => pre.DeliveryDate == deliveryDate);
            
            return _mapper.Map(preciosQuery.ToList());
        }
    }
}