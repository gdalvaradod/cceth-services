﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API_Integraciones.Mappers;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.Intermedia;

namespace API_Integraciones.Repositories
{
    public class LogDetailRepository
    {
        private readonly CCTH_INTERMEDIAEntities _context = new CCTH_INTERMEDIAEntities();
        private readonly LogDetailMapper _mapper = new LogDetailMapper();

        public int Insert(LogDetail logDetail)
        {
            var detLog = new DET_Log();
            _mapper.Map(logDetail, detLog);
            detLog.DLO_CreatedOn = DateTime.Now;
            _context.DET_Log.Add(detLog);
            _context.SaveChanges();
            return detLog.DLO_Id;
        }
    }

}