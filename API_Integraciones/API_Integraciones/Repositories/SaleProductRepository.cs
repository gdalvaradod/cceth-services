﻿using API_Integraciones.Mappers;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.WBC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Integraciones.Repositories
{
    public class SaleProductRepository
    {
        private readonly CCTH_INTERMEDIAEntities _db = new CCTH_INTERMEDIAEntities();
        private readonly SaleProductMapper _mapper = new SaleProductMapper();

        public List<SaleProduct> FindSaleProducts(int saleId)
        {
            var details = _db.DET_Sales.Where(d => d.DSS_SaleId == saleId);
            return _mapper.Map(details.ToList());
        }
    }
}