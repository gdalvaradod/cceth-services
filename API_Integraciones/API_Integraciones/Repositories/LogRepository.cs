﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API_Integraciones.Mappers;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.Intermedia;

namespace API_Integraciones.Repositories
{
    public class LogRepository
    {
        private readonly CCTH_INTERMEDIAEntities _context = new CCTH_INTERMEDIAEntities();
        private readonly LogMapper _mapper = new LogMapper();


        public List<Log> Find(int? cedisId = null, int? routeId = null, int? trip = null, DateTime? deliveryDate = null, int? processSatus = null)
        {
            var logsQuery = from logs in _context.ENC_Log
                       select logs;

            if (cedisId != null)
                logsQuery = logsQuery.Where(log => log.ELO_CedisId == cedisId);
            if (routeId != null)
                logsQuery = logsQuery.Where(log => log.ELO_RouteId == routeId);
            if (trip != null)
                logsQuery = logsQuery.Where(log => log.ELO_Trip == trip);
            if (deliveryDate != null)
                logsQuery = logsQuery.Where(log => log.ELO_DeliveryDate == deliveryDate);
            if (processSatus != null)
                logsQuery = logsQuery.Where(log => log.ELO_ProcessStatus == processSatus);

            return _mapper.Map(logsQuery.ToList());
        }

        public List<Log> Find(out int dbTotalRecords, out int filteredRecords, int? cedisId = null, int? routeId = null, int? trip = null, DateTime? deliveryDate = null, int? processSatus = null, int? start = 0, int? length = 100)
        {
            dbTotalRecords = (from logss in _context.ENC_Log
                              where logss.ELO_Trip != 0
                              select logss.ELO_Id).Count();

            var logsQuery = from logss in _context.ENC_Log
                            where logss.ELO_Trip != 0
                            select logss;
            if (cedisId != null)
                logsQuery = logsQuery.Where(log => log.ELO_CedisId == cedisId);
            if (routeId != null)
                logsQuery = logsQuery.Where(log => log.ELO_RouteId == routeId);
            if (trip != null)
                logsQuery = logsQuery.Where(log => log.ELO_Trip == trip);
            if (deliveryDate != null)
                logsQuery = logsQuery.Where(log => log.ELO_DeliveryDate == deliveryDate);
            if (processSatus != null)
                logsQuery = logsQuery.Where(log => log.ELO_ProcessStatus == processSatus);

            filteredRecords = logsQuery.Count();
            var logs = logsQuery.OrderByDescending(log => log.ELO_Id).Skip((int)start).Take((int)length).ToList();
            return _mapper.Map(logs);
        }

        public Log Find(int id)
        {
            var encLog = _context.ENC_Log.Find(id);
            if (encLog == null)
                return null;
            var log = new Log();
            _mapper.Map(encLog, log);
            return log;
        }

        public int Insert(Log log)
        {
            var encLog = new ENC_Log();
            _mapper.Map(log, encLog);
            encLog.ELO_CreatedOn = DateTime.Now;
            encLog.ELO_ModifiedOn = DateTime.Now;
            _context.ENC_Log.Add(encLog);
            _context.SaveChanges();
            return encLog.ELO_Id;
        }

        public void Edit(Log log)
        {
            var encLog = _context.ENC_Log.Find(log.Id);
            if (encLog == null)
                return;
            _mapper.Map(log, encLog);
            encLog.ELO_ModifiedOn = DateTime.Now;
            _context.SaveChanges();
        }

    }
}