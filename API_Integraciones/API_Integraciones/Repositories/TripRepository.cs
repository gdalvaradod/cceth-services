﻿using API_Integraciones.Mappers;
using API_Integraciones.Models.Entity;
using API_Integraciones.Models.WBC;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace API_Integraciones.Repositories
{
    public class TripRepository
    {
        private CCTH_INTERMEDIAEntities _db = new CCTH_INTERMEDIAEntities();
        private readonly TripMapper _tripMapper = new TripMapper();

        private void Insert(Trip entity, int cedisId)
        {
            var trip = _tripMapper.Map(entity, cedisId);
            trip.REF_TripItems = _tripMapper.Map(entity);
            _db.ENC_Trips.Add(trip);
        }

        public void BulkUpsert(List<Trip> trips, int cedisId)
        {
            foreach (var trip in trips)
            {
                var dbTrip = Find(trip.Id, trip.Route, cedisId, trip.DeliveryDate).SingleOrDefault();
                if (dbTrip != null)
                {
                    var existeDif = (from t in dbTrip.REF_TripItems where !trip.ConfirmedItems.Any(x => x.ItemId == t.TIT_ItemId && x.Qty == t.TIT_Qty && trip.Id == t.TIT_TripId) select t).ToList().Any();
                    if (!existeDif) continue;
                    _db.REF_TripItems.RemoveRange(dbTrip.REF_TripItems);
                    _db.ENC_Trips.Remove(dbTrip);
                    Insert(trip, cedisId);
                }
                else
                {
                    Insert(trip, cedisId);
                }
            }
            _db.SaveChanges();
        }

        public List<ENC_Trips> Find(int? tripId, int? route, int? opeCedisId, DateTime deliveryDate)
        {
            var tripsQuery = from trips in _db.ENC_Trips
                             select trips;
            if (tripId != null)
                tripsQuery = tripsQuery.Where(trip => trip.TRI_TripId == tripId);
            if (route != null)
                tripsQuery = tripsQuery.Where(trip => trip.TRI_Route == route);
            if (opeCedisId != null)
                tripsQuery = tripsQuery.Where(trip => trip.TRI_CedisIdOpeCd == opeCedisId);
            if (deliveryDate != null)
                tripsQuery = tripsQuery.Where(trip => trip.TRI_DeliveryDate == deliveryDate);

            return tripsQuery.ToList();
        }

        public List<Trip> GetInventory(int cedisId, int routeId, DateTime deliveryDate)
        {

            var trips = _db.ENC_Trips.Where(trip => trip.TRI_CedisIdOpeCd == cedisId && trip.TRI_Route == routeId && trip.TRI_DeliveryDate == deliveryDate)
           .Include(trip => trip.REF_TripItems)
           .ToList();

            var distinctTripItems = new List<REF_TripItems>();
            var distinctSaleProducts = new List<REF_TripItems>();
            var tripsResponse = new List<Trip>();

            foreach (var trip in trips.OrderBy(tripp => tripp.TRI_TripId))
            {
                var tripInventory = new List<REF_TripItems>(distinctTripItems);
                tripInventory.AddRange(trip.REF_TripItems);
                distinctTripItems.Clear();

                foreach (var groupItem in tripInventory.GroupBy(item => item.TIT_ItemId))
                {
                    var tripItem = new REF_TripItems();
                    tripItem.TIT_ItemId = groupItem.First().TIT_ItemId;
                    groupItem.ToList().ForEach(item => tripItem.TIT_Qty += item.TIT_Qty);
                    distinctTripItems.Add(tripItem);
                }

                foreach (var saleProduct in distinctSaleProducts)
                {
                    var tripItem = distinctTripItems.SingleOrDefault(item => item.TIT_ItemId == saleProduct.TIT_ItemId);
                    if (tripItem == null)
                        continue;
                    tripItem.TIT_Qty -= saleProduct.TIT_Qty;
                }

                var tripResponse = new Trip();
                tripResponse.Id = trip.TRI_TripId;
                tripResponse.Route = routeId;
                tripResponse.DeliveryDate = deliveryDate;
                foreach (var tripItem in distinctTripItems)
                {
                    if (tripItem.TIT_Qty <= 0)
                        continue;
                    tripResponse.ConfirmedItems.Add(new ItemQty
                    {
                        ItemId = tripItem.TIT_ItemId,
                        Qty = tripItem.TIT_Qty
                    });
                }
                tripsResponse.Add(tripResponse);

                var sales = _db.ENC_Sales.Where(sale => sale.ESS_RouteCode == routeId && sale.ESS_CedisId == cedisId && sale.ESS_DeliveryDate == deliveryDate && sale.ESS_Trip == trip.TRI_TripId)
                .Include(sale => sale.DET_Sales)
                .Include(sale => sale.DET_Promotions)
                .ToList();

                var saleProducts = new List<REF_TripItems>();
                sales.ForEach(sale =>
                    sale.DET_Sales.ToList().ForEach(detSale =>
                        saleProducts.Add(new REF_TripItems
                        {
                            TIT_ItemId = detSale.DSS_Producto.ToString(),
                            TIT_Qty = (int)detSale.DSS_Cantidad
                        })));
                var groupedSaleProducts = saleProducts.GroupBy(saleProd => saleProd.TIT_ItemId);
                distinctSaleProducts = new List<REF_TripItems>();
                foreach (var group in groupedSaleProducts)
                {
                    var item = new REF_TripItems();
                    item.TIT_ItemId = group.First().TIT_ItemId;
                    group.ToList().ForEach(i => item.TIT_Qty += i.TIT_Qty);
                    distinctSaleProducts.Add(item);
                }

                var salePromotions = new List<REF_TripItems>();
                sales.ForEach(sale =>
                     sale.DET_Promotions.ToList().ForEach(promotion =>
                         salePromotions.Add(new REF_TripItems
                         {
                             TIT_ItemId = promotion.DEP_Product.ToString(),
                             TIT_Qty = (int)promotion.DEP_Quantity
                         })));
                var groupedPromotions = salePromotions.GroupBy(saleProm => saleProm.TIT_ItemId);
                foreach (var groupedPromotion in groupedPromotions)
                {
                    var saleProduct = distinctSaleProducts.FirstOrDefault(dsp => dsp.TIT_ItemId == groupedPromotion.First().TIT_ItemId);
                    if (saleProduct != null)
                    {
                        groupedPromotion.ToList().ForEach(promotion => saleProduct.TIT_Qty += promotion.TIT_Qty);
                    }
                    else
                    {
                        var item = new REF_TripItems();
                        item.TIT_ItemId = groupedPromotion.First().TIT_ItemId;
                        groupedPromotion.ToList().ForEach(promotion => item.TIT_Qty += promotion.TIT_Qty);
                        distinctSaleProducts.Add(item);
                    }
                }

            }
            return tripsResponse;
        }
        public List<Trip> GetInventoryWithoutSales(int cedisId, int routeId, DateTime deliveryDate)
        {
            var trips = _db.ENC_Trips.Where(trip => trip.TRI_CedisIdOpeCd == cedisId && trip.TRI_Route == routeId && trip.TRI_DeliveryDate == deliveryDate)
               .Include(trip => trip.REF_TripItems)
               .ToList();
            var tripsResponse = new List<Trip>();
            foreach (var trip in trips)
            {
                var tripResponse = new Trip();
                tripResponse.Id = trip.TRI_TripId;
                tripResponse.Route = routeId;
                tripResponse.DeliveryDate = deliveryDate;
                foreach (var tripItem in trip.REF_TripItems)
                {
                    if (tripItem.TIT_Qty <= 0)
                        continue;
                    tripResponse.ConfirmedItems.Add(new ItemQty
                    {
                        ItemId = tripItem.TIT_ItemId,
                        Qty = tripItem.TIT_Qty
                    });
                }
                tripsResponse.Add(tripResponse);
            }
            return tripsResponse;
        }

    }
}